**Per baixar-nos un repositiori per PRIMER VEGADA amb SSH:**

1. primer configurem el nostre usuari si no ho hem fet.

```git
#  Configuració global del git per poder baixar/pujar 
#  un repositori mitjançant SSH:
git config --global user.name "XXXXXXX"

git config --global user.email "XXXXX@gmail.com"
```

2. creem i afegim una clau SSH al nostre ordinador 

```git
# Primer necessitem anar al directori .ssh:
cd ~/.ssh


# Generar una clau publica en l’ordinador que volem treballar,
# ens demanarà el nom de la clau
ssh-keygen -t rsa -C "XXXXX@gmail.com"


# afegim la clau estan encara al directori .ssh
ssh-add nom_de_la_clau_privada.txt
```

3. copiar la clau SSH publica generada (.pub) al git
   
   - Anem a perfil (dalt dreta) > Preferences > SSH Keys (menu esquerra)
   
   - dintre de SSH, copiem la clau a on posa **Key** i la guardem.

4. Anar al directori on volem realitzar el clonatge i fem:
   
   ```git
   # XXXXXXX ==> nom d'usuari
   # YYYYYYY ==> nom del projecte de git
   git clone git@gitlab.com:XXXXXX/YYYYY.git
   ```
