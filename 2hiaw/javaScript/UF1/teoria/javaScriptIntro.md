**global vs var vs let vs const:**

|            | GLOBAL                | VAR                                                                         | LET                                                                                            | CONST                                                             |
| ---------- | --------------------- | --------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------- | ----------------------------------------------------------------- |
| funció     | globals a tot el codi | locals a la funció on es declara, si es declara fora de la funció és global | local al bloc on es declara                          (bloc està limitat per les claus **{}** ) | tenen el mateix ámbit que el let i no és pot canviar el seu valor |
| declaració | Es pot redeclarar     | Es pot redeclarar                                                           | No es pot redeclarar                                                                           | No es pot redeclarar                                              |










