# Docker PHP+Nginx Básic
Utiliza Docker para cargar nuestro proyecto PHP

## Instrucciones
- Poner en la carpeta app nuestro proyecto (archivos .php i .html)
- Editar .env modificando los parametros que sean necesarios
- Ejecutar: docker compose up -d
- Web: **http://localhost:1080/**


