### **<mark>UF1 - Programación web en entorno servidor.</mark>**



**Front-end => compila en el ordenador**

**Back-end => compila en el servidor**

Nosotros nos centraremos en el back end



#### Modelo en 3 capas

- Capa presentación = html+css+js

- Capa de negocios = servidor, php o java

- Capa de datos = Base de datos, sql



Docker:

és un bloc contenidor amb un sistema molt primari que funciona com una maquina virtual, de manera que no tindrem descarregat el php al ordinador sinó al contenidor. Canvia d'una maquina virtual en que no ha d'emular el hardware que consumeix molts recursos
