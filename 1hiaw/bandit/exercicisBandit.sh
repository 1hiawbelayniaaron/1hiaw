# // bandit0 //
ssh bandit.labs.overthewire.org  -p 2220 -l bandit0
passwd = bandit0

# // bandit1 //
ssh bandit1@bandit.labs.overthewire.org -p 2220 -l bandit1
passwd = boJ9jbbUNNfktd78OOpsqOltutMc3MY1

# // bandit2 //
ssh bandit2@bandit.labs.overthewire.org -p 2220 -l bandit2
passwd = CV1DtqXWVFXTvM2F0k09SHz0YwRINYA9

# // bandit3 //
ssh bandit3@bandit.labs.overthewire.org -p 2220 -l bandit3
passwd = UmHadQclWmgdLOKQ3YNgjWxGoRMb5luK

# // bandit4 //
ssh bandit4@bandit.labs.overthewire.org -p 2220 -l bandit4
passwd = pIwrPrtPN36QITSp3EQaw936yaFoFgAB

# // bandit5 //

file inhere/*
cat $(find . -name -file07)

ssh bandit5@bandit.labs.overthewire.org -p 2220 -l bandit5
passwd = koReBOKuIDDepwhWk7jZC0RTdopnAYKh

# // bandit6 //

# mirem primer quin tipus d'arxiu pilla el file i despres fem cat a l'arxiu
file $(find . -size 1033c ! -executable)
cat $(find . -size 1033c ! -executable)

ssh bandit6@bandit.labs.overthewire.org -p 2220 -l bandit6
passwd = DXjZPULLxYr17uwoI01bNLQbtFemEgo7


# // bandit7 //
#  busquem desde home un fitxer que tingui grup bandit6  de owner bandit7 i que ocupi 33 bytes, la part que no ens interesa la redirigim a /dev/null
cat $(find / -user bandit7 -group bandit6 -size 33c 2>/dev/null)

ssh bandit7@bandit.labs.overthewire.org -p 2220 -l bandit7
passwd = HKBPTKQnIay4Fw76bEy8PVxKEDQRKTzs

# // bandit8 //
#  busquem desde home un fitxer que tingui grup bandit6  de owner bandit7 i que ocupi 33 bytes, la part que no ens interesa la redirigim a /dev/null
grep 'millionth' data.txt 

ssh bandit8@bandit.labs.overthewire.org -p 2220 -l bandit8
passwd = cvX2JJa4CFALtqS87jk27qwqGhBM9plV



# // bandit9 //
#  busquem desde home un fitxer que tingui grup bandit6  de owner bandit7 i que ocupi 33 bytes, la part que no ens interesa la redirigim a /dev/null
echo $(cat data.txt | sort | uniq -u)

ssh bandit9@bandit.labs.overthewire.org -p 2220 -l bandit9
passwd = UsvVyFSfZZWbi6wgC7dAFyFuR6jQQUhR

# // bandit10 //
ssh bandit10@bandit.labs.overthewire.org -p 2220 -l bandit10
passwd = truKLdjsbJ5g7yyJ2X2R0o3a5HQJFuLk

# // bandit11 //
base64 -d data.txt 

ssh bandit11@bandit.labs.overthewire.org -p 2220 -l bandit11
passwd = IFukwKGsFW8MOq3IRFqrxE1hxTNEbUPR

# // bandit12 //
tr 'A-Za-z' 'N-ZA-Mn-za-m' <<< $(cat data.txt)

xxd -r data.txt 

ssh bandit12@bandit.labs.overthewire.org -p 2220 -l bandit12
passwd = 5Te8Y4drgCRfCx8ugdwuEX8KFC6k2EUu


