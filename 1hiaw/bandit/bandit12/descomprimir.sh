#!/bin/bash
# Filename:		descomprimir.sh
# Author:		iaw49656032
# Date:			04/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		descomprimir.sh [arg1...]
# Description:	

# agafem el valor del comprimit
dataCompressed=$(7z l comprimit.gzip | grep "Name" -A 2 | tail -n 1 | awk 'NF{print $NF}') 
# descomprimir i guardar
7z x comprimit.gzip >& /dev/null
contador=1
while [ $? -eq 0 ]
do
7z x $dataCompressed
$dataCompressed=$(7z l $dataCompressed | grep "Name" -A 2 | tail -n 1 | awk 'NF{print $NF}') 
let contador=contador+1
echo $dataCompressed
done
	
	
