import java.util.HashMap;
import java.util.Map;
import java.util.Arrays;

/**
 *
 * @author Oriol
 */
public class Team {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Nom de l'equip de b�squet
        String teamName = "UNIVERSITARI BCN";
                
        String playerName = "Nom"; //Nom de la jugadora
        String playerNum = "N�mero"; //N�mero de la samarreta de la jugadora
        String playerPosition = "Posici�"; //Posici� habitual de la jugadora
        String playerHeight = "Al�ada"; //Al�ada de la jugadora en metres
        
        
        String[][] players;
        int numRows = 12; //n�mero de files (rows)
        int numCols = 4;  //n�mero de columnes (columns)
        players = new String[numRows][numCols];

              
        players[0][0] = "Anna";
        players[1][0] = "Paula";
        players[2][0] = "Marta";
        players[3][0] = "Silvia";
        players[4][0] = "Laura";
        players[5][0] = "Nadja";
        players[6][0] = "Tamara";
        players[7][0] = "Alba";
        players[8][0] = "Judith";
        players[9][0] = "Cris";
        players[10][0] = "Laia";
        players[11][0] = "Mireia";

        players[0][1] = "4";
        players[1][1] = "5";
        players[2][1] = "6";
        players[3][1] = "7";
        players[4][1] = "8";
        players[5][1] = "9";
        players[6][1] = "10";
        players[7][1] = "11";
        players[8][1] = "12";
        players[9][1] = "13";
        players[10][1] = "14";
        players[11][1] = "15";
        
        players[0][2] = "base";
        players[1][2] = "escolta";
        players[2][2] = "pivot";
        players[3][2] = "base";
        players[4][2] = "aler";
        players[5][2] = "aler";
        players[6][2] = "pivot";
        players[7][2] = "escolta";
        players[8][2] = "aler";
        players[9][2] = "pivot";
        players[10][2] = "pivot";
        players[11][2] = "aler";
        
        players[0][3] = "1.72";
        players[1][3] = "1.69";
        players[2][3] = "1.84";
        players[3][3] = "1.64";
        players[4][3] = "1.74";
        players[5][3] = "1.82";
        players[6][3] = "1.81";
        players[7][3] = "1.78";
        players[8][3] = "1.83";
        players[9][3] = "1.88";
        players[10][3] = "1.82";
        players[11][3] = "1.78";        

        System.out.print("Equip: ");
        System.out.println(teamName);                             
          
        System.out.println("");
        
        Team team = new Team();
        team.printCell(0,playerName);
        team.printCell(1,playerNum);
        team.printCell(2,playerPosition);
        team.printCell(3,playerHeight);
        
        System.out.println("");
        System.out.println("");
        
        //Imprimim la taula
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                team.printCell(j,players[i][j]);
            }
            System.out.print("\n");
        }
    }
    
    //M�tode que formata la impressi� del valor d'una cel�la
    //segons a la columna a qu� pertanyi
	//Utilitza recursos que encara no s'han estudiat
	//Per tant utilitzeu-lo com una caixa negra
    private void printCell(int column, String value) {
        Map<Integer,String> columnPositions = new HashMap<Integer,String>();      
        int initialPosition = 8;
        int currentPosition = initialPosition;
        int columnWidth = 2;
        for (int c = 0; c <= column; c++) {
          columnPositions.put(c, "%" + currentPosition + "s");
          currentPosition += columnWidth;
        }
        System.out.format((String)columnPositions.get(new Integer(column)), value);
    } 
}
