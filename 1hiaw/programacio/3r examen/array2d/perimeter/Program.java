/*
 * ProgramTUI.java        1.0 12/12/2011
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class Program {

    /**
     * calculates de perimeter of the matriz.
     * 
     * @param matriu que volem calcular el perimetre.
     * @return perimetre de la matriu
     */  
    public String perimeter(int matriu[][]) {
     int perimetre = 0;
     String resultat = "";
     int maxColumnes = matriu[0].length;
     // Write the array 2D
        for (int i = 0; i < matriu.length ; i++) {
            for (int j = 0; j < matriu[0].length ; j++) { 
                // si es la primera fila o l'ultima
                if (j == 0 || j == maxColumnes) {
                    perimetre = perimetre * matriu[i][j] ;
                    resultat = resultat + " * " + matriu[i][j];
                } else if ((j == 0 && i == 0) || (j == 0 && i == matriu.length)){
                    perimetre = perimetre + matriu[i][0] + matriu[i][maxColumnes-1];
                    resultat = resultat + " * " + matriu[i][j];
                }
            }
        }
        return resultat;
    }

}
