/*
 * Program.java        1.0 24/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Generates the corresponding bibcode for a astronomical reference.
     * 
     * @param reference an astronomical reference
     * @param section a section
     * @return the bibcode
     */
    public String bibcode(String reference, char section) {
        // TODO
        return null;
    }

    /**
     * Builds a string with some content and filled with points.
     * 
     * @param s the content
     * @param len the length of the new string
     * @param alignLeft if true fill with points on the right of the string, otherwise fill on the left.
     * @return a string with the content anf filled with points.
     */
    public String fillWithPoints(String s, int len, boolean alignLeft) {
        // TODO
        return null;
    }
}
