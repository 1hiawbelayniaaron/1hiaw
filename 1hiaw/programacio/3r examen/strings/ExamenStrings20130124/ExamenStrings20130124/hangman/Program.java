/*
 * Program.java        1.0 24/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Random;
import java.util.Scanner;

public class Program {

    /**
     * Builds a string with as many "-" as a secretWord length.
     * 
     * @param secretWord a word
     * @return the hidden word
     */
    public String hide(String secretWord) {
        // TODO
        return null;
    }

    /**
     * Builds a string with the hiddenWord refresehed if the letter is in secret word.
     * 
     * @param secretWord a secret word
     * @param hiddenWord teh corresponding hiddwen word
     * @param letter a letter
     * @return a new refreshed hidden word
     */
    public String refresh(String secretWord, String hiddenWord, String letter) {
        // TODO
        return null;
    }

    /**
     * Main program.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        Program p = new Program();
        Scanner sc = new Scanner(System.in);
        String[] words = { "gat", "gos", "gallina", "vaca" };
        Random r = new Random();
        int secretPosition = r.nextInt(words.length);
        String secretWord = words[secretPosition];
        String hiddenWord = p.hide(secretWord);
        int maxGuesses = 10;
        boolean win = false;
        System.out.println("JOC DEL PENJAT");
        System.out.println("==============\n");

        // TODO
    }

}
