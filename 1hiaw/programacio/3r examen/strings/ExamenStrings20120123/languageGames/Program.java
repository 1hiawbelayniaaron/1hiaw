/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Translates a word from spanish to jerigonza.
     * 
     * @param s a word
     * @return the translated word
     */
    public String spanish2jerigonza(String s) {
        
    }

    /**
     * Translates a word from english to basic pig latin.
     * 
     * @param s a word
     * @return the translated word
     */
    public String english2basicPigLatin(String s) {
        
    }
}
