/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Splits a string around matches of the given substring.
     * 
     * @param s a string
     * @param sub a substring (delimiter)
     * @return an array of string with the resulting substrings
     */
    public String[] mySplit(String s, String sub) {

    }

}
