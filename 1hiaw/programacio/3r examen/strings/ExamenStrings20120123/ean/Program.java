/*
 * Program.java        1.0 23/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Determines if an EAN code is valid.
     * 
     * @param ean an EAN code
     * @return true if it is valid, false otherwise
     */
    public boolean isValidEAN(String ean) {

    }
}
