public class ElsStringsEstanBojos {
 public static void main(String[] args) {
  String str1 = "hola";
  String str2 = new String("hola");
  String str3 = "hola" ;
  
  System.out.println("\nEls Strings estan bojos...\n");
  System.out.println("Hem fet: \nString str1 = \"hola\";\nString str2 = new String(\"hola\");\nString str3 = \"hola\";\n");

  System.out.println("i obtenim:");
  if ( str1 == str2 ) {
   System.out.println("str1 i str2 iguals");
  } else {
   System.out.println("str1 i str2 diferents");
  }
  if ( str1 == str3 ) {
                        System.out.println("str1 i str3 iguals");
                } else {
                        System.out.println("str1 i str3 diferents");
                }
  if ( str2 == str3 ) {
                        System.out.println("str2 i str3 iguals");
                } else {
                        System.out.println("str2 i str3 diferents");
                }
 }
}