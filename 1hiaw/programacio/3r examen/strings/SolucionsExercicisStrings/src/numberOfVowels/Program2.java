/*
 * Program.java        1.0 08/01/2013
 *
 * Models the program.
 *
 * Copyright 2013 Oriol Boix <oboix@escoladeltreball.org>
 *                Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {

    /**
     * Calculates how many vowels a string has.
     * 
     * @param s a string.
     * @return the number of vowels.
     */
    public int numberOfVowels(String s) {
        int nv = 0, i;
        String sl = s.toLowerCase();
        for (i = 0; i < sl.length(); i++) {
            char c = sl.charAt(i);
            String vowels = "aeiou";            
            if (vowels.contains(String.valueOf(c))) {
                nv++;
            }
        }
        return nv;
    }

}
