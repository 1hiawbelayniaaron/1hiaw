/*
 * Program.java        1.0 08/01/2012
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program2 {

    /**
     * Counts how many words a string has.
     * 
     * @param str a string
     * @return the number of words of s
     */
    public int numberOfWords(String str) {
        str = str.trim();
        String[] words = str.split("\\s+");
        return words.length;
    }

}
