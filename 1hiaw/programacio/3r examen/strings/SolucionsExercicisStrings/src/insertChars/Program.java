/*
 * Program.java        1.0 08/01/2012
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Creates a string inserting chars from two strings.
     * 
     * @param str1 a string
     * @param str2 a string
     * @return the creates string.
     */
    public String insertChars(String str1, String str2) {             
        int lenght1 = str1.length();
        int lenght2 = str2.length();
        int minLength = lenght1 < lenght2 ? lenght1 : lenght2;
        int maxLength = lenght1 < lenght2 ? lenght2 : lenght1;
        // Insert chars from two strings
        String str3 = "";
        int  i;
        for (i = 0; i < minLength; i++) {
            str3 += str1.charAt(i);
            str3 += str2.charAt(i);
        }
        // if str1 is longer, add the rest of characters of str1
        // if str2 is longer, add the rest of characters of str2
        if (lenght1 == maxLength) {
            str3 += str1.substring(i);
        } else {
            str3 += str2.substring(i);
        }
        return str3;
    }

}
