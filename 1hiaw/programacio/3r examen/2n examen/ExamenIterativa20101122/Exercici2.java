/*
 * Exercici2Solucio.java        1.0 22/11/2010
 *
 * La seqüència de Sylvester és la seqüència d'enters positius caracteritzada per:
 * El primer element -s- s'anomena llavor i pot ser qualsevol nombre enter positiu.
 * El segon element és igual a la llavor més 1.
 * Els demés elements són el producte de tots els anteriors més 1.
 * 
 * Per exemple, si la llavor és 2, els primers termes de la seqüència són: 
 * 
 * 2, 3, 7, 43, 1807, 3263443, ...
 * 
 * Donats la llavor -s- i un nombre enter positiu -max- més gran que la llavor, 
 * escriu tots els termes de la seqüència de Sylvester inferiors a -max- i els compti.
 * 
 * Per exemple:
 * 
 * Si s=2 i max=1000, escriurà: 2 3 7 43 i escriurà que la seqüència té 4 nombres
 * Si s=3 i max=25000, escriurà: 3 4 13 157 24493 i escriurà que la seqüència té 5 nombres
 * Si s=10 i max=5000, escriurà: 10 11 111 i escriurà que la seqüència té 3 nombres
 *  
 *
 * Copyright 2010 NOM COGNOM EMAIL
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import library.inout.Terminal;

public class Exercici2 {
	
	public void sylvester(int s, int max) {	
		
		
	}
}
