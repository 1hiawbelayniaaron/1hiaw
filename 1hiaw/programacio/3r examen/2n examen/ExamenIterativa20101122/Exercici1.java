/*
 * Exercici1.java        1.0 22/11/2010
 *
 * Donats un nombre enter -n- no negatiu de més de 2 xifres i un altre nombre enter -p- 
 * no negatiu que indica la posició d'una de les seves xifres. 
 * -p- serà major que 1 i menor al nombre de xifres de -n-. 
 * 
 * Determina si la xifra de la posició posterior a -p-, la xifra de la posició -p- i 
 * la xifra de la posició anterior a -p- són consecutives creixents o no.
 *
 * La xifra de les unitats està a la posició 1, la de les desenes a la posició 2, etc.
 *
 * Per exemple:
 *
 * si n = 123, p = 2 llavors són consecutives ja que 1, 2 i 3 són nombres consecutius creixents
 * si n = 579, p = 2 llavors NO són consecutius ja que 5, 7 i 9 NO són consecutius creixents
 * si n = 321, p = 2 llavors NO són consecutius ja que 3, 2 i 1 NO són consecutius creixents
 * si n= 10121 p = 4 llavors NO són consecutius ja que 1, 0 i 1 NO són consecutius creixents
 * si n= 67892345 p = 7 llavors són consecutius ja que 6, 7 i 8 són consecutius creixents
 * si n = 2123567532, p = 5 llavors són consecutius ja que 5, 6 i 7 són consecutius creixents
 * si n = 2123567532, p = 6 llavors NO són consecutius ja que 3, 5 i 6 NO són consecutius creixents
 * 
 * NOTA: no heu de validar si les dades d'entrada són correctes i coherents. Suposarem que ho són.
 *
 * Copyright 2010 NOM COGNOM EMAIL
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import library.inout.Terminal;

public class Exercici1 {

	public boolean consecutiveDigits(int n, int p) {
		return false;
	}
}
