/*
 * Program.java        1.0 27/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculates an approximation of PI according to the following formula:
     * 
     * pi = 4*(1-1/3+1/5-1/7+1/9-1/11+...)
     * 
     * @param precision the maximum error the approximated pi can has compared to Math.PI.
     * @return an approximation of number pi.
     */
    public double pi(double precision) {
        // TODO
        return 0;
    }

}