/*
 * Program.java        1.0 27/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMD <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI {

    /**
     * Generates a random number between 0 and max-1.
     * 
     * @param max an integer number
     * @return the generated random number.
     */
    public int random(int max) {
        return (int) (Math.random() * max);
    }

    /**
     * Simulates a minimal battleship game.
     * 
     * @param args Not used.
     */
    public static void main(String args[]) {
        // TODO
    }
}
