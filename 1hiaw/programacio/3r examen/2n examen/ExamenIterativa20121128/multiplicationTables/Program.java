/*
 * Program.java        1.0 27/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Write a title for a multiplication table. Title format is: 
     * TAULA DEL 4 
     * ===========
     * 
     * There will be as many "=" as needed to underline all the number. 
     * For instance, if the number has 3 digits:
     * TAULA DEL 423 
     * =============
     * 
     * @param n a positive number
     */
    public void writeTitle(int n) {
        // TODO
    }

    /**
     * Writes the multiplication numbers from n1 to n2.
     * Output format for each table is:
     * 
     * TAULA DEL 10
     * ============
     * 10 * 0 = 0
     * 10 * 1 = 10
     * 10 * 2 = 20
     * 10 * 3 = 30
     * 10 * 4 = 40
     * 10 * 5 = 50
     * 10 * 6 = 60
     * 10 * 7 = 70
     * 10 * 8 = 80
     * 10 * 9 = 90
     * 
     * @param n1 a positive number
     * @param n2 a positive number
     */
    public void multiplicationTables(int n1, int n2) {
        // TODO
    }
}
