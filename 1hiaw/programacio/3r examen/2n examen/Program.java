/**
 * Program.java        VERSION DATE
 *
 * ens dona n1! apartir de n1
 *
 * Copyright 2021 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {


 public void triangle(int costat) {
     for (int i = 0; i <= costat; i++){
         for (int j = 0; j < i; j++) {
          System.out.print("*");   
         }
         System.out.println("");
     }
 }
}   
    