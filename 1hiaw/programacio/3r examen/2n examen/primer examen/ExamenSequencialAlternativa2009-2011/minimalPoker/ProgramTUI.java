/*
 * Program.java        1.0 07/11/2011
 *
 * Models the program.
 *
 * Copyright 2011 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI {

    /**
     * Determines if a combination of three cards is "poker" or not. That is,
     * three cards are equal.
     * 
     * @param c1 a card (an integer number between 1 and 12)
     * @param c2 a card (an integer number between 1 and 12)
     * @param c3 a card (an integer number between 1 and 12)
     * @return true if it is poker, false otherwise
     */
    public boolean isPoker(int c1, int c2, int c3) {
        
    }

    /**
     * Determines if a combination of three cards is "one pair" or not. That is,
     * two (and ONLY two) of the three cards are equal.
     * 
     * @param c1 a card (an integer number between 1 and 12)
     * @param c2 a card (an integer number between 1 and 12)
     * @param c3 a card (an integer number between 1 and 12)
     * @return true if it is one pair, false otherwise
     */
    public boolean isOnePair(int c1, int c2, int c3) {
        
    }

    /**
     * Determines if a combination of three cards is "straight" or not. That is,
     * we can sort them as consecutive numbers.
     * 
     * @param c1 a card (an integer number between 1 and 12)
     * @param c2 a card (an integer number between 1 and 12)
     * @param c3 a card (an integer number between 1 and 12)
     * @return true if it is straight, false otherwise
     */
    public boolean isStraight(int c1, int c2, int c3) {
        
    }

    /**
     * Determines what combination of cards we have.
     * 
     * @param c1 a card (an integer number between 1 and 12)
     * @param c2 a card (an integer number between 1 and 12)
     * @param c3 a card (an integer number between 1 and 12)
     * @return "Poker" if it is a poker, "Parella" if it is one pair, "Escala"
     *         if it is straight or "Res" if we have no combination.
     */
    public String resultGame(int c1, int c2, int c3) {
        
    }

    /**
     * Modelizes the UI as a TUI.
     * 
     * @param args not used.
     */
    public static void main(String[] args) {
        
    }
}
