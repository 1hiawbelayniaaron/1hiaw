/*
 *  Exercici1.java
 *   
 *   Comparació de dues dates: Donades dues dates correctes ens diu quina 
 *   data és anterior a l'altra.
 * 
 *  Copyright 2009 [NOM I COGNOMS] - [wida??????]
 * 
 *  This is free software, licensed under the GNU General Public License v3.
 *   See http://www.gnu.org/licenses/gpl.html for more information.
 *
 */

public class Exercici1 {
    
    /**
     * Checks if day1/month1/year1 is before day2/month2/year2. Dates are valid.
     * 
     * @param day1 a day
     * @param month1 a month
     * @param year1 a year
     * @param day2 a day
     * @param month2 a month
     * @param year2 a year
     * @return "day1/month1/year1" if the first date is before the second one
     *         or "day2/month2/year2" otherwise.
     */
    public String isBefore(int day1, int month1, int year1, int day2, int month2, int year2) {
        
    }

}
