/*
 * Programa1.java        1.0 28/10/2010
 * 
 * Models the program.
 *
 * Copyright 2010 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 *
 * ENUNCIAT
 * 
 * Càlcul del preu del rebut mensual de l'agua.
 * El preu de l'aigua es calcula de la següent manera:
 *
 * Per a cada mes s'aplica un preu unitari fix (tarifa mínima) segons el tipus d'habitatge:
 *              * Tipus A         :     1,98 €/mes
 *              * Tipus B i C     :     4,99 €/mes
 *              * Tipus D         :     7,93 €/mes
 *
 * A aquest preu unitari se li suma un preu segons el consum:
 *              * Els primers 6m^3 es cobren a:         0.5969 €/m^3
 *              * Els següents 6m^3 es cobren a:        0.9106 €/m^3
 *              * Els següents es cobren a:             1.3657 €/m^3
 *  
 */

public class Program1 {

    /**
     * Calculates the price of a water bill.
     * 
     * @param volume the consumed volume of water.
     * @param houseType the type of house.
     * @return the price of the bill.
     */
    public double calculateWaterBill(double volume, char houseType) {
        
    }
}
