/*
 * Programa2.java        1.0 28/10/2010
 *
 * Copyright 2010 [NOM I COGNOMS] <[CORREU ELECTRÒNIC]>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 * 
 * ENUNCIAT
 * 
 * Donades dues dates (mes i any), determina si hi ha més de mig any de diferència
 * entre ambdues dates.
 *
 * Suposarem que les dates introduïdes són correctes i
 * que la primera data és anterior a la segona data.
 */

public class Program2 {

    /**
     * Determines if there are more than 6 months between 2 dates.
     * 
     * @param month1 the month of a date
     * @param year1 the year of a date
     * @param month2 the month of a date
     * @param year2 the year of a date
     * @return true if there are more than 6 months, false otherwise.
     */
    public boolean moreThan6months(int month1, int year1, int month2, int year2) {
        
    }
}
