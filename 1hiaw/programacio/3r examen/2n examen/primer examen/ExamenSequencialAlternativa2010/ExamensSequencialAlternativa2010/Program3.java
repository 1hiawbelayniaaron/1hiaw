/*
 * Programa3.java        1.0 03/11/2011
 *
 * Models the program.
 *
 * Copyright 2011 Mònica Ramírez Arceda <mramirez@escoladeltreball.org>
 *                Joaquim Laplana Tarragona <jlaplana@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 * 
 * ENUNCIAT
 * 
 * Donats un nombre enter -n- no negatiu de més de 2 xifres i 
 * un altre nombre enter -p- no negatiu que indica la posició d'una de les seves xifres,  
 * suposem que -p- és major que 1 i menor al nombre de xifres de -n-. 
 * 
 * Determina si la xifra de la posició posterior a -p-, 
 * la xifra de la posició -p- i la xifra de la posició anterior a -p- 
 * són consecutives creixents o no.
 *
 * La xifra de les unitats està a la posició 1, la de les desenes a la posició 2, etc.
 *
 * Per exemple:
 *
 * si n = 123, p = 2, les xifres són consecutives doncs 1, 2 i 3 són nombres consecutius creixents
 * si n = 579, p = 2, les xifres NO són consecutives doncs 5, 7 i 9 NO són nombres consecutius creixents
 * si n = 321, p = 2, les xifres NO són consecutives doncs 3, 2 i 1 NO són nombres consecutius creixents
 * si n= 10121 p = 4, les xifres NO són consecutives doncs 1, 0 i 1 NO són nombres consecutius creixents
 * si n= 67892345 p = 7, les xifres són consecutives doncs 6, 7 i 8 són nombres consecutius creixents
 * si n = 2123567532, p = 5 les xifres són consecutives doncs 5, 6 i 7 són nombres consecutius creixents
 * si n = 2123567532, p = 6 les xifres NO són consecutives doncs 3, 5 i 6 NO són nombres consecutius creixents
 * 
 * NOTA: no heu de validar si les dades d'entrada són correctes i coherents. Suposarem que ho són.
 */

public class Program3 {

    /**
     * Determine if the digits in position p-1, p and p+1 of a number n are
     * consecutive.
     * 
     * @param n a number
     * @param p a position
     * @return true if the digits are consecutive, false otherwise.
     */
    public boolean areConsecutiveDigits(int n, int p) {
        
    }
}
