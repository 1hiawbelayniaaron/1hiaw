/*
 * Program.java        1.0 07/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Recommends one of the two available tariffs: "nocturna" or "xerraire".
     * 
     * @param minutesDay the number of minutes talked during the day
     * @param minutesNight the number of minutes talked during the night
     * @param sms the number of sms
     * @return a string with the recommendation and the approximated price according to input data. String format is
     *         "Tarifa Nocturna: [price]" or "Tarifa Xerraire: [price]"
     */
    public String recommend(int minutesDay, int minutesNight, int sms) {
        // TODO
        return "";
    }

}
