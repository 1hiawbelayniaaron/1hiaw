/*
 * ProgramTUI.java        1.0 07/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Locale;
import java.util.Scanner;

public class ProgramTUI {

    /**
     * Generates a random card.
     * 
     * @return a random number between 1 and 12.
     */
    public int randomCard() {
        return (int) (Math.random() * 12) + 1;
    }

    /**
     * Calculates the sum of 3 cards. The value for cards 8, 9, 10, 11 and 12 is 0.5.
     * 
     * @param c1 a card (an integer number between 1 and 12)
     * @param c2 a card (an integer number between 1 and 12)
     * @param c3 a card (an integer number between 1 and 12)
     * @return the sum.
     */
    public double result(int c1, int c2, int c3) {
        // TODO
        return 0;
    }

    /**
     * Calculates the amount of money a player has after a game.
     * 
     * @param resultGame the sum of the three cards
     * @param money the amount of money the player has initially.
     * @param bet the amount of money the player is betting in the game.
     * @return the final amount of money the player has after the game, depending on the result of the game.
     */
    public double prize(double resultGame, double money, double bet) {
        // TODO
        return 0;
    }

    /**
     * Game TUI.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        // TODO
    }

}
