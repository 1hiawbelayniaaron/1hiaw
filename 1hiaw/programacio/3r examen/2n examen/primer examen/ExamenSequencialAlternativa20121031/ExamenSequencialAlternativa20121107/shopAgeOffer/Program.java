/*
 * Program.java        1.0 07/11/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <MAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculates the age of a person.
     * 
     * @param day a day
     * @param month a month
     * @param year a year
     * @param birthDay the day the person was born
     * @param birthMonth the month the person was born
     * @param birthYear the year the person was born
     * @return the age of the person on day/month/year
     */
    public int age(int day, int month, int year, int birthDay, int birthMonth, int birthYear) {
        // TODO
        return 0;
    }

    /**
     * Calculates the price of the product with the corresponding discount.
     * 
     * @param day the sale's day
     * @param month the sale's month
     * @param year the sale's year
     * @param birthDay the day the consumer was born
     * @param birthMonth the month the consumer was born
     * @param birthYear the year the consumer was born
     * @param basePrice the price without the discount
     * @param unemployed true if the consumer is unemployed, false otherwise
     * @return the price with the applied discount
     */
    public double price(int day, int month, int year, int birthDay, int birthMonth, int birthYear, int basePrice,
            boolean unemployed) {
        // TODO
        return 0;
    }

} 
