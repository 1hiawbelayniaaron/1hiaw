/*
 * Program.java        1.0 28/11/2011
 *
 * Models the program.
 *
 * Copyright 2011 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Draw a right triangle with asterisks where its catheti have length -c-
     * 
     * @param c the length of both catheti.
     * @return the number of asterisks we need to draw the triangle.
     */
    public int drawTriangle(int c) {
        
    }
}
