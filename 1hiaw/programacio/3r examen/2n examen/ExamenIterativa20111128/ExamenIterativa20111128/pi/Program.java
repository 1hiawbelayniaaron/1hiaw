/*
 * Program.java        1.0 28/11/2011
 *
 * Models the program.
 *
 * Copyright 2011 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Calculates an approximated value of the number PI.
     * 
     * @param n the number of points to use (the number of darts to throw).
     * @return the approximated value of PI.
     */
    public double calculatePi(long n) {
        
    }
}
