/*
 * Program.java        1.0 28/11/2011
 *
 * Models the program.
 *
 * Copyright 2011 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {

    /**
     * Determines if a product has reached its use-by date.
     * 
     * @param today in format DDMMAAAA
     * @param useByDate product's use-by date in format DDMMAAAA
     * @return true if product has expired, false otherwise
     */
    public boolean hasExpired(int today, int useByDate) {
        
    }

    /**
     * Modelizes a TUI.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        
    }
}
