/*
 * ProgramTUI.java        1.0 Gen 1, 2022
 *
 * Models the program.
 *
 * Copyright 2022 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class ProgramTUI {
        
 /**
 * Calculate the percentage of vowels in a sentence and
 * calculate the percentage of vowels in a word
 * 
 * @param string where the percentages of the vowels are calculated
 * @return array with all the percentages
 */
 public String[][] getVowelsPercentage(String s) {
  int sLength = s.length();
  char vowel;
  int numVocals= 0;
  int numCaracters = 0;
  char c;
  // calcular numero de vocals en una frase
  for (int i = 0; i < sLength; i++) {
      c = s.charAt(i);
        // mira si es una vocal
            if (c == 'a' || c == 'A' || c == 'e' || c == 'E' || c == 'i' ||
                c == 'I' || c == 'o' || c == 'O' || c == 'u' || c == 'U') {
                numVocals++;
                // quan el caracter es blanc no conta
            } else if (c == ' ') {
                // resta de caracters
                } else {
                    numCaracters++;
                }

            
  }
  return numVocals;
 }

 /**
 * Prints the percentages of the phrase's vowels or word's vowels
 * 
 * @param  array with all the percentages
 * @param string where the percentages of the vowels are calculated
 * @return array with all the percentages
 */
  /**
 public void printSolution(String [][] percentage, String s) {
  // TO DO 
 }

 /**
 * Main program 
 * 
 * @param args Not used
 */

 }

 

