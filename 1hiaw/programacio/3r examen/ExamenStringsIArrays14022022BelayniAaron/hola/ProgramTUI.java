/*
 * ProgramTUI.java        Jan 1, 2022
 *
 * Models the program.
 *
 * Copyright 2022 Aaron belayni <1hiawbelayniaaron@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class ProgramTUI {

 /**
 * creates white character
 * 
 * @param array containing white character 
 */
 public void buildWhiteLetter(char letter[][]) {
  char[][] spaces = {
         {' ',' ',' ',' ',' '},
         {' ',' ',' ',' ',' '}

     };
 }

 /**
 * shows a letter
 * 
 * @param array containing a letter 
 */ 
 public void viewLetter(char letter[][]) {
     for (int i = 0; i < h.length; i++) {
            for (int j = 0; j < h[0].length; j++) {
             
            }
        }
 }

 /**
 * builds the letter H
 * 
 * @param array containing a letter H 
 */
 public void buildLetterH(char letter[][]) {
     char[][] h = {
         {'*',' ',' ',' ','*'},
         {'*',' ',' ',' ','*'},
         {'*','*','*','*','*'},
         {'*',' ',' ',' ','*'},
         {'*',' ',' ',' ','*'}
     };
 }

 /**
 * builds the letter O
 * 
 * @param array containing a letter O 
 */
 public void buildLetterO(char letter[][]) {
  char[][] o = {
         {'*','*','*','*','*'},
         {'*',' ',' ',' ','*'},
         {'*',' ',' ',' ','*'},
         {'*',' ',' ',' ','*'},
         {'*','*','*','*','*'}
     };
 }

 /**
 * builds the letter A
 * 
 * @param array containing a letter A 
 */
 public void buildLetterA(char letter[][]) {
  char[][] a = {
         {'*','*','*','*','*'},
         {'*',' ',' ',' ','*'},
         {'*','*','*','*','*'},
         {'*',' ',' ',' ','*'},
         {'*',' ',' ',' ','*'}
     };
 }
     
 /**
 * builds the letter L
 * 
 * @param array containing a letter L
 */
 public void buildLetterL(char letter[][]) {
  char[][] l = {
         {'*',' ',' ',' ',' '},
         {'*',' ',' ',' ',' '},
         {'*',' ',' ',' ',' '},
         {'*',' ',' ',' ',' '},
         {'*','*','*','*','*'},
     };
 }
     
 public static void main(String[] args) {
  ProgramTUI p = new ProgramTUI();
  int SIZE = 7;
  char letter [][] = new char [SIZE][SIZE];   // Matriu de longitud senar, mínim 5X5
  p.buildWhiteLetter(letter);
  p.buildLetterH(letter);
  p.viewLetter(letter);
  System.out.print("\n\n");
  p.buildWhiteLetter(letter);
  p.buildLetterO(letter);
  p.viewLetter(letter);
  System.out.print("\n\n");
  p.buildWhiteLetter(letter);
  p.buildLetterL(letter);
  p.viewLetter(letter);
  System.out.print("\n\n");
  p.buildWhiteLetter(letter);
  p.buildLetterA(letter);
  p.viewLetter(letter);
 }
}
