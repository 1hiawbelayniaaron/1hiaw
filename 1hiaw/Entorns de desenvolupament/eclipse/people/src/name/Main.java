/* 
 * Main.java       Apr 27, 2022 
 * 
 * 
 * ©Copyright 2022 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com> 
 * 
 * This is free software, licensed under the GNU General Public License v3. * See http://www.gnu.org/licenses/gpl.html for more information. 
 */

/**
 * 
 */
package name;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Person p = new Person("Aaron Belayni","49656032k",19,185);
		System.out.println(" Nom: " + p.getName() + " DNI: " +
		           p.getId() + " Edat: " + p.getEdat() + " Altura: " +
		           p.getAltura());
	}

}
