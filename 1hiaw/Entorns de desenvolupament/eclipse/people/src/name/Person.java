/* 
 * name.java       Apr 27, 2022 
 * 
 * 
 * ©Copyright 2022 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com> 
 * 
 * This is free software, licensed under the GNU General Public License v3. * See http://www.gnu.org/licenses/gpl.html for more information. 
 */

/**
 * 
 */
package name;

import java.util.Objects;

public class Person {

	private String name;
	private String id;
	private int edat;
	private int altura;

	/**
	 * @param name
	 * @param id
	 * @param edat
	 * @param altura
	 */
	public Person() {
		super();
		this.name = name;
		this.id = id;
		this.edat = edat;
		this.altura = altura;
	}

	/**
	 * @param name
	 * @param id
	 * @param edat
	 * @param altura
	 */
	public Person(String name, String id, int edat, int altura) {
		super();
		this.name = name;
		this.id = id;
		this.edat = edat;
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "People [name=" + name + ", id=" + id + ", edat=" + edat + ", altura=" + altura + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(altura, edat, id, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		return altura == other.altura && edat == other.edat && Objects.equals(id, other.id)
				&& Objects.equals(name, other.name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the edat
	 */
	public int getEdat() {
		return edat;
	}

	/**
	 * @param edat the edat to set
	 */
	public void setEdat(int edat) {
		this.edat = edat;
	}

	/**
	 * @return the altura
	 */
	public int getAltura() {
		return altura;
	}

	/**
	 * @param altura the altura to set
	 */
	public void setAltura(int altura) {
		this.altura = altura;
	}

}
