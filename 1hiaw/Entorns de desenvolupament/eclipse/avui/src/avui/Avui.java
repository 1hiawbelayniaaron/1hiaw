/* 
 * Avui.java       Apr 27, 2022 
 * 
 * 
 * ©Copyright 2022 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com> 
 * 
 * This is free software, licensed under the GNU General Public License v3. * See http://www.gnu.org/licenses/gpl.html for more information. 
 */


/**
 * 
 */
package avui;

import org.joda.time.LocalTime;

public class Avui {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LocalTime localTime = new LocalTime();
		System.out.print(localTime);
	}

}
