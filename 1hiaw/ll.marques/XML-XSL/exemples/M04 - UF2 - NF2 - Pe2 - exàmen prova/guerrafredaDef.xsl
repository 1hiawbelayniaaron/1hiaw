<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
          <xsl:template match="/">
            <html>
              <head>
                <link rel="stylesheet" type="text/css" href="./css/estil.css"/>
              </head>
              <body bgcolor="grey">
				<a href="https://es.wikipedia.org/wiki/Guerra_Fría">
				<img src="./banderes/{//logo}" width="100%"></img>
				</a>



			<table border="1" bgcolor="white" align="center" padding="0">
      <tr>
					<th class="vermell" colspan="4">N* de submarins en el xml =
            <xsl:value-of select= "count(.//noms)"/>
          </th>
				</tr>

        <tr>

          <th class="vermell">Nom</th>
          <th class="vermell">Bandol</th>
          <th class="vermell">Rendiment</th>
          <th class="vermell">Armament</th>

        </tr>

      <xsl:for-each select="submarinos/urss">
        <xsl:for-each select="//submarino">
        <tr>

          <th width="25%"><img src="./imatges/{imatges/foto}" width="80%" height="80%"></img>
             <p>classe <xsl:value-of select="nombre"/></p>
           </th>

             <th width="10%"><img src="./banderes/{imatges/bandera}" width="40%" height="40%"></img>
            </th>

          <td width="25%">
            <p>Desplaçament en inmersio: <xsl:value-of select="caracteristicas_generales/desplazamiento_en_inmersion"/></p>
          <p>velocitat en inmersio: <xsl:value-of select="rendimiento/velocidad_maxima/en_inmersion"/></p>
          <p>Profunditat: <xsl:value-of select="rendimiento/profundidad"/></p>

          <p>Profunditat: <xsl:value-of select="rendimiento/profundidad"/></p>&#09;
          <p>Propulsio:</p>
          <p>&#160;&#160;&#160;Reactor: <xsl:value-of select="caracteristicas_generales/propulsion"/></p>&#09;
          <p>&#160;&#160;&#160;Potencia: <xsl:value-of select=" caracteristicas_generales/potencia_watios"/></p>&#09;
          <p>Longitud =  <xsl:value-of select=" caracteristicas_generales/longitud"/></p>&#09;
          <p>Règim d'ascens =  <xsl:value-of select=" caracteristicas_generales/diametro"/></p>&#09;
        </td>

          <td width="25%">
            <xsl:for-each select=" armamento">


              <p><xsl:value-of select="armas/torpedos/torpedo_identificador/nombre/@cantidad"/>
               torpedes
              <xsl:value-of select="armas/torpedos/torpedo_identificador/nombre"/>
              </p>

              <p><xsl:value-of select="armas/torpedos/torpedo_identificador/nombre/@cantidad"/>
               torpedes
              <xsl:value-of select="armas/torpedos/torpedo_identificador/nombre"/>
              </p>

              <p><xsl:value-of select="armas/misiles/misil_identificador/nombre/@cantidad"/>
               missils
              <xsl:value-of select="armas/misiles/misil_identificador/nombre"/>
              </p>

            </xsl:for-each>
          </td>
        </tr>
        <tr >
          <td colspan="4" class="links">


              <a href="{pag_web}"><xsl:value-of select="pag_web"/></a>


        </td>
        </tr>

        </xsl:for-each>
      </xsl:for-each>


			</table>

               </body>
            </html>
		</xsl:template>
</xsl:stylesheet>
