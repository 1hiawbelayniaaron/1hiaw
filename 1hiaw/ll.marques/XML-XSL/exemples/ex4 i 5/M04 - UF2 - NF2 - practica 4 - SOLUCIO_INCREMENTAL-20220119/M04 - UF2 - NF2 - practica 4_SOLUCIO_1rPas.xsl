<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<title>M04 - UF2 - NF2 - PRACTICA 4</title>
		<link href="estil_SOLUCIO.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<xsl:value-of select="root/origen/productor"/>
		<br/>
		<a>
			<xsl:attribute name="href"> 
				<xsl:value-of select="root/origen/enlace"/> 
	    		</xsl:attribute> 
			<xsl:value-of select="root/origen/web"/>
		</a>
		<br/>
		Dades per a: <xsl:value-of select="root/nombre"/> - <xsl:value-of select="root/provincia"/>


		<table border="1" align="center">
			<xsl:for-each select="root/prediccion/dia">
				<tr>
					<td colspan="3" align="left" bgcolor="#6666FF">Data: <xsl:value-of select="./@fecha"/></td>
				</tr>
			</xsl:for-each>

		</table>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>

