<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<title>M04 - UF2 - NF2 - PRACTICA 4</title>
		<link href="estil_SOLUCIO.css" rel="stylesheet" type="text/css" />
	</head>
	<body>
		<xsl:value-of select="root/origen/productor"/>
		<br/>
		<a>
			<xsl:attribute name="href"> 
				<xsl:value-of select="root/origen/enlace"/> 
	    		</xsl:attribute> 
			<xsl:value-of select="root/origen/web"/>
		</a>
		<br/>
		Dades per a: <xsl:value-of select="root/nombre"/> - <xsl:value-of select="root/provincia"/>


		<table border="1" align="center">
			<xsl:for-each select="root/prediccion/dia">
				<tr>
					<td colspan="3" align="left" bgcolor="#6666FF">Data: <xsl:value-of select="./@fecha"/></td>
				</tr>


				<tr>
					<td colspan="3" align="center" bgcolor="#BDBDBD">Probabilitat Precipitació</td>
				</tr>
				<xsl:for-each select="prob_precipitacion">
					<tr>
						<td align="left" bgcolor="#BDBDBD"><xsl:value-of select="./@periodo"/></td>
						<td colspan="2" align="left" bgcolor="#BDBDBD"><xsl:value-of select="."/></td>
					</tr>
				</xsl:for-each>


				<tr>
					<td colspan="3" align="center" bgcolor="#BDBDBD">Dades del vent</td>
				</tr>
				<tr>
					<td align="center" bgcolor="#BDBDBD">Periode</td>
					<td align="center" bgcolor="#BDBDBD">Direcció</td>
					<td align="center" bgcolor="#BDBDBD">Velocitat</td>
				</tr>
				<xsl:for-each select="viento">
					<tr>
						<td align="left" bgcolor="#BDBDBD"><xsl:value-of select="./@periodo"/></td>
						<td align="left" bgcolor="#BDBDBD"><xsl:value-of select="direccion"/></td>
						<xsl:if test="velocidad &lt;&#61; 8">
							<td align="left" bgcolor="#00FF00"><xsl:value-of select="velocidad"/></td>
						</xsl:if>
						<xsl:if test="velocidad &gt; 8">
							<td align="left" bgcolor="#FF0000"><xsl:value-of select="velocidad"/></td>
						</xsl:if>
					</tr>
				</xsl:for-each>

			</xsl:for-each>

		</table>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>

