﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
          <xsl:template match="/">
            <html>
              <body bgcolor="grey">
				<a href="https://es.uefa.com/">
				<img src="logo_UEFA.png" width="150px" height="150px "></img>
				</a>
			

			
			<table border="1" bgcolor="white" align="center">
      <tr>
					<th colspan="3">Setzens</th>
				</tr>
				<xsl:for-each select="//enfrontament">
				
				<tr>
					<xsl:element name="img">
    					<xsl:attribute name="src">
    					  <xsl:value-of select="anada/equip_local/logo"/>
              </xsl:attribute>
					</xsl:element>
					<xsl:value-of select="anada/equip_local/gols"/>
					<td>
					
										<xsl:element name="img">
    					<xsl:attribute name="src">
    					  <xsl:value-of select="anada/equip_visitant/logo"/>
              </xsl:attribute>
					</xsl:element>
					<xsl:value-of select="anada/equip_visitant/gols"/>

					</td>
					
					<td>
      <xsl:choose>
        <xsl:when test="sum(anada/equip_visitant/gols | tornada/equip_visitant/gols) > sum(anada/equip_local/gols | tornada/equip_local/gols)">
        
            <td>
              <xsl:element name="img">
    					<xsl:attribute name="src">
    					  <xsl:value-of select="anada/equip_local/logo"/>
              </xsl:attribute>
					</xsl:element>
              <xsl:value-of select="sum(anada/equip_visitant/gols | tornada/equip_visitant/gols)"/>
					</td>
        
        </xsl:when>
        
        <xsl:otherwise>
        
        <td>
              <xsl:element name="img">
    					<xsl:attribute name="src">
    					  <xsl:value-of select="anada/equip_visitant/logo"/>
              </xsl:attribute>
					</xsl:element>
              <xsl:value-of select="sum(anada/equip_local/gols | tornada/equip_local/gols)"/>
					</td> 
        
        </xsl:otherwise>
      </xsl:choose>

					</td>
				</tr>
				
            <tr>
					<xsl:element name="img">
    					<xsl:attribute name="src">
    					  <xsl:value-of select="tornada/equip_local/logo"/>
              </xsl:attribute>
					</xsl:element>
					<xsl:value-of select="tornada/equip_local/gols"/>
					<td>
					
										<xsl:element name="img">
    					<xsl:attribute name="src">
    					  <xsl:value-of select="tornada/equip_visitant/logo"/>
              </xsl:attribute>
					</xsl:element>
					<xsl:value-of select="tornada/equip_visitant/gols"/>
					</td>
					
				</tr>
				
				</xsl:for-each>

				
				
			</table>
			
               </body>
            </html>
		</xsl:template> 
</xsl:stylesheet>
