﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
          <xsl:template match="/">
            <html>
              <body>
                  <h1 align="center"><xsl:value-of select= "//productor"/></h1>
                  <a href="http://www.aemet.es"> <xsl:value-of select= "//web"/></a>
                  <p>Dades per a:  <a href=""><xsl:value-of select= "//nombre"/>, <xsl:value-of select= "//provincia"/></a></p>
               
                  <table border="1" align="center">
                  
<xsl:for-each select="//dia">
                
                    <tr bgcolor="white">
                         <td colspan="3">Data: <u><xsl:value-of select= "@fecha"/></u></td>
                      </tr>
                      
                    <tr bgcolor="yellow">
                          <td colspan="3">  Probabilitat de precipitacio  </td>
                  </tr>
                  
               <xsl:for-each select="./prob_precipitacion">
                  <tr>
                    <td colspan="2"><xsl:value-of select= "./@periodo "/></td>
                    <td ><xsl:value-of select= "."/></td>
                  </tr>
                </xsl:for-each>
                   
                   <tr bgcolor="yellow">
                          <td colspan="3">  Dades de Vent </td>
                   </tr>
                   
                   <tr bgcolor="grey">
                   <th>Periode</th>
                   <th>Direcció</th>
                   <th>Velocitat</th>
                   </tr>
                   
                   <xsl:for-each select="./viento"> 
                   
                      <xsl:choose>
                        <xsl:when test="velocidad >= 8">
                         <tr>
                                 <td><xsl:value-of select="@periodo"/></td>
                                 <td><xsl:value-of select="direccion"/></td>
                                 
                                 <td bgcolor="red"><xsl:value-of select="velocidad"/></td>
                         </tr>
                        </xsl:when>
                            <xsl:otherwise>
                            <tr>
                                 <td><xsl:value-of select="@periodo"/></td>
                                 <td><xsl:value-of select="direccion"/></td>
                                 <td bgcolor="green"><xsl:value-of select="velocidad"/></td>
                         </tr>
                            </xsl:otherwise>
                         </xsl:choose>
                  </xsl:for-each>
  
                          <tr bgcolor="grey">
                             <th >Quantitat registres vent</th>
                             <th >suma de velocitats</th>
                              <th >Mitjana velocitat</th>
                         </tr>
                         <tr>
                                 <td><xsl:value-of select="count(viento)"/></td>
                                 <td><xsl:value-of select="sum(viento/velocidad)"/></td>
                                 <td><xsl:value-of select="sum(viento/velocidad) div count(viento)"/></td>
                         </tr>
                   
                   <tr bgcolor="grey">
                   <th colspan="3">Temperatura/UV</th>
                   </tr>
                   
                   <tr bgcolor="grey">
                   <th >Temp. Màxima</th>
                   <th >Temp. Mínima</th>
                   <th >UV</th>
                   </tr>
                   
                  <xsl:for-each select="./temperatura">
                         <tr>
                                 <td><xsl:value-of select="maxima"/></td>
                                 <td><xsl:value-of select="minima"/></td>
                                 <td><xsl:value-of select="../uv_max"/></td>
                         </tr>
                         

                         
                  </xsl:for-each>
                  

                  
</xsl:for-each>

                  </table>
                
                </body>
            </html>
          </xsl:template> 
        </xsl:stylesheet>
