﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/UEFA">
		<html>
			<title>UEFA</title>
			<link rel="stylesheet" href="uefa.css"/>
			<header>
				<a href="https://es.uefa.com/"><img align="center" src ="logo_UEFA.png"/></a>
			</header>
			<body>
				<center>
					<table border="1">
						<tr></tr>
						<th colspan="3">Setzens</th>
						<xsl:for-each select="temporada/setzens/enfrontament">
						<tr>
							<td>					
							<xsl:value-of select="anada/equip_local/gols"/>
							<img src="{anada/equip_local/logo}"/>
							</td>
							<td>
							<xsl:value-of select="anada/equip_visitant/gols"/>
							<img src="{anada/equip_visitant/logo}"/>
							</td>
							<xsl:if test ="anada/equip_local/id = guanyador">
							<td rowspan="2"><img src="{anada/equip_local/logo}"/></td>
							</xsl:if>
							<xsl:if test ="anada/equip_visitant/id = guanyador">
							<td rowspan="2"><img src="{anada/equip_visitant/logo}"/></td>
							</xsl:if>
						</tr>
						<tr>
							<td>
							<xsl:value-of select="tornada/equip_local/gols"/>
							<img src="{tornada/equip_local/logo}"/>
							</td>
							<td>
							<xsl:value-of select="tornada/equip_visitant/gols"/>
							<img src="{tornada/equip_visitant/logo}"/>
							</td>
						</tr>
						</xsl:for-each>
					</table>
				</center>
			</body>
		</html>
	</xsl:template> 
</xsl:stylesheet>
