﻿<?xml version="1.0" encoding="UTF-8" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="/">
		<html>
			<body>
				<h2>My CD Collection</h2>
				<table border="1">
					<tr bgcolor="#9acd32">
						<th>Title</th>                     
						<th>Artist</th>
						<th>country</th>
						<th>company</th>
					</tr>
					<tr>                     
						<td><xsl:value-of select="catalog/cd1/title"/></td>                     
						<td><xsl:value-of select="catalog/cd1/artist"/></td>
						<td><xsl:value-of select="catalog/cd1/country"/></td>
            <td><xsl:value-of select="catalog/cd1/company"/></td>
					</tr>   
										<tr>                     
						<td><xsl:value-of select="catalog/cd2/title"/></td>                     
						<td><xsl:value-of select="catalog/cd2/artist"/></td>
						<td><xsl:value-of select="catalog/cd2/country"/></td>
            <td><xsl:value-of select="catalog/cd2/company"/></td>
					</tr>   
				</table>
			</body>
		</html>
	</xsl:template> 
</xsl:stylesheet>
