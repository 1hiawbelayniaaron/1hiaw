<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
   <html>
       <head>
            <link href="estil_SOLUCIO.css" rel="stylesheet" type="text/css" />

       </head>

       <body>

         <a href="{//web}">
         <img src="./logos/{//imatge}" width="100%" height="auto" class="esquinas"></img>
         </a>

         <table border="1" cellspacing="1" bgcolor="FFFFFF" width="100%">
           <tr>
             <th class="titol" colspan="4">
               <p class="titol">N* de noms de naus = <xsl:value-of select="count(//nom)"/></p>
               <p class="titol">N* de noms de naus coneguts = <xsl:value-of select="sum(root/serie/babylon5/nau/noms/@coneguts)"/></p>
               <p class="titol">N* de noms de naus coneguts d'origen Earth_Alliance = <xsl:value-of select="count(//noms)"/></p>
               <p class="titol">N* de noms de naus coneguts d'origen Narn = <xsl:value-of select="count(//nom)"/></p>

             </th>
           </tr>

           <tr class="titol">
             <th>Nom</th>
             <th>Bandol</th>
             <th>Caracteristiques</th>
             <th>Nom coneguts</th>

           </tr>
           <xsl:for-each select="//nau">
             <tr >
             <th width="320px">
               <a href="{url}">
               <img src="./imatges/{imatge}" width="100%" height="auto"></img></a>
               classe <xsl:value-of select="dades_generals/classePrincipal/@classe"/>


             </th>

             <td width="120px"><img src="./logos/{logo}" width="100%" height="20%"></img></td>


               <td width="320px">

                <p class="caract">Tonelatge: <xsl:value-of select="caracteristiques/tonelatge"/> &#160;
               <xsl:value-of select="caracteristiques/tonelatge/@unitat"/></p>

               <p class="caract">Longitud: <xsl:value-of select="caracteristiques/longitud"/>&#160; </p>
              <p class="caract">Tripulacio: <xsl:value-of select="capacitat_humana/tripulacio"/></p> &#160;
              <p>Naus transportades: </p>
              <p class="caract">&#160;&#160;&#160;&#160;Caces: <xsl:value-of select="capacitat_naus/caces"/></p> &#160;
              <p class="caract">&#160;&#160;&#160;&#160;Transport de tropes: <xsl:value-of select="capacitat_naus/transports_tropes"/></p> &#160;
              <p class="caract">abast: <xsl:value-of select="abast/quantitat"/>&#160; <xsl:value-of select="abast/unitat_de_mesura"/></p> &#160;

              <p>Propulsio: </p>
              <p class="caract">&#160;&#160;&#160;&#160;Reactors: <xsl:value-of select="propulsio/reactors/@numero"/> &#160;
              <xsl:value-of select="propulsio/reactors"/></p> &#160;


              <p class="caract">&#160;&#160;&#160;&#160;Motors d'impulsió: <xsl:value-of select="propulsio/numero_de_motors_impulsio"/> &#160;
              <xsl:value-of select="propulsio/motors_impulsio"/></p> &#160;

              <p class="caract">&#160;&#160;&#160;&#160;Potencia standart: <xsl:value-of select="propulsio/potencia_standart"/> &#160;
              <xsl:value-of select="propulsio/unitat_de_mesura_potencia"/> </p>

              <p class="caract">&#160;&#160;&#160;&#160;Potencia militar: <xsl:value-of select="propulsio/potencia_militar"/> &#160;
              <xsl:value-of select="propulsio/unitat_de_mesura_potencia"/> </p>
              </td>

              <td width="150px">
                <xsl:for-each select="//noms">

                  <p><xsl:value-of select="nom"/> </p>

                </xsl:for-each>
                <p>(N* de noms = <xsl:value-of select="noms/@coneguts"/>) </p>
</td>
</tr>
                <tr class="link" ><a href="{url}">
                  <td colspan="4"><xsl:value-of select="url"/></td></a>
                </tr>











           </xsl:for-each>
           <tr>

             <td> </td>

           </tr>

         </table>

       </body>
   </html>
</xsl:template>
</xsl:stylesheet>
