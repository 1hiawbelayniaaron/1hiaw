# Repàs d'HTML i CSS

**Coses bàsiques:**

```html
<!-- Fer paragrafs -->
<p>This is a paragraph.</p>
<p>This is another paragraph.</p>

<!-- Fer links -->
<a href="http://www.w3schools.com">This is a link</a>

<!-- Fer imatges -->
<img src="w3schools.jpg" alt="W3Schools.com" width="104" height="142">

<!-- Comandes -->
This is some text.<hr> Més text <!-- Salt horitzontal -->
<p>This is<br>a paragraph<br>with line breaks.</p> <!-- \n -->

<!-- Aparença: atribut style -->
<h1 style="font-family:verdana;font-size:300%;text-align:center;">This is a heading</h1>
<p style="font-family:courier;font-size:160%;text-align:left;">This is a paragraph.</p>
```

**Formateix del text:**

Tags que manipulen l'aspecte:

- <b>: ho posa en negreta.
- <strong>: resalta el text com si fos "important".
- <i>: ho posa en cursiva.
- <u>: subratlla el text.
- <em>: ho posa en cursiva i emfatitza el text com si fos "important".
- <mark>: marca el text com si l'hagués marcat amb un fosforito.
- <small>: redueix el tamany (ho fa mini).
- <del>: ratlla el text.
- <ins>: subratlla el text.
- <sub>: converteix el text en subíndex.
- <sup>: converteix el text en superíndex.
- <abbr>: converteix el text en una àlies o acrònim. Quan posem el ratolí a sobre ens apareix un títol

**Colors:**

[HTML Color Picker](https://www.w3schools.com/colors/colors_picker.asp)

**CSS:**

CSS = Cascading Style Sheets (fulls d’estil en cascada).
Estils en HTML de 3 maneres:

- Atribut style: vist en l’apartat 6.1
- \<style> en el \<head>: serà un CSS intern que només servirà per aquella pàgina.
- CSS extern.

```html
<!-- Com fer un CSS extern: Posar linea següent al <head> -->
<link rel="stylesheet" href="styles.css">
```

> Exemple de CSS intern, canviar fonts

```html
<!DOCTYPE html>
<html>
    <head>
        <style>
            h1 {
                    color: blue;
                    font-family: verdana;
                    font-size: 300%;
                }
            p {
                    color: red;
                    font-family: courier;
                    font-size: 160%;
               }
        </style>
</head>
<body>
<h1>This is a heading</h1>
<p>This is a paragraph.</p>
</body>
</html>
```

> **<u>Com fer caixes amb CSS:</u>** Per a especificar la vora al voltant de l’element **(border)**, l’espai dins de la vora **(padding)** i l’espai fora de la
> vora **(margin)**

```css
p {
border: 1px solid powderblue;
padding: 30px;
margin: 50px;
}
```

**Atribut ID vs Class:**

1. L'atribut **id** és unic, només pot haber un d'igual per pagina **(#)**

2. L'atribut **class** pot definir un estil per un element que escullim nosaltres **(.)**

```html
<!DOCTYPE html>
<html>
    <head>
        <style>
            #p01 {
               color: blue;
                }
        </style>
    </head>
    <body>
         <p id="p01">I am different</p>
    </body>
</html>
```

**Links:**

```html
<!-- Com fer una imatge linkcable -->
<a href="default.asp">
<img src="smiley.gif" alt="HTML tutorial" style="width:42px;height:42px;border:0;">
</a>
```

**Imatges:**

```html
<img src="dir imatge" alt="text que surt si no carrega la imatge">
```

> És obligatori l'us de l'atribut alt. La propietat **float** ens permet situar la imatge respecte al text que hi ha al seu voltant.

```html
<img src="smiley.gif" alt="a" style="float:left;width:42px;height:42px;">
The image will float to the left of the text.
```

- Els mapes d'imatges són imatges que tenen diverses zones clicables:

```html
<map name="planetmap">
    <area shape="rect" coords="0,0,82,126" alt="Sun" href="sun.htm">
    <area shape="circle" coords="90,58,3" alt="Mercury" href="mercur.htm">
    <area shape="circle" coords="124,58,8" alt="Venus" href="venus.htm">
    </map>
```

**Taules:**

- Les taules estan definides amb el tag  **\<table>**  .

- Les files estan definides amb el tag **\<tr>**.

- Les columnes estan definides amb el tag **\<td>**.

- El títol de les columnes de la taula es poden definir amb el tag **\<th>** .

```html
<!-- Exemple d'una taula:-->

<table style="width:100%" border="1">
<caption>Titol de la taula</caption>
<tr>
    <th>Firstname</th>
    <th>Lastname</th>
    <th>Age</th>
</tr>
<tr>
    <td>Jill</td>
    <td>Smith</td>
    <td>50</td>
</tr>
<tr>
    <td>Eve</td>
    <td>Jackson</td>
    <td>94</td>
</tr>
</table>
```

> - **border:** Posar sempre minim a 1.
> 
> - **padding:** determina la separació entre valors de la taula.
> 
> - **text-align:** determina si el contingut de les cel·les estarà a l'esquerra, centrat o a la dreta.
> 
> - **border-spacing:** determina la separació entre cel.les.
> 
> - **colspan:** fusionar columnes.
> 
> - **rowspan:** fusionar 2 files o més

**Llistes:**

```html
<!-- Exemple de llista no ordenada:-->
<ul style="list-style-type:disc">
    <li>Coffee</li>
    <li>Tea</li>
    <li>Milk</li>
</ul>

<!-- Exemple de llista ordenada:-->
<ol type="A">
    <li>Coffee</li>
<li>Tea</li><li>Milk</li>
</ol>
```

> L'atribut style="list-style-type:" pot ser: disc, circle, square o none.



**Blocs:**

- Hi ha 2 tipus de blocs, els que contenen molts elements diferents (\<div>) i els que són per a text (\<span>).

```html
<!-- div -->
<div style="background-color:black;color:white;padding:20px;">
    <h2>London</h2>
    <p>London is the capital city of England. It is the most populous city in
        the United Kingdom.</p>
</div>

<!-- span -->
<h1>My <span style="color:red">Important</span> Heading</h1>
```

**Layouts:**

<img title="" src="file:///home/aaron/.var/app/com.github.marktext.marktext/config/marktext/images/2022-05-31-19-12-39-image.png" alt="" width="154" data-align="inline">






- **\<header>** per a definir la capçalera.

- **\<nav>** per a posar el menú de navegació de la pàgina (els links de navegació).

- **\<section>** per a definir una secció d’un document.

- **\<article>** per a definir un artícle independent.

- **\<aside>** per a definir un contingut a part del contingut (és com una barra lateral).

- **\<footer>** per a definir un peu de pàgina d’un document o d’una secció.

[Exemple de Layouts](layouts.html)



**Formularis:** 

- El tag **\<form>** és el que farem servir per a crear un formulari amb el qual recollir informació de l'usuari.

- El tag **\<input>** pot ser de diferents tipus en funció del type que li possem (text, radio, submit,...).

```html
<!DOCTYPE html>
<html>
  <body>
    <form action="action_page.php" method="get">
            First name: <input type="text" name="firstname"><br>
            Last name: <input type="text" name="lastname"><br>
            <input type="submit" value="Boto_Enviament">
    </form>
    <br>
        <form action="action_page.php" method="post">
        <input type="radio" name="gender" value="male" checked> Male<br>
        <input type="radio" name="gender" value="female"> Female<br>
        <input type="radio" name="gender" value="other"> Other
        </form>
     <br>
    </body>
</html>
```

- El botó que de tipus submit enviarà les dades del formulari en qüestió.

- cada intup ha de tenir un atribut name perquè la pàgina diferencii les dades de cada input.
