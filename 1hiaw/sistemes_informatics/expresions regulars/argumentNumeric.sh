#!/bin/bash
# Filename:		argumentNumeric.sh
# Author:		aaron
# Date:			02/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		argumentNumeric.sh [arg1...]
# Description:		mirarem que passin un sol argument, sino sortim, mirar que sigui numero enter, si			 no sortim. finalment si es parell o senar


# mirar quants arguments ens passen per linea d'ordres
enter='grep -E "^[+-]?[0-9]+(\.[0-9]+)?$"' 
if test $# -gt 2
	then
		echo "Només es pot entrar un sol numero"
		echo "us $0 numero_que_vols_posar"
		echo "on el numero es enter"
		exit 1
fi

# mirar que l'argument sigui enter
if ! [[ $1 -eq $enter ]]
then

	echo "el nombre ha de ser enter"
	echo "els nombres enters no conten "," ni "." "
	echo "numero enter"
	exit 1
fi

# mirar si l'argument és parell o senar
if [ $(($1 % 2)) -eq 0 ]
then
	echo "$1 és parell"
else
	echo "$1 no és parell"
fi
