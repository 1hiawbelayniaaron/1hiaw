## Exercici 1:

^	inici de linia
$	final de linia
.	caracter qualsevol
*  	part que no es repeteix (def 0, def1 , def2 , def3) => def*
+	part que no es repeteix (def1 , def2 , def3) => def+ => no pot ser def sol.
?	part opcional
[a-z]   un de tots els valors de la A fins a la Z
[0-3] 	un de tots els numeros del 0 al 3
[376a-dm]  un caracter que pot ser 3,7,6,tots els que van de la d (1 cop),m

la \+ vol dir que hi ha d'haver un sol digit minim
echo 'dasdadasd234532' | grep '^[0-9]\+$'

amb $? sabem el nivell d'error
## Exercici 2:

 */ mirar script argumentNumeric.sh /*
 
## Exercici 3:

 */ fer script /*
 
## Exercici 4: 

for userid in `awk -F: '{print $3}' /etc/passwd`; do if (("$userid" >= 1000)); then echo "Valid User" :`cat /etc/passwd | grep $userid | awk -F: '{print $1,$3}'`; fi; done

cat passwd_expresions | grep -E '^[^:]+:[^:]+:99[0-9]:valid user'

## Exercici 5:

 */ per canviar el uid de l'usuari 1000(guest) /*
 
usermod -u 1002 guest

 */ canviar El GECOS d'un usuari =>  username:password:UID:GID:GECOS:homedir:shell /*
 
 sed -i 's/guest:x:1000:1000:*/guest:x:1000:1000:usuari local,,,/' etc/passwd


## Exercici 6:

*/ pasar de minuscula a mayuscula: /*

 	name='aaron'
 	echo ${name^^}
 
*/ pasar de mayuscula a minuscula: /*
 
 	name='AARON'
 	echo ${name,,}
