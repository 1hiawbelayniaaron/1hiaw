<#!/bin/bash
# Filename:		argumentReal.sh
# Author:		aaron
# Date:			02/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		argumentReal.sh [arg1...]
# Description:		mirarem que passin un sol argument, sino sortim, mirar que sigui numero enter, si			 no sortim. finalment si es parell o senar

real=$(echo "$1" | grep -E '^[+-]?[0-9]+(\.[0-9]+)?$') > dev/null # al no posar >& dev/null només els errors van dirigits a dev/null

# mirar quants arguments ens passen per linea d'ordres
if test $# -gt 2 -o $# -gt 2
	then
		echo "Només es pot entrar un sol numero"
		echo "us $0 numero_que_vols_posar"
		echo "on el numero es enter"
		exit 1
fi

# mirar que l'argument sigui enter
if  [[ $1 -eq $real ]] # pot tenir o no el signe + o - , despres els numeros de 0-9 els cops que calgui, 
				  		  # despres pot tenir o no , o . i despres pot tenir decimals de 0-9 els cops que calgui
then
	echo "El nombre és real"

else
	echo "el nombre ha de ser real"
	echo "els nombres reals conten "," ni "." "
	echo "el numero $1 no és un numero enter"
	
fi
