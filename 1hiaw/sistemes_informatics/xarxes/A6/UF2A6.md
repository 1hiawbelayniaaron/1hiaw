# El protocol Ethernet

## Exercici 1. Trames Ethernet unicast.

1. Fes un ping a una ip coneguda i captura la comunicació amb el programa
   Wireshark. Guarda la captura en un fitxer de tipus `libpcap` de nom
   `trames.pcap`
   
    A partir d'aquest paquets capturats, respon:

2. Quina adreça MAC té la màquina a la qual has fet ping?
   
   - (00:22:07:9d:f7:2e)

3. De quina marca és la NIC de la màquina a la qual has fet ping? Quins dígits
   hexadecimals ho determinen?
   
   - intenoBr_
   
   - 00:22:07

4. Quina adreça MAC té la teva màquina?
   
   - 78:2b:46:29:ea:2d

5. De quina marca és la NIC de la teva màquina? Quins dígits hexadecimals ho
   determinen?

6. - Intel Core
   
   - 78:2b:46

7. Fes un diagrama de la capçalera d'una trama Ethernet genèrica i digues quins
   valors té el primer dels teus paques capturats.

## Exercici 2. Trames Ethernet broadcast.

1. Fes un ping a broadcast i captura la comunicació amb el programa Wireshark.
   Guarda la captura en un fitxer de tipus `libpcap` de nom `tramesbr.pcap`.
   
    A partir d'aquests paquets capturats:

2. Crea un filtre per tal de visualitzar les trames Ethernet que contenen els
   paquets ip que tu has enviat, no els que has rebut.
   
    A partir d'un dels paquets obtinguts aplicant el filtre de l'apartat 2,
   respon:

3. A quina adreça MAC va destinat el paquet? Què vol dir?

## Exercici 3. Construcció d'una trama unicast.

### Prèvia

Els switch emmagatzemen a una taula les adreces MAC que arriben per cada port
(boca), la relacio desada és MAC <---> número port.

D'aquesta manera quan hagi d'enviar un paquet a una adreça MAC la posa al port
(boca) del switch corresponent.

+ Si el pcX no ha enviat res al switch, des de que està encés, el switch encara
  no pot relacionar la MAC d'aquest pcX amb un port.

+ Si el switch no sap per quin port ha d'enviar un paquet, *ho envia a tots el
  ports del switch*.
1. Amb el programa `packETH` crea un paquet ICMP que vagi d'un origen A a un
   destí B qualsevol, utilitzant MACS reals relacionades amb IP's reals.
   Observa des de C

2. Amb el programa packETH crea un paquet ICMP que vagi d'un origen A a un
   destí B qualsevol, utilitzant MACS fictícies (les IP's reals). Observa des
   de C. Hi ha diferències entre estar o no la targeta en mode promiscu ?

3. Torna a fer l'exercici anterior en mode promiscu. Què s'ha de fer perquè els
   paquets que crees amb MAC's fictícies (entre A i B) ja no les pugui veure
   una altre ordinador (C) amb wireshark ? (se suposa que A i B es posen d'acord)

## Exercici 4. Filtres

Dissenya filtres per al programa Wireshark (a nivell de la capa 2) per
aconseguir:

1. Capturar tots els paquets que no siguin de tipus ip

2. Capturar tots els paquets que siguin de tipus ip i que provinguin de NICs de
   FOXCONN

3. Capturar tots els paquets de tipus ip que vagin dirigits a la nostra
   màquina.

4. Capturar tots els paquets de tipus ip dirigits a la nostra màquina i tots
   els paquets dirigits a una adreça multicast.

5. Mostra només trafic a la xarxa (192.168.0.x) entre estacions de treball i
   servidors, no Internet.

## OBSERVACIÓ 1

És recomanable que feu un cop d'ull a [la capçalera Ethernet](https://web.archive.org/web/20200223034236/http://www.networksorcery.com/enp/protocol/IEEE8023.htm)

## OBSERVACIÓ 2:

Recordeu que per defecte a GNU/Linux s'ignoren els pings a broadcast. Si es vol
canviar aquest comportament, com a root, s'ha de fer:

```
echo 0 >/proc/sys/net/ipv4/icmp_echo_ignore_broadcasts
```

## OBSERVACIÓ 3

+ Per instal·lar wireshark podeu anar al [howto de debian de
  l'escola](https://gitlab.com/pingui/deb-at-inf/-/raw/master/Debian11/HowTo/install_wireshark.md)

+ Si no està instal·lat packETH, utilitzeu el gestor de paquets per
  instal·lar-lo. No el feu servir sense super-poders, per tant o com a root com a
  usuari que pugui fer servir sudo.
  
  ```
  apt-get install packeth
  ```

+ Primer de tot escollir quina interfície de xarxa fareu servir. Normalment es
  un dels botonets de menú de dalt (Interfície / Interface) i desplega un menú
  amb les diferents targetes de xarxa disponibles.

+ A partir d’aquí omplir les dades depenent de l’exercici, adreces MAC origen i
  destí, protocol de la següent capa, si és IP, adreces IP, si la següent capa
  és un ping, protocol ICMP, tipus de ping (request / reply) ... I finalment botó
  «send». Si es vol enviar més d’un paquet es pot anar al menú que surt del botó
  «Gen-b» per enviar més d’un, o infinits ...
