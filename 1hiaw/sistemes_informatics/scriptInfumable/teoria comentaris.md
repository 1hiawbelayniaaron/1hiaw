# **Com fer scripts bé:**

### Depurar errors:


>**sh** -x nomScript.sh

### comentaris:

##### comentari 1 linea de codi:
>  \# comentari
##### comentari d'un bloc (VIM):
**1.** Anar a la primera linea de comentari
**2.** Ctrl + V (Mode visual Block)
**3.** Shift + I + # + Space
**4.** Esc
