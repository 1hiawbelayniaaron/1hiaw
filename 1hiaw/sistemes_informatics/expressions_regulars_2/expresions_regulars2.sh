### Exercici 1
head -n 15 /etc/passwd | grep -E 2

### Exercici 2
-n 15 /etc/passwd | grep -E ^[a-zA-Z0-9]+:x:2:

### Exercici 3
dni="12345678Z"
echo $dni | grep -E ^[0-9]{8}[a-zA-Z]$

dni="12345678Z"
string="TRWAGMYFDPXBNJZSQVHLCKE"
echo $dni | grep -E ^[0-9]{8}${string:$(let ${nif:0:8})%23)}[a-zA-Z]$
### Exercici 4
data="21-12-2020"

echo $data | grep -E ^[0-3][0-9]-[0-1][0-9]-[0-9][0-9][0-9][0-9]$

### Exercici 5
data2="21/12/2020"
echo $data2 | grep -E ^[0-3][0-9]\/[0-1][0-9]\/[0-9][0-9][0-9][0-9]$

### Exercici 6
data2="21/12/2020"
data="21-12-2020"

echo $data | grep -E ^[0-3][0-9][\-\/][0-1][0-9][\-\/][0-9][0-9][0-9][0-9]$

### Exercici 7
cat noms1.txt | grep -E Jordi && cat noms1.txt | grep -E Anna

### Exercici 8
sed -i 's/Anna/\-nou\-/g' noms1.txt && sed -i 's/Jordi/\-nou\-/g' noms1.txt

### Exercici 9
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; touch passwd.tmp; cat /etc/passwd | head -n 10 > passwd.tmp; cat passwd.tmp; echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; sed -i 's/\/sbin\/nologin/\-noshell/g' passwd.tmp; cat passwd.tmp; rm passwd.tmp

### Exercici 10
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; touch passwd.tmp; cat /etc/passwd | head -n 10 > passwd.tmp; cat passwd.tmp; echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; sed -i '4,8 s/\/sbin\/nologin/\-noshell/g' passwd.tmp; cat passwd.tmp; rm passwd.tmp

### Exercici 11 no em va bé del tot
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"; touch passwd.tmp; cat /etc/passwd > passwd.tmp; echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
sed '/adm/,/halt/ s/\/sbin\/nologin/\-noshell/g' passwd.tmp; 
cat passwd.tmp | grep -E "*\-noshell"; rm passwd.tmp

