#!/bin/bash
# Filename:		mostra_info_mes.sh
# Author:		aaron belayni iaw49656032
# Date:			18/10/2010
# Version:		0.5
# License:		This is free software, licensed under the GNU General Public License v3.
# 				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./header.sh  script_name.sh
# Description:		l'script tindrà un argument (mes) que ens mostrarà les dades referents al 				mes. Nomes hi haurà un argument que rebrà valors de 1 a 12. si no hi ha 			argument podrem triar si s'ens dona la llista amb ordre ascendent o 				descendent 


#quan no hi ha cap argument
if [ $# -eq 0 ]
then
echo "escriu un unic caracter: a = ascendent   d = descendent"
read ordre
fi

#llistat ascendent
if [ $ordre == "a" ]
then 	
	sort trucades_violencia_masclista2020.csv | cut -f 1-2 -d "," trucades_violencia_masclista2020.csv
fi

#llistat descendent
if [ $ordre == "d" ] 
then
	 sort -r trucades_violencia_masclista2020.csv | cut -f 1-2 -d "," trucades_violencia_masclista2020.csv 
fi
# si no posem a o d
if [ $ordre != "d" || $ordre != "a" ]
then
	echo "error: Has d'entrar un únic argument, [a/d]"
	echo "ús: cal que utilitzis un sol argument en funcio de l'ordenació de la llista "
	exit 1

#valor argument [1,12]
if [ $1 -le 12 && $1 -gt 0 ]
then
	echo "error el rang de l'argument mes va de 1 fins a 12 tots dos incluits"
	echo "ús: $0 mes "
	echo "    el mes té un rang máxim de [1,12]"
	exit 1
fi

# obliga a posar un unic argument
if [ $# -gt 1 ]
then
	echo "error: Has d'entrar un únic argument"
	echo "ús: $0 mes "
	echo "    el mes fa referencia al mes de l'any, sempre amb dos digits"
	exit 1
fi

#busca la cadena => mes/ d'aquesta manera nomes sortiran els mesos				
 grep "$1/" trucades_violencia_masclista2020.csv | cut -f 1-2 -d ","  
