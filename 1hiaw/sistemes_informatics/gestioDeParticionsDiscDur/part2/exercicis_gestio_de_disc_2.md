# Exercici 1

a) no ja que no te permisos de lectura
b) No ja que no te permisos de lectura al directori
c) No, ja que només tè permisos de lectura de l'arxiu afile
d) No, ja que només té permisos de lectura però no d'execució per tant no podrà entrar al directori

# Exercici 2

chmod -R u=rwx, g=rw-, o=r--

# Exercici 3

chmod -R o+rwt

# Exercici 4

T = no té permisos d'execució i bit fixe

t = permisos d'execució i bit sticky (només pot borrar dins del directori/fitxer root o l'usuari propietari)

# Exercici 5

ls -l /urs/bin/passwd

-rwsr-xr-x 1 root root

com que hi ha l'ultima 'x' tots els others poden executar i com que hi ha la 's' nomès en el moment de ser executat serà executat com a root

# Exercici 6

466 ==> la mascara seria 200

# Exercici 7

755 ==> la mascara seria 022

# Exercici 8

chown usuari1:usuaris nom_arxiu

# Exercici 9

chown usuari2 nom_fitxer

# Exercici 10

chown :usuari2 nom_fixer

# Exercici 11

T

# Exercici 12

T

# Exercici 13

T

# Exercici 14

F

# Exercici 15

T

# Exercici 16

T

# Exercici 17

T

# Exercici 18

F

# Exercici 19

T (Per no crear cicles d'arbre)

# Exercici 20

T

# Exercici 21

# Exercici 22

find . -inum numero_de_inode

# hard link (https://linuxgazette.net/105/pitcher.html)
