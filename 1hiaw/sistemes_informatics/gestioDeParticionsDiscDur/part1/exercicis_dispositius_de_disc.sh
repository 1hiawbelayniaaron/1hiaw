# Exercici 1

Amb fdisk -l ens mostra les particions del disc dur

# particions disc dur
Device     Boot     Start       End   Sectors   Size Id Type
/dev/sda1  *         2046 400388095 400386050 190.9G  5 Extended
/dev/sda5            2048 195311615 195309568  93.1G 83 Linux
/dev/sda6       195313664 390623231 195309568  93.1G 83 Linux
/dev/sda7       390625280 400388095   9762816   4.7G 82 Linux swap / Solaris


# formatejar = crear sistema de fitxers

# exercici 6

# si comentem el fitxer /etc/fsda i fem reboot, no ens entrarà a l'interfaç gràfica, per solucionar-ho:
mount -o remount, rw /dev/sda5 /
vim /etc/fsda

# i canviem les files comentades
# com que el fitxer /etc/fsda té molta importancia i això fa que al no llegir el fitxer ens monti les particions only readeable i no podem tornar a canviar el fitxer /etc/fsda. Per solucionar-ho hem de fer un remount i donarli permisos de read and write , despres especifiquem la partició.
