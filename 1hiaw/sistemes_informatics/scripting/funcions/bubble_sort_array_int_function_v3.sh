#!/bin/bash
# Filename:		bubble_sort_array_int_function.sh
# Author:		jamoros
# Date:			04/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./bubble_sort_array_int_function.sh [arg1...]
# Description:		Script que ordena amb el mètode de la bombolla un array d'enters
# 				Farem l'array hardcoded i amb valors aleatoris utilitzant la variable RANDOM
# 				Utilitzem FUNCIONS


# function
# receive an array's name 
# sort the array using the bubble sort method (descendent)
# 

function sort_int_array {
	# Calculate the elements number of the array
	array_length=$(eval echo -n \${#$1[@]})
	# Bubble sort apply
	for ((i = array_length - 1; i > 0; i--))
	{
		for ((j=0; j<i; j++ ))
		{
			# If this number pair is disordered, our algorithm swaps
			if [ $(eval echo \${$1[j]}) -lt $(eval echo \${$1[j+1]}) ]
			then
				eval temp=\${$1[j]}
				eval $1[j]=\${$1[j+1]} 
				eval $1[j+1]=$temp
			fi			
		}
	}
}

# function
# receive the array s length
# We use that the array variable is global, defined in the body script

function create_random_int_array {
	#for i in $(seq 1 ${!1})
	for i in $(eval seq 1  \$$1)
	do
		integers[i-1]=$(($RANDOM % max_number + 1 ))
	done
}




max_number=30 # random maximum number

# If there is at least an argument from the command line we supose is the elements number of the array
# No error control about the first argument nature
# Otherwise we will use a 10 length array 
if [ $# -ge 1 ]
then
	elements_n=$1
else
	elements_n=10
fi

declare -a integers # integers is declared as an array
					# we don t need to do so, but according to (1)
					# it could make some array operations runs faster

# Call the create array function
create_random_int_array elements_n $elements_n 

# Show the array
echo ${integers[@]}

# Call the order array function
sort_int_array integers

# Show the ordered array
echo ${integers[@]}



<<OBSERVACIONS
(1) "Adding a superfluous declare -a statement to an array declaration may speed up execution of subsequent operations on the array."
extract from http://tldp.org/LDP/abs/html/arrays.html

(2) si volguessim posar una variable per la longitud de l'array ho podríem fer de la següent manera:
longitud_integers=10
...
	...		for i in $( eval echo {1..$longitud_integers});
	...
...
ja que si no fem res, no substituiria el valor de la variable "longitud_integers"

(3) El bloc:
                temp=${integers[j]}
                integers[j]=${integers[j+1]}
es podria codificar amb C-style com:
				(( temp=integers[j] ))
				(( integers[j]=integers[j+1] ))

(4) enllaç interessant:
	http://stackoverflow.com/questions/5061100/bash-c-style-if-statement-and-styling-techniques

(5) Specifically, indirection isn't possible on the ${!var@}, ${!var*}, ${!var[@]}, ${!var[*]}, and ${#var} forms. 
This means the ! prefix can't be used to retrieve the indices of an array, the length of a string, 
or number of elements in an array indirectly, extret de http://wiki.bash-hackers.org/syntax/pe?s[]=indirect&s[]=reference#indirection

OBSERVACIONS




