#!/bin/bash
# Filename:		char_in_string.sh
# Author:		iaw49656032
# Date:			01/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		show_chars.sh [arg1...]
# Description:		

# si no hi ha dos cadenes o si la segona és un unic caracter alfabetic, sortim de l'script
if [ $# -lt 2 ]
then
	echo "Error: al menys es necessiten 2 arguments, un de tipus string i l'altre de tipus char"
function one_char_per_line {
	local i
	local char = ""
	for (( i = 0; i < ${#1}; i++)) 
	do	
		char=${1:$i:1}
		if test $char = $1
	done
}

# posem cometes per tal de que ho agafi com un sol argument
one_char_per_line "$*"
