#!/bin/bash
# Filename:		bubble_sort_array_int.sh
# Author:		jamoros
# Date:			07/03/2018
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./bubble_sort_array_int.sh [arg1...]
# Description:	Script que ordena amb el mètode de la bombolla un array d'enters
# 				Farem l'array hardcoded i amb valors aleatoris utilitzant la variable RANDOM

# VARIABLES GLOBALS

max_number=30 		# número màxim aleatori
declare -a enters	# això declara la variable enters com a array
# Calcularem 10 números aleatoris i els afegirem a un array 
enters=( $( \
			for i in {1..10};
			do 
				echo $(( $RANDOM % max_number + 1 ))
			done
			)
		)

# FUNCTIONS

function bubble_sort {

	enters=($@)  # (*)
	# Mètode de la bombolla en ordre descendent
	for (( i = ${#enters[@]} - 1; i > 0; i--))
	do	
		for (( j = 0; j < i; j++ ))
		do	
			# Si aquesta parella no està ordenada en ordre decreixent fem un swap
			if (( enters[j] < enters[j+1] )) 
			then
				temp=${enters[j]}
				enters[j]=${enters[j+1]}
				enters[j+1]=$temp
			fi			
		done
	done
}

# MAIN

# Mostrem l'array creat de manera aleatòria
echo ${enters[@]}

# Cridem a la funció que ordena passant-li l'arrai
bubble_sort ${enters[@]}

# Mostrem l'arrai, ara ja ordenat
echo ${enters[@]}

# (*) Alerta aquí amb deixar-se els parèntesi ja que si no els posem no l'interpreta com un vector.
