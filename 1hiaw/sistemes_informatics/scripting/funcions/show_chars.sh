#!/bin/bash
# Filename:		show_chars.sh
# Author:		iaw49656032
# Date:			01/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		show_chars.sh [arg1...]
# Description:		



function one_char_per_line {
	local i
	for (( i = 0; i < ${#1}; i++)) 
	do	
		echo -e ${1:$i:1}
	done
}

# posem cometes per tal de que ho agafi com un sol argument
one_char_per_line "$*"
