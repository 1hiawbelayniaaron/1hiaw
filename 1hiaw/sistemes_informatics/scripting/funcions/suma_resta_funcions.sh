#!/bin/bash
# Filename:		suma_resta_funcions.sh
# Author:		jamoros
# Date:			11/02/2016
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./suma_resta_funcions.sh [arg1...]
# Description:	Script introductori del concepte de funció.
# 				L'script rep 3 arguments per la línia d'ordres.
# 				El 1er i el 3er seràn números mentre que 
# 				el 2on serà l'operador suma (+) o l'operador resta (-).
# 				L'script mostrarà el resultat de realitzar l'operació corresponent.

########################
# Definim les funcions #
########################

function suma {		#també es pot definir com suma(){
	return $(($1 + $2))
}

function resta {
	return $(($1 - $2))
}

###############################################
# Aqui comença l'execució del nostre programa #
###############################################

# Fem un control d'error d'entrada bàsic per no recarregar el codi

echo "resultat de l'operacio demanada per la linia d'ordres: $1 $2 $3"

if [ "$2" = "+" ]
then
	suma $1 $3	# Cridem a la funció amb 2 dels 3 arguments entrats per la línia de comandes
	echo $?		# Mostrem el valor de retorn de la funció (és a dir el seu errorlevel)
elif [ "$2" = "-" ]
then
	resta $1 $3
	echo $?
else
	echo "$2 no és ni '+' ni '-' "
	exit 1 
fi			


# També podríem sumar un parell de números fixos o restar-los
# Si voleu fer proves, descomenteu les següents línies:

#echo "operacions fixes: sumem i restem 20 i 15"

#suma 20 15
#echo $?
#resta 20 15
#echo $?


