#!/bin/bash
# Filename:		exercici1.sh
# Author:		iaw49656032
# Date:			08/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici1.sh [arg1...]
# Description: 		mostra el limit de credit de un determinat client en funció del seu numero de client


# mirem que ens passin un sol argument

if [ $# -ne 1 ]
then
	echo -e "Error: s'ha de posar un unic argument"
	echo -e "Sintaxis: $0 num_client"
	exit 1
fi



##########
# Funció #
##########

get_limit_credit() {
	num_client=$1
	# mirem que el numero de client sigui de 4 xifres
	if [ ${#num_client} -ne 4 ]
	then
		echo "El client $num_client no existeix a la base de dades"
		exit 1
	fi
	limit=$(cat clients.txt | eval grep -E '^$num_client:'| cut -d :  -f 4)
	echo "El limit del client $num_client és $limit"
} 

########
# Main #
########


get_limit_credit $1
