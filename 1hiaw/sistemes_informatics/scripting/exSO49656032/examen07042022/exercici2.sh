#!/bin/bash
# Filename:		exercici2.sh
# Author:		iaw49656032
# Date:			08/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici2.sh [arg1...]
# Description:		Apartir de el numero de client, mostrarem la suma dels imports de les comandes fetes per aquest client


# mirem que ens passin un sol argument
if [ $# -ne 1 ]
then
		echo -e "Error: s'ha de posar un unic argument"
		echo -e "Sintaxis: $0 num_client"
		echo -e "num_client és un nombre enter que representa client"
		exit 1
fi

# mirem si el client té comandres
cat comandes.txt | eval grep -E '^*:*:$1:' >/dev/null
if [ $? -ne 0 ]
then
	echo -e "El client no té pendent cap comanda"
	exit 1
fi


##########
# Funcio #
##########

get_total_debt() {
	num_client=$1
	# Cerquem a comandes, quines son del client que busquem i guardem tots els imports del client
	total_imports=$(cat comandes.txt | eval grep -E '^*:*:$num_client:' | cut -d : -f 8)
	contador=$(cat comandes.txt | eval grep -E '^*:*:$num_client:' | cut -d : -f 8 | wc -l)
	while [ $contador -ne 0 ]
	do	
		# sumem els valors dels imports del client
		num=$(echo $(cat comandes.txt | eval grep -E '^*:*:$num_client:' | cut -d : -f 8) | cut -d ' ' -f $contador)	
		let total+=$num
		let contador=contador-1
	done
	echo "$total"
}


########
# Main #
########

get_total_debt $1
