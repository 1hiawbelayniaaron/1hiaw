#!/bin/bash
# Filename:		arguments.sh
# Author:		Aaron Belayni iaw49656032
# Date:			22/10/2015
# Version:		0.2
# License:		This is free software, licensed under the GNU General Public License v3.
# 			See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./arguments.sh  arg1 arg2 arg3
# Description: 		Script that shows the variables containing the values received from the command line,
#			(and the number of arguments, and the name of the script ...)


# Show the value of variable $0 => nom programa
echo "the value of the variable \$0 is $0"

# Show the value of variable $1
echo "the value of the variable \$1 is $1"

# Show the value of variable $2
echo "the value of the variable \$2 is $2"

# Show the value of variable $3
echo "the value of the variable \$3 is $3"

# Show the value of variable $# => numero d'arguments passats per la linea d'ordres
echo "the value of the variable \$# is $#"

# Show the value of variable $* => tots els arguments
echo "the value of the variable \$* is $*"

# We shift the arguments (by default if the shift command doesn't has an argument the step is 1)
shift
# Again showing all the same values
echo "the value of the variable \$0 is $0"
echo "the value of the variable \$1 is $1"
echo "the value of the variable \$2 is $2"
echo "the value of the variable \$3 is $3"
echo "the value of the variable \$# is $#"
echo "the value of the variable \$* is $*"


