#!/bin/bash
# Filename:		arguments.sh
# Author:		Aaron Belayni iaw49656032
# Date:			22/10/2015
# Version:		0.2
# License:		This is free software, licensed under the GNU General Public License v3.
# 			See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./arguments.sh  arg1 arg2 arg3
# Description: 		Script that shows the variables containing the values received from the command line,
#			(and the number of arguments, and the name of the script ...)




echo "#!/bin/bash" >> $1
echo -e "#Filename:\t \t  $1" >> $1
echo -e "#Author: \t \t " whoami >> $1
