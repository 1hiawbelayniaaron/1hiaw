#!/bin/bash
# Filename:		mostra_arguments
# Author:		iaw49656032
# Date:			26/01/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./mostra_arguments [arg1[arg2...]]
# Description:		l'script et pregunta, s'ha de respondre SI o NO o sinò es mantindrà en el bucle



#mentre que la resposta no sigui 'n' o 's'
#mostrem un missatge informatiu i recollir resposta

echo -ne "Escriu s o n \n"
read resposta

#es un and ja que si es s => donarà el primer false pro el segon true i continuaria al bucle. 

while [ "$resposta" != "s" ]&&[ "$resposta" != "n" ]
do
	echo -ne "Escriu s o n \n
	read resposta
	sleep 1
	
done
