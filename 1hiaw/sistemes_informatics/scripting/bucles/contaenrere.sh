#!/bin/bash
# Filename:		numerar.sh
# Author:		iaw49656032
# Date:			21/01/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		numerar.sh [arg1...]
# Description:		conta enrere i al final mostra un missatge preciós.

#inicialitzem la variable del compte enrere

 time=8
#mentre no arribem a 0
#mostreu la varible comptadora
 while test $time -gt 0
 do
 	 
 echo -ne "\r$time"
 sleep 1
  let time=time-1
 done
