#!/bin/bash
# Filename:		mostra_arguments
# Author:		iaw49656032
# Date:			26/01/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./mostra_arguments [arg1[arg2...]]
# Description:		script que mostra els arguments que pasen per la linea d'ordres. mentre que hi 				hagi arguments pasant a $1

#s'emagatzema a la variable arg el numero total d'arguments
arg=$#

while [ $arg -gt 0 ]
do
	echo -ne "$1 \n"
	shift
	sleep 1
	let arg=arg-1
done
