#!/bin/bash
# Filename:		cripto_cesar.sh
# Author:		aaron
# Date:			07/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		criptocesar.sh 
# Description:		ens permet passar de codi ascii a caracter i de caracter a codi ascii


# iniciem el menu i preguntem a l'usuari quina operació vol executar
echo -e "Escull una de les opcions següents:\t"
echo -e "1) Entrar un caràcter i es mostrarà el codi ascii corresponent\t"
echo "2) Entrar un codi ascii i es mostrarà el seu caràcter corresponent"
read opcio_menu

##########################
# Declarem les funcions  #
##########################

function parse_char_to_ascii {
	codi_ascii=$(echo -n "$1" | od -An -tuC) 
	echo $codi_ascii       
}

function parse_ascii_to_char {
	char=$(printf \\$(printf %o $1))
	echo $char
}

########
# Main #
########

# comprovem l'opció de l'usuari
if [ "$opcio_menu" = "1" ] 
then
	echo "de quin caracter vols saber el codi ascii?"
	read caracter
	
	# un unic caracter
	if test $(echo ${#caracter}) -ne 1
	then
		echo "ERROR: Nomès pots introduir un sol caràcter"
		exit 1
	fi
	parse_char_to_ascii "$caracter"
elif [ "$opcio_menu" = "2" ]
then
	echo "de quin codi ascii vols saber el seu caracter?"
	read codi
	
	# ha de ser un numero
	echo "$codi" | grep -E '^[0-9]*$' >/dev/null
	if test $(echo $?) -ne 0
	then
		echo "Error: Només pots introduïr numeros"
		exit 1
	fi
	parse_ascii_to_char "$codi"

else
	# l'usuari no escull ni la opcio 1 ni la 2
	echo "Error: Només pots triar entre la opció 1 ó 2"
	exit 1
fi
