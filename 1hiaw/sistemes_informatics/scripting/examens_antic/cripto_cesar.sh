#!/bin/bash
# Filename:		menu.sh
# Author:		aaron
# Date:			07/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		menu.sh 
# Description:		ens permet passar de codi ascii a caracter i de caracter a codi ascii




##########################
# Declarem les funcions  #
##########################

function parse_char_to_ascii {
	codi_ascii=$(echo -n "$1" | od -An -tuC) 
	echo $codi_ascii       
}

function parse_ascii_to_char {
	char=$(printf \\$(printf %o $1))
	echo $char
}

function encrypt {
	
	local text=$2
	echo "$2"
	contador=${#text}
	while test $contador -ge 0
	do
		char=${text:$contador:1}
		echo -e $char\t
		let contador-=1
	done
		
}

########
# Main #
########

text=$(cat $2)
numero=$1

encrypt $numero $text


