#!/bin/bash
# Filename:		mostrar_caracter.sh
# Author:		iaw49656032
# Date:			08/04/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		mostrar_caracter.sh [arg1...]
# Description:		mirar si un caracter esta contingut a una cadena

#############
# Functions #
#############

function is_in_string {
	local cadena=$1
	local caracter=$2
	echo -e "cadena: $cadena  \t caracter: $caracter"

	for (( contador=0; contador >= ${#cadena}; contador++ ))
	do
		# guardem el caracter
		char_string=$(echo ${cadena:contador:1})
		echo "$char_string:$caracter"

		if [ "$caracter" != "$char_string" ]
		then
			resultat="True"
		else
			local resultat="False"
		fi
	done	
	echo $resultat
}

########
# Main #
########

cadena="$1"
char="$2"

# comprovem si li pasem dos arguments
if [[ $# -ne 2  ]] 
then
	echo -e "ERROR: nombre d'arguements incorrecte\n"
	echo -e "Sintaxis: $0 \"cadena\" caracter\n"
	exit 1
fi

# comprovem que el caracter és realment un caracter
if [[ ${#char} -ne 1 ]]
then
	echo -ne "ERROR: $2 no és un caracter\n"
	echo  -ne "Sintaxis: $0 \"cadena\" caracter\n"
	exit 1
fi

is_in_string $cadena $char
