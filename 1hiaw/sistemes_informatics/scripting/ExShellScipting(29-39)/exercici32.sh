#!/bin/bash
# Filename:		exercici32.sh
# Author:		iaw49656032
# Date:			04/02/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici32.sh [arg1...]
# Description:		És un script que suma tots els arguments que se li donin i retorna el nombre de l			 a suma.

# variable suma  = 0
 suma=0


# per cada argument afegire

while [ $# -gt 0 ]
do

	suma=$(($suma+$1))
	
#aixo farà que $1 rebi el valor de $2 i desprès de $2...
	shift

done
#mostro el resultat

echo $suma


