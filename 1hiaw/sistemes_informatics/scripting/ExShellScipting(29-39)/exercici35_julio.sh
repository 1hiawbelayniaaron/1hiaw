#!/bin/bash
# Filename:		exercici35.sh
# Author:		iaw49656032
# Date:			07/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici39.sh [arg1...]
# Description:		

# demanar no d'usuari i guardar-lo

echo "Sisplau introdueix el login de l'usuari per saber si està conectat:"
read usuari

# si no entrem cap valor missatge d'error al sistema
if test -z "$usuari" # -z mira que la cadena sigui res
then
	echo "no has entrat cap nom d'usuari"
	exit 1
fi
# Si usuari no exiteis missatge indicant-ho i sortim
grep "^$usuari:" /etc/passwd > /dev/null
if test $? -ne 0
then 
	echo "L'usuari $usuari no existeix"
	exit 2
fi

# si ususari esta conectat mostrar missagte  i sino dir que no ho esta
# tr -s " " serveix per treure els espais en blanc a tot 

who | tr -s " " | cut -f1 -d" " > /dev/null
# també podem fer servir: users | -w
if test $? -ne 0
then 
	echo "L'usuari $usuari no està conectat"
else
	echo "L'usuari $usuari no està conectat"
fi

 
