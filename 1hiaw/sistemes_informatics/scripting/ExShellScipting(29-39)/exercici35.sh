#!/bin/bash
# Filename:		exercici35.sh
# Author:		aaron
# Date:			04/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici35.sh 
# Description:		demana un nom d'usuari i diu si existeix o no. Si existeix ens diu si esta conectat

# demanar el nom d'usuari
echo "nom d'usuari:"
read user


# mirar si existeix l'usuari
userExist=$(who | grep -E $user[^\b] 2>/dev/null | cut -d ' ' -f 1) 
if [ $userExist = $user ] 2>/dev/null
then
	echo "usuari $user existent"
	# existeix l'usuari
	stillLogged=$(last | grep -E "still logged in" | cut -d ' ' -f 1 )
	if [ "$stillLogged" = "$user" ]
	then
		# usuari logejat
		echo "usuari $user està conectat"
	else
		# usuari no conectat
		echo "usuari $user no està conectat"	
	fi
	
else 	# no existeix l'usuari
	echo "usuari $user inexistent"
fi
