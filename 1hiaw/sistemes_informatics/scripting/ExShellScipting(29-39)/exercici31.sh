#!/bin/bash
# Filename:		exercici31.sh
# Author:		pingui
# Date:			01/12/2015
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici31.sh file1
# Description:	script que rep un argument. Si és un directori llista el seu contingut

# Si no es rep un únic argument es mostra un missatge d'error, d'ajuda i sortim indicant l'error al sistema.
if [ $# -ne 1 ]
then
	echo "error: Has d'entrar un únic argument"
	echo "ús: $0 file1 "
	echo "    on file1 és un fitxer"
	exit 1
fi

# Altrament, si no existeix el fitxer mostro un missatge que indica que no existeix. Surto indicant l'error al sistema.
if [ ! -e $1 ]
then
	echo "$1 no existeix"
	exit 2
fi

# Altrament, si no és un directori mostro un missatge que indica que no ho és.
if [ ! -d $1 ]
then
	echo "$1 no és un directori"
# Altrament, fem un llistat de format llarg del directori
else
	ls -l $1
fi

# Nota1: (podria fins i tot indicar quin tipus de fitxer és (enllaç, dispositi per blocs...) amb una estructura 'case' (en llenguatges d'alt nivell com C++ o Java seria estructura 'switch')
# Nota2: podria fer més controls. Per exemple comprovar si hi ha permís de lectura al directori.
