g#!/bin/bash
# Filename:		exercici39.sh
# Author:		iaw49656032
# Date:			07/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici39.sh [arg1...]
# Description:		permet copiar arxius , moure arxius i esborrar fitxers, l'script comprova si la sintaxi es correcta

contador=1
while test $# -ge $contador
do 
	# agafar l'ordre que és vol executar 
	numArgument=$(echo \$$contador)
	argument=$(eval echo $numArgument)
	
	# copiar
	if [ "$argument" = "-c"  ] 
	then
		ls 
		echo "nom de l'arxiu que vols copiar:"
		read arxiu
		echo "nom de la copia:"
		read copia
		cp "$arxiu" "$copia"
		echo "$arxiu ha sigut copiat amb exit"
	fi
	
	
	# borrar arxius
	if [ "$argument" = "-d"  ] 
	then
		echo "nom de l'arxiu que vols borrar:"
		read arxiu
		rm "$arxiu"
		echo "$arxiu ha sigut borrat amb exit"
	fi
	
	# moure arxius
	if [ "$argument" = "-m"  ] 
	then
		echo "nom de l'arxiu que vols moure:"
		read arxiu
		echo "ruta a on el vols moure:"
		read ruta
		rm "$arxiu" "$ruta" 2> /dev/null
		
		# if [ $(echo $?) -ne 0 ]
		# then
		#	echo "error"
		# fi
		
		echo "$arxiu ha sigut mogut amb exit"
	fi
	
	
	
	
	let contador+=1
			
done
