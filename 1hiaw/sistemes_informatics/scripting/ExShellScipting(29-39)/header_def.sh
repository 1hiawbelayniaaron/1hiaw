#!/bin/bash
# Filename:		header.sh
# Author:		aaron belayni iaw49656032
# Date:			18/10/2010
# Version:		0.5
# License:		This is free software, licensed under the GNU General Public License v3.
# 				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./header.sh  script_name.sh
# Description:	Script to automate the creation of headers.
# 				This program wil get the name of the script from the command line
# 				and this program will create the header details, with execute permissions
# 				and also the developer will run vim in insert mode.
					

license="This is free software, licensed under the GNU General Public License v3.
#\t\t\t\tSee http://www.gnu.org/licenses/gpl.html for more information."

echo  '#!/bin/bash' > $1
echo -e "# Filename:\t\t$1" >> $1
echo -e "# Author:\t\t$(whoami)" >> $1
echo -e "# Date:\t\t\t$(date +%d/%m/%Y)" >> $1
echo -e "# Version:\t\t0.1" >> $1
echo -e "# License:\t\t$license"  >> $1
echo -e "# Usage:\t\t$1 [arg1...]" >> $1
echo -en "# Description:\t\t" >> $1 
chmod u+x "$1"           				# set execution owner file
file_size=$(wc -m $1| cut -d" " -f 1) 	# count file chars
vim +"${file_size}go" $1 -c start   	# Start vim in insert mode putting the cursor after the last character.
