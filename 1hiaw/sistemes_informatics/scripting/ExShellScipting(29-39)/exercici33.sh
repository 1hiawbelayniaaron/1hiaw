#!/bin/bash
# Filename:		exercici33.sh
# Author:		iaw49656032
# Date:			04/02/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici33.sh [arg1...]
# Description:		Script que conta els caracters de els noms dels fitxers del directori actual

# capturem els fitxers del directori actual

tots_fitxers=$(ls -1) # ls -1 fa que tinguem en una llista pro de 1 en un

for filename in $tots_fitxers
do
	num_caracters=$(echo $filename |wc -m)
	echo $filename:$sum_caracters
done

