#!/bin/bash
# Filename:		lse.sh
# Author:		aaron
# Date:			05/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		lse.sh [arg1...]
# Description:		mostra els fitxers del directori actual de manera esgraonada

# llistem el contingut del directori i l'emagatzem
fitxers=$(ls)
# emagatzem el numero de fitxers
num_fitxers=$(ls | wc -l)
counter=1
escala="|_"
punts=""
fitxer=$(echo $(ls)| cut -d ' ' -f 1)
while test $num_fitxers -ge $counter
do
	let counter=$counter+1
	echo $punts$escala$fitxer
	fitxer=$(echo $(ls)| cut -d ' ' -f $counter)
	punts+="."
	
done


