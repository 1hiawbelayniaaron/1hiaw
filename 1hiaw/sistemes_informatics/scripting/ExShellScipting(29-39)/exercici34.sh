#!/bin/bash
# Filename:		exercici34.sh
# Author:		iaw49656032
# Date:			04/02/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici34.sh [arg1...]
# Description:		Script que determina si els arguments que se li passen son parells o senars

# si nombre d'arguments=0 mostrem missatge error

if test $# -eq 0
then
	echo "es necessita un numero com a minim"
	echo "us $0 numero1 [numero2 [numero3 ... ][numeroX]...]]"
	echo "on numeros són numeros enters"
	exit 1
fi


# mentres que hi hagi arguments i sigui enter

while test $# -gt 0 && echo $1 | grep -E ^[0-9]+[^\.][0-9]$ > /dev/null
do
# el grep comprova que sigui un numero enter i nomes un


	if [ $(($1 % 2)) -eq 0 ]
	then
		echo "$1 és parell"
	else
		echo "$1 es senar"
	fi
	shift
done

# si el nombre no es enter posem missatge d'error
if ! echo -n $1 | grep -E ^[0-9]+[^\.][0-9]$ > /dev/null #si no es enter
then
	echo "error: el nombre ha de ser enter, el nombre $1 no és un enter"
	exit 1 
fi
	
