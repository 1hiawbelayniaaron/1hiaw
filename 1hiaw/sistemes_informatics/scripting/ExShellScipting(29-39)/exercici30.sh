#!/bin/bash
# Filename:		exercici30.sh
# Author:		pingui
# Date:			01/12/2015
# Version:		0.2
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		./exercici30.sh
# Description:	Entrarem un caràcter i direm si és un número, lletra o diferent d'aquests. 
# 				Tot i que l'exercici no diu res al respecte, farem un petit control d'errors: 
# 				si hi ha més o menys d'un caràcter sortirem.

# Demanem un caràcter i l'emmagatzemem
echo "Sisplau entra un únic caràcter"
read caracter
num_caracters=$(echo -en $caracter | wc -m)

# Si no hem entrat exactament un caracter sortim mostrant l'error a l'usuari i al sistema 
if [ $num_caracters -ne 1 ]
then
	echo "error: no has entrat un únic caràcter"
	exit 1
fi

# En cas contrari, si és una lletra mostrem un missatge indicant-lo
echo -en $caracter | grep '[[:alpha:]]' > /dev/null 2> /dev/null
if [ $? -eq 0 ]
then
	echo "És una lletra"
else
	# Altrament, si és un número mostrem un missatge indicant-lo
	echo -en $caracter | grep '[[:digit:]]' > /dev/null 2> /dev/null
	if [ $? -eq 0 ]
	then
		echo "És un dígit"
	# Si no, mostrem un missatge indicant que no és ni lletra ni nombre
	else
		echo "No és ni una lletra ni un dígit"
	fi
fi

# Nota: recordeu que wc -c compta bytes, no caràcters, i per exemple el caràcter 'é' necessita dos bytes
