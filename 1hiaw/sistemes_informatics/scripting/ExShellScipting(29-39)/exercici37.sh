#!/bin/bash
# Filename:		exercici37.sh
# Author:		aaron
# Date:			05/03/2022
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		exercici37.sh [arg1...]
# Description:		sumar mides dels fitxers que li passin com a arguments, si no existeix o són directoris donar un error


numArguments=1
argument=""
pes=0
argument_def="$1"


# mentres que hi hagi arguments que vagi fent
while test $# -ge $numArguments
do


	# si el paramatre no existeix
	if ! [ -f "$argument_def" ];then
		echo "ERROR: un dels paramatres entrats no éxisteix"
		echo "El paramatre $argumentDef no existeix al directori actual"
		exit 1
	fi

	# si és un directori
	if [ -d "$argument_def" ] #argument def
	then	
		# si és un directori
		echo "ERROR: un dels paramatres entrats és un directori"
		echo "El paramatre $argumentDef és un directori"
		echo "sintaxi: $0 nom_arxiu1 nom_arxiu2 ..."
		exit 2
	fi

		argument=$(echo '\$'$numArguments) # argument="$1 , $2 , $3 ..."
		argument_def=$(eval echo "$argument") # argumentDef="valor_arg1 , valor_arg2, valor_arg3 ..."
		echo $argumentDef
		# trobar pes de l'arxiu
		# let pes+=$(du $argumentDef | grep -E *$argumentDef | cut -f 1)
		# echo $argumentDef
		# let numArguments+=1
done

echo "El pes dels arxius és de $pes"
	
# provar nif="12345678A"
# echo ${nif:0:8}
