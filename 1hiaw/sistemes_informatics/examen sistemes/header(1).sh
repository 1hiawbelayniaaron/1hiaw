#!/bin/bash
# Filename:			header.sh
# Author:			Julio Amorós iaw3.141592
# Date:				19/12/2018
# Version:			0.7
# License:			This is free software, licensed under the GNU General Public License v3.
# 					See http://www.gnu.org/licenses/gpl.html for more information."
# Usage:			./header.sh script_name[.sh]
# Description:		L'script rep un argument que serà el nom del fitxer script a crear
# 					i l'afegeix una capçalera semblant a la d'aquest script.
# 					Fem control d'errors: si no hi ha un únic argument sortim de l'script,
# 					si el fitxer ja existeix sortim de l'script, 
# 					si l'argument que li passem no conté l'extensió '.sh' l'afegim nosaltres

license="This is free software, licensed under the GNU General Public License v3.
#\t\t\t\tSee http://www.gnu.org/licenses/gpl.html for more information."

modeline="# vim:ai:ts=4:sw=4:syntax=sh" # afegirem el modeline al final del fitxer.

# Si el nombre d'arguments no és exactament 1, mostrem missatge d'error, ajuda de sintaxi i sortim amb errorlevel diferent de 1
if [ $# -ne 1 ]
then
	echo "L'ordre necessita un argument"
	echo "ús: ./$0 nom_fitxer"
	echo "on nom_fitxer és el nom de l'script sense extensio"
	exit 1
fi


# Si el fitxer ja exixteix, ho indiquem i sortim de l'script

if [ -f "$1" ] || [ -f "$1.sh"  ]
then
	echo "El fitxer "$1" o "$1.sh" ja exixteix"
	exit 2
fi


# Si l'usuari no li ha posat l'extensió '.sh' creem una variable amb aquesta cadena
echo $1 | grep '\.sh$' # Com podem fer-ho perquè no es mostri per pantalla el possible missatge si el troba (s'enten aquest comentari?:D)
if [ $? -ne 0 ]
then
	EXTENSION=".sh"
fi


echo  '#!/bin/bash' > $1$EXTENSION
echo -e "# Filename:\t\t$1" >> $1$EXTENSION
echo -e "# Author:\t\t$(whoami)" >> $1$EXTENSION
echo -e "# Date:\t\t\t$(date +%d/%m/%Y)" >> $1$EXTENSION
echo -e "# Version:\t\t0.1" >> $1$EXTENSION
echo -e "# License:\t\t$license"  >> $1$EXTENSION
echo -e "# Usage:\t\t./$1 [arg1...]" >> $1$EXTENSION
echo -e "# Description:\t" >> $1$EXTENSION 
echo >> $1$EXTENSION
echo "$modeline" >> $1$EXTENSION

chmod u+x "$1$EXTENSION"           # establim el permís d'eXecució per l'usuari propietari del fitxer
num_chars_fitxer=$(wc -m $1$EXTENSION| cut -d" " -f 1) # comptem el número de caràcters del fitxer
num_chars_extres=$(wc -m <<<$modeline)
num_chars_fitxer=$((num_chars_fitxer-num_chars_extres-1))
vim +"${num_chars_fitxer}go" $1$EXTENSION -c start   # ens col·loquem a la posició indicada pel número de caràcters.

# vim:ai:ts=4:sw=4:et:syntax=sh
