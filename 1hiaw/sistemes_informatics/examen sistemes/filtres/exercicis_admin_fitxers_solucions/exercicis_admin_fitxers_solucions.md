## Exercicis tema LPI 103.3 Tasques bàsiques d'administració de fitxers



##### Exercici 1.

Acabeu de treballar el listing 4 dels apunts de IBM suposant que teniu un fitxer qualsevol _text1_ que es troba al nostre `$HOME/iawxxxxx` de gandhi. Feu:

```
ls -i text1
```

Després:

```
mv text1 un_directori_de_gandhi/text2
ls -i un_directori_de_gandhi/text2
```

I finalment fer l'operació anterior però desant una còpia del nostre fitxer al pen:

```
mv  un_directori_de_gandhi/text2  un_directori_del_pen_usb/text2
ls -i un_directori_del_pen_usb/text2
```

Compareu els inodes que ens han sortit en els 3 casos i expliqueu aquest comportament?

Per acabar copieu un fitxer prou gran de _public_ a `/tmp`,com per exemple `/home/groups/inf/public/install/IDE/android/android-studio/android-studio-ide-141.2135290-linux.zip` i poseu davant de l'ordre `cp` l'ordre `time`.

Després moveu el mateix fitxer zip de `/tmp` a un subdirectori qualsevol de `/tmp`.
Triga el mateix temps? Perquè?

**Solució:**

*Quan treballem al mateix sistema de fitxers, moure o canviar el nom d'un fitxer no canvia el seu inode. En canvi, si el movem a un altre sistema de fitxers sí.*

---

_Utilitzeu l'ordre `find` per aconseguir el que es demana als següents exercicis:_

##### Exercici 2.

Llisteu tots els fitxers del directori actual que han estat modificats a l'últim dia.

**Solució:**

```
find . -mtime -1  # no cal el caràcter .
```

##### Exercici 3.

Llisteu tots els fitxers del sistema que són més grans d'1 MB.

**Solució:**

```
find / -size +1024k  # o també +1M
```

##### Exercici 4.

Elimineu tots els fitxers amb extensió "class" de l'estructura de directori que comença al vostre directori personal. Aquest exercici pot ser molt perillós, es recomana fer una còpia del directori a on teniu els fitxers de Java al directori `/tmp` i jugar a `/tmp`.

**Solució:**

```
find /tmp -name "*.class" -exec rm '{}' \;  # hem canviat $HOME per /tmp
```

##### Exercici 5.

Llisteu els inodes de tots els fitxers java del directori actual.

**Solució:**

```
find . -name "*.java" -ls
```

o també:

```
find . -name "*?.java" -exec ls -i '{}' \;
```

Obs: si posem "\*.java" l'ordre `find` troba també un possible directori ".java" (Una altra opció seria posar `-type f`, o fins i tot els dos alhora)

##### Exercici 6.

Llisteu tots els fitxers del sistema de fitxers local (per tant sense incloure els de gandhi ja que pertanyen a un altre sistema de fitxers) que hagin estat modificats a l'últim mes.

**Solució:**

Necessitem trobar l'opció que fa que no descendim a directoris que pertanyin a d'altres sistemes de fitxers (recordem que per exemple a l'escola `/home/groups` es troba a un altre pc, _gandhi_, i per tant a un altre sistema de fitxers)

```
find / -mtime -30 -mount
```

També `-xdev`, però es recomana l'ús de l'altra opció per compatibilitat amb altres versions de `find`

##### Exercici 7.

Trobeu tots els fitxers del vostre directori personal que tinguin extensió *java* o *sql*.

**Solució:**

```
find  ~ -name "*.java" -o -iname "*.sql"
```

Recordem que l'opció anterior pot trobar un subdirectori `.java` o `.sql`

(existeix també l’opció no POSIX `-or`)

##### Exercici 8.

Trobeu els fitxers ocults (regulars, o sigui no directoris) que es troben al nostre directori `$HOME`

**Solució:**

```
find ~  -type f -name ".*"
```

(no cal fer `".?*"` ja que el directori `"."` ja s'elimina amb `-type f`)

##### Exercici 9.

Trobeu els subdirectoris que pengen directament del nostre $HOME (i.e sense incloure els subdirectoris dels subdirectoris)

**Solució:**

```
find $HOME -maxdepth 1  -type d
```

##### Exercici 10.

Executeu les següents instruccions des del vostre HOME per exemple:

```
[jamoros@heaven ~]$ ls -la
total 520592
drwx--x--x. 24 jamoros inf       4096 Nov 19 11:47 .
drwxr-xr-x. 17 root    root      4096 Oct 24 15:38 ..
-rw-------.  1 jamoros inf      17061 Nov 19 11:32 .bash_history
-rw-------.  1 jamoros inf         18 Sep  4 13:29 .bash_logout
...
```

```
[jamoros@heaven ~]$ find -maxdepth 1 -type d | wc -l
23
```

Veieu alguna relació entre el *24* de la línia del directori `.` a la primera instrucció i el *23* de la 2a instrucció?

**Solució**
El 24 indica quants enllaços durs (*hards*) té aquest fitxer (directori en aquest cas)
D'altra banda cada subdirectori (d'aquest directori conté un fitxer `..` que apunta al *pare* i per tant és un enllaç dur, si li sumem el propi fitxer original (directori en aquest cas) això fan els 24.
 
##### Exercici 11.

Comenteu quina és la diferència entre les següents ordres:
```
find -name "*.sh" -print -exec cat '{}' \;
find -name "*.sh" -print | xargs cat
```
**Solució:**

A la primera ordre es troba una ocurrència (un fitxer _bash script_ en aquest cas) i s'aplica el `cat` a aquest fitxer. Després es troba la següent ocurrència i s'aplica el `cat` ...

A la segona ordre, primer trobem totes les ocurrències i després apliquem el `cat` a cada una d'aquestes.

---

##### Exercici 12.

Creeu un directori anomenat 'pare' que contingui tres arxius anomenats fill1.txt, fill2.txt i fill3.txt amb el text “Hola, soc el fill NUM”, on NUM es el numero de fill de cada fitxer. Comprimiu cadascú d'aquests fitxers amb gzip i poseu-los en un directori anomenat “paregz”.

1. Coneixeu alguna família de comandes que permeti visualitzar els continguts dels fitxers comprimits sense tenir que descomprimir-los?

	**Solució:**

	```
	zcat nom_de_fitxer_comprimit
	```

	De fet descomprimeix cap a la sortida estàndard però manté el fitxer comprimit. Vàlid per a compressió amb `gzip`

	```
	gunzip -c nom_de_fitxer_comprimit # equivalent a l'ordre anterior
	```

	i també `zmore` i `zless` però utilitzant les diferents característiques dels diferents editors de fluxos

	```
	bzcat  nom_de_fitxer_comprimit
	bunzip2 -c nom_de_fitxer_comprimit
	```

	i també `bzmore` i `bzless`

	Per a aquestes dues últimes comandes serveixen els mateixos comentaris que abans però per a fitxers comprimits amb `bzip2`.

	Encara que no són famílies compatibles (és a dir, no es pot comprimir amb `gzip` i descomprimir el mateix fitxer amb `bunzip2`) `gunzip` permet descomprimir fitxers que s'hagin comprimit amb *certs* compressors diferents a `gzip`.

	L'ordre `tar` permet veure el contingut d'un directori comprimit amb aquesta ordre. El `file-roller` (gestor d'arxius gràfic) permet també visualitzar tots el contingut. Però, de fet, `file-roller` utilitza principalment les ordres del bash de línia d'ordres com tar, gzip...

2. Comprimiu el directori *pare* i tot el seu contingut mitjançant `gzip` amb el grau màxim de compressió. Feu el mateix utilitzant `bzip2`. Compareu els resultats i comenteu-los. Creieu que les conclusions que n'heu extret d'aquesta prova són extensibles a la compressió d'altres fitxers? per què?

	**Solució:**

	Si hem de comprimir el directori utilitzarem l'ordre `tar` que transforma una estructura de directoris en un fitxer. Ens col·loquem al directori pare de *pare* i executem:

	```
	tar  vcf  pare.tar pare/
	gzip --best pare.tar    # per defecte és -6
	bzip2 --best pare.tar   # aquí --best és l'opció per defecte i no cal posar-la
	ls -l pare.tar.gz pare.tar.bz2  # mostra 324 i 288 bytes respectivament
	```

	Aquest és un bon exemple de que les coses de vegades no surten com sembla que haurien de sortir. El fet que, en aquest cas, `gzip` comprimeixi més que `bzip2` no és significatiu donat que els fitxers que ha de comprimir són molt petits. De fet l'algoritme que utilitza `bzip2` es més efectiu que el que utilitza `gzip`. Però tots dos comprimeixen encara que el resultat sigui més gran que l'original. Per exemple `bzip2` sol donar fitxers comprimits més grans quan l'original no supera els 100 bytes.

	Algunes comparacions que s'han fet demostren que el que es guanya amb compressió amb `bzip2` (comparat amb `gzip`) es perd en temps d'execució. Per tant, si només ens interessa comprimir un fitxer, el qual no tornarem a utilitzar en molt temps, i que ocupi quan menys espai millor utilitzarem `bzip2` en comptes de `gzip`. En cas que siguin fitxers que s'estan comprimint i descomprimint constantment potser ens serà més eficient `gzip`.

3. Descomprimiu els dos últims fitxers que heu creat (*.gz* i *.bz2*) en directoris diferents, i comproveu de forma automàtica que els continguts siguin idèntics.

	**Solució:**
	```
	diff -r  parebzip2/ paregzip/
	```
	Sense l'opció *-r* només miraria les entrades del directori al primer nivell.

	No news, good news. Encara que amb `echo $?` (error level) podríem comprovar que ha anat bé.

##### Exercici 13.

Contesteu les següents qüestions sobre la comanda `file` i els magic numbers:

* On es configuren i defineixen tots els magic numbers usats per la comanda `file`?

	**Solució:**

	[Definició magic number](http://www.linfo.org/magic_number.html)

	`/usr/share/misc/magic`, a Fedora també a `/usr/share/magic` i  `/usr/share/file/magic`.

	Pregunta: són tots els fitxers iguals?

	(resposta: només un és fitxer regular, els altres són enllaços)

* Quina funció fa la comanda `hexdump`?

	**Solució:**

	Filtre que formata fitxers o la entrada estàndard si no es passa cap fitxer. Ens mostra els bytes del fitxer transformant-los a caràcters si volem. Permet jugar amb les diferents bases, octal decimal. Una ordre semblant és `od` (octal dump)

* Creeu un arxiu *tar* (podeu crear un conjunt d'arxius qualsevol o be copiar alguns arxius existents) i a continuació comprimiu-lo amb gzip generant per exemple l'arxiu arxiu.tar.gz. Feu la mateixa operació però comprimint amb bzip2 obtenint per exemple el fitxer arxiu2.tar.bz2. (es poden fer servir els arxius comprimits de la pregunta anterior)

* Mitjançant la comanda hexdump comproveu que cada tipus de fitxer (gzip i bzip2) es corresponen amb l'especificat a la definició del magic number corresponent al seu format (feu man hexdump per veure els diferents tipus de sortida de la comanda). Comenteu el que heu obtingut/vist.

	**Solució:**

	Per a tot els arxius, la informació que ens permet determinar el seu tipus es troba emmagatzemada als primers bytes de l'arxiu. Aquesta informació es comparada amb la del fitxer dels magic numbers.

	Amb la comanda hexdump (amb el modificador -c en hexadecimal o -b en octal) podem obtenir aquesta informació.

	Si mirem el fitxer `/usr/share/file/magic` i busquem *gzip* trobem que els 2 bytes que hauríem de trobar al principi de fitxer ens els dona aquesta línia:

	```
	0       string          \037\213        gzip compressed data
	```
	(que en hexadecimal seria 1F 8B)

	Aquest seria el *magic number* que serviria per que l'ordre `file` pogués fer servir un dels seus tests i mostrar-nos quin és el tipus de fitxer.

	Podem utilitzar l'opció -b (o -c):
	```
	hexdump -b pare.tar.gz
	```

	que ens mostra:
	```
	0000000 037 213 010 010 350 202 341 107 002 003 160 141 162 145 056 164
	0000010 141 162 000 355 323 275 152 302 140 024 306 361 103 272 070 027
	......
	```

	Si ens fixem en els dos primers caràcters coincideix.

	Anàlogament per al comprimit amb bzip2 segons el fitxer `/usr/share/file/magic` ara hauríem de cercar al principi de fitxer la cadena *Bzh*. La línia és:

	```
	0       string          BZh             bzip2 compressed data
	```
	Ara podem tant utilitzar l'opció -C, com -c:

	```
	hexdump -C pare.tar.bz2
	00000000  42 5a 68 39 31 41 59 26  53 59 13 4f 5c b1 00 01  |BZh91AY&SY.O\...|
	00000010  4d 7f e7 da c0 14 60 d8  61 ff a0 00 e0 a5 09 63  |M.....`.a......c|
	......
	```
	o també:
	```
	hexdump -c pare.tar.bz2
	0000000   B   Z   h   9   1   A   Y   &   S   Y 023   O   \ 261  \0 001
	0000010   M 177 347 332 300 024   ` 330   a 377 240  \0 340 245  \t   c
	```

	En comptes de hexdump podem fer servir l'ordre ja coneguda `od`

****************


Diferències entre mtime, ctime, atime (paràmetres de l'ordre find):

* http://www.geekride.com/inode-structure-ctime-mtime-atime/

* http://www.linuxtotal.com.mx/index.php?cont=info__tips_022


[Curiositats amb comprimits recursius](https://research.swtch.com/zip)


Afegim preguntes extres:

Com trobar el número total de fitxers que hi ha a tot el sistema?

```
[root@pc82 ~]# find / -mount   | wc -l 
252122 
```

Com trobar el número de fitxers regulars ?

```
[root@pc82 ~]# find / -mount -type f  | wc -l 
208584
```
Com trobar el número de fitxers directoris ?
```
[root@pc82 ~]# find / -mount -type d  | wc -l 
30231
```
Com trobar el número de fitxers enllaços ?
```
[root@pc82 ~]# find / -mount -type l  | wc -l 
13255 
```
La suma no quadra, 252122−208584−30231−13255 = 52
O sigui que em falten 52 fitxers, mirem al man, de quin tipus podrien ser i quin és el tipus que em falta.

Després de provar amb unes quantes opcions descobreixo que són els fitxers sockets:
```
[root@pc82 ~]# find / -mount -type s  | wc -l 
52 
```

Per cert, trobem quants fitxers de tipus xml tenim al sistema:

```
[root@profe2O1D ~]# find / -mount   -name "*.xml" | wc -l 
9294 
```

Acabem de baixar-nos un fitxer, no me'n recordo de com es deia i no sé on el tinc, només sé que fa menys de 10 minuts que el tinc, amb quina instrucció el puc trobar fent servir l'ordre `find`?
