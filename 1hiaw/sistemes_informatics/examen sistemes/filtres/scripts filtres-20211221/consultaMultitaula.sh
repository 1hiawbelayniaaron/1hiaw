#!/bin/bash
# Filename:	consultaMultitaula.sh
# Author:	Julio Amoros iam0000000
# Date:		01/11/2013
# Version:	0.1
# Description:	Script que rep de manera interactiva el numero d'un treballador-repventas,
# 		així com el número de columna de la "taula" oficines i, a continuació,
# 		mostra la informació corresponent a aquest número de columna de la taula d'oficines
# 		del venedor entrat.
# 		Es necessita tenir els fitxers repventas.dat i oficinas.dat
#		al mateix directori on s'executa l'script
# License:	"This is free software, licensed under the GNU General Public License v3.
#		See http://www.gnu.org/licenses/gpl.html for more information."

# Emmagatzemmem els fitxers originals 
taula_repventas=repventas.dat 
taula_oficinas=oficinas.dat

# Establim el delimitador
delimitador=$'\t'

# Creem els fitxers ordenats, repventas i oficinas pel 1er camp
cat $taula_repventas | sort -t "$delimitador" -k1 > ${taula_repventas}ord1
cat $taula_oficinas | sort -t "$delimitador" -k1  > ${taula_oficinas}ord1

echo "escriu el número del treballador i nosaltres et mostrarem dades de la seva oficina"
read numVenedor
echo "escriu el número de l'opcio que vols"
echo "1: número de l'oficina"
echo "2: ciutat de l'oficina"
echo "3: regió de l'oficina"
echo "4: número del director de l'oficina"
echo "5: objectiu de l'oficina"
echo "6: vendes de l'oficina"
read campOficina

# Cerquem el numero d'oficina del venedor
numOficina=$(echo "$numVenedor" |  join   ${taula_repventas}ord1 -  -t"$delimitador" -1 1 -2 1 | cut -f4 -d"$delimitador")

# Mostrem el camp escollit
echo $numOficina | join ${taula_oficinas}ord1 -  -t"$delimitador"  -1 1 -2 1  | cut -f$campOficina

# Eliminem els fitxers creats
rm -rf  ${taula_repventas}ord1  ${taula_oficinas}ord1

# No hem fet control d'errors ni en el número de venedor, ni en el número de camp de l'oficina,
# ni si es tracta d'un venedor que no té oficina (valor null)
