--1\. Mostrar la suma de las cuotas y la suma de las ventas totales de
--todos los representantes.
SELECT COALESCE(SUM(cuota)) cuota_total, COALESCE(SUM(ventas)) total_ventas
FROM repventa ;
--2\. ¿Cuál es el importe total de los pedidos tomados por Bill Adams?
SELECT SUM(importe)
FROM repventa r LEFT JOIN pedido p
ON r.repcod = p.repcod
WHERE LOWER(r.nombre) = 'bill adams';
--3\. Calcula el precio medio de los productos del fabricante ACI.
SELECT AVG(precio) "Preu mitjà"
FROM producto 
WHERE LOWER(fabcod) = 'aci';

--4\. ¿Cuál es el importe medio de los pedido solicitados por el cliente
--2103
 SELECT AVG(importe)
FROM pedido 
WHERE cliecod = 2103;

--5\. Mostrar la cuota máxima y la cuota mínima de las cuotas de los
--representantes.
SELECT MAX(cuota) "cuota maxima",  MIN(cuota) "cuota minima"
FROM repventa;
--6\. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?
SELECT MIN(fecha)
FROM pedido ;

--7\. ¿Cuál es el mejor rendimiento de ventas de todos los representantes?
--(considerarlo como el porcentaje de ventas sobre la cuota).

--8\. ¿Cuántos clientes tiene la empresa?
SELECT COUNT(*) FROM cliente;

--9\. ¿Cuántos representantes han obtenido un importe de ventas superior a
--su propia cuota?

SELECT COUNT(*)
FROM repventa 
WHERE ventas > cuota;

--10\. ¿Cuántos pedidos se han tomado de más de 150 euros?

SELECT COUNT(*)
FROM pedido
WHERE importe > 150;

--11\. Halla el número total de pedidos, el importe medio, el importe total
--de los mismos.
SELECT COUNT(*), ROUND(AVG(importe),2) , SUM(importe)
FROM pedido ;

--12\. ¿Cuántos puestos de trabajo diferentes hay en la empresa?
SELECT COUNT(DISTINCT puesto)
FROM repventa ;

--13\. ¿Cuántas oficinas de ventas tienen representantes que superan sus
--propias cuotas?
SELECT COUNT(DISTINCT ofinum)
FROM 	repventa
WHERE ventas > cuota;

--14\. ¿Cuál es el importe medio de los pedidos tomados por cada
--representante?


--15\. ¿Cuál es el rango de las cuotas de los representantes asignados a
--cada oficina (mínimo y máximo)?

--16\. ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad
--y número de representantes.

SELECT COALESCE(ofi.ciudad,'NO ASSIGNATS') ciutat, COUNT(r.repcod) "numero de representant"
FROM repventa r LEFT JOIN oficina ofi
ON r.ofinum = ofi.ofinum
GROUP BY ofi.ofinum;

--17\. ¿Cuántos clientes ha contactado por primera vez cada representante?
--Mostrar el código de representante, nombre y número de clientes.

--18\. Calcula el total del importe de los pedidos solicitados por cada
--cliente a cada representante.

--19\. Lista el importe total de los pedidos tomados por cada
--representante.

--20\. Para cada oficina con dos o más representantes, calcular el total de
--las cuotas y el total de las ventas de todos sus representantes.

--21\. Muestra el número de pedidos que superan el 75% de las existencias.

--## D. Subconsultas

--0. Mostrar el nombre y el puesto de los que son jefe (ya está hecho
  --on self join, ahora con subconsultas)
SELECT nombre, puesto
FROM repventa
WHERE repcod IN (SELECT jefe
                 FROM repventa);


--1\. Obtener una lista de los representantes cuyas cuotas son iguales ó
--superiores al objetivo de la oficina de Atlanta.

2\. Obtener una lista de todos los clientes (nombre) que fueron
contactados por primera vez por Bill Adams.

3\. Obtener una lista de todos los productos del fabricante ACI cuyas
existencias superan a las existencias del producto 41004 del mismo
fabricante.

4\. Obtener una lista de los representantes que trabajan en las oficinas
que han logrado superar su objetivo de ventas.

5\. Obtener una lista de los representantes que no trabajan en las
oficinas dirigidas por Larry Fitch.

6\. Obtener una lista de todos los clientes que han solicitado pedidos
del fabricante ACI entre enero y junio de 2003.

7\. Obtener una lista de los productos de los que se ha tomado un pedido
de 150 euros ó mas.

8\. Obtener una lista de los clientes contactados por Sue Smith que no
han solicitado pedidos con importes superiores a 18 euros.

9\. Obtener una lista de las oficinas en donde haya algún representante
cuya cuota sea más del 55% del objetivo de la oficina. Para comprobar vuestro
ejercicio, haced una Consulta previa cuyo resultado valide el ejercicio.

10\. Obtener una lista de los representantes que han tomado algún pedido
cuyo importe sea más del 10% de de su cuota.

11\. Obtener una lista de las oficinas en las cuales el total de ventas
de sus representantes han alcanzado un importe de ventas que supera el
50% del objetivo de la oficina. Mostrar también el objetivo de cada
oficina (suponed que el campo ventas de oficina no existe).
