\echo '\n******************************************************************'
\echo '******* Apartat C: AGGREGATE FUNCTIONS (Funcions de grup) ********'
\echo '******************************************************************'

\echo '\nPer veure l''efecte, executeu-lo amb el client psql de manera offline'

\echo '\n\n1. Mostrar la suma de las cuotas y la suma de las ventas totales de todos los representantes.\n'

SELECT SUM(cuota) "total quota", SUM(ventas) "total ventas"
FROM repventa;

\echo '\n2. ¿Cual es el importe total de los pedidos tomados por Bill Adams?\n'

SELECT SUM(importe) "total pedidos"
FROM pedido p  JOIN repventa r ON p.repcod = r.repcod
WHERE LOWER(nombre) = 'bill adams';

\echo '\n3. Calcula el precio medio de los productos del fabricante ACI.\n'

SELECT ROUND(AVG(precio), 2) "media productos"
FROM producto
WHERE LOWER(fabcod) = 'aci';

\echo '\n4. ¿Cual es el importe medio de los pedidos solicitados por el cliente 2103?\n'

SELECT ROUND(AVG(importe), 2) "media pedidos"
  FROM pedido
 WHERE cliecod = 2103;

\echo '\n5. Mostrar la cuota máxima y la cuota MINima de las cuotas de los representantes.\n'

SELECT MAX(cuota) "cuota máxima", MIN(cuota) "cuota mínima"
FROM repventa;

\echo '\n6. ¿Cual es la fecha del pedido más antiguo que se tiene registrado?\n'
SELECT MIN(fecha) FROM pedido;

\echo '\n7. ¿Cual es el mejor rendimiento de ventas de todos los representantes?'
\echo '(considerarlo como el porcentaje de ventas sobre la cuota).\n'

SELECT ROUND(MAX(ventas * 100 / cuota), 2) "mejor rendimiento"
FROM repventa;

\echo '\n8. ¿Cuantos clientes tiene la empresa?\n'

SELECT COUNT(*) "Clients que té l'empresa"
FROM cliente;

\echo '\n9. ¿Cuantos representantes han obtenido un importe de ventas'
\echo '\nsuperior a su propia cuota?\n'

SELECT COUNT(*) "Representants que han venut més del que tenien estipulat"
FROM repventa
WHERE ventas > cuota;

\echo '\n10. ¿Cuantos pedidos se han tomado de más de 150€?\n'

SELECT COUNT(*)
FROM pedido
WHERE importe > 150;

\echo '\n11. Halla el número total de pedidos, el importe medio'
\echo 'y el importe total de los mismos.\n'

SELECT 	COUNT(*) "total pedidos",
	   		ROUND(AVG(importe),2) "importe medio",
	   		SUM(importe) "importe total"
FROM pedido;

\echo '\n12. ¿Cuantos puestos de trabajo diferentes hay en la empresa?\n'

SELECT COUNT(DISTINCT puesto)
FROM repventa;

\echo '\n13. ¿Cuantas oficinas de ventas tienen representantes que superan'
\echo '\nsus propias cuotas?\n'

SELECT COUNT(DISTINCT ofinum)
FROM 	repventa
WHERE ventas > cuota;

\echo '\n14. ¿Cual es el importe medio de los pedidos tomados por cada representante?\n'

SELECT nombre "Representante", ROUND(AVG(importe),2) "media pedido"
FROM pedido p JOIN repventa r ON p.repcod = r.repcod
GROUP BY r.repcod, nombre;

\echo '\n15. ¿Cual es el rango de las cuotas de los reprentantes'
\echo 'asignados a cada oficina? (mínimo y máximo).\n'

SELECT ciudad "Oficina", MIN(cuota) "Minim", MAX(cuota) "Màxim"
FROM repventa r JOIN oficina o ON r.ofinum = o.ofinum
GROUP BY r.ofinum, ciudad;

\echo '\nMostro enlloc de codi d\'oficina, la ciutat'

	SELECT 	ciudad, MIN(cuota) "Mínim", MAX(cuota) "Màxim"
		FROM 	oficina o JOIN repventa r ON o.ofinum = r.ofinum
GROUP BY o.ofinum;

\echo '\n16. ¿Cuantos reprentantes hay asignados a cada oficina?'
\echo '\nMostrar ciudad y numero de representantes.\n'

SELECT ciudad, COUNT(*) "representants"
FROM oficina o JOIN repventa r ON r.ofinum = o.ofinum
GROUP BY ciudad;

\echo '\n17. ¿Cuantos clientes ha contactado por primera vez cada representante?'
\echo 'Mostrar el codigo de representante, nombre y numero de clientes.\n'

SELECT 	c.repcod "Codigo",
				r.nombre "Representante",
				COUNT(*) "Contactados"
FROM cliente c JOIN repventa r 	ON c.repcod = r.repcod
GROUP BY c.repcod, r.nombre;

\echo '\n18. Calcula el total del importe de los pedidos solicitados'
\echo 'por cada cliente a cada representante. Mostrar ordenado por cliente y por representante \n'

	SELECT 	c.nombre "Cliente",
					r.nombre "Representante",
	 				SUM(importe) "Total facturado"
		FROM 	pedido p JOIN repventa r ON p.repcod = r.repcod
					JOIN cliente c ON p.cliecod = c.cliecod
GROUP BY 	c.cliecod, c.nombre, r.repcod, r.nombre
ORDER BY 1, 2;

\echo '\n19. Lista el importe total de los pedidos tomados por cada representante.\n'

SELECT nombre "Representante", SUM(importe) "total pedido"
FROM pedido p JOIN repventa r ON p.repcod = r.repcod
GROUP BY r.repcod, nombre
HAVING COUNT(*)>=3 and SUM(importe)>30000;

\echo '\n20. Para cada oficina con dos o más representantes, calcular el total de'
\echo 'las cuotas y el total de las ventas de todos sus representantes.'
\echo 'Mostrar ciudad, cuota total y ventas totales\n'

SELECT 	o.ciudad "oficina",
		SUM(cuota) "cuota total",
		SUM(r.ventas) "ventas totales"
FROM oficina o JOIN repventa r ON r.ofinum = o.ofinum
GROUP BY o.ofinum
HAVING COUNT(*) >= 2;

\echo '\n 20.bis  IDEM que 20, però afegint les columnes objetivo y ventas de oficina\n'

SELECT  o.ciudad "oficina",
        objetivo,
        SUM(cuota) "Objectiu calculat",
        objetivo-SUM(cuota) "Diferència",
        o.Ventas,
        SUM(r.ventas) "Vendes calculades",
        o.ventas - SUM(r.ventas) "Diferència"
FROM oficina o JOIN repventa r ON r.ofinum = o.ofinum
GROUP BY o.ofinum
HAVING COUNT(*) >= 2;

\echo '\n21. Muestra el numero de pedidos que superan el 75%'
\echo 'de las existencias.\n'

SELECT COUNT(*) "comandes superiors al 75% existencies"
FROM pedido pe JOIN producto pr
	ON pe.fabcod = pr.fabcod and pe.prodcod = pr.prodcod
WHERE cant > exist*0.75;
