--0.Mostrar el nombre y el puesto de los que son jefe (ya está
--hecho con self JOIN, ahora con subconsultas).

SELECT  nombre, puesto
FROM  repventa
WHERE  repcod IN (SELECT  jefe
		 FROM  repventa);

--1.Obtener una lista de los representantes cuyas cuotas son
--iguales o superiores al objetivo de la oficina de Atlanta.

SELECT  nombre
FROM  repventa
WHERE  cuota >= (SELECT  objetivo
		FROM  oficina
		WHERE  LOWER(ciudad) = 'atlanta');

--2.Obtener una lista de todos los clientes (nombre) que
--fueron contactados por primera vez por Bill Adams.

SELECT  nombre
FROM  cliente
WHERE  repcod = (SELECT  repcod
		FROM  repventa
		WHERE  LOWER(nombre) = 'bill adams');

--3.Obtener una lista de todos los productos del fabricante
--ACI cuyas existencias superan a las existencias del producto
--41004 del mismo fabricante.

SELECT  *
FROM  producto
WHERE  LOWER(fabcod) = 'aci'
	  AND  exist > (SELECT  exist
	  	       			FROM  producto
		       				WHERE  prodcod = '41004' AND  LOWER(fabcod) = 'aci');

--4.Obtener una lista de los representantes que trabajan en
--las oficinas que han logrado superar su objetivo de ventas.

SELECT  nombre
FROM  repventa
WHERE  ofinum IN (SELECT  ofinum
	   	 						FROM  oficina
		 							WHERE  ventas > objetivo);

--5. Obtener una lista de los representantes que no trabajan
--en las oficinas por Larry Fitch.

SELECT  nombre
FROM  repventa
WHERE  ofinum NOT IN (SELECT  o.ofinum
		     							FROM  oficina o LEFT JOIN repventa r
		    									ON director = repcod
		     							WHERE  LOWER(nombre) = 'larry fitch');

--6. Obtener una lista de todos los clientes que han solicitado
--pedidos del fabricante ACI entre enero y junio de 1990

SELECT  nombre
FROM  cliente
WHERE  cliecod IN (SELECT  cliecod
		  							FROM  pedido
		  							WHERE  LOWER(fabcod) = 'aci' AND
											to_char(fecha,'yyyy-mm') between '1990-01' AND  '1990-06');

--7. Obtener una lista de los productos de los que se ha
--tomado un pedido de 150 euros o más.

SELECT  *
FROM  producto
WHERE  fabcod||prodcod IN (	SELECT  fabcod||prodcod
														FROM  pedido
														WHERE  importe > 150);

--8. Obtener una lista de los clientes contactados por
--Sue Smith que no han solicitado pedidos con importes
--superiores a 18 euros.

SELECT  *
FROM  cliente
WHERE  repcod IN (SELECT  repcod
		 FROM  repventa
		 WHERE  LOWER(nombre) = 'sue smith')   
		 AND  cliecod NOT IN (SELECT  cliecod
		      	  	      FROM  pedido
		      	  	      WHERE  importe >18);

-- Altra forma de fer-lo

SELECT  c.nombre
FROM  cliente c JOIN repventa r
		ON c.repcod = r.repcod
WHERE  LOWER(r.nombre) = 'sue smith'
      AND  cliecod NOT IN (SELECT  cliecod
                          FROM  pedido
                          WHERE  importe > 18);

--9. Obtener una lista de las oficinas en donde haya
--algún representante cuya cuota sea más del 55%
--del objetivo de la oficina.

SELECT  *
FROM  oficina
WHERE  ofinum IN (SELECT  ofinum
                 FROM  repventa
                 WHERE  cuota > objetivo * 0.55);


--10. Obtener una lista de los representantes que han
--tomado algún pedido cuyo importe sea más del 10%
--de su cuota.

SELECT  *
FROM  repventa
WHERE  repcod IN (SELECT  repcod
		 							FROM  pedido
		 							WHERE  importe > cuota * 0.1);

--11. Obtener una lista de las oficinas en las cuales
--el total de ventas de sus representantes han alcanzado
--un importe de ventas que supera el 50% del objetivo
-- de la oficina. Mostrar tambien el objetivo de cada oficina
--(suponed que el campo ventas de oficina no existe).

SELECT  ciudad, objetivo
FROM  oficina
WHERE  objetivo * 0.5 < (SELECT  SUM(ventas)
		        						FROM  repventa
		        						WHERE  ofinum = oficina.ofinum
		        						GROUP BY ofinum);

-- versió amb JOINS
SELECT  ciudad, objetivo
FROM  oficina JOIN repventa ON oficina.ofinum = repventa.ofinum
GROUP BY oficina.ofinum, ciudad, objetivo
HAVING objetivo * 0.5 < SUM(repventa.ventas);
