--iaw49656032
--Aaron Belayni

\c template1
drop database if exists clubnautic;
create database clubnautic;
\c clubnautic

create sequence numSoci_seq
 START WITH 11100000
 increment by 1;

CREATE TABLE patro(
    DNI varchar(9) constraint DNIpatro_pk primary key,
    nom varchar(60),
    adreça varchar(60),
    telefon varchar(9)
          );
insert into patro values ('12345678k','BERENGUER','C. Bisbe morgades','678900345');
CREATE TABLE Soci(
    numSoci int constraint numSoci_pk primary key,
    DNI varchar(9),
    nom varchar(60),
    adreça varchar(60),
    telefon varchar(9),
    dataAlta date DEFAULT current_date,
    ciutat varchar(40)
        );
INSERT INTO Soci
VALUES (nextval('numSoci_seq'),'12345678a','Jaume','C. Soledat num. 4','628105687',to_date('12-12-2020','dd/mm/yyyy'),'Vic'),
      (nextval('numSoci_seq'),'87654321h','Pere','C. andres num. 6','675234098',to_date('06-05-2020','dd/mm/yyyy'),'Barcelona'),
      (nextval('numSoci_seq'),'45676098j','Mohamed','C. pirrei Aviles num. 16','000111000',to_date('23-10-2020','dd/mm/yyyy'),'Torello');


CREATE TABLE Vaixell(
    matricula varchar(8) constraint matricula_pk primary key, -- 6907 bcd
    numSoci int,
    nomVaixell varchar(50),
    numAmarre int,
    quota decimal(6,2),
CONSTRAINT numSoci_vaixell_fk FOREIGN KEY (numSoci) 
REFERENCES Soci(numSoci) ON UPDATE CASCADE,
CONSTRAINT nomVaixell_UC UNIQUE (nomVaixell)
      );
      
insert into vaixell values ('6907bcd',11100000,'perla negra IV',2,2000.00),
			   ('1234acb',11100002,'Vergantin VI',503,2753.00),
			   ('6784hku',11100001,'Vergantin XI',503,2753.00);
			   
CREATE TABLE Sortida(
        dataSortida date, --hores
        vaixell varchar(8),
        patro varchar(9),
        dataRetorn date, --hores
        destinacio varchar(100),
CONSTRAINT dataSortidaVaixellcod_pk PRIMARY KEY (dataSortida,vaixell),
CONSTRAINT dataSortida_dataRetorn_ck CHECK(dataSortida<dataRetorn),
CONSTRAINT vaixell_Sortida_fk FOREIGN KEY(vaixell)
REFERENCES Vaixell(matricula),
CONSTRAINT patro_Sortida_fk FOREIGN KEY(patro)
REFERENCES patro(DNI) ON DELETE SET NULL ON UPDATE CASCADE
      );
      
-- insert into Sortida values (to_date('12:00','24hh mm'),'6907bcd','12345678k',to_date('14:25','24hh'),'Malaga');
