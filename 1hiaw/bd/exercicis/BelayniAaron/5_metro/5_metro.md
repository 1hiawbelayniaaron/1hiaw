# 5. Metro

Ens demanen  dissenyar  l'esquema E/R d'una bases de dades per gestionar les línies de metro de la ciutat de Barcelona. Cada línia està composada d'un conjunt d'estacions en un ordre determinat (tenint estació d'inici i de final). Cada línia té un identificador composat per una L i un número (L1, L2, L3, ..). Es vol emmagatzemar el color, hora de sortida del primer tren de la línia i del darrer. De cada línia volem saber l'estació d'inici i l'estació final de línia.

Les estacions tenen un codi numèric que les distingeix de les altres. També tenen un nom diferent per cadascuna. S'han d'emmagatzemar els horaris d'apertura i tancament de cada estació.

Una estació pot pertanyer a més d'una línia (p.e. Passeig de Gràcia pertany a la L4 i L2).

Cada estació té un o més accesos des de l'exterior i pot tindre ascensor, escales mecàniques o escales normals. Un accés se numera en funció de la línia, de manera que entre les diferents línies un número d'accés es por repetir. Els accesos estan numerats de manera correlativa (1, 2, 3, ..) per a cada estació, de manera que entre estacions la numeració coincidirà. Per cada accés s'emmagatzema el carrer i número de portal més proper on està situat.

Cada línia té assignats un conjunt de trens. No pot succeir que un tren estigui assignat a més d'una línia, però si pot passar que un tren no estigui associada a cap perquè està en reparació. Cada tren té un codi (que és únic), data de compra, model i número  de vagons.

En algunes estacions hi ha cotxeres per aparcar els trens quan no estan de servei. CAda tren té assignada una cotxera. Cada cotxera es distingeix pel mateix codi que l'estació. També es vol saber el  úmero de màquines i el de vagons que es poden allotjar.

Mitjançant un sistema GPS es coneix la posició de cada tren en servei.  Es necessita emmagatzemar l'estació a la que està arribant cada tren i els minuts estimats de l'arribada. Quan el tren està a l'anden els minuts valdran 0. En la mateixa estació poden haver-hi varis trens, un per cada sentit de cada línia en l'estació.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas metro](https://drive.google.com/file/d/1SiFtn3hKZ9yBJfOvxMK1d5CKO3iv51Mx/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas metro](./5_metro.drawio.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  Estacio(<ins>CodiEstacio</ins>, HorariAperturaEstacio, HorariTancamentEstacio)  
  Acces(<ins>CodiEstacio, CodiAcces</ins>, CarrerAcces, NumPortalAcces, Ascensor, EscalaNormal, EscalaMecanica)  
  Linia(<ins>CodiLinia</ins>, ColorLinia, HorariSortidaPrimerTren, HorariSortidaUltimTren, CodiEstacio)  
  LiniaPerEstacio(<ins>CodiLinia, CodiEstacio</ins>)  
  Tren(<ins>CodiTren</ins>, DataCompraTren, ModelTren, NumVagonsTren, CodiLinia, CodiCotxera)  
  Cotxera(<ins>CodiCotxera</ins>, NumMaquinesCOtxera, NumVagonsCotxera, CodiEstacio)  
  InfoPanell(<ins>CodiTren, CodiEstacio</ins>, MinutsArribada, Sentit)  
  \...

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
Acces|CodiEstacio|Estacio
Linia|CodiEstacio|Estacio
LiniaPerEstacio|CodiEstacio|Estacio
LiniaPerEstacio|CodiLinia|Linia
Tren|CodiLinia|Linia
Tren|CodiCotxera|Cotxera
InfoPanell|CodiTren|Tren
InfoPanell|CodiEstacio|Estacio
Cotxera|CodiEstacio|Estacio

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./metro.sql)
