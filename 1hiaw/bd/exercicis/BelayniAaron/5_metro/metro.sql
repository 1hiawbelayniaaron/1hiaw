\c template1
drop database if exists metro;
create database metro;
\c metro

create sequence CodiEstacio_seq
 START WITH 1000
 increment by 100;
 
 create sequence CodiLinia_seq
 START WITH 10
 increment by 10;
 
 create sequence CodiAcces_seq
 START WITH 1
 increment by 1;

CREATE TABLE Estacio(
	CodiEstacio int constraint CodiEstacio_pk primary key, 
	HorariAperturaEstacio varchar(10), 
	HorariTancamentEstacio varchar(10)
	);  
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');
INSERT INTO estacio VALUES (nextval('CodiEstacio_seq'),'5:00 am.', '00:00 am.');

CREATE TABLE Acces(
	CodiEstacio int, 
	CodiAcces int constraint CodiAcces_pk primary key,
	CarrerAcces varchar(100), 
	NumPortalAcces smallint, 
	Ascensor bool, 
	EscalaNormal bool, 
	EscalaMecanica  bool,
constraint CodiEstacio_Acces_fk foreign key (CodiEstacio) references estacio(CodiEstacio)
	);
INSERT INTO Acces VALUES (1000,nextval('CodiAcces_seq'),'c/ soledat.',4,'t','t','t' );
INSERT INTO Acces VALUES (1100,nextval('CodiAcces_seq'),'c/ soledat.',6,'t','f','t' );
  
CREATE TABLE Linia(
	CodiLinia varchar(3) constraint CodiLinia_pk primary key, 
	ColorLinia varchar(20), 
	HorariSortidaPrimerTren varchar(10), 
	HorariSortidaUltimTren varchar(10), 
	CodiEstacio int,
constraint CodiEstacio_Linia_fk foreign key (CodiEstacio) references estacio(CodiEstacio)
	);  
  
INSERT INTO linia VALUES 
('L1','vermella','5.00 am.','23:59',1000),
('L2','blava','6.00 am.','23:59',1100),
('L3','verd','5.30 am.','23:59',1200),
('L4','groc','5.00 am.','23:59',1300),
('L5','blava','5.00 am.','23:59',1400);


CREATE TABLE LiniaPerEstacio(
	CodiLinia varchar(3), 
	CodiEstacio int,
constraint CodiEstacio_LiniaPerEstacio_fk foreign key (CodiEstacio) references estacio(CodiEstacio),
constraint CodiLinia_LiniaPerEstacio_fk foreign key (CodiLinia) references Linia(CodiLinia)
	  );  
  
  

  
CREATE TABLE Cotxera(
	CodiCotxera int constraint CodiCotxera_pk primary key, 
	NumMaquinesCOtxera smallint, 
	NumVagonsCotxera smallint, 
	CodiEstacio int,
constraint CodiEstacio_Cotxera_fk foreign key (CodiEstacio) references estacio(CodiEstacio)
	);  
	

CREATE TABLE Tren(
	CodiTren int constraint CodiTren_pk primary key, 
	DataCompraTren date, 
	ModelTren varchar(20), 
	NumVagonsTren smallint, 
	CodiLinia varchar(3), 
	CodiCotxera int,
constraint CodiLinia_Tren_fk foreign key (CodiLinia) references Linia(CodiLinia),
constraint CodiCotxera_Tren_fk foreign key (CodiCotxera) references Cotxera(CodiCotxera)
	);  
  
  
CREATE TABLE InfoPanell(
	CodiTren int, 
	CodiEstacio int,
	MinutsArribada smallint,
	Sentit bool,
constraint CodiEstacio_InfoPanell_fk foreign key (CodiEstacio) references estacio(CodiEstacio),
constraint CodiTren_InfoPanell_fk foreign key (CodiTren) references Tren(CodiTren)
	);  
