\c template1
drop database if exists publicitat;
create database publicitat;
\c publicitat

create sequence idEns_seq
 START WITH 1000
 increment by 10;
 
 create sequence idCanal_seq
 START WITH 1010000
 increment by 1;
 
 create sequence idSpot_seq
 START WITH 501
 increment by 101;
 
 create sequence idFranja_seq
 START WITH 1
 increment by 1;


CREATE TABLE Ens(
	idEns int constraint idEns_pk primary key, 
	Nom varchar(30), 
	Tipus varchar(6), --  public/privat
	constraint Tipus_ck check(Tipus='public' or Tipus='privat')			 
	);
  
CREATE TABLE Canal(
	idCanal int constraint idCanal_pk primary key, 
	Nom varchar(20), 
	Descripcio varchar(400), 
	idEns int,
constraint idEns_Canal_fk foreign key (idEns) references Ens(idEns)
	); 
 

 
CREATE TABLE Empresa(
	CIF varchar(10) constraint CIF_pk primary key, --(B – 76365789)
	Nom varchar(50), 
	Adreca varchar(150), 
	Telefon varchar(11), 
	Tipus varchar(20)
	);  

CREATE TABLE Spot(
	idSpot int constraint idSpot_pk primary key, 
	Idioma varchar(3), 
	Propietaria varchar(50),
	Productora varchar(50)
	); 
 
CREATE TABLE Equivalencia(
	idSpot int, 
	SpotEquivalent bool -- els quals podrien ser aquells que tenen les mateixes imatges però tenen diferent idioma.
	);   

CREATE TABLE Emissio(
	idCanal int, 
	Franja varchar(10) constraint Franja_pk primary key, --(com poden ser: matí, migdia, tarda, nit, ...)
	idSpot int, 
	Data date, 
	Preu numeric(7,2), 
	NumEmissions int,
constraint idCanal_Emissio_fk foreign key (idCanal) references Canal(idCanal),
constraint idSpot_Emissio_fk foreign key (idSpot) references Spot(idSpot)
	);
	
CREATE TABLE Franja(
	idFranja int constraint idFranja_pk primary key, --seq (1,2,3,...)
	Franja varchar(7), 
	HoraInici date, 
	HoraFi date,
constraint Franja_Franja_fk foreign key (Franja) references Emissio(Franja)
	); 
  


CREATE TABLE TipusProducteXSpot(
	idSpot int, 
	TipusProducte varchar(7) constraint tipusProducte_pk primary key,
constraint idSpot_TipusProducteXSpot_fk foreign key (idSpot) references Spot(idSpot)
	);  
	

CREATE TABLE TipusProducte(
	IdTipusProducte int constraint IdTipusProducte_pk primary key, -- fer seq
	TipusProducte varchar(7),
constraint TipusProducte_TipusProducte_fk foreign key (TipusProducte) references TipusProducteXSpot(TipusProducte)
	);  


CREATE TABLE TipusProducteProhibit(
	Franja varchar(10), 
	TipusProducte varchar(7), 
	GrauPenalitzacio smallint,
	
constraint GrauPenalitzacio_ck check(GrauPenalitzacio<=3),
constraint TipusProducte_TipusDeProducteProhibit_fk foreign key (TipusProducte) references TipusProducteXSpot(TipusProducte),
constraint Franja_TipusProducteProhibit_fk foreign key (Franja) references Emissio(Franja)
	);
