/* 
	Nom i cognom: Aaron Belayni  
	codi: iaw49656032k
*/

-- 1. Inserir en la taula DEPT la informació corresponent a un nou departament de consultoria, de codi 50 i que estigui ubicat a SANTANDER. Per a l’ocasió creeu la seqüència deptno_seq (la qual començarà en 50 i s’incrementarà de 10 en 10) i la utilitzeu per la sentència SQL. 



INSERT INTO dept VALUES (nextVal('deptno_seq'), 'CONSULTORIA', 'SANTANDER');




-- 2. Donar d’alta a un nou empleat de nom Andreu, que exercirà el lloc de analyst en el departament 30 i el seu cap serà l’empleat 8200. De moment es desconeixen els altres valors dels camps.



 INSERT INTO emp VALUES (nextVal('empno_seq'), 'ANDREU', 'ANALYST', 7788, NULL, NULL, NULL, 30);

-- es el mateix que
 INSERT INTO emp  (empno, ename, job, mgr, deptno)
 VALUES (nextVal('empno_seq'), 'ANDREU', 'ANALYST', 7788, 30);



-- 3. Canviar la data de l’empleat SCOTT per la d’avui.



UPDATE emp
SET hiredate = CURRENT_DATE
WHERE ename = 'SCOTT';



-- 4. L’empleat MILLER, a causa de els seus èxits és ascendit al lloc de analyst, augmentant-seu salari en un 20%, se li canvia al departament 30 i el seu nou cap serà l’empleat 7566.


UPDATE emp
SET job = 'ANALYST', sal = sal + sal *0.20, deptno  = 30, mgr = 7566
WHERE ename = 'MILLER';


-- 5. Arran de la signatura d’el conveni anual de l’empresa, s’ha determinat incrementar el salari de tots els empleats en un 6%.

UPDATE emp
SET sal = sal + (sal * 0.6);




-- 6. L’empleat JAMES causa baixa a l’empresa.
DELETE FROM emp
WHERE ename = 'JAMES'; 




-- 7. Es contracta a SANZ, amb número 1657, per al departament 30 i amb sou 3000.
INSERT INTO emp VALUES (1657, 'SANZ', 'SALESMAN', 7698, CURRENT_DATE, 3000, NULL, 30);




-- 8. SANZ canvia al departament 40. 
UPDATE emp
SET deptno = 40
WHERE ename = 'SANZ';





-- 9. SANZ treballa de salesman, amb una comissió de 4000.
 UPDATE emp
SET comm  = 4000
WHERE ename = 'SANZ';



-- 10. Es decideix augmentar les comissions. Augmenten totes les comissions en un 20% de l’salari.
 UPDATE emp
 SET comm = comm + (sal * 0.2);


/*

-- 11. Es decideix augmentar un 35% el salari als empleats que guanyin menys que SANZ (*).
 UPDATE emp
SET comm = sal + (sal * 0.35)
WHERE sal < 3000;
UPDATE 7
 SELECT *
from emp;
*/

-- 12. S’acomiada a SANZ.
DELETE FROM emp
WHERE ename = 'SANZ';



-- 13. El departament 30 desapareix. S’han d’acomiadar a tots els treballadors que hi treballen. Escriviu les sentències SQL necessàries per portar-lo a terme.


-- em dona error: Key (deptno)=(30) is still referenced from table "emp".
--DELETE FROM dept
--WHERE deptno = 30;


/* Per mes endavant

-- 14. L’empresa està en crisi i ha d’acomiadar a tots els que treballen al departament ACCOUNTING. (*)
DELETE FROM dept
 WHERE job = ACCOUNTING;

SELECT *
from dept;
*/

