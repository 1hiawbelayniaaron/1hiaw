# 1. ETT

Desitgem informatitzar la informació que manipula una ETT sobre empreses que ofereixen llocs de treball i persones que busquen feina. Les empreses ofereixen llocs de treball, informant de la professió sol·licitada, el lloc de treball destinat i les condicions exigides per a aquest lloc. De les persones que busquen feina tenim el seu DNI, nom, estudis i professió desitjada. Ens interessa saber quines persones poden optar a un lloc de treball, és a dir, poden participar en el procés de selecció. La persona interessada en un lloc es podrà apuntar per fer una entrevista per a aquest lloc. Per a cada lloc de treball es poden inscriure totes les persones interessades. En alguns casos es formalitzaran contractes entre les empreses i les persones, i emmagatzemarem la data de signatura, durada i sou del contracte. 

De les empreses tenim les dades de CIF, nom i sector. A més, es distingiran pimes i multinacionals: de les primeres emmagatzemarem la ciutat en la qual s'ubica i de les segones el nombre de països en els quals té representació.
Nota: establiu les claus primàries que considereu oportunes.

# 2. Model conceptual
## 2.1. Enllaç públic a l'esquema
[cas ett](https://drive.google.com/file/d/1yBwg4BMadmegRS1UEaXJBeLJjMHd5rJl/view?usp=sharing)
## 2.2. Esquema conceptual (EC ó ER)
  ![cas ett](./4_ett.drawio.png)
# 3. Model lògic relacional
## 3.1. Esquema lògic
  Empresa(<ins>CIFEmpresa</ins>, NomEmpresa, SectorEmpresa, Tipus, CiutatPime, PaisMultinacional)  
  LlocDeTreball(<ins>IdLlocDeTreball</ins>, ProfessioSolicitada, CondicionsExigides, NomLlocDeTreball, CIFEmpresa)  
  Persona(<ins>DNIPersona</ins>, NomPersona, EstudisPersona, ProfessioDesitjada)  
  Entrevista(<ins>DNIPersona, IdLlocDeTreball</ins>)  
  Contracte(<ins>DNIPersona, CIFEmpresa</ins>, Sou, DataSignatura, DuradaContracte)
  \...

## 3.2. Diagrama referencial

Relació referencial|Clau aliena|Relació referida
-|:-:|-
LlocDeTreball|CIFEmpresa|Empresa
Entrevista|DNIPersona|Persona
Entrevista|IdLlocDeTreball|LlocDeTreball
Contracte|DNIPersona|Persona
Contracte|CIFEmpresa|Empresa

# 4. Model físic
## 4.1 Enllaç a l'esquema físic

[script ett.sql](./ett.sql)
