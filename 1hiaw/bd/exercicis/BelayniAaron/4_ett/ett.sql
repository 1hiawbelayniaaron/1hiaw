\c template1
drop database if exists ett;
create database ett;
\c ett

create sequence idLlocDeTreball_seq
START WITH 1000
increment by 10;


CREATE TABLE Empresa(
	CIFEmpresa varchar(10) constraint CIFEmpresa_pk primary key, 
	NomEmpresa varchar(60), 
	SectorEmpresa varchar(40), 
	Tipus bool, 
	CiutatPime varchar(70), 
	PaisMultinacional varchar(100)
	);
 INSERT INTO Empresa VALUES ('123456789A', 'Google S.A' , 'telecomunicacions', 't','toronto','Estats units');
INSERT INTO Empresa VALUES ('987654321B', 'facebook' , 'telecomunicacions', 't','Menlo park, california','Estats units');


CREATE TABLE LlocDeTreball(
	IdLlocDeTreball smallint constraint IdLlocDeTreball_pk primary key, 
	ProfessioSolicitada VARCHAR(70), 
	CondicionsExigides VARCHAR(100), 
	NomLlocDeTreball VARCHAR(50), 
	CIFEmpresa VARCHAR(10),
constraint CIFEmpresa_LlocDeTreball_fk foreign key(CIFEmpresa) references empresa(CIFEmpresa) on update cascade on delete SET NULL
	);
INSERT INTO LlocDeTreball VALUES (nextval('idLlocDeTreball_seq'), 'programador' , '4h, 5 dies a la setmana', 'Programador web','123456789A');
INSERT INTO LlocDeTreball VALUES (nextval('idLlocDeTreball_seq'), 'programador' , '4h, 6 dies a la setmana', 'disseny físic de bdd','987654321B');


CREATE TABLE Persona(
 	DNIPersona VARCHAR(9) constraint DNIPersona_pk primary key, 
 	NomPersona VARCHAR(50), 
 	EstudisPersona VARCHAR(150), 
 	ProfessioDesitjada VARCHAR(70)
 	);
INSERT INTO persona VALUES ('12345678k', 'Ramon Prat sunyer' , 'grau superior desenvolupament web', 'programador');
INSERT INTO persona VALUES ('87654321k', 'Mohamed Badda' , 'enginyeria informatica (UAB)', 'programador');

CREATE TABLE Entrevista(
	DNIPersona VARCHAR(9),
	IdLlocDeTreball smallint,
constraint IdLlocDeTreball_entrevista_fk foreign key(IdLlocDeTreball) references LlocDeTreball(IdLlocDeTreball),
constraint DNIPersona_entrevista_fk foreign key(DNIPersona) references persona(DNIPersona) on update cascade on delete SET NULL

	);
  
  
CREATE TABLE Contracte(
	DNIPersona VARCHAR(9),
	CIFEmpresa VARCHAR(10),
	Sou varchar(6), 
	DataSignatura date, 
	DuradaContracte varchar(10),
	constraint CIFEmpresa_contracte_fk foreign key(CIFEmpresa) references empresa(CIFEmpresa),
	constraint DNIPersona_contracte_fk foreign key(DNIPersona) references persona(DNIPersona)
	);
