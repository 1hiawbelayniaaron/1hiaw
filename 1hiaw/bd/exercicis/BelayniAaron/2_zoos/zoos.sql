\c template1
drop database if exists zoos;
create database zoos;
\c zoos

CREATE SEQUENCE idEspecie_seq
  START WITH 10
  INCREMENT BY 10;
  
CREATE SEQUENCE idZoo_seq
  START WITH 101
  INCREMENT BY 101;

CREATE SEQUENCE idAnimal_seq
  START WITH 1001
  INCREMENT BY 1;

CREATE TABLE ESPECIE(
	idEspecie VARCHAR(9) CONSTRAINT idEspecie_animal_pk PRIMARY KEY,
	nomVulgar VARCHAR(50),
	nomCientific VARCHAR(50),
	FamiliaEspecie VARCHAR(100),
	perillExtensio BOOL
);
INSERT INTO especie VALUES (nextval('idEspecie_seq'), 'tauro', 'Selachimorpha', 'peix','t');
INSERT INTO especie VALUES (nextval('idEspecie_seq'), 'anec capbusador', 'morell capblanc', 'ocell','f');
INSERT INTO especie VALUES (nextval('idEspecie_seq'), 'lleo', 'Pantethera leo', 'felí','t');
INSERT INTO especie VALUES (nextval('idEspecie_seq'), 'tigre', 'Phantera tigris', 'felí','t');

CREATE TABLE ANIMAL(
	IdZoo varchar(10),
	IdAnimal varchar(10) constraint idAnimal_pk PRIMARY KEY,
	sexe varchar(10) CONSTRAINT sexeAnimal CHECK ( sexe='MACHO' OR sexe='HEMBRA' OR sexe='HERMAFRODITA'),
	AnyNaixement date,
	PaisOrigen varchar(30), 
	ContinentOrigen varchar(30), 
	IdEspecie varchar(10)
	--constraint idEspecie_fk FOREIGN KEY(idEspecie) REFERENCES (ESPECIE(idEspecie))
);
INSERT INTO animal VALUES (nextval('idZoo_seq'), nextval('idAnimal_seq'), 'MACHO',TO_DATE('17-10-1980', 'DD-MM-YYYY'),'GHANA', 'AFRICA',nextval('idEspecie_seq'));
INSERT INTO animal VALUES (nextval('idZoo_seq'), nextval('idAnimal_seq'), 'HEMBRA',TO_DATE('16-10-1980', 'DD-MM-YYYY'),'GHANA', 'AFRICA',nextval('idEspecie_seq'));
INSERT INTO animal VALUES (nextval('idZoo_seq'), nextval('idAnimal_seq'), 'MACHO',TO_DATE('17-11-1980', 'DD-MM-YYYY'),'SENEGAL', 'AFRICA',nextval('idEspecie_seq'));
INSERT INTO animal VALUES (nextval('idZoo_seq'), nextval('idAnimal_seq'), 'MACHO',TO_DATE('23-09-1980', 'DD-MM-YYYY'),'ETIOPIA', 'AFRICA',nextval('idEspecie_seq'));



CREATE TABLE ZOO(
	idZoo varchar(10) constraint idZoo_pk PRIMARY KEY,
	nomZoo varchar(50),
	CiutatZoo varchar(50),
	PaisZoo varchar(30),
	midaEnMetresQuadrats int,
	PressupostAnual int4
);

	INSERT INTO ZOO values(currval('idZoo_seq'),' Zoo de Barcelona','barcelona','españa',200000,1500000);
	INSERT INTO ZOO values(nextval('idZoo_seq'),' Zoo de Madrid','madrid','españa',203000,1700000);
	INSERT INTO ZOO values(nextval('idZoo_seq'),' Zoo den  pitus','barcelona','españa',2000,30000);

	

