\c template1
drop database if exists cinefil;
create database cinefil;
\c cinefil

create sequence codTema_seq
 START WITH 11100000
 increment by 1;
 
create sequence idActor_seq
 START WITH 1010
 increment by 10;



CREATE TABLE tema(
	codTema int constraint codTema_pk primary key, 
	descripcioTema varchar
	
);

insert into tema values(nextval('codTema_seq'),'Comedia');
insert into tema values(nextval('codTema_seq'),'Accion');
insert into tema values(nextval('codTema_seq'),'Thriller');
insert into tema values(nextval('codTema_seq'),'Amor');
insert into tema values(nextval('codTema_seq'),'Comedia-Romantica');

CREATE TABLE actor(
	idActor int constraint idActor_pk primary key, 
	nomActor varchar(50)

);
insert into actor values(nextval('idActor_seq'),'Jack Nicholson');
insert into actor values(nextval('idActor_seq'),'Marlon Brando');
insert into actor values(nextval('idActor_seq'),'Charlton Heston');
insert into actor values(nextval('idActor_seq'),'Dustin Hoffman');
insert into actor values(nextval('idActor_seq'),'Paul Newman');

CREATE TABLE especialitzacio(
	idActor int,		--nose que fer amb la pk
	codTema int,
	habilitat varchar(30),
	actorPrincipal varchar(30) constraint actorPrincipal_pk primary key,
	constraint especialitzacio_idActor_fk foreign key (idActor) references Actor (idActor),
	constraint especialitzacio_codTema_fk foreign key (codTema) references tema (codTema) 
		ON DELETE SET NULL ON UPDATE CASCADE

);
insert into especialitzacio values(1010,11100001 ,'carecteritzacio','rambo');
insert into especialitzacio values(1020,11100003,'carisma personal','agallas');
insert into especialitzacio values(1050,11100002,'encatador','Macbeth');
insert into especialitzacio values(1040,11100002,'galan','Scream');

CREATE TABLE substitucio(
	idActor int,
	actorPrincipal varchar(30), --nose que fer amb la pk (grau es repetirà)
	grau int, --de 1 a 10.
	constraint substitucio_idActor_fk foreign key (idActor) references Actor (idActor),
	constraint substitucio_actorPrincipal_fk foreign key (actorPrincipal) references especialitzacio(actorPrincipal),
	constraint substitucio_grau_ck check(grau>0 and grau<11)
);

insert into substitucio values(1010,'rambo',6);
insert into substitucio values(1020,'agallas',7);
insert into substitucio values(1040,'Scream',2);
insert into substitucio values(1050,'Macbeth',10);

CREATE TABLE compatibilitat(
	idActor int,
	company varchar(50) constraint company_pk primary key,  
	afinitat varchar(40),
	constraint compatibilitat_idActor_fk foreign key (idActor) references Actor (idActor)
);

CREATE TABLE paper(
	codPaper int constraint codPaper_pk primary key,
	descripcioPaper varchar(150)

);

CREATE TABLE companyia(
	idCompanyia int constraint idCompanyia_pk primary key,
	companyia varchar(50)
	
);


CREATE TABLE pelicula(
	ISBN varchar(13),
	titol varchar(30),
	anyEstrena varchar(4),   --nose que fer amb la pk 
	pressupost decimal(10,2),
	director varchar(50),
	idCompanyia int,
	codTema int,
	constraint pelicula_idCompanyia_fk foreign key (idCompanyia) references companyia (idCompanyia),
	constraint pelicula_codTema_fk foreign key (codTema) references tema (codTema)
);

CREATE TABLE interpretacio(
	ISBN varchar(13) constraint ISBN_pk primary key,
	idActor int, 
	codPaper int,
	premi varchar(10),
	constraint interpretacio_idActor_fk foreign key (idActor) references actor (idActor),
	constraint interpretacio_codPaper_fk foreign key (codPaper) references paper (codPaper)
);

CREATE TABLE fase(
	iniciFase varchar(20) constraint iniciFase_pk primary key
	
);

CREATE TABLE ciutat(
	codCiutat int constraint codCiutat_pk primary key,
	ciutat varchar(20),
	pais varchar(20)
);

CREATE TABLE rodatge(
	ISBN varchar(13),
	codCiutat int,            --nose que fer amb la pk
	iniciFase varchar(20),
	fiFase varchar(20),
	constraint rodatge_ISBN_fk foreign key (ISBN) references interpretacio(ISBN),
	constraint rodatge_codCiutat_fk foreign key (codCiutat) references ciutat(codCiutat),
	constraint rodatge_iniciFase_fk foreign key (iniciFase) references fase(iniciFase)	
);


CREATE TABLE seu(
	idCompanyia int,          --nose que fer amb la pk
	codCiutat int,
	adreça varchar(50),
	constraint seu_idCompanyia_fk foreign key (idCompanyia) references companyia(idCompanyia),
	constraint seu_codCiutat_fk foreign key (codCiutat) references ciutat(codCiutat)	
);

CREATE TABLE data(
	dataInici date constraint dataInici_pk primary key
	
);

CREATE TABLE contracte(
	idActor int,
	dataInici date,   --nose que fer amb la pk
	idCompanyia int,
	oferta varchar(30),
	dataFi date,
	constraint contracte_idActor_fk foreign key (idActor) references actor(idActor),
	constraint contracte_dataInici_fk foreign key (dataInici) references data(dataInici),
	constraint contracte_idCompanyia_fk foreign key (idCompanyia) references companyia(idCompanyia)
);

