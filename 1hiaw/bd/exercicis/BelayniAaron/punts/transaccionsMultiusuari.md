Ismael, Sergi i Aaron

# Transacciones (Multiusuari)

7. Omple la següent taula, tenint en compte que cada sentència s'executa en una connexió determinada.  

| Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                 |
| -------- | ------------------------- | --------------------------------------------------------------------- |
| 0 Sergi  | -                         | -                                                                     |
| 1 Aaron  | D                         | Inici de transsacció                                                  |
| 2 Ismael | D                         | ens dona una sola fila perquè l'usuari 1 no ha acabat la transsacció. |

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```

8. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
   
   | Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                           |
   | -------- | ------------------------- | --------------------------------------------------------------- |
   | 0  Aaron | -                         | -                                                               |
   | 1 Ismael | D                         | las transacciones no afectan unas a otras porque son diferentes |
   | 2 Sergi  | D                         | las transacciones no afectan unas a otras porque son diferentes |

```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
SELECT * FROM punts; -- Connexió 2
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

SELECT * FROM punts; -- Connexió 1
COMMIT; -- Connexió 1

SELECT * FROM punts WHERE id = 91; -- Connexió 0
```

9. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
   
   | Connexió  | Blocat (B)/ Desblocat (D) | Motiu                                                                          |
   | --------- | ------------------------- | ------------------------------------------------------------------------------ |
   | 0  Ismael | -                         | -                                                                              |
   | 1 Sergi   | D                         | -                                                                              |
   | 2 Aaron   | B                         | se bloquea porque editamos la misma fila y la conexion 1 no ha hecho el commit |

```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0
```

10. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
    
    | Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                          |
    | -------- | ------------------------- | ------------------------------------------------------------------------------ |
    | 0  Sergi | -                         | -                                                                              |
    | 1 Aaron  | D                         | -                                                                              |
    | 2 Ismael | B                         | se bloquea porque editamos la misma fila y la conexion 1 no ha hecho el commit |

```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```

11. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
    Indica què veu cada usuari al select corresponent.
    
    | Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                          |
    | -------- | ------------------------- | ------------------------------------------------------------------------------ |
    | 0  Aaron | -                         | -                                                                              |
    | 1 Ismael | B                         | se bloquea porque editamos la misma fila y la conexion 2 no ha hecho el commit |
    | 2 Sergi  | D                         | como hay un deadlock el sistema gestor lo desbloquea                           |
    | 1 Ismael | D                         | porque la conexion 2 ha hecho un rollback                                      |

```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```

12. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
    
    | Connexió  | Blocat (B)/ Desblocat (D) | Motiu                                                             |
    | --------- | ------------------------- | ----------------------------------------------------------------- |
    | 0  Ismael | -                         | -                                                                 |
    | 1 Aaron   | D                         |                                                                   |
    | 2 Sergi   | B                         | la conexion 1 ha hecho update de la misma fila                    |
    | 2 Sergi   | D                         | porque la conexión 1 hace un rollback y ya no edita la misma fila |

```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```
