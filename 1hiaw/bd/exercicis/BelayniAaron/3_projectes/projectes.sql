\c template1
drop database if exists projectes;
create database projectes;
\c projectes

create sequence idDepartament_seq
START WITH 1000
increment by 10;

create sequence codProjecte_seq
START WITH 10
increment by 10;

CREATE TABLE PROJECTE
	(
	CodiProjecte smallint constraint codiProjecte_pk primary key,
	TitolProjecte varchar(60),
	DuradaEstimada varchar(50),
	DuradaReal varchar
	);
INSERT INTO PROJECTE VALUES (nextval('codProjecte_seq'), 'marqueting llençament aplicació' , '15 setmanes', '20 setmanes');
INSERT INTO PROJECTE VALUES (nextval('codProjecte_seq'), 'desenvolupament aplicacio' , '30 setmanes', '32 setmanes');

CREATE TABLE DEPARTAMENT
	(
	nomDepartament varchar(40),
        tasquesDepartament varchar(140),
	idDepartament smallint constraint idDepartament_pk primary key
	);
INSERT INTO departament VALUES ('marqueting' , 'disseny de la linea de productes', nextval('idDepartament_seq'));
INSERT INTO departament VALUES ('disseny de bdd' , 'desenvolupament de la base de dades de la aplicació', nextval('idDepartament_seq'));


/* em surt ERROR:  no existe relacion empleat


CREATE TABLE EMLEAT
	(
	DNIEmpleat varchar(9),
	NomEmpleat varchar(50),
	AdreçaEmpleat varchar(100),
	telefonEmpleat varchar(11),
	idDepartament smallint,
	codiProjecte smallint,
	DNIcap varchar(9)
	);
INSERT INTO departament VALUES ('12345678k' , 'Joaquim Voltes', 'carrer pirrei Aviles, num 4, 3r', '123123123',currval('idDepartament_seq'),nextval('codProjecte_seq'),'12345678b');
INSERT INTO departament VALUES ('87654321g' , 'Aaron Belayni', 'carrer pirrei Aviles, num 5, 4r', '628105687',nextval('idDepartament_seq'),nextval('codProjecte_seq'),'12345678b');
alter table empleat add constraint codProjecte_fk foreign key (codiProjecte) references projecte;
*/

