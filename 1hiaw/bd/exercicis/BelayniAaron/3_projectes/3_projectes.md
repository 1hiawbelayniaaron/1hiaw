# 1. Projectes

Es desitja dissenyar una base de dades que reculli la informació sobre la gestió de projectes que es porta a terme a l'empresa i la participació dels empleats.

De cada empleat necessitarem saber el seu DNI, nom, adreça, telèfon i projecte en el qual participa. Cada projecte s'identificarà per un codi que serà únic, tindrà un títol, una durada estimada, una durada real, un pressupost.

De cada projecte es vol saber el director del projecte, de manera que la mateixa persona pot dirigir diferents projectes. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.

Es desitja saber de cada empleat qui és el seu cap en cas que el tingui. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.

Cada empleat està assignat a un únic departament que s'encarregarà d'unes tasques determinades dins de l'empresa. Del departament de vol conèixer el nom. Modificar l'esquema anterior per recollir els nous requeriments d'usuari.

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[cas projectes](https://drive.google.com/file/d/1YxqriU-Zgl7q6BDUDm3d_HmQG7PgRsYd/view?usp=sharing)

## 2.2. Esquema conceptual (EC ó ER)

  ![cas projectes](./3_projectes.drawio.png)

# 3. Model lògic relacional

## 3.1. Esquema lògic

  Departament(<ins>IdDepartament</ins>, NomDepartament, TasquesDepartament)  
  Empleat(<ins>DNIEmpleat</ins>, NomEmpleat, AdrecaEmpleat, TelefonEmpleat, IdDepartament, CodiProjecte, DNICap)
  Projecte(<ins>CodiProjecte</ins>, TitolProjecte, DuradaEstimada, DuradaReal, Pressupost, DNIEmpleat)
  \...

## 3.2. Diagrama referencial

| Relació referencial | Clau aliena   | Relació referida |
| ------------------- |:-------------:| ---------------- |
| Empleat             | IdDepartament | Departament      |
| Empleat             | CodiProjecte  | Projecte         |
| Projecte            | DNIEmpleat    | Empleat          |
| Empleat             | DNICap        | Empleat          |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./projectes.sql)
