--() { :; }; exec psql training -f "$0"
/*************************
-- Funcions Training --
*************************/

\c training

-- Creació de seqüències si escau
create sequence if not exists cliecod_seq;

select setval('cliecod_seq', (select max(cliecod) from cliente), true);

create sequence if not exists pednum_seq;

select setval('pednum_seq', (select max(pednum) from pedido), true);

-- Definició de funcions

/*****************************************************************
    Task: Comprova si existeix un client amb un codi de client donat a la taula clients
    In:
	    -> (int) p_cliecod : Codi de client
    Out:
		-> (boolean) True si existeix un client amb el codi donat. En cas contrari, false.
******************************************************************/
drop function if exists existeixClient(int);
create or replace function existeixClient(p_cliecod int)
returns boolean
as $$
	declare
	 basura varchar(1);
begin
		-- utilitzarem into strict + exception
	select '1'
	into strict basura
	from cliente
	where cliecod = p_cliecod;
	return true;
exception
	when no_data_found then
		return false;
end;
$$ language plpgsql;

/*****************************************************************
    Task: Dóna d'alta un client a la taula clients de la base de dades training.
    In:
	    -> (varchar) p_nombre : Nom del client a afegir
		-> (int)     p_repcod : Codi del representant de vendes que va captar aquest client.
		-> (numeric) p_limcred : Límit de crèdit del client.
    Out:
		-> (varchar) Missatge de confirmació de alta de client.
******************************************************************/
drop function if exists altaClient(varchar, int, numeric);
create or replace function altaClient(p_nombre varchar, p_repcod int, p_limcred numeric)
returns varchar
as $$
begin
	insert into cliente(cliecod, nombre, repcod, limcred)
	values
	(nextval('cliecod_seq'), p_nombre, p_repcod, p_limcred);
	return 'Client <'||p_nombre||'> amb codi <' || currval('cliecod_seq') || '> afegit amb exit';
end
$$ language plpgsql;

/*****************************************************************
    Task: Comprova si a l'stock d'un producte determinat queda un cert nombre d'unitats
    In:
	    -> (int)     p_cant : Nombre d'unitats a comprovar.
		-> (varchar) p_fabcod : Codi del fabricant del producte.
		-> (varchar) p_prodcod : Codi del producte. Únic per a cada fabricant.
    Out:
		-> (boolean) True si al stock queden suficients unitats del producte. En cas contrari, false..
******************************************************************/
drop function if exists stockOk(int, varchar, varchar);
create or replace function stockOk(p_cant int, p_fabcod varchar, p_prodcod varchar)
returns boolean
as $$
declare
   basura varchar(1);
begin
			-- Utilitzem into + found
		select 'a'
		into basura
		from producto
		where (fabcod, prodcod) = (p_fabcod, p_prodcod) and exist >= p_cant;
		return found;

/* altra opció podria ser declarar la vble. v_exist i fer el següent codi
		select exist
		into v_exists
		from producto
		where (fabcod, prodcod) = (p_fabcod, p_prodcod);
		return (v_exists>=p_cant);
*/

end
$$ language plpgsql;


/*****************************************************************
    Task: Dóna d'alta una comanda al sistema.
    In:
		-> (date)    p_fecha : Data de la comanda (opcional, valor per defecte = current_timestamp)
	    -> (int)     p_cliecod : Codi de client que realitza la comanda.
		-> (int)     p_repcod : Codi del representant que gestiona la comanda.
		-> (varchar) p_fabcod : Codi del fabricant del producte.
		-> (varchar) p_prodcod : Codi del producte.
		-> (int)     p_cant :    Nombre d'unitats del producte a la comanda.
    Out:
		-> (varchar) Missatge de confirmació d'alta de la comanda.
******************************************************************/
drop function if exists altaComanda(date, int, int, varchar, varchar, int);
create or replace function altaComanda(p_fecha date, p_cliecod int, p_repcod int, p_fabcod varchar, p_prodcod varchar, p_cant int)
returns varchar
as $$
declare
	v_importe numeric;
	v_pednum int;
	message varchar(200);
	v_producto producto%rowtype;
begin
	-- Comprovar que el client existeix (es llençarà una excepció si no)
	if not existeixClient(p_cliecod) then
		message := 'ERROR: Client <' || p_cliecod || '> no existeix.';
	else

	-- Comprovar que existeixen suficients unitats del producte donat (asumim que el producte existeix, de moment, però llençarem excepció si no hi ha suficients unitats)
	  if not stockOk(p_cant, p_fabcod, p_prodcod) then
			message = ('ERROR: No hi ha '|| p_cant ||' d''el producte '||p_fabcod||p_prodcod);
	  else 	-- a partir d'ara ja puc donar d'alta la comanda

	  	-- Calcular import de la comanda.
			select *
			INTO STRICT v_producto
			from producto
			where (fabcod, prodcod) = (p_fabcod, p_prodcod);
				-- NOTA: plpgsql inicia una transacció de forma automàtica
				-- dins la execució de cada funció, per tant,
				-- no és necessari declarar transaccions.
				-- Inserim la comanda
			v_importe :=	v_producto.precio * p_cant;
			insert into pedido (pednum, fecha, cliecod, repcod, fabcod, prodcod, cant, importe)
			values
			(nextval('pednum_seq'), p_fecha, p_cliecod, p_repcod, p_fabcod, p_prodcod, p_cant, v_importe);
					-- Actualitzar estoc del producte
			update producto
			set exist = (exist - p_cant)
			where (fabcod, prodcod) = (p_fabcod, p_prodcod);
      message:= 'Comanda del producte '||v_producto.descrip|| ' realitzada exitosament.';
			message:= message ||chr(10) || 'Unitats demanades: '||p_cant||' estocatge: '||v_producto.exist;
	  end if; -- stockok
  end if; -- if principal
	return message;
end --de la funció
$$ language plpgsql;
