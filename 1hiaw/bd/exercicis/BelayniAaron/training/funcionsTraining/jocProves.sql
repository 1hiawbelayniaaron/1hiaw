--() { :; }; exec psql training -f "$0"
\t

\echo '---------------------------------'
\echo '  J O C     D E    P R O V E S'
\echo '---------------------------------'
\echo 'Tres escenaris possibles:\n* client no existeix'
\echo '* No hi ha prou estocatge \n* tot ok, la comanda es dóna d''alta\n'

\echo '* Comprovació existeixClient'
\echo '-------------------------------------'
select 'client 2111 (t) =>'|| existeixClient(2111),
      'client 2112 (t) =>'|| existeixClient(2112),
      'client 205 (f) =>'|| existeixClient(205);

\echo '* INSERTS i Seqüències: Alta dels clients McFly Corp. (2131) i Family Partners SL(2132)'
\echo '----------------------------------------------------------------------------------------'
select altaclient('McFly Corp.', 101, 37500);
select altaclient('Family Partners SL', 110, 48720);

\echo '* Comprovació stockOk'
\echo '-------------------------------------'
select 'Producte imm779c es demanen 10 uts, te 9 (f) =>' || stockok(10, 'imm', '779c');
select 'Producte rei2a44g es demanen 14 uts, te 14 (t) =>' || stockok(14, 'rei', '2a44g');

\echo '* Comprovació altaComanda'
\echo '-------------------------------------'

select 'Client 1 inexistent => (f) =>' || altacomanda(current_date, 1, 102, 'aci'::varchar, '41004'::varchar, 5);
select 'Estocatge insuficient=> (f) =>' || altacomanda(current_date, 2111, 102, 'aci'::varchar, '41004'::varchar, 999);

select 'Estocatge abans de la comanda: '|| exist
from producto
where fabcod||prodcod = 'aci'|| '41004';

select altacomanda(current_timestamp::date, 2111, 102, 'aci', '41004', 10);

select 'Estocatge després de comanda: '|| exist
from producto
where fabcod||prodcod = 'aci'|| '41004';
