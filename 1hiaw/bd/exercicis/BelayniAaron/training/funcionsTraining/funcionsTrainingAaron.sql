-- AARON BELAYNI 
-- iaw49656032
DROP SEQUENCE IF EXISTS cliecod_seq ;
CREATE SEQUENCE cliecod_seq;
select setval('cliecod_seq', (select max(cliecod) from cliente), true);

create sequence if not exists pednum_seq;

select setval('pednum_seq', (select max(pednum) from pedido), true);

-- I. Funcion

--1)
DROP FUNCTION IF EXISTS existeixClient(INT);
CREATE OR REPLACE FUNCTION  existeixClient(p_cliecod INT)
RETURNS BOOLEAN
AS $$

BEGIN
    SELECT cliecod
    INTO STRICT p_cliecod
    FROM cliente
    WHERE cliecod = p_cliecod;
    IF p_cliecod IS NOT NULL THEN
    RETURN TRUE;
    END IF;
    

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

--2)

DROP FUNCTION IF EXISTS altaClient(varchar,int,numeric);
CREATE OR REPLACE FUNCTION  altaClient(p_nombre varchar,p_repcod int, p_limcred numeric)
RETURNS VARCHAR
AS $$

BEGIN
    INSERT INTO cliente VALUES (nextval('cliecod_seq'),p_nombre,p_repcod,p_limcred);
    RETURN 'Client '  || p_nombre || ' s"ha donat d"alta';

END;
$$ LANGUAGE PLPGSQL;


--3) 
drop function if exists stockOk(int, varchar, varchar);
create or replace function stockOk(p_cant int, p_fabcod varchar, p_prodcod varchar)
returns boolean
as $$
declare
   basura varchar(1);
begin
			-- Utilitzem into + found
		select 'a'
		into basura
		from producto
		where (fabcod, prodcod) = (p_fabcod, p_prodcod) and exist >= p_cant;
		return found;

/* altra opció podria ser declarar la vble. v_exist i fer el següent codi
		select exist
		into v_exists
		from producto
		where (fabcod, prodcod) = (p_fabcod, p_prodcod);
		return (v_exists>=p_cant);
*/

end
$$ language plpgsql;

--4)
DROP FUNCTION IF EXISTS altaComanda(date,INT,varchar,varchar,int);
CREATE OR REPLACE FUNCTION  altaComanda(p_fecha date , p_cliecod INT,p_repcod int, p_fabcod varchar, p_prodcod varchar,p_cant int)
RETURNS VARCHAR
AS $$

DECLARE
	v_result varchar;
	v_importe int;
	v_producto producto%rowtype;

begin
-- comporovar si el client existeix
	if not existeixClient(p_cliecod) then
		v_result := 'ERROR: El client ' ||  p_cliecod || ' no existeix';
-- comprovar si hi ha existencies
	else
	 if not stockOk(p_cant, p_fabcod, p_prodcod) then
		v_result := 'ERROR: No hi ha ' || p_cant || ' unitats del producte: ' || (p_fabcod||p_prodcod);
	else
		-- calcular l'import (preu.producte * cant)
		SELECT *
		INTO STRICT v_producto
		FROM producto
		WHERE fabcod || prodcod = p_fabcod || p_prodcod;
		v_importe := p_cant*v_producto.precio;

-- entrar a la taula pedido el resultat
		INSERT INTO pedido VALUES(nextval('pednum_seq'),
		p_fecha,p_cliecod,p_repcod,p_fabcod,p_prodcod,p_cant,v_importe);


-- actualitzar l'stock
		UPDATE producto
		SET exist = exist - p_cant
		WHERE LOWER(fabcod||prodcod) = LOWER(p_fabcod||p_prodcod);
		v_result := 'La comanda del producte ' || p_fabcod||p_prodcod || ' de ' || p_cant || ' unitats, té un preu de ' || v_importe || ' $.';
	end if;
	end if;
	
RETURN v_result;
	
	
EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN 'NO_DATA_FOUND';  
END;
$$ LANGUAGE PLPGSQL;	

--5)
DROP FUNCTION IF EXISTS preuSenseIVA(INT);
CREATE OR REPLACE FUNCTION  preuSenseIVA(p_precio INT)
RETURNS INT
AS $$

DECLARE v_senseIVA INT;

BEGIN
    v_senseIVA := COALESCE(p_precio/1.21,0);
    RETURN v_senseIVA;

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

--6)
DROP FUNCTION IF EXISTS preuAmbIVA(INT);
CREATE OR REPLACE FUNCTION  preuAmbIVA(p_precio INT)
RETURNS INT
AS $$

DECLARE v_ambIVA INT;

BEGIN
    v_ambIVA := COALESCE(p_precio*1.21,0);
    RETURN v_ambIVA;

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

 



