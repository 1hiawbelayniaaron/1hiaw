# Transacciones (Multiusuari)

7. Omple la següent taula, tenint en compte que cada sentència s'executa en una connexió determinada.  

```
DELETE FROM punts; -- Connexió 0
INSERT INTO punts (id, valor) VALUES (70,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

SELECT COUNT(*) FROM punts; -- Connexió 2
```

>  El COUNT de la conexión 2 mostrará un 1 porque la transacción de la conexión 1 no ha finalizado.

8. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (90,5); -- Connexió 0

BEGIN; -- Connexió 1
DELETE FROM punts; -- Connexió 1

BEGIN; -- Connexió 2
SELECT * FROM punts; -- Connexió 2 
INSERT INTO punts (id, valor) VALUES (91,9); -- Connexió 2
COMMIT; -- Connexió 2

SELECT * FROM punts; -- Connexió 1 
COMMIT; -- Connexió 1 

SELECT * FROM punts WHERE id = 91; -- Connexió 0 
```

> Aparece 1 fila con id 90 después del primer SELECT de la conexión 2.  
>  Sale una fila con id 91 después del SELECT * FROM punts de la conexión 1.  
>  Se realiza el COMMIT de la conexión 1 sin problemas.  
>  Se realiza el SELECT sin problemas y aparece una sola fila con id 91 y valor 9  

9. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (100,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 100; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 100; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 100; -- Connexió 0 
```

>  Aparece el valor 7 en el último SELECT dado que la conexión 2 ha sido la última en realizar el commit.

| Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                                                                                         |
| -------- | ------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------- |
| 2        | B                         | Conexión 2 ha intentado modificar el valor de una fila que ya había sido modificada por la conexión 1 y aún no se había finalizado con commit |
| 2        | D                         | Al hacer el COMMIT la conexión 1, la conexión 2 se desbloquea realizando su commit                                                            |

10. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (110,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (111,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 110; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 110; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 111; -- Connexió 2
SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 110; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 111; -- Connexió 0
```

> La conexión 2 se queda bloqueada después del primer UPDATE. Todo lo que realiza a partir de ese momento se ejecutará después del COMMIT de la conexión 1. De esta manera se deshace el UPDATE anterior al SAVEPOINT a y se actualiza el valor del id 110 a 7.  
> El último SELECT mostrará valor 7 para el id 111.

| Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                                                 |
| -------- | ------------------------- | ----------------------------------------------------------------------------------------------------- |
| 2        | B                         | Se ha bloqueado porque ha intentado modificar una fila que ya había sido modificada por la conexión 1 |
| 2        | D                         | Conexión 1 desbloquea la conexión después de hacer el commit                                          |

11. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.
    Indica què veu cada usuari al select corresponent.

```
INSERT INTO punts (id, valor) VALUES (80,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (81,9); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 4 WHERE id = 80; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 81; -- Connexió 2

UPDATE punts SET valor = 10 WHERE id = 81; -- Connexió 1

UPDATE punts SET valor = 6 WHERE id = 80; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 80; -- Connexió 0
```

> Debido a un arror de deadlock en la conexión 2 por rtatar de modificar una fila que ya había sido modicficada por la conexión 1 (que estaba bloquada), la transacción de la conexión 2 ha quedado deshecha por completo.  
>  Todas las acciones de la conexión 1 se han realizado correctamente, ya que se ha desbloquado después de que la conexión 2 quedara abortada por el deadlock  
> El último SELECT en la conexión 0 muestra el valor 4 para el id 80.  

| Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                                                                                                                                          |
| -------- | ------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 1        | B                         | Se bloquea la conexión 1 después del segundo UPDATE en el id 81 porque la conexión 2 ya había modificado esa fila                                                                              |
| 2        | B                         | Bloqueo de la conexión 2 debido a que trata de modificar una fila previamente modificada por connexión 1. Salta error de deadlock debido a que la connexión 1 ya estaba previamente bloqueada. |
| 2        | D                         | Se desbloquea con un ROLLBACK después de tratar de hacer un COMMIT debido al error de deadlock.                                                                                                |
| 1        | D                         | La conexión 2 recibe un error cuando trata de realizar el UPDATE en una fila que ya había sido modificada. Debido a un error deadlock la conexión 1 se desbloquea                              |

12. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Finalment digues quin valor s'obtindrà amb l'últim SELECT. Tenint en compte que cada sentència s'executa en una connexió determinada.

```
INSERT INTO punts (id, valor) VALUES (120,5); -- Connexió 0
INSERT INTO punts (id, valor) VALUES (121,5); -- Connexió 0

BEGIN; -- Connexió 1
UPDATE punts SET valor = 6 WHERE id = 121; -- Connexió 1
SAVEPOINT a;
UPDATE punts SET valor = 9 WHERE id = 120; -- Connexió 1

BEGIN; -- Connexió 2
UPDATE punts SET valor = 7 WHERE id = 120; -- Connexió 2

ROLLBACK TO a; -- Connexió 1

SAVEPOINT a; -- Connexió 2
UPDATE punts SET valor = 8 WHERE id = 120; -- Connexió 2
ROLLBACK TO a; -- Connexió 2
COMMIT; -- Connexió 2

COMMIT; -- Connexió 1

SELECT valor FROM punts WHERE id = 121; -- Connexió 0
```

>  El valor del último SELECT será de 6 para el id 121. Después de desbloquarse la conexión 2, el resto de movimientos se han realizado sin bloqueos.

| Connexió | Blocat (B)/ Desblocat (D) | Motiu                                                                                                                                                               |
| -------- | ------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| 2        | B                         | Al intentar modificar la fila cuyo id es 120 debido a que la conexión 1 ya la había modificado                                                                      |
| 2        | D                         | La conexión 2 se desbloquea al hacer la conexión 1 un ROLLBACK TO a un punto de guardado anterior a la modificación de la fila que mantenía bloquada la conexión 2. |

6. Analitzant les següents sentències explica quins canvis es realitzen i on es realitzen. Explica si la transacció en curs ha acabat. Finalment digues quin valor s'obtindrà amb l'últim SELECT.
   
   ```
   DELETE FROM punts;
   INSERT INTO punts (id, valor) VALUES (60,5);
   BEGIN;
   UPDATE punts SET valor = 4 WHERE id = 60;
   SAVEPOINT a;
   INSERT INTO punts (id, valor) VALUES (61,8);
   SAVEPOINT b;
   INSERT INTO punts (id, valor) VALUES (61,9);
   ROLLBACK TO b;
   COMMIT;
   SELECT SUM(valor) FROM punts;
   ```

```
>  t0 = Inicia la transacción con 1 única fíla id = 60, valor = 5  
>  t1 = Actualizamos el valor de id 60 a 4  
>  t2 = Realizamos un punto de guardado  
>  t3 = Insertamos una nueva fila, ahora hay 2 filas  
>  t4 = Realizamos un nuevo punto de guardado  
>  t5 = Se inserta una nueva fila que dará error. No podrá ser añadida  
>  t6 = Volvemos al segundo punto de guardado y se deshace el error  
>  t7 = Finalizamos la transacción. Tenemos 2 filas: id 60 valor 4 y id 61 valor 8  
>    
>  Fuera de la transacción la suma del campo valor nos dará 12  
```
