-- AARON BELAYNI 
-- iaw49656032
DROP SEQUENCE IF EXISTS cliecod_seq ;
CREATE SEQUENCE cliecod_seq;
select setval('cliecod_seq', (select max(cliecod) from cliente), true);

-- I. Funcion

--1)
CREATE OR REPLACE FUNCTION  existeixClient(p_cliecod INT)
RETURNS BOOLEAN
AS $$

BEGIN
    SELECT cliecod
    INTO STRICT p_cliecod
    FROM cliente
    WHERE cliecod = p_cliecod;
    IF p_cliecod IS NOT NULL THEN
    RETURN TRUE;
    END IF;
    

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

--2)


CREATE OR REPLACE FUNCTION  altaClient(p_nombre varchar,p_repcod SMALLint, p_limcred numeric(8,2))
RETURNS VARCHAR
AS $$

BEGIN
    INSERT INTO cliente VALUES (nextval('cliecod_seq'),p_nombre,p_repcod,p_limcred);
    RETURN 'Client '  || p_nombre || ' s"ha donat d"alta';

END;
$$ LANGUAGE PLPGSQL;

--3) // NO SE QUE HE DE FER //

CREATE OR REPLACE FUNCTION  stockOk(p_cant INT, p_fabcod varchar, p_prodcod varchar)
RETURNS BOOLEAN
AS $$

DECLARE
	v_exist INT;

BEGIN
    SELECT exist, fabcod, prodcod
    INTO STRICT  v_exist, p_fabcod, p_prodcod
    FROM producto
    
    WHERE LOWER(fabcod || prodcod) = LOWER(p_fabcod || p_prodcod);
    RETURN p_cant <= v_exist;

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

--4)


-- Aún no estoy listo para esta función



--5)

CREATE OR REPLACE FUNCTION  preuSenseIVA(p_precio INT)
RETURNS INT
AS $$

DECLARE v_senseIVA INT;

BEGIN
    v_senseIVA := COALESCE(p_precio/1.21,0);
    RETURN v_senseIVA;

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

--6)

CREATE OR REPLACE FUNCTION  preuAmbIVA(p_precio INT)
RETURNS INT
AS $$

DECLARE v_ambIVA INT;

BEGIN
    v_ambIVA := COALESCE(p_precio*1.21,0);
    RETURN v_ambIVA;

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN FALSE;  
END;
$$ LANGUAGE PLPGSQL;

 



