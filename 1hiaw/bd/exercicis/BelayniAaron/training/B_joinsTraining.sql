-- --------------------------------------------------------------------
-- Apartat B:
-- CONSULTAS MULTITABLA
-- --------------------------------------------------------------------

--1. Lista los nombres de los representantes, mostrando
--la ciudad de la oficina donde trabaja y la region.

SELECT nombre, ciudad, region
FROM repventa JOIN oficina
	ON repventa.ofinum = oficina.ofinum;

--2. Obtener una lista de todos los pedidos, mostrando el
--número de pedido, su importe, el nombre de cliente que
--lo realizó y el límite de crédito de dicho cliente.

SELECT pednum, importe, nombre "realizado por cliente",
	   limcred "límite crédito cliente"
FROM pedido JOIN cliente
		ON pedido.cliecod = cliente.cliecod
ORDER BY 3;

--3.Obtener una lista de representantes ordenada alfabéticamente,
-- en la que se muestre el nombre
-- del representante, codigo de la oficina donde trabaja, ciudad y la
-- región a la que vende.

SELECT nombre, o.ofinum,  region
FROM repventa r JOIN oficina o  ON r.ofinum = o.ofinum
ORDER BY nombre;

--4. Obtener una lista de las oficinas (ciudades, no códigos)
--que tienen un objetivo superior a 3600 euros. Para cada oficina
--mostrar la ciudad, su objetivo, el nombre de su director y
--puesto del mismo.

SELECT ciudad, objetivo,  nombre "director", puesto
FROM oficina JOIN repventa ON director = repcod
WHERE  objetivo > 3600;

--5.- Obtener una lista de todos los pedidos mostrando su
--número, el codigo completo del producto, la descripción del producto
-- solicitado y el importe

SELECT	pednum,
	upper(pe.fabcod||pe.prodcod) producto,
	descrip "descripcion",
	importe
FROM pedido pe JOIN producto pr
	ON pe.fabcod = pr.fabcod and pe.prodcod = pr.prodcod;

--6.Obtener una lista de los pedidos cON importes superiores
-- a 150 euros. Mostrar el nombre del cliente que solicitó el pedido,
-- numero del pedido, importe del mismo, la descripción del producto
-- solicitado y el nombre del representante que lo tomó. Ordenad la
-- lista por cliente alfabéticamente y luego por importe de mayor a menor.

SELECT	c.nombre "Client",
	pednum "Pedido",
	importe,
	descrip Producto,
	r.nombre "Representant"
FROM 	pedido pe JOIN producto pr ON pe.fabcod||pe.prodcod = pr.fabcod||pr.prodcod
				JOIN cliente c ON pe.cliecod = c.cliecod
				LEFT JOIN repventa r ON pe.repcod = r.repcod
WHERE   importe > 150
ORDER BY 1, 3 DESC;

--7. Obtener una lista de los pedidos cON importes superiores
-- a 2000 euros, mostrando el número de pedido, importe, nombre del
-- cliente que lo solicitó y el nombre del representante que contactó
-- cON el cliente por primera vez.

SELECT	pednum,
		importe,
		c.nombre "Client",
		r.nombre "Representant que fidelizó el cliente"
FROM pedido p JOIN cliente c ON p.cliecod = c.cliecod
	JOIN repventa r ON c.repcod = r.repcod
WHERE  importe > 2000;

--8.- Obtener una lista de los pedidos cON importes superiores
--a 150 euros, mostrando el código del pedido, el importe,
--el nombre del cliente que lo solicitó, el nombre del
--representante que contactó cON él por primera vez y la
--ciudad de la oficina donde el representante trabaja.

SELECT pednum "nº pedido", importe,
		cliente.nombre "Client",
		repventa.nombre "Representant que fidelizó el cliente",
		ciudad "oficina del repr."
FROM pedido JOIN cliente ON pedido.cliecod = cliente.cliecod
	JOIN repventa ON cliente.repcod = repventa.repcod
	JOIN oficina 	ON repventa.ofinum = oficina.ofinum
WHERE  importe > 150;

--9.- Lista los pedidos tomados durante el mes de octubre
--del año 2003, mostrando solamente el numero del pedido,
--su importe, el nombre del cliente que lo realizó, la fecha
--y la descripción del producto solicitado.

SELECT pednum "nº pedido", importe,
		descrip "producto",
		nombre "cliente", fecha
FROM pedido JOIN producto
		ON pedido.fabcod = producto.fabcod
		and pedido.prodcod = producto.prodcod
	JOIN cliente
		ON pedido.cliecod = cliente.cliecod
WHERE  to_char(fecha + 5000, 'mm-yyyy') = '10-2003';

--10.- Obtener una lista de todos los pedidos tomados
--por representantes de las oficinas de la regiON Este,
--mostrando solamente el numero de pedido, la descripción
--del producto y el nombre del representante que lo tomó.

SELECT pednum "nº pedido", descrip "producto",
		nombre "representante"
FROM pedido JOIN producto ON pedido.fabcod = producto.fabcod
		and pedido.prodcod = producto.prodcod
	JOIN repventa ON pedido.repcod = repventa.repcod
	JOIN oficina ON repventa.ofinum = oficina.ofinum
WHERE  lower(region) = 'este';

--11.- Obtener los pedidos tomados en los mismos días
--en que un nuevo representante fue contratado. Mostrar
--número de pedido, importe, fecha pedido.

SELECT distinct pednum "nº pedido", importe, fecha
FROM pedido, repventa
WHERE  to_char(fecha,'dd-mm-yyyy') = to_char(fcontrato, 'dd-mm-yyyy');

--12.- Obtener una lista cON parejas de representantes y
--oficinas en donde la cuota del representante es mayor o
--igual que el objetivo de la oficina, sea o no la oficina en
--la que trabaja. Mostrar Nombre del representante, cuota del
--mismo, Ciudad de la oficina, objetivo de la misma.

SELECT nombre "representant", cuota, ciudad, objetivo
FROM repventa, oficina
WHERE  cuota >= objetivo;




--13.- Muestra el nombre, las ventas y la ciudad de la
--oficina de cada representante de la empresa.

SELECT nombre "representant", repventa.ventas, ciudad
FROM repventa JOIN oficina ON repventa.ofinum = oficina.ofinum;

--14.- Obtener una lista de la descripción de los productos
--para los que existe algún pedido en el que se solicita
--una cantidad mayor a las existencias de dicho producto.

SELECT 	descrip "producte"
FROM 		producto JOIN pedido ON pedido.fabcod = producto.fabcod
													AND pedido.prodcod = producto.prodcod
WHERE  cant > exist;
/*
15.- Lista los nombres de los representantes que tienen
una cuota superior a la de su director. Mostrar nombre y cuota del representante
y nombre y cuota de su jefe
*/
SELECT 	rep.nombre "representant",
				rep.cuota,
				dir.nombre "jefe",
				dir.cuota
FROM repventa rep JOIN repventa dir
		ON rep.jefe = dir.repcod
WHERE  coalesce(rep.cuota, 0) > coalesce(dir.cuota, 0);

/*
 representant |   cuota   |   jefe    |   cuota
--------------+-----------+-----------+-----------
 Bill Adams   | 350000.00 | Bob Smith | 200000.00
 Mary Jones   | 300000.00 | Sam Clark | 275000.00
 Dan Roberts  | 300000.00 | Bob Smith | 200000.00
 Larry Fitch  | 350000.00 | Sam Clark | 275000.00
 Paul Cruz    | 275000.00 | Bob Smith | 200000.00
(5 rows)
*/

--16.- Obtener una lista de los representantes que trabajan
--en una oficina distinta de la oficina en la que trabaja
--su director, mostrando también el nombre del director
--y el código de la oficina donde trabaja cada uno de ellos.


SELECT 	r.nombre "representant",
				r.ofinum "nº oficina representant",
				dire.nombre "director",
				dire.ofinum "nº oficina director"
FROM 	repventa r JOIN oficina o
					ON r.ofinum = o.ofinum
			JOIN repventa dire
					ON o.director = dire.repcod
WHERE  r.ofinum <> dire.ofinum;

/*
representant  | nº oficina representant |  director   | nº oficina director
---------------+-------------------------+-------------+---------------------
 Nancy Angelli |                      22 | Larry Fitch |                  21
(1 row)
*/

--17.- El mismo ejercicio anterior, mostrando en lugar de
--su codigo de la oficina, las ciudades de ambos.

SELECT 	r.nombre "representant",
				o.ciudad "nº oficina representant",
				dire.nombre "director",
				ofiDire.ciudad "nº oficina director"
FROM 	repventa r JOIN oficina o ON r.ofinum = o.ofinum
		 	JOIN repventa dire ON o.director = dire.repcod
		 	JOIN oficina ofiDire 	ON dire.ofinum = ofiDire.ofinum
WHERE  r.ofinum <> dire.ofinum;

/*
 representant  | nº oficina representant |  director   | nº oficina director
---------------+-------------------------+-------------+---------------------
 Nancy Angelli | Denver                  | Larry Fitch | Los Angeles
(1 row)
*/

--18.- Mostrar el nombre y el puesto de los que son jefe.

SELECT	DISTINCT jefe.nombre "jefe",
				jefe.puesto "puesto"
FROM 		repventa r JOIN repventa jefe 	ON r.jefe = jefe.repcod;

/*
    jefe     |        puesto
-------------+----------------------
 Bob Smith   | Dir Ventas
 Larry Fitch | Dir Ventas
 Dan Roberts | Representante Ventas
 Sam Clark   | VP Ventas
(4 rows)
*/
