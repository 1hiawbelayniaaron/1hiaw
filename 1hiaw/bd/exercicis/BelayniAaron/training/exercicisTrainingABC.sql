-- AARON BELAYNI 
-- iaw49656032

-- A. Consultas Simples

-- 1. Obtener los datos de los productos cuyas existencias estén entre 25 y 40 unidades.
SELECT *
FROM producto
WHERE exist BETWEEN 25 AND 40;

-- 2. Obtener los códigos de los representantes que han tomado algún pedido (evitando su repetición).
SELECT DISTINCT repcod
FROM repventa;

-- 3. Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111.
SELECT * 
FROM pedido
WHERE cliecod = 2111;

-- 4. Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111 y que han sido tomados por el representante cuyo código es el 103.
SELECT * 
FROM pedido
WHERE cliecod = 2111 AND repcod = 103;

-- 5. Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111, que han sido tomados por el representante cuyo código es el 103 y que solicitan artículos del fabricante cuyo código es ACI.
SELECT *
FROM pedido
WHERE cliecod = 2111 AND repcod = 103 AND UPPER(fabcod) = 'ACI';

-- 6. Obtener una lista de todos los pedidos ordenados por cliente y, para cada cliente, ordenados por la fecha del pedido (ascendentemente)
SELECT pednum, cliecod, fecha
FROM pedido
ORDER BY fecha, cliecod ASC;

-- 7. Obtener los datos de los representantes que pertenecen a la oficina de código 12 y 13 (cada representante solo pertenece a una oficina).
SELECT *
FROM repventa
WHERE ofinum BETWEEN 12 AND 13;

-- 8. Obtener los datos de productos de los que no hay existencias o bien éstas son desconocidas.
SELECT *
FROM producto
WHERE exist = 0 OR exist = NULL;

-- 9. Mostrar los representantes que fueron contratados en el 2003 (sumem 5000 a la data de contracte)
SELECT *, fcontrato+5000 AS fcontrato2
FROM repventa
WHERE to_char (fcontrato, 'YYYY') > '1988';

-- 10. Mostrar el nombre y días que lleva contratados los representants
SELECT nombre, CURRENT_DATE - fcontrato as disCOntratados
FROM repventa;

-- /// B. Consultas multitabla ///

-- 1. Muestra de los representantes su nombre, la ciudad de su oficina así como su región.
SELECT nombre, COALESCE(o.ciudad,'** no té valor **') , COALESCE(o.region,'** no té valor **')
FROM Repventa r LEFT JOIN oficina o ON o.ofinum = r.ofinum;

-- 2. Obtener una lista de todos los pedidos, mostrando el número de pedido, su importe, el nombre del cliente que lo realizó y el límite de crédito de dicho cliente.
SELECT pednum, importe, c.nombre, c.limcred 
FROM pedido p LEFT JOIN cliente c ON p.cliecod = c.cliecod;

-- 3. Obtener una lista de representantes ordenada alfabéticamente, en la que se muestre el nombre del representante, codigo de la oficina donde trabaja, ciudad y la región a la que vende.
SELECT repventa.nombre , o.ofinum, o.ciudad, o.region
FROM repventa LEFT JOIN oficina o ON repventa.ofinum = o.ofinum
ORDER BY nombre;


-- 4.  Obtener una lista de las oficinas (ciudades, no códigos) que tienen un objetivo superior a 360000 euros. Para cada oficina mostrar la ciudad, su objetivo, el nombre de su director y puesto del mismo.

-- ## ERROR ##
SELECT oficina.ciudad, oficina.objetivo, oficina.director direc ,repventa.jefe, repventa.puesto
FROM oficina JOIN repventa ON oficina.ofinum = repventa.ofinum
WHERE oficina.objetivo > 360000;

-- ## hi ha d'haber 6 files i hi ha 8 ##
SELECT oficina.ciudad, oficina.objetivo, oficina.director direc ,repventa.jefe, repventa.puesto
FROM oficina JOIN repventa ON oficina.director = repventa.jefe
WHERE oficina.objetivo > 360000;


-- 5.  Obtener una lista de todos los pedidos mostrando su número, el importe y la descripción de los productos solicitados.

-- ## Al ser una clave ajena compuesta necesitamos poner un AND en FROM ##
SELECT pedido.pednum, pedido.importe, producto.descrip
FROM pedido JOIN producto ON pedido.prodcod = producto.prodcod AND pedido.fabcod = producto.fabcod;

-- ## Al ser una clave ajena compuesta necesitamos poner un AND en FROM ##
SELECT pedido.pednum, pedido.importe, producto.descrip
FROM pedido JOIN producto ON pedido.prodcod || producto.prodcod = pedido.fabcod || producto.fabcod;


-- 6.  Obtener una lista de los pedidos con importes superiores a 4000. Mostrar el nombre del cliente que solicitó el pedido, numero del pedido, importe del mismo, la descripción del producto solicitado y el nombre del representante que lo tomó. Ordenad la lista por cliente alfabéticamente y luego por importe de mayor a menor.


--  no ser ferho

SELECT c.nombre, rep.nombre, p.pednum, p.importe, pro.descrip
FROM cliente c JOIN pedido p ON c.cliecod = p.cliecod
		JOIN producto pro ON (pro.prodcod, pro.fabcod) = (p.prodcod, p.fabcod)
		JOIN repventa rep ON rep.repcod = p.repcod
WHERE p.importe > 4000
ORDER BY c.nombre ASC, p.importe DESC;


-- 7. Obtener una lista de los pedidos con importes superiores
--	a 2000 euros, mostrando el número de pedido, importe, nombre del
--	cliente que lo solicitó y el nombre del representante que contactó
--	con el cliente por primera vez.  
	
SELECT ped.pednum, ped.importe, cl.nombre, rep.nombre
FROM pedido ped JOIN cliente cl ON ped.cliecod = cl.cliecod
		JOIN repventa rep ON ped.repcod = rep.repcod
WHERE importe > 2000;

-- 8. Obtener una lista de los pedidos con importes superiores a 150
--  euros, mostrando el código del pedido, el importe, el nombre del
--  cliente que lo solicitó, el nombre del representante que contactó
--  con él por primera vez y la ciudad de la oficina donde el
--  representante trabaja. 
    
SELECT ped.pednum, ped.importe, cl.nombre, rep.nombre, ofi.ciudad
FROM pedido ped
JOIN cliente cl ON ped.cliecod = cl.cliecod
JOIN repventa rep ON ped.repcod = rep.repcod
JOIN oficina ofi ON rep.jefe = ofi.director
WHERE importe > 150;

-- 9.  Lista los pedidos tomados durante el mes de octubre del año 2003 ,
--  mostrando solamente el número del pedido, su importe, el nombre del
--  cliente que lo realizó, la fecha y la descripción del producto
--  solicitado 
    



-- 10. Obtener una lista de todos los pedidos tomados por representantes de
--  oficinas de la región Este, mostrando solamente el número del
--  pedido, la descripción del producto y el nombre del representante
--  que lo tomó 
    
SELECT ped.pednum, prod.descrip, rep.nombre, ofi.region
FROM pedido ped
JOIN producto prod ON ped.fabcod = prod.fabcod
JOIN repventa rep ON ped.repcod = rep.repcod
JOIN oficina ofi ON rep.jefe = ofi.director
WHERE region = 'Este';

-- 11. Obtener los pedidos tomados en los mismos días en que un nuevo
--  representante fue contratado. Mostrar número de pedido, importe,
--  fecha pedido. 
    
SELECT pednum, importe, pedido.fecha
FROM pedido LEFT JOIN repventa ON repventa.fcontrato = pedido.fecha
WHERE pedido.fecha = repventa.fcontrato;

-- 12. Obtener una lista con parejas de representantes y oficinas en donde
--  la cuota del representante es mayor o igual que el objetivo de la
--  oficina, sea o no la oficina en la que trabaja. Mostrar Nombre del
--  representante, cuota del mismo, Ciudad de la oficina, objetivo de la
--  misma. 

SELECT nombre, cuota, ciudad, objetivo
FROM oficina o LEFT JOIN repventa r ON r.jefe = o.director
WHERE r.cuota > o.objetivo;


-- 13. Muestra el nombre, las ventas y la ciudad de la oficina de cada
--     representante de la empresa.

SELECT rep.nombre, rep.ventas, ofi.ciudad
FROM repventa rep JOIN oficina ofi
ON rep.ofinum = ofi.ofinum;

-- 14. Obtener una lista de la descripción de los productos para los que existe algún pedido en el que se solicita una cantidad mayor a las existencias de dicho producto.

SELECT pro.descrip, pro.exist, ped.cant
FROM producto pro JOIN pedido ped
ON (pro.fabcod, pro.prodcod) = (ped.fabcod, ped.prodcod)
WHERE ped.cant > pro.exist;

-- 15. Lista los nombres de los representantes que tienen una cuota superior a la de su director.

-- Amb el director
SELECT rep.nombre JEFE, rep.cuota CuotaJEFE, jefe.cuota CuotaEMP, jefe.nombre EMP
FROM repventa rep JOIN repventa jefe
ON rep.repcod = jefe.jefe
WHERE rep.cuota < jefe.cuota;

-- Sense el director



-- 16. Obtener una lista de los representantes que trabajan en una oficina distinta de la oficina en la que trabaja su director, mostrando también el nombre del director y el código de la oficina donde trabaja cada uno de ellos.

SELECT rep.nombre JEFE, rep.ofinum OficinaJEFE, jefe.nombre EMP, jefe.ofinum OficinaEMP
FROM repventa rep JOIN repventa jefe
ON rep.repcod = jefe.jefe
WHERE rep.ofinum != jefe.ofinum;

-- 17. El mismo ejercicio anterior, pero en lugar de ofinum, la ciudad.

-- acabar de fer
SELECT rep.nombre JEFE, ofi.ciudad CiudadJEFE, jefe.nombre EMP, jefi.ciudad CiudadEMP
FROM repventa rep JOIN repventa jefe
ON rep.repcod = jefe.jefe JOIN oficina ofi
ON ofi.ofinum = rep.ofinum JOIN oficina jefi
ON ofi.ciudad = jefi.ciudad
WHERE rep.ofinum != jefe.ofinum;


-- 18. Mostrar el nombre y el puesto de los que son jefe.

SELECT DISTINCT rep.nombre JEFE, rep.puesto PUESTO
FROM repventa rep JOIN repventa jefe
ON rep.repcod = jefe.jefe;


-- C.

--1\. Mostrar la suma de las cuotas y la suma de las ventas totales de todos los representantes.

SELECT SUM(cuota) cuota_total, SUM(ventas)ventes_totals
FROM repventa;

--2\. ¿Cuál es el importe total de los pedidos tomados por Bill Adams?

SELECT SUM(importe) import_total
FROM pedido ped LEFT JOIN repventa r
ON ped.repcod = r.repcod
WHERE UPPER(nombre) = 'BILL ADAMS';

--3\. Calcula el precio medio de los productos del fabricante ACI.

SELECT AVG(importe) import_mitja
FROM pedido 
WHERE LOWER(fabcod) = 'aci';

-- 4\. ¿Cuál es el importe medio de los pedido solicitados por el cliente 2103

SELECT AVG(importe) import_mitja
FROM pedido 
WHERE cliecod = 2103;


-- 5\. Mostrar la cuota máxima y la cuota mínima de las cuotas de los representantes.

SELECT MIN(cuota) "Cuota minima", MAX(cuota) "Cuota maxima" 
FROM repventa;


-- 6\. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?

SELECT MIN(fecha)
FROM pedido;

-- 7\. ¿Cuál es el mejor rendimiento de ventas de todos los representantes? (considerarlo como el porcentaje de ventas sobre la cuota).

SELECT ROUND(MAX((ventas/cuota)*100),2)
FROM repventa;

-- 8\. ¿Cuántos clientes tiene la empresa?

SELECT COUNT(*)
FROM cliente ;

-- 9\. ¿Cuántos representantes han obtenido un importe de ventas superior a su propia cuota?

SELECT COUNT(NVL(cuota,0)) , COUNT(ventas)
FROM repventa
WHERE ventas > cuota;

-- 10\. ¿Cuántos pedidos se han tomado de más de 150 euros?
SELECT COUNT(pednum)
FROM pedido
WHERE importe > 150;


-- 11\. Halla el número total de pedidos, el importe medio, el importe total de los mismos.
SELECT COUNT(pednum) "numero de pedidos", AVG(importe) "importe medio", SUM(importe) "importe total"      
FROM pedido;


-- 12\. ¿Cuántos puestos de trabajo diferentes hay en la empresa?

SELECT COUNT(DISTINCT puesto) 
FROM repventa;

-- 13\. ¿Cuántas oficinas de ventas tienen representantes que superan sus propias cuotas?
SELECT COUNT(DISTINCT ofinum)
FROM repventa
WHERE ventas > cuota;


-- 14\. ¿Cuál es el importe medio de los pedidos tomados por cada representante?
SELECT r.nombre, ROUND(AVG(importe),2) "importe medio de los pedidos"
FROM pedido p JOIN repventa r ON r.repcod = p.reocod
GROUP BY r.repcod, r.nombre;

-- 15\. ¿Cuál es el rango de las cuotas de los representantes asignados a cada oficina (mínimo y máximo)?
SELECT MIN(ventas) "Minim", MAX(ventas)
FROM repventa;


-- 16\. ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad y número de representantes.
SELECT ofi.ciudad ciutat, COUNT(r.repcod) "numero de representant"
FROM repventa r LEFT JOIN oficina ofi
ON r.ofinum = ofi.ofinum
GROUP BY ofi.ofinum;

-- 17\. ¿Cuántos clientes ha contactado por primera vez cada representante? Mostrar el código de representante, nombre y número de clientes.

SELECT r.repcod, r.nombre, COUNT(c.cliecod)
FROM repventa r LEFT JOIN  cliente c 
ON r.repcod = c.repcod
GROUP BY r.repcod;


-- 18\. Calcula el total del importe de los pedidos solicitados por cada cliente a cada representante.

SELECT c.cliecod, c.repcod, COALESCE(SUM(p.importe),0) "import total"
FROM cliente c LEFT JOIN pedido p
ON c.cliecod = p.cliecod
GROUP BY c.cliecod;

-- 19\. Lista el importe total de los pedidos tomados por cada representante.

SELECT r.repcod, COALESCE(SUM(p.importe),0) "import total"
FROM repventa r LEFT JOIN pedido p
ON r.repcod = p.repcod
GROUP BY r.repcod;


-- 20\. Para cada oficina con dos o más representantes, calcular el total de las cuotas y el total de las ventas de todos sus representantes.

-- acabar:

SELECT o.ofinum, COALESCE(SUM(r.cuota),0) "cuota total" , COALESCE(SUM(r.ventas),0) "ventas totales"
FROM oficina o LEFT JOIN repventa r 
ON o.ofinum = r.ofinum
GROUP BY o.ofinum
HAVING COUNT(r.repcod)>40;


-- 21\. Muestra el número de pedidos que superan el 75% de las existencias.
