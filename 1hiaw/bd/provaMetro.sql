\c template1
drop database if exists Metro2;
create database Metro2;
\c Metro2

  create table Estacio(
	  CodiEstacio varchar(4) constraint codiEstacio_pk primary key NOT NULL, 
	  HorariAperturaEstacio timestamp, 
	  HorariTancamentEstacio timestamp
  		);
    
  create table Acces(
	  CodiEstacio varchar(5) NOT NULL, --E1234
	  CodiAcces varchar(4) NOT NULL, --A123 
	  CarrerAcces varchar(60), 
	  NumPortalAcces smallint, --(1,2,3,) 
	  Ascensor bool, 
	  EscalaNormal bool, 
	  EscalaMecanica bool,
	  constraint codiEstacio_CodiAcces_pk primary key (CodiEstacio,CodiAcces)
	  	);  
  
  create table Linia(
  	CodiLinia varchar(2), 
  	ColorLinia varchar(12), 
  	HorariSortidaPrimerTren timestamp, 
  	HorariSortidaUltimTren timestamp, 
  	CodiEstacio varchar(5),
  	constraint CodiEstacio_Linia_fk foreign key (CodiEstacio) references Acces(CodiEstacio) 
  		ON DELETE SET NULL ON UPDATE CASCADE
  		):  
  
  create table LiniaPerEstacio(
  	CodiLinia varchar(2) NOT NULL, 
  	CodiEstacio varchar(5) NOT NULL,
  	constraint CodiLinia_CodiEstacio_pk primary key (CodiLinia,CodiEstacio)
  		);  
  
  create table Tren(
	  CodiTren varchar(6), -- T-0000 
	  DataCompraTren date, 
	  ModelTren varchar(5), -- s-100 
	  NumVagonsTren smallint, 
	  CodiLinia varchar(2), 
	  CodiCotxera int, --sequence
	  constraint CodiCotxera_Tren_fk foreign key (CodiCotxera) references Acces(CodiEstacio)  				
	 		ON DELETE SET NULL ON UPDATE CASCADE
	  	);	  
  
  create table Cotxera(
	  CodiCotxera int constraint CodiCotxera_pk primary key, 
	  NumMaquinesCOtxera int, 
	  NumVagonsCotxera int, 
	  CodiEstacio varchar(5),
	  constraint CodiEstacio_Cotxera_fk foreign key (CodiCotxera) references Cotxera(CodiCotxera)
	  			ON DELETE SET NULL ON UPDATE CASCADE
	  	);	
  
  create table InfoPanell(
  CodiTren varchar(6), 
  CodiEstacio varchar(5), 
  MinutsArribada timestamp(mm), 
  Sentit varchar(30),
  constraint codiTrenEstacio_pk primary key (CodiTren,CodiEstacio)
  	);
