DROP DATABASE if exists ett;
CREATE DATABASE ett;
\c ett
/*DROP TABLE entrevista;
DROP TABLE contracte;
DROP TABLE oferta;
DROP TABLE empresa;
DROP TABLE persona;
*/
CREATE TABLE persona(
	dni VARCHAR(9) constraint persona_dni_pk PRIMARY KEY,
	nom VARCHAR(50),
	cognoms VARCHAR (100),
	dataalta date,
	estudis VARCHAR(200),
	professio VARCHAR(200)
);

INSERT INTO persona
VALUES ('12345678Z','Martina','Fernandez',to_date('2021-06-15','yyyy-mm-dd'),'Técnico Superior Desarrollo Aplicaciones Web','Informátic@'),
('87654321X','Arnau','Bofill',to_date('2014-03-02','yyyy-mm-dd'),'Enginyeria tècnica informàtica gestio','Informatic'),
('12334521L','Faemino','Cansado',to_date('2013-01-01','yyyy-mm-dd'),'Enginyeria tècnica informàtica gestio','Informatic'),
('12312344M','Clark','Kent',to_date('2012-01-01','yyyy-mm-dd'),'Superheroi','Titulació Superior Escola Superherois');

CREATE TABLE empresa (
	cif VARCHAR(10) constraint empresa_pk PRIMARY KEY,
	nom VARCHAR(50),
	sector VARCHAR(30),
	poblacio VARCHAR(50)
);

insert into empresa (cif, nom, sector,poblacio) values
('A-28599033', '22@ Indra','Telecomunicacions i Serveis IT','Barcelona'),
('B-43543449', 'T-SYSTEMS','Telecomunicacions i Serveis IT','Barcelona');

CREATE TABLE oferta (
	idoferta VARCHAR(8) constraint oferta_pk PRIMARY KEY,
	professio VARCHAR(50),
	data date,
	descripcio VARCHAR(200),
	sou numeric(6,2),
	poblacio VARCHAR(50),
	cif VARCHAR(10),
	sector VARCHAR(100),
	constraint oferta_fk foreign key(cif) references empresa(cif)
);

insert into oferta values
('4REC','Informatic', current_date, 'Serveis de consultoria',1500,'Barcelona','A-28599033','Informatica'),
('FRM21','Limpiador@', current_date-380, 'Serveis de Neteja',800,'Barcelona','A-28599033','Neteja'),
('5JK32','Informatic', current_date-30, 'Explotació Business Intelligence',1600,'Barcelona','B-43543449','Informatica'),
('4GR30','Informatic', current_date-15, 'Desenvolupardor frontend',1500,'Barcelona','B-43543449','Informatica');

CREATE TABLE entrevista(
	idOferta 	VARCHAR(8),
	dni 		VARCHAR(9),
	avaluacio 	VARCHAR(200),
	PRIMARY KEY (idOferta, dni),
	foreign key (idOferta) 	references oferta,
	foreign key (dni) 	references persona);

insert into entrevista values
('4REC','12345678Z','Candidata amb experiencia. A contractar'),
('4REC','87654321X','Candidat neofit. No te experiencia. Descartat'),
('4GR30','87654321X','Candidat poc preparat. Baix coneixement de la matèria'),
('4GR30','12312344M','Candidat massa qualificat per a la feina. No el contractarem');

CREATE TABLE contracte(
	dataIni 	date,
	dni 		VARCHAR(9),
	cif 		VARCHAR(10),
	dataFi 		date,
	sou 		numeric(8,2) 	not null,
	jornada numeric(5,2),
	PRIMARY KEY (dataIni, dni, cif),
	foreign key (cif) 	references empresa,
	foreign key (dni) 	references persona);

insert into contracte values
(current_date-90 ,'12345678Z','B-43543449',current_date-50, 29500.00,40),
(current_date ,'12345678Z', 'A-28599033',null, 33000.00,20);
