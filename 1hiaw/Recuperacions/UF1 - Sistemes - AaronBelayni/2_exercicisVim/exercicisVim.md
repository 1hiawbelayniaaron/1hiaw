### Practicant amb vim

##### Exercici 1

Com es marca un bloc de línies al vim?    

**R: colocant-nos en mode VISUAL BLOCK (Ctrl + V) i amb les fletxes del teclat selecciones lo desitjat.**  

##### Exercici 2

Com es copia i s'enganxa el que s'ha marcat a un altre lloc amb el vim?

**R: seleccionem el que volem copiar, premem la tecla `y` per copiar-ho i premem la tecla `p` per enganxar-ho.**

##### Exercici 3

Com t'ho faries per passar de tenir el fitxer repventas.dat a repventas2.dat? (_hint_: un visual diferent dels anteriors)  **R: `Ctrl + v`**

##### Exercici 4

Com ens podem col·locar a una línia concreta? Per exemple la 15. 

**R: amb `Num+G` i tambè podem fer servir `Num+gg`.**

##### Exercici 5

Com es pot esborrar la línia on es troba el cursor?

**R: amb `dd`**

##### Exercici 6

Com es poden esborrar 10 línies? (la del cursor i 9 més)

**R: amb `10dd` on num seria el numero de linees que volem esborrar**

##### Exercici 7

Tinc un fitxer amb dues línies ja escrites, amb el vim obro el fitxer i creo una línia buida entre aquestes dues, que és el que passa si, en mode inserció, faig `Ctrl + e`? I si faig `Ctrl + y`?  
**R: si fem `Ctrl + e`  escriu la linea d'abaixi si fem `Ctrl + y` escriurà la linea que tenim a dalt.**

##### Exercici 8

Estem fent substitució al vim i utilitzem un paràmetre opcional que s'escriu al principi per indicar el rang de línies a les quals afecta la substitució.
Com s'indicaria que aquest rang fos de la línia 1 a la 3?

**R:**

```bash
:1,3s/patro/substitucio_del_patro/g
```

##### Exercici 9

El fitxer _Secret.txt_ s'ha corromput. Afortunadament s'ha detectat que on hi ha un _pitostu_ hauria d'haver una _e_ i just després on hi ha un _kekew_ hi hauria d'haver una _a_.
Un cop fet l'exercici no llegeixis el text sisplau.

**R: he arreglat el arxiu fent un `%s/pitostu/e/g`  i `:%s/kekew/a/g`**

##### Exercici 10

El fitxer _sonet.txt_ també s'ha corrumput, de fet es tracta d'un sonet i s'han barrejat certs blocs.
Heu de trobar l'ordre original utilitzant _delete/paste_ en mode _Visual_ i al menys un parell de registres/buffers. A més volem eliminar de _manera elegant_ les línies buides.

Segurament ja hareu mirat a la wikipedia l'estructura d'un sonet:

```
primer cuartet
segon cuartet
primer tercet
segon tercet
```

##### Exercici 11

Com podem passar a majuscules un text que està marcat en mode visual? I a minúscules?

##### Exercici 12

Si edito un fitxer de nom _.vimrc_ al meu _$HOME_ i el seu contingut tingués:

```
set tabstop=4
set number
set autoindent
```

Quines serien les *conseqüències*?

**R: el tabulador seria de 4 i el text d'autoidentaria**

##### Exercici 13

Si estic en mode ordre, quina tecla (o combinació de tecles) desfà l'últim canvi fet? i si estic en mode inserció? (en anglès es diu _undo_ _un-do_)

**R: amb la tecla `u` i per al mode inserció `Alt + u`**

##### Exercici 14

I quina tecla (o combinació de tecles) torna a deixar les coses com estaven? És a dir a des-fer el canvi en mode ordre? (en anglès es diu "redo": re-do, "undo the undos")  
**R:    `Ctrl+R`**

##### Exercici 15

És possible executar una ordre del bash si estem a dintre del vim? Si la resposta és afirmativa com ho podríem fer?

**R: Si, amb `!ordre_que_volem_executar`**

##### Exercici 16

Volem comentar un bloc de línies amb el vim. Com ho podem fer? (Hint: Bloc Visual + r]eplace)  
**R: en primer lloc anem a la primera linea, despres fem un `Ctrl + V` (Bloc visual) fins on vulguem comentar, despres fem `Shift + i`. es posarà en mode INSERT i posem # i despres li donem a ESC.**

##### Exercici 17

malauradament el nou acudit està al mig de la llista i per tant haurem de
modificar la numeració dels acudits.

Existeix una combinació de tecles en mode ordre que incrementi un número en una unitat?

I que el decrementi?

Existeix algun caràcter en mode ordre que repeteixi la darrera instrucció executada al vim?

Per exemple, com ho faries per numerar correctament aquest text combinant els comentat abans?

```
###### Acudit 1

- Domènech, on va tan rabent, circumspecte, esmaperdut i esparverat ?
- … anava a cagar, però ara vaig a buscar un diccionari.

###### Acudit 1

L’estrany cas de la paraula francesa FOIE.
S’escriu amb 3 de les 5 vocals i es pronuncia amb les 2 que falten.

###### Acudit 2
Els cinc símptomes de la mandra:
1)

###### Acudit 3
- És aquí el curs de viatgers del Temps?
- Va ser demà.
- D’acord, tornaré ahir.

###### Acudit 4
He decidit deixar de fer preguntes retòriques. Al capdavall, de què serveixen?

###### Acudit 5
"Esteu tots i totes acomiadats i acomiadades!" és llenguatge inclusiu o exclusiu?
```

**R:**

- Busquem el patró /Acudit

- anem al numero en qüestió i `Ctrl +a`

- prenem la tecla `n` per anar a la següent  ocurrencia.

- prenem la tecla `.` podem repetir l'ultima ordre executada

##### Exercici 18

Volem obrir una adreça URL o fins i tot un fitxer local amb la seva
trajectòria. Només he de col·locar el cursor a sobre de l'adreça i prémer 2
tecles alhora. Quines?

R: `Ctrl + Clic`