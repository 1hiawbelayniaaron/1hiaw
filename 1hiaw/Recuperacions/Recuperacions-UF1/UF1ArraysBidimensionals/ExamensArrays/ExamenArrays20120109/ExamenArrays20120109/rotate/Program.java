/*
 * Program.java        1.0 09/01/2012
 *
 * Models the program.
 *
 * Copyright 2012 NOM I COGNOMS <EMAIL>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class Program {

    /**
     * Rotates one position in a matrix forward or backwards.
     * 
     * @param a an array of integer numbers.
     * @param forward true if we want to rotate forward, false if we want to
     *        rotate backwards.
     */
    public void rotate(int[] a, boolean forward) {

    }
}
