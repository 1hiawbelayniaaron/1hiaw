//import java.util.Locale;
import java.util.Scanner;

public class ProgramTUIPrintf {

    /**
     * Swap two variables.
     * 
     * @param args Not used.
     */
    public static void main(String[] args) {
        int a = 3;
        double b = 3.75;
        char c = 'j';
        double d = 15.6987;
        double e = 15.698754;
        
        System.out.print("\n\nEXEMPLES D'ÚS DE print, println I printf\n\n");
        System.out.printf("Variable 'a'.Nombre enter: %d\n", a);
        System.out.printf("Variable 'b'.Nombre amb decimals: %f\n", b);
        System.out.printf("Variable 'c'.Caràcter: %c\n", c);
        System.out.printf("Variable 'd'.Nombre amb 15 espais: %+15f\n", d);
        System.out.printf("Variable 'd'.Nombre amb 15 espais omplint amb 0: %015f\n", d);
        System.out.printf("Variable 'e'.Nombre amb 2 decimals: %.2f\n", e);
        System.out.printf("Variable 'd'.Nombre amb 15 espais i 2 decimals: %15.2f\n", d);
        System.out.printf("Variable 'd'.Nombre amb 15 espais i 2 decimals omplint amb 0: %015.2f\n", d);
        System.out.printf("%30s Més text.\n", "Alineació a la dreta.");
        System.out.printf("%-30s Més text.\n", "Alineació a l'esquerra.");

        System.out.printf("\n\nEXEMPLE DE LLISTAT\n\n");
        System.out.println("------------------------------------------------------");
        System.out.printf("|%-30s|%-10s|%-10s|\n", "ARTICLE", "PREU", "DESCOMPTE");
        System.out.println("------------------------------------------------------");
        System.out.printf("|%-30s|%10.2f|%9.1f%%|\n", "Mel", 7.4, 3.);
        System.out.printf("|%-30s|%10.2f|%9.1f%%|\n", "Ous", 3., 4.12);
        System.out.printf("|%-30s|%10.2f|%9.1f%%|\n", "Poma", 2.2, 2.589);
        System.out.printf("|%-30s|%10.2f|%9.1f%%|\n", "Préssec", 2.199, 0.);
        System.out.printf("------------------------------------------------------\n\n");
    }
}