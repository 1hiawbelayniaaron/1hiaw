/*
 * ProgramTUI.java        1.0 Nov 30, 2021
 *
 * Models the program.
 *
 * Copyright 2021 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {

    /**
     * Calculate a total number of supplies
     * 
     * @return the total number 
     */
     public int totalComponents() {
       Scanner s = new Scanner(System.in);
       int dia = 0;
       String model = "";
       int hores = 0;
       int nComponents = 0;
       int tComponents = 0;
       
       while (dia != -1) {
         System.out.println("Dia : ");
         dia = s.nextInt();
         System.out.println("Model : ");
         model = s.nextLine();
         model = s.nextLine();
         System.out.println("Hores : ");
         hores = s.nextInt();
         System.out.println("Nombre Components : ");
         nComponents = s.nextInt();
         tComponents += nComponents;
       }
       return tComponents;
     }

     public static void main(String[] args) {
        ProgramTUI p = new ProgramTUI();
        System.out.println("Nombre total de components = " +  p.totalComponents());
    }
}
