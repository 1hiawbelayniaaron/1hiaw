/*
 * ProgramTUI.java        1.0 Oct 30, 2021
 *
 * Models the program.
 *
 * Copyright 2021 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {

   /**
   * Calculate the resistance factor of a vaccine for a period of time
   * 
   * @param initial number of infected
   * 
   */
   public void infection(double initialNumberInfected) {
     double percentatge = 1.5;
     double infectats = initialNumberInfected;
     String result = "";

     String any[] = {"primer","segon","tercer", "quart", "cinqué", "sisé"};
     String mes[] = {"Octubre","Novembre","Desembre","Gener","Febrer","Març"};
     int numeroAny = 0;
     while (percentatge <= 5) {
       result += "*************************************************************************************\n";
       result += "El nombre d'infectats al " + any[numeroAny] + " és de " + (infectats) + " persones.\n";
       result += "Amb un factor resistència del " + percentatge + "%.\n";
       result += "*************************************************************************************\n";
       
       for (int i = 0; i < 6; i++) {
         infectats += Math.round(infectats/100*percentatge);
         result += "Després del mes de " + mes[i] + " el nombre d'infectats és de " + (infectats) +
           " persones\n";
         
       }
       result += "*************************************************************************************\n\n\n";
       percentatge++;
       numeroAny++;
     }
     
     System.out.println(result);
      
   }
   
   
   
   public static void main(String[] args) {  
       Scanner s = new Scanner(System.in);
       ProgramTUI p = new ProgramTUI();
       int infectatsInici = 0;
       infectatsInici = s.nextInt();
       p.infection(infectatsInici);
       
}
}
