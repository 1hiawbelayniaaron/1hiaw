/*
 * ProgramTUI.java        1.0 Nov 30, 2021
 *
 * Models the program.
 *
 * Copyright 2021 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {


    /**
     * Calculate averge amount
     * 
     * @return a string with ALL the information of the bill
     */
     public String avergeAmount() {
       Scanner s = new Scanner(System.in);
       int dia = 0;
       String model ;
       int hores = 0;
       int nComponents = 0;
       int tComponents = 0;
       int preu = 0;
       int totalDies = 1;
       double importMitja = 0;
       int reparats = 0;
       String marca = "";
       String bill = "****************************************************************\nFactura\n";
       while (dia != -1) {
         System.out.println("Dia : ");
         dia = s.nextInt();
         if (dia == -1) {
         } else {
         bill += "Dia = " + dia;
         System.out.println("Quantitat de mobils reparats : ");
         reparats = s.nextInt();
    
         
         for (int i = 0; i< reparats; i++) {
         System.out.println("Model : ");
         model = s.nextLine();
         model = s.nextLine();
         System.out.println("Hores : ");
         hores = s.nextInt();
         
         
         if (model.equals("S")) {
           preu += 10 * hores;
           marca = "Samsung";
         } else if (model.equals("A")) {
           preu += 20 * hores;
           marca = "Apple";
           
           
         }
         
         System.out.println("Nombre Components : ");
         nComponents = s.nextInt();
         bill += "\n" + (i+1) + ") Reparats\n\t Marca = " + marca + "    Nombre de components = " +  nComponents + "\n";
         tComponents += nComponents;
         
         }
         totalDies++;

       }
       }
       importMitja =  preu / totalDies;
       
       bill += "****************************************************************\nTOTAL FACTURA\n";
         bill += "Import Mitjà = " + importMitja + "\n";
         bill += "Total Components = " + tComponents;
       
       
       return bill;
     }
     
     
     
     
     

     public static void main(String[] args) {
     ProgramTUI p = new ProgramTUI();
     System.out.println(p.avergeAmount());
     
    }
}
