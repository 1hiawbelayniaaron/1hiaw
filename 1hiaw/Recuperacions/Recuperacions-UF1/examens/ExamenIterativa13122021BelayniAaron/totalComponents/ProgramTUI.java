/*
 * ProgramTUI.java        1.0 Nov 30, 2021
 *
 * Models the program.
 *
 * Copyright 2021 Rafel Botey Agusti <rbotey@escoladeltreball.org>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */
import java.util.Scanner;
public class ProgramTUI {

    /**
     * Calculate a total number of supplies
     * 
     * @return the total number 
     */
     public int totalComponents() {
     int dia = 0;
     char model;
     int hores;
     int components = 0;

     Scanner sc = new Scanner(System.in);
     while (dia != -1) {
     
     //dia
     System.out.println("Dia ");
     dia = sc.nextInt();
     
     //model
     System.out.println("Model ");
     model = sc.next().charAt(0);
     
     //hores
     System.out.println("Hores ");
     hores = sc.nextInt();
     
     //components
     
     System.out.println("Nombre de components ");
     hores = sc.nextInt();
     components += components;
     }
     return components;
     }

     public static void main(String[] args) {
         ProgramTUI p = new ProgramTUI();
         
         System.out.println("Nombre total de components = " + p.totalComponents() );       
    }
}
