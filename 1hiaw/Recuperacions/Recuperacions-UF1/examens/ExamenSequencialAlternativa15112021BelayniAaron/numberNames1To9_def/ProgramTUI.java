

import java.util.Scanner;


public class ProgramTUI {
     
    
     /**
     * Write a number on Catalan 1 to 9 numbers
     * 
     * @param number The number to write
     * @return The number written in catalan
     */
    public String numberNames1To9(int number) {
        String solucio;
        if (number == 1) {
            solucio = "u";
        } else if (number == 2) {
            solucio = "dos";
        } else if (number == 3) {
            solucio = "tres";
        } else if (number == 4) {
            solucio = "quatre";    
        } else if (number == 5) {
            solucio = "cinc";
        } else if (number == 6) {
            solucio = "sis";
        } else if (number == 7) {
            solucio = "set";
        } else if (number == 8) {
            solucio = "vuit";
        } else {
            solucio = "nou";
        }
       return solucio;
      
}
 
     /**
     * Write a number on Catalan  20 to 99
     * 
     * @param number The number to write
     * @return The number written in catalan 20 to 99
     */
    public String numberNames(int number) {
      String solucio = "";
      if (number == 20) {
        solucio = "vint";
      } else if (number > 20 && number < 30) {
        solucio = "vint-i-" + numberNames1To9(number-20);  
      } else if (number == 30) {
        solucio = "trenta";
      } else if (number > 30 && number < 40) {
        solucio = "trenta-" + numberNames1To9(number-30);
      } else if (number == 40) {
        solucio = "quaranta";
      }  else if (number > 40 && number < 50) {
        solucio = "quaranta-" + numberNames1To9(number-40);
      } else if (number == 50) {
        solucio = "cinquanta";
      }  else if (number > 50 && number < 60) {
        solucio = "cinquanta-" + numberNames1To9(number-50);
      } else if (number == 60) {
        solucio = "seixanta";
      }  else if (number > 60 && number < 70) {
        solucio = "seixanta-" + numberNames1To9(number-60);
      } else if (number == 70) {
        solucio = "setanta";
      }  else if (number > 70 && number < 80) {
        solucio = "setanta-" + numberNames1To9(number-70);
      } else if (number == 80) {
        solucio = "vuitanta";
      }  else if (number > 80 && number < 90) {
        solucio = "vuitanta-" + numberNames1To9(number-80);
      } else if (number == 90) {
        solucio = "noranta";
      }  else if (number > 90 && number < 100) {
        solucio = "nortanta-" + numberNames1To9(number-90);
      } 
      return solucio;
      
    }
        
    
    
    public static void main(String args[]) {
     System.out.print("digues un numero entre 20 i 99 ? ");  
     Scanner s = new Scanner(System.in);
     int nombreInicial = 0;
     nombreInicial = s.nextInt();
     ProgramTUI p = new ProgramTUI();
     System.out.println(p.numberNames(nombreInicial));
    }
}
