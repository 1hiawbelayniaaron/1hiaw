/* 
 * Program.java         
 * 
 * Write a number on Catalan 1 to 9 numbers. 
 * 
 * Copyright 2021 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com> 
 * 
 * This is free software, licensed under the GNU General Public License v3. 
 * See http://www.gnu.org/licenses/gpl.html for more information. 
 */ 
 
public class Program {
 
    /**
     * Write a number on Catalan 1 to 9 numbers
     * 
     * @param number The number to write
     * @return The number written in catalan
     */
    public String numberNames1To9(int number) {
        String solucio;
        if (number == 1) {
            solucio = "u";
        } else if (number == 2) {
            solucio = "dos";
        } else if (number == 3) {
            solucio = "tres";
        } else if (number == 4) {
            solucio = "quatre";    
        } else if (number == 5) {
            solucio = "cinc";
        } else if (number == 6) {
            solucio = "sis";
        } else if (number == 7) {
            solucio = "set";
        } else if (number == 8) {
            solucio = "vuit";
        } else {
            solucio = "nou";
        }
       return solucio;
    }    
}
