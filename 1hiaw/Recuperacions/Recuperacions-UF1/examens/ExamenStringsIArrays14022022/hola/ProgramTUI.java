/*
 * Program.java          1.0  14/12/2021
 * 
 * Realitzeu un algorisme que permeti mostrar per pantalla les lletres
 * H O L A mitjançant asteriscs, tal com es mostra a continuació:
 * 
 * Les lletres, s'emmagatzemaran en matrius de NxN de tipus caràcter i cal recórrer aquestes, 
 * posició a posició mitjançant estructures iteratives per tal de construir-les. El valor de
 * N podrà ser 5 o bé 7. Aquest valor N ens indicarà la grandària de la lletra. En la 
 * figura anterior el valor de N és 5. En la següent figura el valor de N és 7:
 * 
 * Copyright 2021 Claudio Nicolás Díaz Pino <1hiaw.diaz.pino.claudio.nicolas@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

import java.util.Scanner;

public class ProgramTUI {

 /**
 * creates white character
 * 
 * @param array containing white character 
 */
 public void buildWhiteLetter(char letter[][]) {
     // creo la matriu NxN amb caracters espai
     int arrayLength = letter.length;
     for (int i = 0; i < arrayLength; i++) {
         for (int j = 0; j < arrayLength; j++) {
             letter[i][j] = ' ';
         }
     }
 }

 /**
 * shows a letter
 * 
 * @param array containing a letter 
 */ 
 public void viewLetter(char letter[][]) {
     // Escric per pantalla
     int arrayLength = letter.length;
     for (int i = 0; i < arrayLength; i++) {
         for (int j = 0; j < arrayLength; j++) {
             System.out.printf("%c", letter[i][j]); 
         }
         System.out.printf("\n");
     }
 }

 /**
 * builds the letter H
 * 
 * @param array containing a letter H 
 */
 public void buildLetterH(char letter[][]) {
     // recorro la matriu i posso '*' en certes condicions
     int arrayLength = letter.length;
     for (int i = 0; i < arrayLength; i++) {
         for (int j = 0; j < arrayLength; j++) {
             // per la primera columna , per la ultima columna , i tota la fila d'enmig
             if ( j == 0 || j == arrayLength - 1 || i == (arrayLength - 1) / 2 ) {
                 letter[i][j] = '*';
             }
         }
     }
 }

 /**
 * builds the letter O
 * 
 * @param array containing a letter O 
 */
 public void buildLetterO(char letter[][]) {
     // recorro la matriu i posso '*' en certes condicions
     int arrayLength = letter.length;
     for (int i = 0; i < arrayLength; i++) {
         for (int j = 0; j < arrayLength; j++) {
             // escric * a la primera i ultima fila i columna
             if ( j == 0 || j == arrayLength - 1 || i == 0 || i == arrayLength - 1 ) {
                 letter[i][j] = '*';
             }
         }
     }
 }

 /**
 * builds the letter A
 * 
 * @param array containing a letter A 
 */
 public void buildLetterA(char letter[][]) {
     // recorro la matriu i posso '*' en certes condicions
     int arrayLength = letter.length;
     for (int i = 0; i < arrayLength; i++) {
         for (int j = 0; j < arrayLength; j++) {
             // per la primera fila, i la fila un index mes gran que la meitat, la primera columna, i la ultima
             if ( j == 0 || j == arrayLength - 1 || i == (arrayLength - 1) / 2 + 1 || i == 0) {
                 letter[i][j] = '*';
             }
         }
     }
 }
     
 /**
 * builds the letter L
 * 
 * @param array containing a letter L
 */
 public void buildLetterL(char letter[][]) {
     // recorro la matriu i posso '*' en certes condicions
     int arrayLength = letter.length;
     for (int i = 0; i < arrayLength; i++) {
         for (int j = 0; j < arrayLength; j++) {
             // per la primera columna i la ultima fila
             if ( j == 0 || i == arrayLength - 1) {
                 letter[i][j] = '*';
             }
         }
     }
 }
     
 public static void main(String[] args) {
  ProgramTUI p = new ProgramTUI();
  int SIZE = 7;
  char letter [][] = new char [SIZE][SIZE];   // Matriu de longitud senar, mínim 5X5
  p.buildWhiteLetter(letter);
  p.buildLetterH(letter);
  p.viewLetter(letter);
  System.out.print("\n\n");
  p.buildWhiteLetter(letter);
  p.buildLetterO(letter);
  p.viewLetter(letter);
  System.out.print("\n\n");
  p.buildWhiteLetter(letter);
  p.buildLetterL(letter);
  p.viewLetter(letter);
  System.out.print("\n\n");
  p.buildWhiteLetter(letter);
  p.buildLetterA(letter);
  p.viewLetter(letter);
 }
}
