/*
 * Program.java          1.0  14/12/2021
 * 
 * Realitzeu un algorisme que, donada una cadena determini quins
 * són els percentatges de cada vocal sobre el total de caràcters no blancs de la frase i
 * determini també quins són els percentatges de cada vocal sobre el total de caràcters
 * de cada una de les paraules.
 * Suposeu que no hi ha accents i tingueu en compte que els caràcters 'a' i 'A' són la
 * mateixa vocal, 'e' i 'E' són la mateixa vocal, etc.
 * 
 * Copyright 2021 Claudio Nicolás Díaz Pino <1hiaw.diaz.pino.claudio.nicolas@gmail.com>
 * 
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class ProgramTUI {
        
 /**
 * Calculate the percentage of vowels in a sentence and
 * calculate the percentage of vowels in a word
 * 
 * @param string where the percentages of the vowels are calculated
 * @return array with all the percentages
 */
 public String[][] getVowelsPercentage(String s) {
  String[] paraules = s.split(" +"); // divideixo en array de paraules dividides per espais
  char[] vocals = {'A','E','I','O','U'};
  int numParaules = paraules.length;
  String [][] percentage = new String [paraules.length + 1][6]; // creo la matriu arrel del num de paraules
  s = s.replace(" ","");
  percentage[0][0] = s; // li trec els espais a la unica frase que conte espais, la primera
  String aux = "";
  
  
  
  /*
  int count = 0;
  // guardem les paraules a la primera columna
  for (int i = 1, j = 0;i < percentage.length;i++,j++) {
    percentage[i][0] = paraules[j];
  }
  
  // cada paraula la passem a mayuscula
  char actual ;
  for (int i = 0; i < paraules.length; i++) {
    aux = paraules[i].toUpperCase();
    for (int k = 0; k < vocals.length;k++) { //
      for (int j = 0; j < aux.length();j++) {
        if (aux.charAt(j) == vocals[k]) {
          count++;
        }
          percentage[i][k+1] = String.format("%f",((count/(double)aux.length())*100));
        }
      }
    } 
  
  
  */
  
  
  for (int i = 1, j = 0; i < percentage.length; i++, j++) { // assigno les paraules a la nova matriu
      percentage[i][0] = paraules[j];
  }
  
  
  
  for (int i = 0; i < percentage.length; i++) { // per cada paraula
      
      aux = percentage[i][0].toUpperCase(); // transformo en mayuscules per la comparacio i el compte
      for (int j = 0; j < vocals.length; j++) { // per cada vocal, veig quants cops apareix a la paraula
           double count = 0;
          for(int k = 0; k < aux.length(); k++) { //per cada caracter de la paraula, comparo amb la vocal
              if (vocals[j] == aux.charAt(k)) count++; // sumo a count el total de vegades que surt la vocal
          }
          percentage[i][j + 1] =  String.format("%.2f",(count/(double)aux.length()*100));    // String.format("%4.2f", count/(double)aux.length()*100); 
          // mitjançant format, assigno a la posicio corresponent, el percentatge d'aparicio de cada vocal
      }
  }
  return percentage;
 }

 /**
 * Prints the percentages of the phrase's vowels or word's vowels
 * 
 * @param  array with all the percentages
 * @param string where the percentages of the vowels are calculated
 * @return array with all the percentages
 */
 public void printSolution(String [][] percentage, String s) {
     System.out.printf("********************************************\n");
     //System.out.printf("%s\n", s); 
     // imprimeixo el String s que em donen, ja que laltre lhe modificat treient els espais
     String vocals = "AEIOU";
     String [][] resultat = percentage;
     
     for (int i = 0; i < resultat.length; i++) {
         
         
         for (int j = 0; j < resultat[0].length; j++) {
           //  if(i != 0 || j != 0) { // no faig servir la frase sense espais de la matriu, q es la posiicio 0,0
                 if(j == 0) System.out.printf("%s \n", resultat[i][j]); 
                 else {
                     if(j == 1) {
                         System.out.printf("%9c %9c %9c %9c %9c \n", vocals.charAt(0), vocals.charAt(1),
                                           vocals.charAt(2),vocals.charAt(3),vocals.charAt(4));
                         System.out.printf("%9s ", resultat[i][j]);
                     } else System.out.printf("%9s ", resultat[i][j]);
                 }
            // }

         }
         System.out.printf("\n");
         
     }
     System.out.printf("\n");
     
 }
 

 /**
 * Main program 
 * 
 * @param args Not used
 */
 public static void main(String[] args) {
  String s1 = "Hola Anna!";
  String s2 = "aeiou       AEIOU";
  String s3 = "Programar Es Divertit!!!";
  ProgramTUI p = new ProgramTUI();
  p.printSolution(p.getVowelsPercentage(s1),s1);
  p.printSolution(p.getVowelsPercentage(s2),s2);
  p.printSolution(p.getVowelsPercentage(s3),s3);
 }
}
