﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<title>Activitat 2 XSLT</title>
	</head>
	<body>
		<table border="1" align="center">
			<th colspan="4" align="center" bgcolor="#BDBDBD">COMANDA</th>
			<tr bgcolor="#BDBDBD">
				<th align="center">Nom</th>
				<th align="center">Adreça</th>
				<th align="center">Ciutat</th>
				<th align="center">C.P.</th>
			</tr>
			<tr>
				<td align="center" bgcolor="#0174DF"><b><xsl:value-of select="Pedido/Destino/Nombre" /></b></td>
				<td align="center"><xsl:value-of select="Pedido/Destino/Direccion" /></td>
				<td align="center"><xsl:value-of select="Pedido/Destino/Ciudad" /></td>
				<td align="center"><xsl:value-of select="Pedido/Destino/CodPostal" /></td>
			</tr>
			<tr>
				<td colspan="4">&#160;</td>
			</tr>
			<tr>
				<td colspan="4" align="center">
						LLista amb "Precio &gt; 25" i "Precio &lt;&#61; 100" <br/><br/>
          
				</td>
			</tr>
			<tr>
				<th colspan="2" align="center">Producte</th>
				<th align="center">Preu</th>
				<th align="center">Quantitat</th>
			</tr>
			<xsl:for-each select="Pedido/Contenido/Producto">
				<xsl:sort select="Nombre"/>
				<xsl:if test="Precio &gt; 25 and Precio &lt;&#61; 100">
					<tr>
						<xsl:choose>
							<xsl:when test="Precio &gt; 25 and Precio &lt; 50">
								<td colspan="2">
									<xsl:for-each select="Nombre">
										<xsl:value-of select="." /> (codi = <xsl:value-of select="../@codigo" />)
										<br/>
									</xsl:for-each>
									
								</td>
								<td align="center"><xsl:value-of select="Precio" /></td>
								<td bgcolor="yellow" align="center"><xsl:value-of select="Cantidad" /></td>
							</xsl:when>
							<xsl:when test="Precio &gt; 50 and Precio &lt; 75">
							
								<td colspan="2">
									<xsl:value-of select="Nombre" /> (codi = <xsl:value-of select="./@codigo" />)
								</td>
								
								<td align="center"><xsl:value-of select="Precio" /></td>
								
								<td bgcolor="green" align="center"><xsl:value-of select="Cantidad" /></td>
							</xsl:when>
							<xsl:otherwise>
								<td colspan="2">
									<xsl:value-of select="Nombre" /> (codi = <xsl:value-of select="./@codigo" />)
								</td>
								
								<td align="center"><xsl:value-of select="Precio" /></td>
								<td bgcolor="red" align="center"><xsl:value-of select="Cantidad" /></td>
							</xsl:otherwise>
						</xsl:choose>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</table>
	</body>
</html>
</xsl:template>
</xsl:stylesheet>

