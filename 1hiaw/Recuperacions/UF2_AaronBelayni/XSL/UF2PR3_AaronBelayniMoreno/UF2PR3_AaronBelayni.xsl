﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
<xsl:template match="/">
<html>
	<head>
		<title>Activitat 3 XSLT</title>
	</head>
	<body>
		<table border="1" align="center" style="width:60%">
			<xsl:for-each select="bookstore/book">
			<xsl:sort select="@category"/>
				<tr>
					<td>
						<table border="0" style="width:100%">
							<tr>
								<td colspan="2" >
									      <b>Categoria:</b>&#160;<xsl:value-of select="@category"/>
								</td>
							</tr>
							<tr>
								<td>
								      	Títol (idioma: <xsl:value-of select="title/@lang"/>)
								</td>
								      <td style="text-align:right">
									<xsl:value-of select="title"/>
								</td>
							</tr>
							<tr>
								<td>
									Any
								</td>
						      		<td style="text-align:right">
									<xsl:value-of select="year"/>
								</td>
							</tr>
							<tr>
								<td>
									Preu
								</td>
								<td style="text-align:right">
									<xsl:value-of select="price"/>
								</td>
							</tr>
							<tr>
								<td colspan="2" style="text-align:center;background-color:green">
									<b>Autors:</b>
								</td>
							</tr>
							<tr>
					    			<td colspan="2">
									<xsl:for-each select="author">
									<xsl:sort select="."/>
										<xsl:value-of select="."/> <br/>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			    </xsl:for-each>
		</table>
	  </body>
</html>
    </xsl:template>
</xsl:stylesheet>

