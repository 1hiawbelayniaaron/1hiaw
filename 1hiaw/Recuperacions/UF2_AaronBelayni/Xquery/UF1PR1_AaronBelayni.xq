



(:1a:)
//vehicle[any=2010]
(:2a:)
//marca_model
(:3a:)
//vehicle[any<2010]/marca_model
(:4a:)
//vehicle[any > 2008 and any < 2011]/marca_model
(:5a:)
for $i in doc("tenda.xml")/tenda/vehicles/vehicle
let $j:=$i/any
let $k:=$i/marca_model
where $j > 2008 and $j < 2011
order by $k
return $k

(:6a:)
for $i in doc("tenda.xml")/tenda/vehicles/vehicle
let $j:=$i/any
let $k:=$i/marca_model
where $j > 2007 
order by number ($j) descending
return $k
(:7a:)
for $i in doc("tenda.xml")/tenda/vehicles/vehicle
let $j:=$i/preu
order by number ($j)
return data($j)

(:8a:)
for $i in doc("tenda.xml")/tenda/vehicles/vehicle
let $j:=$i/preu
order by number ($j)
where $j > 10000
return data($j)
(:9a:)
let $i:=  doc("tenda.xml")/tenda/vehicles/vehicle
let $j:=$i/preu
return avg ($j)

(:10a:)
let $i:=  doc("tenda.xml")/tenda/vehicles/vehicle
let $j:=$i[preu> 10000 and preu < 20000]/preu
return avg($j)
