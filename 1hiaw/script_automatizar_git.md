## Automatització del git push amb crontab

1. En primer lloc necessitarem crear un script que comprovi  l'estat del repositori de git

```bash
#!/bin/bash
# Filename:        git_push.sh
# Author:        iaw49656032
# Date:            17/12/2021
# Version:        0.1
# License:        This is free software, licensed under the GNU General Public License v3.
#                See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:        git_push.sh [arg1...]
# Description:        L'script comprova si hi ha canvis a dintre del repositori del git en qüestió                 de manera que si hi ha s'encarrega d'actualitzar aquests canvis al nuvol



# comprovem si hi ha canvis en el repositori
if [ $(git status --porcelain | wc -l) -eq "0" ]; then
  # no hi ha canvis

  echo "  🟢 arxius actualitzats."
else
  # hi ha canvis
    echo -e "  🔴 s'ha de pujar els arxius"
    git add *
    git commit -m "Update documentation ($(date +%F' // '%R))"
    git push origin main
fi
```

2. Després en segona instancia caldrà afegir un crontab amb l'usuari amb que utilitzem el repositori de git

```bash
# Amb l'ordre següent obrim l'arxiu de crontab de l'usuari en qüestió
crontab -e -u iawXXXXXXXX


# Un cop obert el document de crontab per al nostre usuari 
# afegim una nova linea que s'ajusti al temps que requerim
20 * * * * cd /home/users/inf/hiaw1/iaw49656032/Documents/1hiaw/ &&  ./push.sh


# Finalment per comprovar que el crontab d'ha afegit correctament
crontab -l -u iawXXXXXXXX
```

      Per ajustar el temps en el que volem que s'executi el crontab us deixo aquesta       pàgina web :  [generador de crontabs](https://crontab-generator.org/)



3. Si volem comprovar com funciona fem que el crontab s'executi cada minut i fem un canvi al repositori de git, esperem un minut i fem `git status`.


