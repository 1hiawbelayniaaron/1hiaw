/* 
 * Program.java        1.0 Oct 30, 2022 
 * 
 * Models the program. 
 * 
 * Copyright 2022 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com> 
 * 
 * This is free software, licensed under the GNU General Public License v3. 
 * See http://www.gnu.org/licenses/gpl.html for more information. 
 */
import java.util.Scanner;
public class ProgramTUI {
  
  /**
   * the taxpayer calculates rates
   * @param the taxpayer's income
   * @param marital status of the taxpayer 
   * @return Returns to pay rates
   */
  public double taxReturn(double income, int status) {  
    double percentage = 0;
    double aux = 0;
    double incomeSolve = 0;
    
    
    if (status == 1) {
      // franja 1
      if (income >= 0) { 
        aux += 0.15*20000;

      }
        // franja 2
        if (income > 20000) {
        incomeSolve = income - 20000;
        aux += 0.28*incomeSolve;

      }
        
        // franja 3
        if (income > 50000){
        incomeSolve = income - 50000;
        aux += 0.31*incomeSolve;

      } 
    } else if (status == 0) {
      // franja 1
      if (income >= 0) { 
        aux += 0.15*30000;

      }
        // franja 2
        if (income > 30000) {
        incomeSolve = income - 30000;
        aux += 0.28*incomeSolve;

      }
        
        // franja 3
        if (income > 80000){
        incomeSolve = income - 80000;
        aux += 0.31*incomeSolve;

      } 
    }
    return aux;
  }
   
  /**
   * calculates rates.
   * 
   * @param args Not used.
   */
  public static void main(String[] args) {
    ProgramTUI p = new ProgramTUI();
    Scanner sc = new Scanner(System.in);
    char euro = (char)0x20AC;
    String str = "casat";
    System.out.printf("(0)-Casat o (1)-solter ? \n");
        int status = sc.nextInt();
    System.out.printf("Ingressos ? \n");
    double income = sc.nextDouble();
     double taxes = p.taxReturn(income,status);
     if (status == 1) {
       str = "solter";
     }
    System.out.printf("*************************************************************\n");
    System.out.printf("Ingressos \t\t Taxes\t\tEstat Civil\n");
    System.out.printf("%.2f%c  \t %.2f%c \t\t %s\n",income,euro,taxes,euro,str);
    System.out.printf("*************************************************************\n");
  }
 
}
