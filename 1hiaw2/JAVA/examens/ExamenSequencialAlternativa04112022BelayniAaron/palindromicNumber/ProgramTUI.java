/*
 * Program.java        1.0 Oct 30, 2022
 *
 * Models the program.
 *
 * Copyright 2022 Aaron Belayni Moreno <1hiawbelayniaaron@gmail.com>
 *
 * This is free software, licensed under the GNU General Public License v3.
 * See http://www.gnu.org/licenses/gpl.html for more information.
 */

public class ProgramTUI {
  
    /**
     * A palindromic number number
     * 
     * @param a number
     * @return true if is a palindromic number
     */
    public boolean palindromicNumber(long number) {
       boolean isPalindromicNumber = false;
       long aux = 0;
       long primer = number / 10000;
       aux = number - primer*10000;
       long segon = aux / 1000;
       aux -= segon*1000;
       long tercer = aux / 100;
       aux -= tercer*100;
       long quart = aux / 10;
       aux -= quart*10;
       long cinque = aux;
       
       
       if (primer == cinque && segon == quart){
         isPalindromicNumber = true;
       }
       return isPalindromicNumber;
    }
    
   /**
   * A palindromic number number
   * 
   * @param args Not used.
   */               
    public static void main(String[] args) {
      ProgramTUI p = new ProgramTUI();
      System.out.println(p.palindromicNumber(12321)); // true
      System.out.println(p.palindromicNumber(12341)); // false
    }
}


