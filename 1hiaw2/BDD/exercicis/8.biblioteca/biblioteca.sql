
\c template1
drop database if exists biblioteca;
create database biblioteca;
\c biblioteca

CREATE TABLE libro(
	ISBN INT,
	idioma varchar(50),
	titulo varchar (50),
	CONSTRAINT PK_LIBRO_ISBN PRIMARY KEY (ISBN)
);

CREATE TABLE autor(
	idAutor varchar(10),
	nombre varchar(50) NOT NULL,
	nacionalidad varchar(50),
	CONSTRAINT PK_AUTOR_idAutor PRIMARY KEY (idAutor)
);

CREATE TABLE libroXautor(
	ISBN INT,
	idAutor varchar(10),
	CONSTRAINT PK_LIBROXAUTOR PRIMARY KEY (ISBN, idAutor),
	CONSTRAINT FK_LIBROXAUTOR_ISBN FOREIGN KEY (ISBN) REFERENCES libro (ISBN),
	CONSTRAINT FK_LIBROXAUTOR_idAutor FOREIGN KEY (idAutor) REFERENCES autor (idAutor)
);

CREATE TABLE editorial(
	idEditorial int,
	nombre varchar(50) NOT NULL,
	direccion varchar(100),
	ciudad varchar(50),
	pais varchar(30),
	CONSTRAINT PK_EDITORIAL_idEditorial PRIMARY KEY (idEditorial)
);

CREATE TABLE libroXeditorial(
	ISBN INT,
	idEditorial int,
	CONSTRAINT PK_LIBROXEDITORIAL PRIMARY KEY (ISBN, idEditorial),
	CONSTRAINT FK_LIBROXEDITORIAL_ISBN FOREIGN KEY (ISBN) REFERENCES libro (ISBN),
	CONSTRAINT FK_LIBROXEDITORIAL_idEditorial FOREIGN KEY (idEditorial) REFERENCES editorial (idEditorial)
);

CREATE TABLE tema(
	idTema int,
	CONSTRAINT PK_TEMA_idTema PRIMARY KEY (idTema)
);

CREATE TABLE libroXtema(
	ISBN INT,
	idTema int,
	CONSTRAINT PK_LIBROXTEMA PRIMARY KEY (ISBN, idTema),
	CONSTRAINT FK_LIBROXTEMA_ISBN FOREIGN KEY (ISBN) REFERENCES libro (ISBN),
	CONSTRAINT FK_LIBROXTEMA_idTema FOREIGN KEY (idTema) REFERENCES tema (idTema)
);

CREATE TABLE ejemplar(
	idEjemplar INT,
	caracteristica varchar(100),
	ISBN INT,
	CONSTRAINT PK_EJEMPLAR_idEjemplar PRIMARY KEY (idEjemplar)
);

CREATE TABLE socio(
	DNIsocio varchar(9),
	nombre varchar(50),
	direccion varchar(50),
	telefono varchar(15),
	titulacion varchar(30),
	tipo varchar(30),
	CONSTRAINT PK_SOCIO_DNIsocio PRIMARY KEY (DNIsocio),
	CONSTRAINT CK_SOCIO_tipo check(tipo='alumno' OR tipo='alumno de doctorado' OR tipo='profesor')
);

CREATE TABLE plazo(
	codPlazo int, 
	CONSTRAINT PK_PLAZO_codPlazo PRIMARY KEY (codPlazo)
);

CREATE TABLE prestamo(
	idEjemplar INT,
	codPlazo int,
	DNIsocio varchar(9),
	CONSTRAINT PK_PRESTAMO PRIMARY KEY (idEjemplar,codPlazo),
	CONSTRAINT FK_PRESTAMO_idEjemplar FOREIGN KEY (idEjemplar) REFERENCES ejemplar (idEjemplar),
	CONSTRAINT FK_PRESTAMO_codPlazo FOREIGN KEY (codPlazo) REFERENCES plazo (codPlazo),
	CONSTRAINT FK_PRESTAMO_DNIsocio FOREIGN KEY (DNIsocio) REFERENCES socio (DNIsocio)
);
	
	
