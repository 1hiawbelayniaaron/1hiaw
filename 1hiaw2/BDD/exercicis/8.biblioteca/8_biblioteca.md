# 8. Biblioteca

Se trata de informatizar un proceso manual de getión de los préstamos de los libros de una biblioteca. Actualmente se tienen fichas de dos tipos :

- fichas en donde se recogen las características de los libros Titulo, Autor/es, Editorial, Año de publicación,
- fichas relativas a los préstamos que se han efectuado , título del libro, la persona a la cuál se le ha prestado, la fecha de préstamo y la de devolución.
  Además de estas fichas se ha recabado información sobre el sistema deseado mediante el conjunto de entrevistas con los usuarios que se puede resumir de la siguiente forma:
  Para los libros interesa saber, además de lo que aparece actualmente en las fichas, el idioma en el que están escritos. Además, se deseará hacer búsquedas en función de palabras clave, de forma que podremos encontrar un libro a través de diferentes palabras clave. Por ejemplo, el libro Sistemas y bases de datos sería un de los libros que nos saldría en la consulta por palabra clave “modelo semántico”.
  Cada ejemplar del libro tendrá un codigo que será único y además se recogerán características propias (tiene la tapa rota, hace falta encuadernar, etc). Cada libro tratará de uno o varios temas, lo que interesa reflejar para poder realizar consultas del tipo “Libros o artículos que tenemos acerca de Bases de datos Multimedia”, “Artículos que podemos consultar sobre el lenguaje QUEL”, etc.
  Los temas se pueden dividir en subtemas y así sucesivamente (no se sabe en cuantos niveles). Por ejemplo, en el tema de DISEÑO podemos distinguir una serie de subtemas, como DISEÑO CONCEPTUAL, DISEÑO LÓGICO, DISEÑO FÍSICO, DISEÑO GRÁFICO, DISEÑO ASISTIDO POR ORDENADOR, etc.Cada tema tendrá un nombre y una descripción.
  De los autores, además del nombre, nos interesa conocer su nacionalidad. De las editoriales almacenaremos su nombre , dirección, ciudad y país.
  En la biblioteca queremos distinguir tres tipos de socios: alumnos, a los que a lo sumo se les prestará una obra durante tres días, alumnos de doctorado o proyectos de fin de carrera, que tendrán acceso como máximo a dos obras durante la semana y los profesores y otras bibliotecas, a los que se les dejará tres obras durante un plazo máximo de un mes.
  De cada uno de ellos almacenaremos su DNI, Nombre, Dirección, Telefono, Titulación en la que están estudiando o impartiendo clases.
  Los codigos serán los propios de nuestro sistema, excepto el DNI y el ISBN. Se tiene que contemplar la mayor cantidad de información posible (lo más cercano a la realidad).
  Proponer un esquema lógico así como su diagrama referencial

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[caso biblioteca](https://drive.google.com/file/d/1fS2ZAufyoA2RogEO5SUKCmNnBMwgTPbS/view?usp=sharing)

## 2.2. Esquema conceptual (EC ó ER)

<img title="" src="./biblioteca-definitiu.png" alt="caso publicidad" height="" width="703">

# 3. Model lògic relacional

## 3.1. Esquema lògic

  Socio(<ins>idSocio</ins>,DNI,nombre,direccion,telefono,titulacion,tipo)    

  Fecha(<u><ins>idFecha</ins></u>,fechaPrestamo,fechaDevuelta)  

  Ejemplar(<u><ins>idEjemplar</ins></u>,caracteristicas,  ISBN)   

  Reserva(<u><ins>idEjemplar, idFecha</ins></u>, idSocio, fechaDevolucion)  

  Libro(<u><ins>ISBN</ins></u>, titulo, autor, año, idioma)  

Autor(<u><ins>idAutor</ins></u>, nombre, nacionalidad)

AutorXTema(<u><ins>idAutor,ISBN</ins></u>)

Tema(<u><ins>idTema</ins></u>, nombre, descripcion)

LibroXTema(<u><ins>ISBN,IdTema</ins></u>)

Editorial(<u><ins>idEditorial</ins></u>, nombre, direccion, ciudad, pais)

LibroXEditorial(<u><ins>ISBN,idEditorial</ins></u>)

  \...

## 3.2. Diagrama referencial

| Relació referencial | Clau aliena                  | Relació referida     |
| ------------------- |:----------------------------:| -------------------- |
| Ejemplar            | ISBN                         | LIBRO                |
| Reserva             | idSocio, idEjemplar, idFecha | Socio,Ejemplar,Fecha |
| AutorXTema          | idAutor,ISBN                 | Autor, Libro         |
| AnuncioXSoporte     | idAnuncio                    | Anuncio              |
| LibroXEditorial     | ISBN, idEditorial            | Libro, Editorial     |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script publicidad.sql](./museus.sql)
