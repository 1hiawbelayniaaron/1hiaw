-- AARON BELAYNI 
-- a211762hb

-- A. Consultas Simples

-- 1. Obtener los datos de los productos cuyas existencias estén entre 25 y 40 unidades.
SELECT *
FROM producto
WHERE exist BETWEEN 25 AND 40;

-- 2. Obtener los códigos de los representantes que han tomado algún pedido (evitando su repetición).
SELECT DISTINCT repcod
FROM repventa;

-- 3. Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111.
SELECT * 
FROM pedido
WHERE cliecod = 2111;

-- 4. Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111 y que han sido tomados por el representante cuyo código es el 103.
SELECT * 
FROM pedido
WHERE cliecod = 2111 AND repcod = 103;

-- 5. Obtener los datos de los pedidos realizados por el cliente cuyo código es el 2111, que han sido tomados por el representante cuyo código es el 103 y que solicitan artículos del fabricante cuyo código es ACI.
SELECT *
FROM pedido
WHERE cliecod = 2111 AND repcod = 103 AND UPPER(fabcod) = 'ACI';

-- 6. Obtener una lista de todos los pedidos ordenados por cliente y, para cada cliente, ordenados por la fecha del pedido (ascendentemente)
SELECT pednum, cliecod, fecha
FROM pedido
ORDER BY fecha, cliecod ASC;

-- 7. Obtener los datos de los representantes que pertenecen a la oficina de código 12 y 13 (cada representante solo pertenece a una oficina).
SELECT *
FROM repventa
WHERE ofinum BETWEEN 12 AND 13;

-- 8. Obtener los datos de productos de los que no hay existencias o bien éstas son desconocidas.
SELECT *
FROM producto
WHERE exist = 0 OR exist = NULL;

-- 9. Mostrar los representantes que fueron contratados en el 2003 (sumem 5000 a la data de contracte)
SELECT *, fcontrato+5000 AS fcontrato2
FROM repventa
WHERE to_char (fcontrato, 'YYYY') > '1988';

-- 10. Mostrar el nombre y días que lleva contratados los representants
SELECT nombre, CURRENT_DATE - fcontrato as disCOntratados
FROM repventa;
