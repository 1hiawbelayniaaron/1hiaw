
--1\. Mostrar la suma de las cuotas y la suma de las ventas totales de todos los representantes.

SELECT SUM(cuota) cuota_total, SUM(ventas)ventes_totals
FROM repventa;

--2\. ¿Cuál es el importe total de los pedidos tomados por Bill Adams?

SELECT SUM(importe) import_total
FROM pedido ped LEFT JOIN repventa r
ON ped.repcod = r.repcod
WHERE UPPER(nombre) = 'BILL ADAMS';

--3\. Calcula el precio medio de los productos del fabricante ACI.

SELECT AVG(importe) import_mitja
FROM pedido 
WHERE LOWER(fabcod) = 'aci';

-- 4\. ¿Cuál es el importe medio de los pedido solicitados por el cliente 2103

SELECT AVG(importe) import_mitja
FROM pedido 
WHERE cliecod = 2103;


-- 5\. Mostrar la cuota máxima y la cuota mínima de las cuotas de los representantes.

SELECT MIN(cuota) "Cuota minima", MAX(cuota) "Cuota maxima" 
FROM repventa;


-- 6\. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?

SELECT MIN(fecha)
FROM pedido;

-- 7\. ¿Cuál es el mejor rendimiento de ventas de todos los representantes? (considerarlo como el porcentaje de ventas sobre la cuota).

SELECT ROUND(MAX((ventas/cuota)*100),2)
FROM repventa;
