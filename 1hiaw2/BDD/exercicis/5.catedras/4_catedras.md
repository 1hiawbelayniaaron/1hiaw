# 5. Catedras

Dissenyar una base de dades que reculli l'organització d'una Universitat. Es considera que:

Els departaments poden estar en una sola facultat o ser interfacultatius.
Una càtedra es troba en un únic departament.
Una càtedra pertany a una sola facultat.
Un professor està sempre assignat a un únic departament i adscrit a una o diverses càtedres, podent canviar de càtedra, però no de departament. Interessa la data en què un professor és adscrit a una càtedra.
Hi ha àrees de coneixement, i tot departament tindrà una única àrea de coneixement.

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[cas catedras](https://drive.google.com/file/d/16BzBhHya1jKo7Dl4XpSJ-WVBqaZhe7uZ/view?usp=sharing)

## 2.2. Esquema conceptual (EC ó ER)

  ![cas zoos](./catedras-def.png)

# 3. Model lògic relacional

## 3.1. Esquema lògic

  a_Coneixement(<ins>IdArea</ins>, nombre)  
  Departament(<ins>IdDepartament</ins>,nomDeptartament,IdArea)  
  Professor(<ins>idProfessor</ins>,DNI,nom,idDepartament)

  InscripcioCatedra(<u>idCatedra,idProfessor</u>,data)   

  Catedra(<u>idCatedra</u>, nombre, idFacultat, idDepartament)

  Facultat(<u>codFacultat</u>, nom)

  DepartamentXFacultat<u>(idDepartament,idCatedra</u>)
  \...

## 3.2. Diagrama referencial

| Relació referencial  | Clau aliena   | Relació referida |
| -------------------- |:-------------:| ---------------- |
| Departament          | idArea        | a_Coneixement    |
| Professor            | idDepartament | Departament      |
| IncripcioCatedra     | idCatedra     | Catedra          |
| IncripcioCatedra     | idProfessor   | Professor        |
| Facultat             | idDepartament | Departament      |
| DepartamentXFacultat | idDepartament | Departament      |
| DepartamentXFacultat | idCatedra     | Catedra          |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script zoos.sql](./zoos.sql)
