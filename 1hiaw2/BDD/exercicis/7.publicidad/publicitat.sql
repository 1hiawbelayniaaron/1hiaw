\c template1
drop database if exists publicitat;
create database publicitat;
\c publicitat

create sequence idEns_seq
 START WITH 1000
 increment by 10;
 
 create sequence idCanal_seq
 START WITH 1010000
 increment by 1;
 
 create sequence idSpot_seq
 START WITH 100
 increment by 10;
 
 create sequence idFranja_seq
 START WITH 1
 increment by 1;


CREATE TABLE Ens(
	idEns INT constraint idEns_pk primary key, 
	Nom varchar(30), 
	Tipus varchar(6), --  public/privat
constraint Tipus_ck check(Tipus='public' or Tipus='privat')			 
	);
INSERT INTO Ens VALUES (nextval('idEns_seq'),'CCMA','public');
INSERT INTO Ens VALUES (nextval('idEns_seq'),'A3M','privat');
  
CREATE TABLE Canal(
	idCanal INT constraint idCanal_pk primary key, 
	Nom varchar(20), 
	Descripcio varchar(400), 
	idEns INT,
constraint idEns_Canal_fk foreign key (idEns) references Ens(idEns)
	); 
 INSERT INTO Canal VALUES (nextval('idCanal_seq'),'neox','canal de atresmedia.',1010);
INSERT INTO Canal VALUES (nextval('idCanal_seq'),'la sexta','canal de atresmedia.',1010);
INSERT INTO Canal VALUES (nextval('idCanal_seq'),'antena 3','canal de atresmedia.',1010);
 INSERT INTO Canal VALUES (nextval('idCanal_seq'),'nova','canal de atresmedia.',1010);


INSERT INTO Canal VALUES (nextval('idCanal_seq'),'tv3','canal de CCMA.',1000); 
INSERT INTO Canal VALUES (nextval('idCanal_seq'),'canal 33','canal de CCMA.',1000);  
INSERT INTO Canal VALUES (nextval('idCanal_seq'),'super3','canal de CCMA.',1000); 
 INSERT INTO Canal VALUES (nextval('idCanal_seq'),'324','canal de CCMA.',1000); 
 
CREATE TABLE Empresa(
	CIF varchar(10) constraint CIF_pk primary key, --(B – 76365789)
	Nom varchar(50), 
	Adreca varchar(150), 
	Telefon varchar(11), 
	TipusEmpresa varchar(20),
constraint TipusEmpresa_ck check(TipusEmpresa='firma comercial' or TipusEmpresa='agencia publicitaria')
	);  

INSERT INTO Empresa VALUES ('B-12345678','anunciosForever','NAYARIT NO. 1050, PUEBLO NUEVO, 23060','963071761','firma comercial'); 	
INSERT INTO Empresa VALUES ('B-87654321','Creamos TU anuncio S. A.','Salaspils, bld. 18/1','963072345','agencia publicitaria');


-- DELETE FROM Empresa
-- WHERE CIF = 'B-12345678';
 

CREATE TABLE Spot(
	idSpot INT constraint idSpot_pk primary key, 
	Idioma varchar(3), 
	propietaria varchar(50),
	productora varchar(50),
constraint propietaria_Empresa_fk foreign key (propietaria) references Empresa(CIF),
constraint productora_Empresa_fk foreign key (productora) references Empresa(CIF)
	); 
 
 INSERT INTO Spot VALUES (nextval('idSpot_seq'),NULL,'B-12345678','B-12345678'); 
  INSERT INTO Spot VALUES (nextval('idSpot_seq'),'ESP','B-12345678','B-87654321'); 
 INSERT INTO Spot VALUES (nextval('idSpot_seq'),'FR','B-12345678','B-87654321'); 
 INSERT INTO Spot VALUES (nextval('idSpot_seq'),'ING','B-12345678','B-87654321'); 
 
 UPDATE Spot
 SET idioma = 'FR'
 WHERE idSpot = 100;
 
 

 
CREATE TABLE Equivalencia(
	idSpot INT, 
	SpotEquivalent INT, -- els quals podrien ser aquells que tenen les mateixes imatges però tenen diferent idioma.
constraint idSpot_SpotEquivalent_pk primary key (idSpot , SpotEquivalent),
constraint Equivalencia_Spot_fk foreign key (idSpot) references Spot(idSpot),
constraint Equivalencia_SpotEquivalent_fk foreign key (SpotEquivalent) references Spot(idSpot)
	);   
INSERT INTO Equivalencia VALUES (100,110); 
INSERT INTO Equivalencia VALUES (100,120); 
INSERT INTO Equivalencia VALUES (100,130); 
INSERT INTO Equivalencia VALUES (110,120); 
INSERT INTO Equivalencia VALUES (110,130); 
CREATE TABLE Franja(
	idFranja INT constraint idFranja_pk primary key, --seq (1,2,3,...)
	Franja varchar(20), 
	HoraInici timestamp, 
	HoraFi timestamp
	); 
INSERT INTO Franja VALUES (nextval('idFranja_seq'),'Mati',
	to_timestamp('07:00','HH:MM'),
	to_timestamp('12:00','HH:MM')
	); 
INSERT INTO Franja VALUES (nextval('idFranja_seq'),'Mig dia',
	to_timestamp('12:00','HH:MM'),
	to_timestamp('3:00','HH:MM')
	); 
INSERT INTO Franja VALUES (nextval('idFranja_seq'),'Tarda',
	to_timestamp('3:00','HH:MM'),
	to_timestamp('6:00','HH:MM')
	); 
INSERT INTO Franja VALUES (nextval('idFranja_seq'),'Vespre',
	to_timestamp('6:00','HH:MM'),
	to_timestamp('9:00','HH:MM')
	); 	
INSERT INTO Franja VALUES (nextval('idFranja_seq'),'Nit',
	to_timestamp('9:00','HH:MM'),
	to_timestamp('07:00','HH:MM')
	); 
	
CREATE TABLE Emissio(
	idCanal INT, 
	idFranja INT, --(com poden ser: matí, migdia, tarda, nit, ...)
	idSpot INT, 
	Data date, 
	Preu numeric(7,2), 
	NumEmissions INT,
constraint id_Canal_Franja_idSpot_pk primary key (idCanal, idFranja, idSpot),
constraint idCanal_Emissio_fk foreign key (idCanal) references Canal(idCanal),
constraint idSpot_Emissio_fk foreign key (idSpot) references Spot(idSpot),
constraint Franja_Emissio_fk foreign key (idFranja) references Franja(idFranja)
	);
	
INSERT INTO Emissio VALUES ( 
	1010000,1,100,
	to_date('12/12/2022','DD/MM/YYYY'),
	300.56, 5
	); 
INSERT INTO Emissio VALUES ( 
	1010001,1,110,
	to_date('06/12/2022','DD/MM/YYYY'),
	423.74, 12
	);
	
INSERT INTO Emissio VALUES ( 
	1010002,3,120,
	to_date('08/12/2022','DD/MM/YYYY'),
	234.25, 6
	);

INSERT INTO Emissio VALUES ( 
	1010003,5,130,
	to_date('23/12/2022','DD/MM/YYYY'),
	178.95, 14
	);

CREATE TABLE TipusProducte(
	IdTipusProducte INT constraint IdTipusProducte_pk primary key, -- fer seq
	TipusProducte varchar
	);  

INSERT INTO TipusProducte VALUES (10,'belleza');
INSERT INTO TipusProducte VALUES (20,'hogar');
INSERT INTO TipusProducte VALUES (30,'niños');
INSERT INTO TipusProducte VALUES (40,'deportes');
INSERT INTO TipusProducte VALUES (50,'peliculas');
INSERT INTO TipusProducte VALUES (60,'apuestas');
INSERT INTO TipusProducte VALUES (70,'sexo');

CREATE TABLE TipusProducteXSpot(
	idSpot INT, 
	idTipusProducte INT,
constraint idSpot_idTipusProducte_pk primary key (idSpot, idTipusProducte),
constraint idSpot_TipusProducteXSpot_fk foreign key (idSpot) references Spot(idSpot),
constraint TipusProducte_TipusProducteXSpot_fk foreign key (idTipusProducte) references TipusProducte(IdTipusProducte)
	);  
	
INSERT INTO TipusProducteXSpot VALUES (100,40);
INSERT INTO TipusProducteXSpot VALUES (110,30);
INSERT INTO TipusProducteXSpot VALUES (120,50);
INSERT INTO TipusProducteXSpot VALUES (130,10);

CREATE TABLE TipusProducteProhibit(
	Franja INT, 
	idTipusProducte INT, 
	GrauPenalitzacio smallint,
constraint GrauPenalitzacio_ck check(GrauPenalitzacio<=3),
constraint franja_tipusProducte_pk primary key (Franja,idTipusProducte),
constraint TipusProducte_TipusDeProducteProhibit_fk foreign key (idTipusProducte) references TipusProducte(idTipusProducte),
constraint Franja_TipusProducteProhibit_fk foreign key (Franja) references Franja(idFranja)
	);
INSERT INTO TipusProducteProhibit VALUES (1,60,3);
INSERT INTO TipusProducteProhibit VALUES (1,70,3);
INSERT INTO TipusProducteProhibit VALUES (2,70,2);
INSERT INTO TipusProducteProhibit VALUES (4,60,3);
INSERT INTO TipusProducteProhibit VALUES (4,70,3);
