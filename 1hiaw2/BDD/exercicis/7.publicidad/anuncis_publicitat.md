# Anuncis publicitat

Es proposa dissenyar una BD per millorar el control dels "spots" (anuncis) per a la televisió, i de tot el seu entorn, com són els canals de TV, franges horàries, agències de publicitat, tipus de productes que s'anuncien etc.

  Al país hi ha diversos ens televisius. Es vol reflectir que uns són de titularitat pública, per exemple CCMA (Corporació Catalana de Mitjans Audiovisuals) i RTVE (Ràdio-Televisió Espanyola) i altres són de titularitat privada com Atresmedia. Aquests ens televisius disposen d'un o més canals d'emissió. Per exemple, CCMA disposa de TV3, El 33, Canal Super3, etc. RTVE disposa de La 1, La 2, 24 hores, Tododeporte, etc. Atresmedia disposa de La Sexta, Antena 3, etc. Cada canal s'identificarà sempre per un codi, tindrà un nom i una descripció.

  Tots els "espots" suposarem que s'identifiquen per un codi assignat per una suposada "Oficina de Mitjans de Comunicació". Ha de considerar l'existència de "spots" equivalents, els quals podrien ser aquells que tenen les mateixes imatges però tenen diferent idioma.

  Cada "espot" fa referència a un (el normal) o més (excepcionalment) tipus de productes (pensar en rentadores i detergents), i es vol tenir constància d'aquestes referències. Fins i tot s'ha de pensar que hi ha tipificats certs tipus de productes, independentment de si hi ha o no, en aquest moment, espots que facin referència a ells. Els "spots" són sempre propietat d'una única firma comercial, que és la que els paga, fins i tot en el cas d'anunciar més d'un tipus de producte.
  Els "spots" són filmats, normalment, per una agència de publicitat encara que, en alguns casos, és la mateixa firma comercial que, amb mitjans propis, els produeix sense intervenció de cap agència publicitària.

  Tal com s'ha indicat a l'principi, interessa també conèixer l'emissió dels "spots" en els diversos canals televisius. A efectes d'audiència, les 24 hores del dia no s'entenen com a tals hores, sinó com a "franges horàries" (com poden ser: matí, migdia, tarda, nit, ...) perfectament diferenciades. És més, el preu vigent (únic que interessa) de l'emissió en cada canal, depèn de la seva franja horària. Per simplificar el cas, farem la hipòtesi que el preu no depèn del dia de la setmana. Es vol tenir constància de l'nombre de vegades que cada "espot" s'ha emès en els diferents canals, la seva data d'emissió i la seva corresponent franja horària.

  Finalment, hi ha un aspecte legal molt important a considerar. Hi ha alguns tipus de productes (realment molt pocs) que no estan permesos anunciar en certes franges horàries. L'incompliment serà penalitzat amb multes de gravetat qualificada d'1 a 3. Interessarà tenir constància de les prohibicions legals anteriors.

Opcional:

De les agències de publicitat interessa conèixer el nom de el director artístic. De les firmes comercials interessa conèixer el nom de el cap de màrqueting. Però d'unes i altres són tipus d'empreses, de les quals interessa conèixer el CIF (codi d'identificació fiscal), nom, adreça i telèfon.

Les agències de publicitat disposen de directors cinematogràfics per filmar els "espots". Interessa conèixer l'historial de la contractació per part de les diferents agències d'aquests directors. Un director treballa, en un moment donat, per a una sola agència. Les agències volen conèixer de cada "spot" que director l'ha dirigit. Dels "espots" produïts directament per les firmes comercials, no interessa ni es vol tenir constància del seu director.

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[cas Anuncis](https://)

## 2.2. Esquema conceptual (EC ó ER)

  ![cas Anuncis](./publicitat.png)

# 3. Model lògic relacional

## 3.1. Esquema lògic

Ens(<ins>idEns</ins>, Nom, Tipus)  
Canal(<ins>idCanal</ins>, Nom, Descripcio, Ens)  
Franja(<ins>idFranja</ins>, Franja, HoraInici, HoraFi)  
Empresa(<ins>CIF</ins>, Nom, Adreca, Telefon, Tipus)  
Spot(<ins>idSpot</ins>,Idioma, Propietaria,Productora)  
Equivalencia(<ins>Spot, SpotEquivalent</ins>)   
Emissio(<ins>Canal, Franja, Spot</ins>, Data, Preu, NumEmissions)  
TipusProducte(<ins>IdTipusProducte</ins>, TipusProducte)  
TipusProducteXSpot(<ins>Spot, TipusProducte</ins>)  
TipusProducteProhibit(<ins>Franja,TipusProducte</ins>, GrauPenalitzacio)

## 3.2. Diagrama referencial

| Relació referencial   | Clau aliena    | Relació referida |
| --------------------- |:--------------:| ---------------- |
| Canal                 | Ens            | Ens              |
| Spot                  | Propietaria    | Empresa          |
| Spot                  | Productora     | Empresa          |
| Equivalencia          | Spot           | Spot             |
| Equivalencia          | SpotEquivalent | Spot             |
| Emissio               | Canal          | Canal            |
| Emissio               | Franja         | Franja           |
| Emissio               | Spot           | Spot             |
| TipusProducteXSpot    | TipusProducte  | TipusProducte    |
| TipusProducteXSpot    | Spot           | Spot             |
| TipusProducteProhibit | Franja         | Franja           |
| TipusProducteProhibit | TipusProducte  | TipusProducte    |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script publicitat.sql](./publicitat.sql)
