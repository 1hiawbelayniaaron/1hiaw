-- aaron belayni moreno
-- a211762hb

\c template1
drop database if exists gabinet;
create database gabinet;
\c gabinet

CREATE SEQUENCE numExpedient_seq
  START WITH 1000
  INCREMENT BY 10;
  
  
CREATE TABLE client (
	DNI VARCHAR(9) CONSTRAINT client_dni_pk PRIMARY KEY,
	nom VARCHAR(20),
	adreca VARCHAR(40),
	telefon VARCHAR(10)
);


INSERT INTO CLIENT VALUES ('12345678j', 'JOAN' , 'C. soledat', '123456789');
INSERT INTO CLIENT VALUES ('87654321h', 'PERE' , 'C. virrei Aviles', '687019211');
INSERT INTO CLIENT VALUES ('86543219x', 'AARON' , 'C. manlleu', '687019211');
INSERT INTO CLIENT VALUES ('86543213i', 'BERENGUER' , 'C. bisbe morgades', '687019701');


CREATE TABLE assumpte (
	numExpedient VARCHAR(6) CONSTRAINT assumpte_numexpedient_pk PRIMARY KEY,
	dataApertura DATE,
	dataTancament DATE,
	estat BOOL,
	dniClient VARCHAR(9),
	CONSTRAINT assumpte_dni_fk FOREIGN KEY (dniClient) REFERENCES CLIENT (dni)
);

INSERT INTO ASSUMPTE VALUES (nextval('numExpedient_seq'),TO_DATE('17-10-1980', 'DD-MM-YYYY'),TO_DATE('20-02-1981', 'DD-MM-YYYY'), 'yes','12345678j');
	
INSERT INTO ASSUMPTE VALUES (nextval('numExpedient_seq'),TO_DATE('17-10-1980', 'DD-MM-YYYY'),TO_DATE('20-02-1981', 'DD-MM-YYYY'), 'no','87654321h');
	
INSERT INTO ASSUMPTE VALUES (nextval('numExpedient_seq'),TO_DATE('17-10-1980', 'DD-MM-YYYY'),TO_DATE('20-02-1981', 'DD-MM-YYYY'), 'yes','86543219x');
	
INSERT INTO ASSUMPTE VALUES (nextval('numExpedient_seq'),TO_DATE('17-10-1980', 'DD-MM-YYYY'),TO_DATE('20-02-1981', 'DD-MM-YYYY'), 'no','86543213i');


CREATE TABLE procurador (
	DNIprocurador VARCHAR(9) CONSTRAINT procurador_dni_pk PRIMARY KEY,
	nomProcurador VARCHAR(50),
	telefonProcurador SMALLINT
);
	
CREATE TABLE assumptexProcurador (
	numExpedient VARCHAR(6),
	dniProcurador VARCHAR(9),
	CONSTRAINT assumptexProcurador_pk PRIMARY KEY (numExpedient, dniProcurador),
	CONSTRAINT assumptexProcurador_numExpedient_fk FOREIGN KEY (numExpedient)
				REFERENCES assumpte(numExpedient),
	CONSTRAINT assumptexProcurador_dniProcurador_fk FOREIGN KEY (dniProcurador)
				REFERENCES PROCURADOR(dniProcurador)

);
