# 1. Gavinet d'advocats

Es vol dissenyar una base de dades relacional per a emmagatzemar informació
sobre els assumptes que porta un gabinet d'advocats.

Cada assumpte té un número d'expedient que l'identifica i correspon a un sol
client.
De l'assumpte s'ha d'emmagatzemar la data d'inici, data d'arxiu (finalització)
, el seu estat (en tràmit, arxivat, etc), així com les dades personals del
client al qual pertany (DNI, nom, adreça, telèfon).
Alguns assumptes són portats per un o diversos procuradors i viceversa, dels
quals ens interessa també les dades personals (DNI, nom, adreça, telèfon).

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[cas GavinetAdvocats](https://drive.google.com/file/d/1v0xIKS7MWFwomepeLG__D4gLFWEWvuYq/view?usp=sharing)

## 2.2. Esquema conceptual (EC ó ER)

  ![cas GavinetAdvocats](./gavinetAdvocats.png)

# 3. Model lògic relacional

## 3.1. Esquema lògic

  Client(<ins>DNIClient</ins>, NomClient, AdrecaClient, TelefonClient)  
  Assumpte(<ins>NumExpedient</ins>, DataInici, DataArxiu, Estat, DNIClient)  
  Procurador(<ins>DNIProcurador</ins>, NomProcurador, AdrecaProcurador, TelefonProcurador)  
  AssumptePerProcurador(<ins>idAssumpte, DNIProcurador</ins>)
  \...

## 3.2. Diagrama referencial

| Relació referencial   | Clau aliena   | Relació referida |
| --------------------- |:-------------:| ---------------- |
| Assumpte              | DNIClient     | Client           |
| AssumptePerProcurador | idAssumpte    | Assumpte         |
| AssumptePerProcurador | DNIProcurador | Procurador       |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script xxx.sql](./gavinetsAdvocats.sql)
