\c template1
drop database if exists habitatges;
create database habitatges;
\c habitatges

CREATE TABLE municipi (
	codiMunicipi varchar(5) CONSTRAINT codi_municipi_pk PRIMARY KEY,
	nom VARCHAR(40),
	numHab int 
	);
	
CREATE TABLE habitatge (
	numCadastre int CONSTRAINT num_cadastre_pk PRIMARY KEY,
	adreça VARCHAR(40),
	tipus VARCHAR(20),
	codiMunicipi varchar(5), 
constraint codiMunicipi_fk foreign key (codiMunicipi) references municipi(codiMunicipi)
	);

CREATE TABLE Persona(
	DNI varchar(9) CONSTRAINT DNI_pk PRIMARY KEY,
	nom VARCHAR(40),
	codiPostal int,
	adreça VARCHAR(40),
	DNICap varchar(9),
	numCadastre int,
constraint DNICap_fk foreign key (DNICap) references Persona(DNICap),
constraint numCadastre_fk foreign key (numCadastre) references habitatge(numCadastre)
    );
    
CREATE TABLE Propietat(
	numCadastre int,
	 DNI varchar(9),
constraint propietat_pk primary key (numCadastre,DNI),
constraint numCadastre_propietat_fk foreign key (numCadastre) references habitatge(numCadastre),
constraint DNI_propietat_fk foreign key (DNI) references persona(DNI)
	 );
