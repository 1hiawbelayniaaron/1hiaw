# 1. Habitatges

Dissenyar un esquema E/R que reculli l'organització d'un sistema d'informació en el qual es vol tenir la informació sobre municipis, habitatges i persones. Cada persona només pot habitar en un habitatge, però pot ser propietària de més d'una. També ens interessa la interrelació de les persones amb el seu cap de família.
Feu els supòsits semàntics (`requeriments d'usuari`) complementaris necessaris.

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[cas Habitatges](https://drive.google.com/file/d/1SI1goMsp-sC8hN1xhMqX44ac5spoFIog/view?usp=sharing)

## 2.2. Esquema conceptual (EC ó ER)

  ![cas GavinetAdvocats](./1_habitatges.drawio.png)

# 3. Model lògic relacional

## 3.1. Esquema lògic

  Municipi(<ins>codiMunicipi</ins>, nom, numHab)  
  Habitatge(<ins>numCadastre</ins>, adreça, tipus,**codiMunicipi**)  
  Persona(<ins>DNI</ins>, nom, codiPostal, adreça, **DNICap,numCadastre**)
  Propietat(<u>numCadastre, DNI</u>)
  \...

## 3.2. Diagrama referencial

| Relació referencial | Clau aliena  | Relació referida |
| ------------------- |:------------:| ---------------- |
| Habitatge           | codiMunicipi | Municipi         |
| Persona             | DNICap       | Persona          |
| Propietat           | numCadastre  | Habitatge        |
| Propietat           | DNI          | Persona          |
| Persona             | numCadastre  | Habitatge        |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script habitatges.sql](./habitatges.sql)
