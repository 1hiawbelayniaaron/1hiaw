
\c template1
drop database if exists museus;
create database museus;
\c museus

CREATE TABLE museu(
	idMuseu int,
	nom varchar(50) NOT NULL,
	direccio varchar(60),
	ciutat varchar(50),
	pais varchar(50),
	CONSTRAINT PK_MUSEU_idMuseu PRIMARY KEY (idMuseu)
);

CREATE TABLE salaMuseu(
	idMuseu int,
	idSala int,
	nom varchar(50) NOT NULL,
	CONSTRAINT PK_MUSEU_salaMuseu PRIMARY KEY (idMuseu,idSala),
	CONSTRAINT FK_SALAMUSEU_idMuseu FOREIGN KEY (idMuseu) REFERENCES museu (idMuseu)
);

CREATE TABLE autor(
	idAutor int,
	nom varchar(50) NOT NULL,
	nacionalitat varchar(50),
	CONSTRAINT PK_AUTOR_idAutor PRIMARY KEY (idAutor)
);

CREATE TABLE obraArt(
	idObra int,
	titol varchar(100) NOT NULL,
	tipus varchar(20),
	idMuseu int,
	idSala int,
	idAutor int,
	CONSTRAINT PK_OBRAART_idObra PRIMARY KEY (idObra),
	CONSTRAINT FK_OBRAART_idMuseu FOREIGN KEY (idMuseu) REFERENCES museu (idMuseu),
	CONSTRAINT FK_OBRAART FOREIGN KEY (idMuseu, idSala) REFERENCES salaMuseu (idMuseu, idSala)
);
