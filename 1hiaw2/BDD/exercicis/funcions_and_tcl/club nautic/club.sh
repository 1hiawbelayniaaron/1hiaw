#!/bin/bash

# Script Name: biblioteca.sh
# Author: a211762hb
# Date: 19/04/2023
# Version: 2.0
# License: This is free software, licensed under the GNU General Public License v3. See http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 
# Description: 

# borrar y crear la base de datos
dropdb club
createdb club
# cargar tablas
psql club -f 'club.sql'
# cargar las funciones
psql club -f 'funcionsClub.sql'
# cargar juego de pruebas
psql club -f 'jocProves.sql'

#psql biblioteca -f 'funcions.sql'




