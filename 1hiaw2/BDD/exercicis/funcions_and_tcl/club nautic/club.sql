/*
Es demana el seguent:
Al directori UF3/examen al repositori que teniu compartit amb mi, haureu de lliurar el
següent:
- club.sh, on resetejaré la BD i carregaré, crearé els objectes necessaris i testaré
 amb un joc de proves vàlid.
- fitxer jocProves.sql (joc de proves vàlid)
- fitxer funcionsClub.sql la qual tindrà
    - funció reservarPista (p_idusuari,p_dia, p_hora, p_pista) Reserva una p_pista
    segons el que s'explica a continuació.
    - funció confirmar (p_idusuari). Quan l'usuari arriba poc abans al club i passa pel
    mostrador, s'identifica i es cobra la reserva (s'afegeix el preu de la reserva)
    - funció anularPista (p_id)
- fitxer triggers.sql


Un club de pàdel vol gestionar les reserves telemàticament a través d'una app web.

Cada pista té un id i dos preus segons si es hora vall o hora punta.

- La franja vall representa una hora amb menys afluència, amb el què serà més barata.
Va de dilluns a divendres de 7 del matí fins abans de les 17 h i caps de
 setmana des de 14 a 21 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

- L'hora punta serà més cara per una demanda més alta a aquelles hores. Va de
dilluns a divendres de des les 17 a 21 hores, caps de setmana de 7 fins
 abans de les 14 hores. Els festius tenen la mateixa consideració que el cap de
 setmana.

Una reserva de padel podrà ser d'una hora o d'hora i mitja, segons el que desitgi
l'usuari del club.

Les franges de reserva son de 30 minuts. La taula franja

D'usuaris hi ha de dos tipus, els que son abonats i els que no.
- Els que son abonats podran reservar  48 hores abans del dia i hora a que vol jugar.
- Els abonats podran reservar fins a 72 hores abans del dia i hora a que vol jugar.

Un soci podrà fer una reserva per un dia de la setmana si en aquella setmana no
té una reserva que encara no ha consumit. Amb un exemple seria el següent:

- Un usuari el dilluns reserva pel dimecres i el dijous vol reservar pel dissabte.
Com ja ha consumit la seva reserva, el sistema li deixa reservar (evidentment altra cosa
és que hi hagi disponibilitat)
- Un usuari el dilluns reserva pel divendres per jugar. El dijous vol reservar pel
dissabte però l'app li ha de contestar que no pot perquè té una reserva pendent de
consumir.

IMPORTANT (1) No us poso les restriccions de clau aliena per tenir major llibertat a l'hora
de programar, però s'han de tenir en compte. Podeu fer JOINS entre els camps que
es diuen igual.

IMPORTANT (2): Si hi ha errors sintàctics no es corregirà l'examen

*/
\c template1
drop database if exists club;
create database club;
\c club

create table usuari(
  idusuari int primary key,
  nom varchar(50),
  cognoms varchar(100),
  abonat boolean
);

create table pista(
  idpista int primary key,
  preuhoravall numeric(4,2),
  preuhorapunta numeric(4,2)
);

insert into pista
  values (1, 16.50,22.15),
        (2, 14.25,18.40);

create table franja (
  idpista int,
  dia date,
  hora varchar(4),
  idreserva int, -- la franja està lliure si el camp està buit
  primary key (idpista, dia, hora)
);
insert into franja
values
  (1,current_date, '0800',null),
  (1,current_date, '0830',null),
  (1,current_date, '0900',null),
  (1,current_date, '0930',null),
  (1,current_date, '1000',null),
  (1,current_date, '1030',null);

  insert into franja
  values
    (2,current_date, '0800',null),
    (2,current_date, '0830',null),
    (2,current_date, '0900',null),
    (2,current_date, '0930',null),
    (2,current_date, '1000',null),
    (2,current_date, '1030',null);



-- aqui tindrem els dies que son festius, per exemple el dia 1 de maig del 2020

create table festius(
  diafestiu date primary key
);

create table reserva (
  idreserva serial primary key,
  idpista int,
  dia date,
  horaini varchar(4), --IMPORTANT: s'emmagatzema com varchar. Ex 0930 representa les 9:30
  durada numeric(2,1),
  idusuari int,
  datareserva timestamp,
  import numeric (4,2)
)