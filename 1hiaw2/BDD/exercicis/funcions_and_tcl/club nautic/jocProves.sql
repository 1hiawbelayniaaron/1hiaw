--() { :; }; exec psql biblioteca -f "$0"
\t


-- Insertar usuarios
INSERT INTO usuari (idusuari, abonat) VALUES (1, true);
INSERT INTO usuari (idusuari, abonat) VALUES (2, false);
INSERT INTO usuari (idusuari, abonat) VALUES (3, true);



-- Insertar franjas horarias disponibles
INSERT INTO franja (idpista, dia, hora, idreserva) VALUES (1, current_date + INTERVAL '2 days', '0800', NULL);
INSERT INTO franja (idpista, dia, hora, idreserva) VALUES (2, current_date + INTERVAL '2 days', '0830', NULL);


-- Juego de pruebas para las funciones
\echo '----------------------------------------------------------'
-- Reserva de pista válida para un usuario abonado
\echo 'Prueba 1: Reserva de pista válida para un usuario abonado'
SELECT reservarPista(1, (current_date + INTERVAL '2 days')::date, '0800', 1); -- Se espera un mensaje de éxito
\echo '----------------------------------------------------------'
-- Reserva de pista válida para un usuario no abonado
\echo 'Prueba 2: Reserva de pista válida para un usuario no abonado'
SELECT reservarPista(2, (current_date + INTERVAL '2 days')::date, '0830', 2); -- Se espera un mensaje de éxito
\echo '----------------------------------------------------------'
-- Intento de reserva de pista cuando la franja horaria no está disponible
\echo 'Prueba 3: Intento de reserva de pista cuando la franja horaria no está disponible'
SELECT reservarPista(3, (current_date + INTERVAL '2 days')::date, '0800', 1); -- Se espera un mensaje de error
\echo '----------------------------------------------------------'
-- Intento de reserva de pista después de la fecha límite para reservar
\echo 'Prueba 4: Intento de reserva de pista después de la fecha límite para reservar'
SELECT reservarPista(4, (current_date + INTERVAL '1 day')::date, '0800', 1); -- Se espera un mensaje de error
\echo '----------------------------------------------------------'
-- Confirmación de reservas pendientes de un usuario
\echo 'Prueba 5: Confirmación de reservas pendientes de un usuario'
SELECT confirmar(1); -- Se espera un mensaje de éxito
\echo '----------------------------------------------------------'
-- Confirmación de reservas pendientes de un usuario sin reservas pendientes
\echo 'Prueba 6: Confirmación de reservas pendientes de un usuario sin reservas pendientes'
SELECT confirmar(3); -- Se espera un mensaje de error
\echo '----------------------------------------------------------'
-- Anulación de una reserva válida
\echo 'Prueba 7: Anulación de una reserva válida'
SELECT anularPista(1); -- Se espera un mensaje de éxito
\echo '----------------------------------------------------------'
-- Intento de anulación de una reserva inexistente
\echo 'Prueba 8: Intento de anulación de una reserva inexistente'
SELECT anularPista(5); -- Se espera un mensaje de error
\echo '----------------------------------------------------------'