CREATE OR REPLACE FUNCTION reservarPista(p_idusuari INT, p_dia DATE, p_hora VARCHAR(4), p_pista INT) RETURNS VOID AS $$
DECLARE
    v_abonat BOOLEAN;
    v_datareserva TIMESTAMP;
    v_import NUMERIC(4,2);
    v_durada NUMERIC(2,1);
BEGIN
    -- Obtener el estado de abonado del usuario
    SELECT abonat INTO v_abonat FROM usuari WHERE idusuari = p_idusuari;

    -- Comprobar si el usuario es abonado
    IF v_abonat THEN
        -- Calcular la fecha y hora límite para hacer la reserva
        v_datareserva := current_timestamp + INTERVAL '72 hours';
    ELSE
        v_datareserva := current_timestamp + INTERVAL '48 hours';
    END IF;

    -- Obtener la duración y el importe de la pista
    SELECT preuhoravall, preuhorapunta INTO v_import FROM pista WHERE idpista = p_pista;

    -- Comprobar si la franja horaria está disponible
    IF EXISTS (
        SELECT 1 FROM franja WHERE idpista = p_pista AND dia = p_dia AND hora = p_hora AND idreserva IS NULL
    ) THEN
        -- Comprobar si se cumple la restricción de fecha límite para reservar
        IF current_timestamp < v_datareserva THEN
            -- Calcular la duración de la reserva (1 hora o 1.5 horas)
            IF EXISTS (
                SELECT 1 FROM reserva
                WHERE idusuari = p_idusuari AND dia = p_dia AND horaini = p_hora
                AND durada = 1.5
            ) THEN
                v_durada := 1;
            ELSE
                v_durada := 1.5;
            END IF;

            -- Insertar la reserva en la tabla reserva
            INSERT INTO reserva (idpista, dia, horaini, durada, idusuari, datareserva, import)
            VALUES (p_pista, p_dia, p_hora, v_durada, p_idusuari, current_timestamp, v_import);

            -- Actualizar el idreserva en la tabla franja
            UPDATE franja SET idreserva = currval('reserva_idreserva_seq')
            WHERE idpista = p_pista AND dia = p_dia AND hora = p_hora;

            RAISE NOTICE 'Reserva realizada correctamente.';
        ELSE
            RAISE EXCEPTION 'No se puede realizar la reserva. Se ha superado la fecha límite para reservar.';
        END IF;
    ELSE
        RAISE EXCEPTION 'No se puede realizar la reserva. La franja horaria no está disponible.';
    END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION confirmar(p_idusuari INT) RETURNS VOID AS $$
BEGIN
    -- Calcular el importe total de las reservas pendientes de confirmar
    UPDATE reserva
    SET import = CASE
        WHEN durada = 1.5 THEN preuhorapunta
        ELSE preuhoravall
    END
    FROM pista
    WHERE reserva.idpista = pista.idpista
    AND idusuari = p_idusuari
    AND datareserva IS NOT NULL
    AND import IS NULL;

    -- Comprobar si se han actualizado reservas
    IF FOUND THEN
        RAISE NOTICE 'Confirmación realizada correctamente.';
    ELSE
        RAISE EXCEPTION 'No hay reservas pendientes de confirmar.';
    END IF;
END;
$$ LANGUAGE plpgsql;



CREATE OR REPLACE FUNCTION anularPista(p_id INT) RETURNS VOID AS $$
DECLARE
    v_idpista INT;
    v_dia DATE;
    v_hora VARCHAR(4);
BEGIN
    -- Obtener los datos de la reserva a anular
    SELECT idpista, dia, horaini INTO v_idpista, v_dia, v_hora
    FROM reserva WHERE idreserva = p_id;

    -- Comprobar si la reserva existe
    IF FOUND THEN
        -- Eliminar la reserva de la tabla reserva
        DELETE FROM reserva WHERE idreserva = p_id;

        -- Actualizar el campo idreserva en la tabla franja
        UPDATE franja SET idreserva = NULL
        WHERE idpista = v_idpista AND dia = v_dia AND hora = v_hora;

        RAISE NOTICE 'Reserva anulada correctamente.';
    ELSE
        RAISE EXCEPTION 'La reserva no existe.';
    END IF;
END;
$$ LANGUAGE plpgsql;

