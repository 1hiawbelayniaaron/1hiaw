#!/bin/bash

# Script Name: matricula.sh
# Author: a211762hb
# Date: 19/04/2023
# Version: 2.0
# License: This is free software, licensed under the GNU General Public License v3. See http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 
# Description: 

# borrar y crear la base de datos
dropdb matricula
createdb matricula
# cargar tablas
psql matricula -f 'taules_matricula_examen.sql'
# cargar las funciones
psql matricula -f 'a211762hb.sql'
# cargar juego de pruebas
psql matricula -f 'joc_proves_matricula.sql'

#psql matricula -f 'funcions.sql'
