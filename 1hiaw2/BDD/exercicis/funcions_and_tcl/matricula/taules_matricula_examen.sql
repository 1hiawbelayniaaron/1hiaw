--() { :; }; exec psql template1 -f "$0"

-- Script: 				taules_matricula.sql
-- Descripció: 		Matrícula d'alumnes a l'EdT
-- Autor: 						Jordi Andúgar
-- Darrera revisió:		30/06/2023
-- Versió:						0.3

\c template1
drop database if exists matricula;
create database matricula;
\c matricula
--===========================================
--
--	CREACION DE TABLAS Y SECUENCIAS
--
-- ==========================================

-- ****************************************
--		TABLA ALUMNO
-- ****************************************

CREATE TABLE ALUMNO
(
  DNI		VARCHAR(9)	CONSTRAINT PK_ALUMNO PRIMARY KEY,
  NOMBRE	VARCHAR(18)NOT NULL,
  APELLIDOS	VARCHAR(31)NOT NULL,
  DIRECCION VARCHAR(40),
  TELEFONO  VARCHAR(9),
  FECHA_NAC	DATE
);

INSERT INTO ALUMNO(DNI,NOMBRE,APELLIDOS,FECHA_NAC) VALUES
	('12345678Z','ADRIANA','LOPEZ',null),
    ('87654321X','VERONICA','PUIG',null),
    ('11223344B','ARNAU','HOMS',null);

-- ****************************************
--		TABLA MODULO
-- ****************************************

CREATE TABLE MODULO
(
  IDMODULO	VARCHAR(5)	CONSTRAINT PK_MODULO PRIMARY KEY,
  NOMBRE	VARCHAR(100) NOT NULL,
  HORAS SMALLINT,
  CICLO VARCHAR(10),
  PRECIO NUMERIC(4,2)
);

INSERT INTO MODULO VALUES
 ('M01','SISTEMES OPERATIUS',null,'DAW',30),
 ('M02','BASES DE DADES',null,'DAW',40),
 ('M03','PROGRAMACIO',null,'DAW',30),
 ('M04','XML',null,'DAW',20),
 ('M05','ENTORNS DE DESENVOLUPAMENT',null,'DAW',34),
 ('M06','DESENVOLUPAMENT WEB ENTORN CLIENT',null,'DAW',20),
 ('M07','DESENVOLUPAMENT WEB ENTORN SERVIDOR',null,'DAW',27),
 ('M08','DESPLEGAMENT D''APLICACIONS WEB',null,'DAW',34),
 ('M09','DISSENY D''INTERFICIES WEB',null,'DAW',20),
 ('M12','PROJECTE DE DESENVOLUPAMENT D''APLICACIONS WEB',null,'DAW',20);

-- ****************************************
--		TABLA GRUPO
-- ****************************************

CREATE TABLE GRUPO
(
  CODGRUPO		SMALLINT,
  GRUPO		 VARCHAR(10),
  CAPACIDAD  	SMALLINT	NOT NULL,
  CONSTRAINT PK_GRUPO PRIMARY KEY (CODGRUPO)
);

-- tinc dos grups amb capacitats 2 i 1 respectivament
INSERT INTO GRUPO VALUES (1,'1HIAWA',2);
INSERT INTO GRUPO VALUES (2,'1HIAWB',1);

-- ****************************************
--		TABLA EXPEDIENTE
-- ****************************************

CREATE TABLE EXPEDIENTE
(
  DNI			VARCHAR(9)	,
  CURSOACAD		VARCHAR(7)    DEFAULT '2022/23',
  IDMODULO		VARCHAR(5)	,
  CODGRUPO		SMALLINT	NOT NULL,
  CONVOCATORIA	SMALLINT DEFAULT 1,
  NOTA			NUMERIC(3,2),
CONSTRAINT PK_EXPEDIENTE PRIMARY KEY (DNI,CURSOACAD,IDMODULO),
CONSTRAINT FK_EXPEDIENTE1 FOREIGN KEY (IDMODULO) REFERENCES MODULO,
CONSTRAINT FK_EXPEDIENTE2 FOREIGN KEY (CODGRUPO) REFERENCES GRUPO,
CONSTRAINT CK_EXPEDIENTE CHECK (CONVOCATORIA IN (1,2,3,4,5) )
);


-- ****************************************
--		TABLA PRERREQUISITO
-- ****************************************

CREATE TABLE PRERREQUISITO
(
  IDMODULO		VARCHAR(5)	,
  IDPRERRE		VARCHAR(5) ,
  CONSTRAINT PK_PRERREQUISITO PRIMARY KEY (IDMODULO,IDPRERRE)
 );

 INSERT INTO PRERREQUISITO VALUES
 ('M07','M03'),
 ('M06','M03'),
 ('M09','M04');


CREATE SEQUENCE SEQ_NUMMATRI
START WITH 1 INCREMENT BY 1;

-- ****************************************
--		TABLA MATRICULA
-- ****************************************
CREATE TABLE MATRICULA
(NUMMATRI INTEGER DEFAULT NEXTVAL('SEQ_NUMMATRI') PRIMARY KEY,
DNI VARCHAR(9) NOT NULL,
CURSOACAD		VARCHAR(7)    DEFAULT '2022/23',
IMPORTE NUMERIC(6,2)
);
