-- a212762hb
-- Aaron Belayni Moreno


drop function if exists grupoCompleto(p_idModulo VARCHAR(5), p_codgrupo SMALLINT);
CREATE OR REPLACE FUNCTION grupoCompleto(p_idModulo VARCHAR(5), p_codgrupo SMALLINT)
RETURNS BOOLEAN AS $$
DECLARE
    v_capacidad SMALLINT;
    v_matriculados SMALLINT;
BEGIN
    SELECT CAPACIDAD INTO v_capacidad FROM GRUPO WHERE CODGRUPO = p_codgrupo;
    IF v_capacidad IS NULL THEN
        RETURN FALSE;
    ELSE
        SELECT COUNT(*) INTO v_matriculados FROM EXPEDIENTE  WHERE idmodulo = p_idModulo AND CODGRUPO = p_codgrupo;
        RETURN v_matriculados >= v_capacidad; 
        /* RETURN TRUE; */
    END IF;
END;
$$ LANGUAGE plpgsql;



drop function if exists estaAprobada(p_idModulo VARCHAR(5), p_dni VARCHAR(9));
CREATE OR REPLACE FUNCTION estaAprobada(p_idModulo VARCHAR(5), p_dni VARCHAR(9)) 
RETURNS BOOLEAN AS $$
BEGIN
    -- comprovamos que exista la asignatura aprovada en la tabla expediente, con EXISTS() si existe nos dará true y sino false
    RETURN (SELECT EXISTS(SELECT * FROM EXPEDIENTE WHERE DNI = p_dni AND IDMODULO = p_idModulo AND NOTA >= 5));
END;
$$ LANGUAGE plpgsql;


drop function if exists asignaturasPendientes(p_idModulo VARCHAR(5), p_dni VARCHAR(9));
CREATE OR REPLACE FUNCTION asignaturasPendientes(p_idModulo VARCHAR(5), p_dni VARCHAR(9)) 
RETURNS BOOLEAN AS $$
DECLARE
    v_prerrequisitos INT;
BEGIN
    -- miramos si hay algun prerrequisito
    SELECT COUNT(*) INTO v_prerrequisitos
    FROM PRERREQUISITO
    -- tiene que estar matriculada y aprobada
    WHERE IDMODULO = p_idModulo AND IDPRERRE NOT IN (
        SELECT IDMODULO
        FROM EXPEDIENTE
        WHERE DNI = p_dni AND NOTA >= 5
    );
        -- comprovamos que no tenga prerrequisitos y que tenga la asignatura matriculada
    IF v_prerrequisitos = 0 /* AND (SELECT COUNT(*) FROM expediente WHERE IDMODULO = p_idModulo AND DNI = p_dni) > 0 */ THEN
        RETURN FALSE;
    ELSE
        RETURN TRUE;
    END IF; 

    /* SELECT 'caso 1: No pendiente (f) => ' || asignaturasPendientes('M03', '11111111A') ; -- Debe retornar false
    SELECT 'caso 2: No pendiente (f) => ' || asignaturasPendientes('M03', '22222222B') ; -- Debe retornar false
    SELECT 'caso 3: Pendiente (f) => ' || asignaturasPendientes('M03', '33333333C') ; -- Debe retornar true */

END;
$$ LANGUAGE plpgsql;


drop function if exists matriculaAsignatura(p_dni VARCHAR(9), p_idModulo VARCHAR(5), p_codgrupo SMALLINT, p_cursoacad VARCHAR(7));
CREATE OR REPLACE FUNCTION matriculaAsignatura(p_dni VARCHAR(9), p_idModulo VARCHAR(5), p_codgrupo SMALLINT, p_cursoacad VARCHAR(7))
RETURNS VOID AS $$
DECLARE
    v_grupo_completo BOOLEAN;
    v_asignaturas_pendientes BOOLEAN;
    v_importe NUMERIC(6, 2);
BEGIN
    v_grupo_completo := grupoCompleto(p_idModulo, p_codgrupo);
    v_asignaturas_pendientes := asignaturasPendientes(p_idModulo, p_dni);
    
    IF NOT v_grupo_completo AND NOT v_asignaturas_pendientes THEN
        -- Actualizar importe en MATRICULA
        v_importe := (SELECT PRECIO FROM MODULO WHERE IDMODULO = p_idModulo);
        -- si es la primera vez que matruculamos una asignatura
        IF (SELECT COUNT(*) FROM matricula WHERE dni = p_dni) = 0
        THEN
            INSERT INTO MATRICULA (DNI, CURSOACAD, IMPORTE) VALUES (p_dni, p_cursoacad, v_importe);
        END IF;
        -- insertamos también en la tabla expediente
        INSERT INTO EXPEDIENTE (DNI, CURSOACAD, IDMODULO, CODGRUPO, CONVOCATORIA, NOTA)
        VALUES (p_dni, p_cursoacad, p_idModulo, p_codgrupo, null, null);
        RAISE NOTICE 'La matrícula del módulo % para el alumno % se ha realizado correctamente.', p_idModulo, p_dni;
    ELSE
        RAISE NOTICE 'No se pudo realizar la matrícula del módulo % para el alumno % debido a los siguientes motivos:', p_idModulo, p_dni;
        IF v_grupo_completo THEN
            RAISE NOTICE '- El grupo seleccionado está completo.';
        END IF;
        IF v_asignaturas_pendientes THEN
            RAISE NOTICE '- El alumno no cumple con los prerrequisitos necesarios para la asignatura.';
        END IF;
    END IF;
END;
$$ LANGUAGE plpgsql;

