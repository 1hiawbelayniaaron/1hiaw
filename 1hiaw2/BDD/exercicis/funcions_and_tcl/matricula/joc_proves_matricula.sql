--() { :; }; exec psql biblioteca -f "$0"
\t


-- Inserción de alumnos
INSERT INTO ALUMNO(DNI, NOMBRE, APELLIDOS, FECHA_NAC) VALUES
    ('11111111A', 'Juan', 'Pérez', '1990-01-01'),
    ('22222222B', 'María', 'Gómez', '1992-05-15'),
    ('33333333C', 'Pedro', 'López', '1995-11-30');

-- Inserción de módulos
INSERT INTO MODULO (IDMODULO, NOMBRE, HORAS, CICLO, PRECIO) VALUES
    ('M03', 'PROGRAMACIO', NULL, 'DAW', 30),
    ('M04', 'XML', NULL, 'DAW', 20),
    ('M05', 'ENTORNS DE DESENVOLUPAMENT', NULL, 'DAW', 34);



-- Inserción de prerrequisitos
INSERT INTO PRERREQUISITO (IDMODULO, IDPRERRE) VALUES
    ('M02', 'M01'),
    ('M03', 'M01'),
    ('M03', 'M02');

-- Inserción de expedientes
-- Expediente de alumno 11111111A
INSERT INTO EXPEDIENTE (DNI, CURSOACAD, IDMODULO, CODGRUPO, CONVOCATORIA, NOTA)
VALUES ('11111111A', '2022/23', 'M01', 1, 1, 2),
       ('11111111A', '2022/23', 'M02', 1, 1, 1),
       ('11111111A', '2022/23', 'M03', 1, 1, 2);

-- Expediente de alumno 22222222B
INSERT INTO EXPEDIENTE (DNI, CURSOACAD, IDMODULO, CODGRUPO, CONVOCATORIA, NOTA)
VALUES ('22222222B', '2022/23', 'M01', 1, 1, 6),
       ('22222222B', '2022/23', 'M02', 1, 1, 7),
       ('22222222B', '2022/23', 'M03', 1, 1, 8);

-- Expediente de alumno 33333333C
INSERT INTO EXPEDIENTE (DNI, CURSOACAD, IDMODULO, CODGRUPO, CONVOCATORIA, NOTA)
VALUES ('33333333C', '2022/23', 'M01', 1, 1, 7);




-- Juego de pruebas


-- Prueba de grupoCompleto
\echo '-----------------------------------'
\echo 'grupoCompleto'
\echo '-----------------------------------'
SELECT 'caso 1: El grupo no esta completo (f) => ' || grupoCompleto('M01', 2::smallint) ; -- Debe retornar false
SELECT 'caso 2: El grupo esta completo (t) => ' || grupoCompleto('M02', 1::smallint) ; -- Debe retornar true
\echo '-----------------------------------'
\echo 'estaAprobada'
\echo '-----------------------------------'
-- Prueba de estaAprobada
SELECT 'caso 1: Suspendida (f) => ' || estaAprobada('M01', '11111111A') ; -- Debe retornar false
SELECT 'caso 2: Suspendida (f) => ' || estaAprobada('M02', '11111111A') ; -- Debe retornar false
SELECT 'caso 3: Suspendida (f) => ' || estaAprobada('M03', '11111111A') ; -- Debe retornar false
SELECT 'caso 4: Aprobada (t) => ' || estaAprobada('M01', '22222222B') ; -- Debe retornar true
SELECT 'caso 5: Aprobada (t) => ' || estaAprobada('M02', '22222222B') ; -- Debe retornar true
SELECT 'caso 6: Aprobada (t) => ' || estaAprobada('M03', '22222222B') ; -- Debe retornar true
SELECT 'caso 7: Aprobada (t) => ' || estaAprobada('M01', '33333333C') ; -- Debe retornar true
\echo '-----------------------------------'
\echo 'asignaturasPendientes'
\echo '-----------------------------------'
-- Prueba de asignaturasPendientes
SELECT 'caso 1: No pendiente (f) => ' || asignaturasPendientes('M03', '11111111A') ; -- Debe retornar false
SELECT 'caso 2: No pendiente (f) => ' || asignaturasPendientes('M03', '22222222B') ; -- Debe retornar false
SELECT 'caso 3: Pendiente (f) => ' || asignaturasPendientes('M03', '33333333C') ; -- Debe retornar true
\echo '-----------------------------------'
\echo 'matriculaAsignatura'
\echo '-----------------------------------'
-- Prueba de matriculaAsignatura
SELECT 'caso 1: Matricula exitosa => ' || matriculaAsignatura('11111111A', 'M04', 1::smallint, '2022/23'); -- La matrícula debe ser exitosa
SELECT 'caso 2: Matricula no exitosa por los prequisitos => ' ||matriculaAsignatura('11111111A', 'M05', 1::smallint, '2022/23'); -- No se puede matricular debido a prerrequisitos
SELECT 'caso 3: Matricula exitosa => ' || matriculaAsignatura('22222222B', 'M04', 1::smallint, '2022/23'); -- La matrícula debe ser exitosa
SELECT 'caso 4: Matricula exitosa => ' || matriculaAsignatura('33333333C', 'M05', 1::smallint, '2022/23'); -- La matrícula debe ser exitosa
