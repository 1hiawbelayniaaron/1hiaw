CREATE OR REPLACE FUNCTION mostrarDep(p_empno INT)
RETURNS VARCHAR
AS $$
        DECLARE
        v_dname VARCHAR(50);
BEGIN
        SELECT dname
        INTO STRICT v_dname
        FROM emp e JOIN dept d
        ON e.deptno = d.deptno
        WHERE empno = p_empno;
RETURN 'El departament és' || d.deptno::varchar;
END;
$$ LANGUAGE PLPGSQL;

