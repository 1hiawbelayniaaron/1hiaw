--Aaron Belayni
--Exemplar Disponible
DELETE FROM prestec WHERE idexemplar = 5;
INSERT INTO prestec VALUES (5,current_timestamp,current_timestamp,1,0);
-- passar un segon argument, format (varchar)
CREATE
OR REPLACE FUNCTION codiExemplarDisponible(p_titol varchar) returns integer as $$ DECLARE v_codiExemplar integer;

BEGIN
SELECT
	e.idexemplar INTO STRICT v_codiExemplar
FROM
	exemplar e
	LEFT JOIN document d ON e.iddocument = d.iddocument
	LEFT JOIN prestec p ON e.idExemplar = p.idExemplar
WHERE
	lower(d.titol) = lower(p_titol)
	AND e.estat = 'Disponible'
	AND (
		p.idexemplar IS NULL
		OR p.datadev IS NOT NULL
	)
LIMIT
	1;

IF v_codiExemplar IS NOT NULL THEN RETURN v_codiExemplar;

END IF;

EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN 0;

end $$ language plpgsql;

/*SELECT codiExemplarDisponible('Highway to hell');
 SELECT codiExemplarDisponible('Fundamentos de base de datos');
 SELECT codiExemplarDisponible('La La Land');*/
CREATE
OR REPLACE FUNCTION prestecDocument(p_idUsuari integer, p_titol varchar) RETURNS void AS $$ DECLARE v_codiExemplar integer;

v_bloqueado boolean;

v_datadev TIMESTAMP;

v_usuari integer;

BEGIN -- Mira si el usuario esta bloqueado
SELECT
	bloquejat INTO v_bloqueado
FROM
	usuari
WHERE
	idusuari = p_idUsuari;




-- Si el Usuario esta bloqueado
IF v_bloqueado = TRUE THEN RAISE EXCEPTION 'Usuari amb carnet bloquejat';

END IF;
	-- Obtenemos el codigo de un ejemplar disponible | 0 = no hay ninguno disponible
SELECT
	codiExemplarDisponible(p_titol) INTO v_codiExemplar;


SELECT
	datadev,
	idusuari INTO v_datadev,
	v_usuari
FROM
	prestec
WHERE
	idexemplar = v_codiExemplar;

--RAISE NOTICE 'data: %     iduser: %   codi: %', v_datadev,v_usuari,v_codiExemplar;
-- si no hay ningun ejemplar disponible
IF v_codiExemplar = 0 THEN 
	-- guardar info en las variables

	
	-- Si renueva
	IF v_datadev IS NULL AND v_usuari = p_idUsuari THEN --RAISE NOTICE 'ejemplar disponible, para renovar';
		
	ELSE
		RAISE EXCEPTION 'No esta disponible el ejemplar "%", para el prestamo',p_titol;
	END IF;



END IF;








--RAISE NOTICE 'p = %  || v = %  || datadev = %',p_idUsuari , v_usuari, v_datadev;
-- mirar si el usuario coincide
IF p_idUsuari != v_usuari THEN RAISE EXCEPTION 'El id de usuario no coincide';

--UPDATE prestec SET datapres = current_timestamp, datadev = null WHERE idexemplar = v_codiExemplar;  
END IF;

--RAISE NOTICE 'datadev: %  ,  titol: %   ,   ejemplar: %',v_datadev,p_titol, v_codiExemplar;
-- si hay un ejemplar disponible
IF v_datadev IS NOT NULL THEN --RAISE NOTICE 'ejemplar disponible';
UPDATE
	prestec
SET
	idexemplar = v_codiExemplar,
	datapres = current_timestamp,
	idusuari = p_idUsuari,
	datadev = null
WHERE
	idexemplar = v_codiExemplar; 

RAISE NOTICE 'Realiza el préstamo del libro "%" para el usuario con id %',
v_codiExemplar,
p_idUsuari;

ELSE 

	/* UPDATE
		prestec
	SET
		idexemplar = v_codiExemplar,
		datapres = current_timestamp,
		idusuari = p_idUsuari,
		vegadesrenovat = vegadesrenovat + 1
	WHERE
		idexemplar = v_codiExemplar;

	RAISE NOTICE 'Realiza la renovación del préstamo del libro "%" para el usuario con id %',
	v_codiExemplar,
	p_idUsuari; */
	RAISE EXCEPTION 'comprovar si se puede renovar el prestamo';

END IF;



END;

$$ LANGUAGE plpgsql;







CREATE
OR REPLACE FUNCTION renovarDocument(p_codiExemplar integer, p_idUsuari integer) returns varchar as $$ 
DECLARE 
	v_vegadesrenovat  integer;
	v_msg varchar default 'Se ha renovado correctamente el documento';
	v_dataexpirado timestamp;
	v_bloqueado boolean;
BEGIN
	-- guardar el valor de bloquejat
	BEGIN
		SELECT bloquejat INTO v_bloqueado
		FROM usuari
		WHERE idusuari = p_idUsuari;
	exception
		when no_data_found then
			return 'Error: l''usuari ' || p_idUsuari || ' no existeix';
	END; 

	-- Si el carnet esta bloqueado
	IF v_bloqueado = TRUE THEN 
		RETURN 'El usuario tiene el carnet bloqueado';
	END IF;

	-- guardar vegades que renova
	SELECT vegadesrenovat INTO v_vegadesrenovat 
	FROM prestec 
	WHERE idexemplar = p_codiExemplar;
	-- que no s'hagi renovat 3 vegades
	IF v_vegadesrenovat >= 3 THEN
		RETURN 'Se ha renovado ya 3 veces el documento';
	END IF;

	-- mirar si data retorn a expirado
	SELECT datapres + INTERVAL '30 days' INTO v_dataexpirado
	FROM prestec
	WHERE idExemplar = p_codiExemplar;

	-- si ha expirat data de retorn
	IF v_dataexpirado < current_timestamp  THEN
		RETURN 'La data de retorno de 30 dias ha expirado';
	END IF;

	-- no estigui reservat per a algun usuari
	IF p_codiExemplar IN (SELECT idExemplar FROM reserva) THEN
		RETURN 'El ejemplar ya está reservado';
	END IF;
	-- si se puede prestar
	UPDATE prestec 
	SET vegadesrenovat = vegadesrenovat + 1 
	WHERE idExemplar = p_codiExemplar;

	RETURN v_msg::varchar;
EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN 'No se ha podido renovar el documento';

end $$ language plpgsql;







CREATE OR REPLACE FUNCTION marcarReserva(p_idExemplar integer)
RETURNS  varchar
AS $$
DECLARE
	v_estat varchar;
	v_nom varchar;
	v_idUsuari integer;
BEGIN
	-- miramos el estado del ejemplar
	SELECT estat INTO STRICT v_estat FROM exemplar WHERE idexemplar = p_idExemplar;

	-- comprovar si el ejemplar existe
	IF v_estat IS NULL THEN
		RETURN 'El ejemplar no existe';
	END IF;

	IF LOWER(v_estat) = 'disponible' THEN
		-- guardamos el id del usuario para buscar el nombre
		SELECT idusuari INTO v_idUsuari 
		FROM reserva 
		WHERE idexemplar = p_idExemplar AND datareserva = (SELECT MIN(datareserva) 
															FROM reserva);
		-- guardamos el nombre del usuario
		SELECT u.nom INTO v_nom
		FROM reserva r RIGHT JOIN usuari u ON r.idusuari = u.idusuari 
		WHERE r.idusuari = v_idUsuari;

		-- cambiamos el estado del ejemplar a "reservat"
		UPDATE exemplar
		SET estat = 'Reservat'
		WHERE idexemplar = p_idExemplar;

		-- escribimos la data de aviso, en este punto se avisará al usuario
		UPDATE reserva
		SET dataavis = current_timestamp 
		WHERE idexemplar = p_idExemplar AND datareserva = (SELECT MIN(datareserva) 
												FROM reserva);

		RETURN 'El ejemplar ' || p_idExemplar || ' esta reservado por el usuario ' || v_nom;
	END IF;
	-- el ejemplar no esta disponible
	RETURN 'El ejemplar no esta disponible';
EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN 'Error: El ejemplar no se ha encontrado';
END;
$$ LANGUAGE PLPGSQL;


-- Sintaxis
CREATE OR REPLACE FUNCTION retornarDocument(p_idExemplar integer)
RETURNS  varchar
AS $$
DECLARE
   v_dataexpirado timestamp;
   v_dias integer;
   v_idUsuari integer;
   v_estat varchar;
   v_respuesta varchar;
BEGIN
	-- comprovar que hayan pasado los 30 dias
	SELECT datapres + INTERVAL '30 days' , idusuari INTO v_dataexpirado , v_idUsuari
	FROM prestec
	WHERE idexemplar = p_idExemplar AND datadev IS NULL;

	-- miramos el estado del ejemplar
	SELECT estat INTO v_estat FROM exemplar WHERE idexemplar = p_idExemplar;

	IF v_dataexpirado < current_timestamp  THEN
		-- por cada documento y por cada dia el carnet recibe 1 punto

		-- mirar los dias de retraso, del prestamo ACTUAL
		v_dias := TO_CHAR(current_timestamp - v_dataexpirado, 'DD')::integer;
		-- refrescamos los puntos del usuario
		UPDATE usuari
		SET punts = v_dias
		WHERE idusuari = v_idUsuari ;
		/* IF (SELECT COUNT(*) FROM idusuari WHERE idusuari = v_idUsuari) = 0 THEN
			INSERT INTO penalitzacio VALUES (v_idUsuari, p_idExemplar, current_timestamp, v_dias);
		ELSE
			-- si ya esta en la tabla
			UPDATE penalitzacio
			SET	demerit = v_dias
			WHERE idusuari = v_idUsuari AND idexemplar = p_idExemplar;
		END IF; */
		 
		-- comprovar que el carnet tenga 50 puntos
		IF v_dias > 50 THEN
			-- bloquear carnet 15 dias
			UPDATE usuari
			SET bloquejat = true, datadesbloqueig = current_timestamp + INTERVAL '15 days', punts = 0
			WHERE idusuari = v_idUsuari;
			-- comprovar que hayan pasado los 15 dias
				-- reiniciar puntos del carnet
			/* IF (SELECT penalitzacio.data + INTERVAL '15 days' 
				FROM penalitzacio 
				WHERE idusuari = v_idUsuari AND idexemplar = p_idExemplar) < current_timestamp THEN
				
				-- reiniciar contador de puntos
				UPDATE penalitzacio
				SET	demerit = null , penalitzacio.data = null
				WHERE idusuari = v_idUsuari AND idexemplar = p_idExemplar;

				-- desbloquear carnet
				UPDATE usuari
				SET bloquejat = false;
				WHERE idusuari = v_idUsuari;

			END IF; */
		END IF;
	END IF;

	UPDATE prestec
	SET datadev = current_timestamp 
	WHERE datadev IS NULL AND idexemplar = p_idExemplar;


	SELECT marcarReserva(p_idExemplar) INTO v_respuesta;
	--RAISE NOTICE '%',v_respuesta;
	-- comprovamos que ningun usuario haya reservado ya el libro
	return v_respuesta;
		



END;
$$ LANGUAGE PLPGSQL;



/* -- usuario bloqueado
SELECT
	prestecDocument(4, 'La La Land');

-- no esta disponible ningún ejemplar
SELECT
	prestecDocument(1, 'La La Land');


-- hace el update ya que si que esta disponible
SELECT
	prestecDocument(1, 'Highway to hell');

-- renueva el prestamo
SELECT
	prestecDocument(1, 'Highway to hell'); */