--() { :; }; exec psql biblioteca -f "$0"
\t


INSERT INTO prestec VALUES (5,current_timestamp,null,1,0);
INSERT INTO prestec VALUES (3,current_timestamp,null,1,3);
INSERT INTO prestec VALUES (8,current_timestamp,null,3,0);
-- reserva
INSERT INTO reserva values (8,3,current_timestamp,null);

\echo 'renovar documento'
\echo '---------------------------------'
-- renovar document
    -- puede renovar el documento
    select 'caso 1: El usuario puede renovar el documento => ' || renovarDocument(5,1);
    \echo '---------------------------------'
    -- ya ha renovado 3 veces
    select 'caso 2: El usuario ya ha renovado 3 veces el documento => ' || renovarDocument(3,1) ;
    \echo '---------------------------------'
    select 'caso 3: la data de retorno ha expirado => ' || renovarDocument(1,1);
    \echo '---------------------------------' 
    -- data de retorno expirada
    select 'caso 4: El usuario tiene el carnet bloqueado => ' || renovarDocument(5,4);
    \echo '---------------------------------'
    -- carnet bloqueado
    select 'caso 5: El documento ya ha sido reservado por otro usuario => ' || renovarDocument(8,3);
    \echo '---------------------------------'
    -- reservado por otro usuario
    
    /* -- user no existe
    SELECT renovarDocument(1,123) "renovarDocument(1,123)"; */


\echo 'MARCAR RESERVA'
\echo '---------------------------------'
select 'caso 1: El ejemplar esta disponible y el usuario que lo pidio primero es "Alèxia" => ' || marcarReserva(8);
\echo '---------------------------------'
select 'caso 2: El ejemplar no existe => ' || marcarReserva(100);
\echo '---------------------------------'
select 'caso 3: El ejemplar existe pero no esta disponible => ' || marcarReserva(2);
-- mostrar ejemplares disponibles
/* SELECT codiExemplarDisponible('Highway to hell','cd');
SELECT codiExemplarDisponible('Fundamentos de base de datos','Llibre');
SELECT codiExemplarDisponible('La La Land','dvd'); */
\echo 'RETORNAR DOCUMENT'
\echo '---------------------------------'
select 'caso 1: devolver el documento 1 => ' || retornarDocument(1);
select 'caso 1: devolver el documento 5 => ' || retornarDocument(5);
select 'caso 1: devolver el documento 8  => ' || retornarDocument(8);
-- user blocat
/* SELECT prestecDocument(4, 'la la land','revista');

-- 
SELECT prestecDocument(2, 'la la land','dvd'); */

--SELECT prestecDocument(3, 'la la land','cd');

--SELECT prestecDocument(4, 'la la land','dvd');