--() { :; }; exec psql biblioteca -f "$0"
-- Nom: Jordi Andúgar
-- Data: 09/04/19
-- Nom script: funcionsBiblioteca.sql

\c biblioteca;


/* ************************************************************
codiExemplarDisponible

Tasca:
	Donat un títol de document com a argument, torna el primer idExemplar disponible
	IMPORTANT: cal tenir en compte l'estat!!
	Ha de retornar un codi 0 si no hi ha cap exemplar disponible


Entrada:
		 p_titol (varchar): Títol del document a cercar
Retorna:
		 v_idExemplar: id d'exemplar disponible o 0 en cas què no n'hagi cap
************************************************************ */


drop function if exists codiExemplarDisponible(p_titol varchar,p_format varchar);

create or replace function codiExemplarDisponible(p_titol varchar,p_format varchar)
returns int
as $$
	declare
    v_exemplar record;
		c_exemplar CURSOR IS
		  select idExemplar
			from exemplar e inner join document d on e.idDocument = d.idDocument
			where  lower(titol) = lower (p_titol) and lower(estat) = 'disponible'
						and lower(p_format) = lower(format)
							and idExemplar not in (	select idExemplar
																			from prestec
																			where datadev is null);
begin
	-- Utilitzem CURSOR per obtenir una fila, perquè sinó dóna l'error:
	-- ERROR:  query returned more than one row
	open c_exemplar;
	fetch c_exemplar into v_exemplar;
	close c_exemplar;
	return coalesce(v_exemplar.idExemplar,0);
end;
$$ language plpgsql;

/* ************************************************************
funció:		prestecDocument
Tasca: 		Gestiona el prèstec d'un document a un usuari
Entrada: 	p_idUsuari, p_titol
Retorna: 	Missatge informant de com ha anat l'operació
************************************************************ */

--Esborrem la funció si existeix per evitar conflictes
drop function if exists prestecDocument(p_idUsuari prestec.idUsuari%type, p_titol document.titol%type, p_format varchar);

--Creem la funció
create or replace function prestecDocument(p_idUsuari prestec.idUsuari%type, p_titol document.titol%type, p_format varchar)
returns varchar
as $$
	declare
		v_usuari varchar(200);
		v_datadesbloqueig date;
		v_bloquejat boolean;

		v_missatge varchar default 'El prèstec s''ha fet amb èxit';
		v_idExemplar integer;
		v_diesBloqueig integer;
			-- constants pel tope de documents segons format
		maxRevistesLlibres constant integer := 2;
		maxCDs constant integer := 1;
		maxDVDs constant integer := 1;
		  -- vbles que emmagatzemaran la quantitat de prèstecs per tipus i usuari
		v_revistesLlibres integer;
		v_dvds integer;
		v_cds integer;
begin
				-- bloc comprovació usuari
	begin
		select nom || ' ' || cognoms usuari, dataDesbloqueig, bloquejat
		into strict v_usuari, v_datadesbloqueig, v_bloquejat
		from usuari
		where idUsuari=p_idUsuari;
	 exception
			when no_data_found then
			 	return 'Error: l''usuari ' || p_idUsuari || ' no existeix';
	end;   -- fi bloc comprovació usuari

			-- bloc comprovació prèstecs actuals de l'usuari
			-- Comptem els prèstecs que té actualment l'usuari
if v_bloquejat then
   --OJO amb els nuls, si hi ha un NULL a dataDesbloqueig, la concatenació és NULL
  v_missatge := 'Error: Usuari amb compte bloquejat fins el ' || coalesce(v_datadesbloqueig::varchar,'(data desconeguda)');
else
	--Calcular per aquest usuari ACTUALMENT quants prèstecs té de cada tipus
		select
				sum(case when lower(format)='cd' then 1 else 0 end) numCDs,
				sum (case when lower(format) in ('revista', 'llibre') then 1 else 0 end) numRevistesLlibres,
				sum (case when lower(format)='dvd' then 1 else 0 end) numDVDs,
				count (*) totalPrestecs
		into strict v_cds, v_revistesLlibres, v_dvds
		from exemplar e, document d, prestec p
    where d.idDocument = e.idDocument and p.idExemplar=e.idExemplar
							and datadev is null and idUsuari = p_idUsuari;
			-- Informem si l'usuari es passa del màxim de prèstecs simultanis
    case
			when lower(p_format) = 'cd' and v_cds = maxCDs then  -- constant
				v_missatge := 'Error: l''usuari '|| v_usuari||' té actualment en prèstec el màxim de CDs (' || v_cds || ')';
			when lower(p_format) = 'dvd' and v_dvds = maxDVDs then
		  	v_missatge := 'Error: l''usuari '|| v_usuari||' té actualment en prèstec el màxim de DVDs (' || v_dvds || ')';
		  when lower(p_format) in ('llibre','revista') and v_revistesLlibres = maxRevistesLlibres	then
  			v_missatge := 'Error: l''usuari '|| v_usuari||'  té actualment en prèstec el màxim de llibre i revistes (' || v_revistesLlibres || ')';
			-- Comprovem la disponibilitat del document
			else
			  v_idExemplar := codiExemplarDisponible(p_titol, p_format);
				if v_idExemplar = 0 then
					v_missatge := 'El document que vols llogar no està disponible';
				else
				-- Si arribem aquí, vol dir que s'han superat els 3 requeriments
				  insert into prestec
				  values (v_idExemplar, current_timestamp, null, p_idUsuari);
				end if; -- Comprovació exemplar disponible
		end case;   -- Comprovació de limit de prèstecs segons tipus
	end if;       -- Comprovació usuari bloquejat/no bloquejat
	return v_missatge;
end;
$$ language plpgsql;


--marcaReserva
CREATE OR REPLACE FUNCTION marcaReserva(p_idExemplar INT)
RETURNS VARCHAR
AS $$
DECLARE
	v_idUsuari INT;
BEGIN
	--de les reserves d'aquest exemplar, només 1 sera valida
	SELECT idusuari
	INTO STRICT v_idUsuari
	FROM reserva
	WHERE idexemplar = p_idExemplar AND dataAvis IS NULL;
	ORDER BY datareserva;
	--Si retorna dades, vol dir que hiha un usuari esperant aquest exemplar

	--marcar la data d'avis de la reserva
	UPDATE reserva
	SET dataavis = CURRENT_TIMESTAMP
	WHERE idexemplar = p_idExemplar AND dataAvis IS NULL;

	--codi que envii email, sms, etc.
	-- SI un usuari vol tornar a reservar el mateix exemplar
	--caldrà dona una solucio pq la CP es exemplar+soci
	--sol1: tenir un historic i esborrar de la taula reserva
	--sol2: canviar la CP a ex+dataReserva

	--marcar com a reservat el exemplar, perq no surti com a PRESTABLE
	UPDATE exemplar
	SET estat = 'Reservat'
	WHERE idexemplar = p_idExemplar;

	RETURN 'el ejemplar ha sido reservado';
-- si NO retorna dades, vol dir que NO HI HA cap reserva
EXCEPTION
WHEN NO_DATA_FOUND THEN
RETURN 'Este ejemplar no ha sido reservado por ningun usuario';
END;
$$ LANGUAGE PLPGSQL;

--retornaDocument
CREATE OR REPLACE FUNCTION retornarDocument(p_idexemplar INT) -- solo 1 parametro pq ejemplar se ha prestado muchas veces y si es NULL solo cogerá 1 usuario.
RETURNS VARCHAR
AS $$
DECLARE
	v_datapres TIMESTAMP;
	v_idusuari INT;
	v_diesTotalPrestec TIMESTAMP;
	resposta VARCHAR := 'Document retornat correctament\n';
	v_punts INT;
BEGIN
	SELECT idUsuari, dataPres
	INTO STRICT v_datapres, v_idusuari
	FROM prestec
	WHERE idExemplar = p_idexemplar AND datadev IS NULL;

	--exception exemplar no prestat

	--comprovar quants dies han passat
	--v_diesTotalPrestec := CURRENT_TIMESTAMP - v_datapres; asi lo hice yo

	--opció 1
	--v_diesTotalPrestec := TO_CHAR(CURRENT_TIMESTAMP - v_datapres, 'DD') :: INT
	--cal extreure les hores
	--opció 2
	v_diesTotalPrestec := v_datapres + INTERVAL '30 days';
	IF v_diesTotalPrestec > CURRENT_TIMESTAMP --te penalitzacio si entra
	THEN
	--penalitzacio, 1 punt x dia de més
	v_punts := TO_CHAR(v_diesTotalPrestec - CURRENT_TIMESTAMP, 'DD')::INT;

	resposta := resposta || 'Tens una penalitzacio de ' || v_punts;
	UPDATE usuari
	SET punts = punts + v_punts
	WHERE idusuari = v_idusuari;

	SELECT punts
	INTO STRICT v_punts_usuari
	FROM usuari
	WHERE idUsuari = v_idusuari;

	IF punts >= 50 THEN
	--cal bloquejar usuari
	UPDATE USUARI
	SET bloquejat = TRUE, datadesbloqueig = CURRENT_DATE + 15, punts = punts - 50
	WHERE idUsuari = v_idusuari;
	END IF;

	marcaReserva(p_idExemplar);

	--tancar el prèstec
	UPDATE prestec
	set datadev = CURRENT_TIMESTAMP
	WHERE idexemplar = p_idExemplar AND dataDev IS NULL;

	RETURN resposta;

EXCEPTION
WHEN NO_DATA_FOUND THEN
RETURN 'Error: lexemplar no esta en prestec';
END;
$$ LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION recollirReserva(p_idUsuari INT)
RETURNS VARCHAR
AS $$
DECLARE
	info_reserva RECORD;
BEGIN
	SELECT *
	INTO STRICT info_reserva
	FROM reserva
	WHERE idUsuari = p_idUsuari AND dataAvis IS NOT NULL;

	IF info_reserva.dataAvis + INTERVAL '7 days' > CURRENT_TIMESTAMP
	THEN
		prestecDocument(info_reserva.idUsuari, v_titol, v_format, info_reserva);
		DELETE FROM reserva
		WHERE idUsuari = p_idUsuari AND dataAvis IS NOT NULL;
	END IF;

EXCEPTION
WHEN NO_DATA_FOUND THEN
RETURN 'no tienes niguna reserva pendiente de recoger';

END;
$$ LANGUAGE PLPGSQL;
