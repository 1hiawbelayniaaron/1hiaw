#!/bin/bash

# Script Name: biblioteca.sh
# Author: a211762hb
# Date: 19/04/2023
# Version: 2.0
# License: This is free software, licensed under the GNU General Public License v3. See http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 
# Description: 

# borrar y crear la base de datos
dropdb biblioteca
createdb biblioteca
# cargar tablas
psql biblioteca -f 'taulesBiblioteca.sql'
# cargar las funciones
psql biblioteca -f 'funcionsBiblioteca.sql'
# cargar juego de pruebas
psql biblioteca -f 'proves.sql'

#psql biblioteca -f 'funcions.sql'




