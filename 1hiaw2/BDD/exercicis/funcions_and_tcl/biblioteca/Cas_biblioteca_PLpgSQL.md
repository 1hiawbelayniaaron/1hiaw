# Cas Biblioteca
---

Es desitja implementar la gestió de prèstecs de documents als usuaris de la
biblioteca.
Hi ha 4 tipus de documents per agafar en prèstec o consultar in situ:
* llibre
* CD
* DVD
* revista

Consulta al catàleg de la biblioteca [aqui](http://aladi.diba.cat/)

Si es revista, cal la data d'edició, ja que no es pot prestar la darrera revista

Hi ha 4 fets de negoci bàsics a dintre del model biblioteca:  
* Prèstec de document
* Renovació de document
* Retorn de document
* Reserva de document


## Prèstec de documents

**funció prestecDocument**

La normativa de les biblioteques de la Diputació de Barcelona és que es poden
tenir en prèstec d'un màxim 30 documents simultànis, concretament:
* 15 llibres i revistes
* 6 dvds (pelis, documentals)
* 9 altres docs (cd, cd-rom) cançons, contes narrats, etc

__Per simplificació, considerarem que un usuari podrà tenir simultàniament un
màxim de 4 docs:__
- 2 docs del tipus llibre/revista
- 1 DVD
- 1 CD

Cada document pot tenir un o varis exemplars. Cada exemplar es presta a un usuari determinat de la biblioteca en una data en concret. Aquest exemplar es retornarà i el podrà agafar altre usuari, de manera que un exemplar tindrà a la bd milers de prèstecs, però el que és segur que **actualment** només podrà estar prestat a un usuari (fixeu-vos en l'estructura de la taula prestec). Què representarà el camp datadev buit?

Cada **exemplar** té un dels quatre possibles **estats**:
- Disponible: es pot prendre en prèstec, sempre i quan no estigui actualment prestat.
- Exclòs de prèstec: només es pot consultar in situ a la biblioteca.
- En exposició: temporalment està de mostra en un aparador i queda exclòs de prèstec.
- Reservat: queda exclòs de prèstec perquè un usuari espera el seu torn de prèstec.

Per facilitar la implementació d'aquesta funció, farem en primer lloc la funció **codiExemplarDisponible**, la qual donat un titol de document, ens retornarà:
- Codi del primer exemplar que es pugui afagar en prèstec ó
- Un zero, en cas que no hi hagi cap exemplar disponible en aquests moments.

<ins>Un exemplar el podrem afagar en prèstec si actualment no està en prèstec i a més a més el seu estat és _Disponible_.</ins>
En qualsevol altre cas, no el podriem afagar en prèstec.

Per la funció prestecDocument, __hem de tenir un exemplar disponible i evidentment un usuari que no tingui blocat el carnet degut a penalitzacions acumulades.__

## Renovació de documents

**funció renovarDocument**

Es poden renovar fins a tres vegades, la qual cosa es pot fer en persona,
telèfon o internet.

Els documents es poden renovar sempre i quan:

* no s'hagin renovat ja tres vegades
* no hagi expirat la data de retorn (l'usuari disposa de 30 dies des de la data de prèstec)
* no es tingui el carnet de préstec bloquejat per algun motiu
* no estiguin reservats per un altre usuari

La renovació és de 30 dies naturals sobre la data de prèstec.

La renovació d'un document s'implementa a la BD a la taula prestec (número de renovacions).
La data de prèstec no es modifica en cap moment.

## Comprovació de si el document que es retorna està reservat per un nou usuari.

** funció marcarReserva **

Quan es retorni un exemplar, abans de que quedi disponible per prèstec per altres
usuaris, s'haurà de comprovar que no l'esperi cap usuari, és a dir, que altre usuari
el tingui reservat. Si fos el cas, l'estat de l'exemplar haurà de passar a _reservat_
(amb el que quedarà immediatament fora de la llista d'exemplars disponibles).
Com que l'exemplar ja està disponible per l'usuari que l'havia reservat, se marca
la data d'avís a la reserva.

L'usuari tindrà a partir d'aquest moment una setmana per passar a recollir-lo. Si
passada una setmana no el recollís, s'esborraria la reserva i l'exemplar tornaria a
estar en circulació per ser prestat. Aquest darrer paràgraf és purament descriptiu,
però no l'implementarem, perquè s'ha de fer amb un nou objecte de base de dades
anomenat **job**

Compteu que un usuari pot tenir més d'una reserva i que un exemplar pot estar reservat
per més d'un usuari. En aquest cas, ¿quin de tots els usuaris serà el primer que està
esperant la reserva?

Aquesta funció la farem servir a dintre de la funció **retornarDocument**

## Retorn de prèstecs i penalització (sistema de punts)

**funció retornarDocument**  
Estudieu les taules usuari i penalització.

Per al bon funcionament del servei de préstec et recordem que has de ser puntual
 a l'hora de retornar els documents. Quan es presta un document es tenen **30 dies**
 per retornar-lo. Si passats aquests 30 dies no s'ha retornat, per cada  document
 en préstec i dia de retard, el teu carnet rebrà 1 punt de penalització. Per cada
 50 punts, el  carnet quedarà bloquejat durant 15 dies. Passat aquest temps, el carnet tornarà
  a estar operatiu.

Quan un usuari superi els 50 punts de demèrit, és reiniciarà el comptador de
punts.

Hi ha llibres que no es presten (són  només de consulta a la biblioteca).

Si l'exemplar que es retorna està reservat, NO ha d'aparèixer com a disponible
pel públic en general.


# EXAMEN  

**funció marcarReserva** (Exercici 1)  

Entrada: p_idExemplar  
Sortida: Misatge indicant "Exemplar X està reservat per l'usuari Nom_usuari"  
Tasca:  Quan un exemplar queda lliure, es mira la taula reserva i es mira quin és l'usuari que porta més temps esperant (sí, pot ser que per un exemplar hi hagi més d'un usuari esperant). Aquest exemplar ha de quedar marcat com a reservat i s'ha de marcat la reserva com que se li avisa a l'usuari a data d'avui (mireu taula reserva).  

Compteu que un usuari pot tenir més d'una reserva i que un exemplar pot apareixer més d'una vegada reservat per varis usuaris.


**funció reservarDocument**  Exercici 2 examen

Entrada: p_idUsuari, p_idExemplar  
Sortida: Missatge del que ha passat  
Tasca: Mirem si el document està en reserva i si ja l'han retornat. Ens quedem amb la data de la reserva que ha fet l'usuari per tal de poder calcular fins quan pot recollir el document. D'altra banda, s'ha de calcular la data límit que té l'usuari per recollir el document. Hem d'informar que té 7 dies per recollir el document i mostrar la data límit de recollida.  
			
Exercici 3: jocProves.sql

Feu les comprovacions amb aquest fitxer, tal com s'ha explicat a classe (teniu exemples penjats)






## Recollir la reserva de l'exemplar

**funció recollirReserva**

Aquesta funció eliminarà la reserva i la transformarà en un prèstec.




**NOTA**

Cada funció ha d'acomplir aquestes dues regles:

* Tenir el mínim nombre d'arguments necessaris
* Han de retornar un missatge dient el que ha passat.
