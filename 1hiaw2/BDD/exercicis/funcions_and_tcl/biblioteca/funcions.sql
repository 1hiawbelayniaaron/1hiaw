CREATE
OR REPLACE FUNCTION codiExemplarDisponible(p_titol varchar, p_format varchar) returns integer as $$ DECLARE v_codiExemplar integer;

BEGIN
SELECT
    e.idexemplar INTO STRICT v_codiExemplar
FROM
    exemplar e
    LEFT JOIN document d ON e.iddocument = d.iddocument
    LEFT JOIN prestec p ON e.idExemplar = p.idExemplar
WHERE
    lower(d.titol) = lower(p_titol)
    AND e.estat = 'Disponible'
    AND (
        p.idexemplar IS NULL
        OR p.datadev IS NOT NULL
    )
    AND LOWER(d.format) = LOWER(p_format)
LIMIT
    1;

IF v_codiExemplar IS NOT NULL THEN RETURN v_codiExemplar;

END IF;

EXCEPTION
WHEN NO_DATA_FOUND THEN RETURN 0;

end $$ language plpgsql;

CREATE
OR REPLACE FUNCTION prestecDocument(p_idUsuari integer, p_titol varchar, p_format varchar) RETURNS void AS $$ 
DECLARE 

v_codiExemplar integer;

v_cd smallint;

v_llibre smallint;

v_dvd smallint;

v_revista smallint;

v_docs smallint;

v_format_prestec varchar;

v_bloqueado boolean;

v_datadev TIMESTAMP;

v_usuari int;
BEGIN -- Mira si el usuario esta bloqueado
SELECT
    bloquejat INTO STRICT v_bloqueado
FROM
    usuari
WHERE
    idusuari = p_idUsuari;

-- Si el Usuario esta bloqueado
IF v_bloqueado = TRUE THEN RAISE EXCEPTION 'USUARIO CON EL CARNET BLOQUEADO';

END IF;

-- importante siempre que hacemos select necesitamos encapsular la excepción excepto si utilizamos COUNT()
--BEGIN
SELECT
    COUNT(*) INTO STRICT v_cd
from
    prestec
    LEFT JOIN exemplar USING(idexemplar)
    LEFT JOIN document USING(iddocument)
WHERE
    idUsuari = p_idUsuari
    AND LOWER(format) = 'cd';

SELECT
    COUNT(*) INTO STRICT v_docs
from
    prestec
    LEFT JOIN exemplar USING(idexemplar)
    LEFT JOIN document USING(iddocument)
WHERE
    idUsuari = p_idUsuari
    AND LOWER(format) IN ('llibre', 'revista');

SELECT
    COUNT(*) INTO STRICT v_dvd
from
    prestec
    LEFT JOIN exemplar USING(idexemplar)
    LEFT JOIN document USING(iddocument)
WHERE
    idUsuari = p_idUsuari
    AND LOWER(format) = 'dvd';


-- sumamos 1 a el total del documento que quiera adquirir el user

IF LOWER(p_format) IN ('llibre','revista') THEN v_docs := v_docs + 1;
END IF;


 IF LOWER(p_format) = 'cd' THEN v_cd :=  v_cd + 1;
END IF;


IF LOWER(p_format) = 'dvd' THEN v_dvd := v_dvd + 1;
END IF; 


--EXCEPTION
--WHEN NO_DATA_FOUND THEN 
--RETURN 'Error:'
--END;
-- comprovar el numero de documentos se pasa del que toca
IF (v_docs >= 3) THEN RAISE EXCEPTION 'NO PUEDES PEDIR PRESTADOS MÁS DOCUMENTOS IMPRESOS PORQUE YA TIENES 2';
END IF;

IF (v_cd >= 2) THEN RAISE EXCEPTION 'NO PUEDES PEDIR PRESTADOS MÁS CD PORQUE YA TIENES 1';
END IF;

IF (v_dvd >= 2) THEN RAISE EXCEPTION 'NO PUEDES PEDIR PRESTADOS MÁS DVD PORQUE YA TIENES 1';
END IF;


SELECT
	codiExemplarDisponible(p_titol, p_format) INTO v_codiExemplar;

IF v_codiExemplar = 0 THEN 
    RAISE EXCEPTION 'El ejemplar no esta disponible';
ELSE
    SELECT
        datadev,
        idusuari INTO v_datadev,
        v_usuari
    FROM
        prestec
    WHERE
        idexemplar = v_codiExemplar;  
END IF;



-- SI SE PUEDE RESERVAR MIRAR SI SE RENUEVA O NO
IF v_datadev IS NULL THEN
-- si el usuario coincida renueva el prestamos
    IF v_usuari = p_idUsuari THEN
        UPDATE
            prestec
        SET
            datapres = current_timestamp,
            idusuari = p_idUsuari,
            vegadesrenovat = vegadesrenovat + 1
        WHERE
            idexemplar = v_codiExemplar; 

        RAISE EXCEPTION 'Renovado el prestamo de "%", para el prestamo',p_titol;
    -- el prestamo no se da
    ELSE
        RAISE EXCEPTION 'No esta disponible el ejemplar "%", para el prestamo',p_titol;
    END IF;
-- SI SE PUEDE RESERVAR PERO NO RENUEVA
ELSIF v_datadev IS NOT NULL THEN
    UPDATE
        prestec
    SET
        datapres = current_timestamp,
        idusuari = p_idUsuari,
        datadev = null
    WHERE
        idexemplar = v_codiExemplar; 

    RAISE NOTICE 'Realizado el préstamo del % "%" para el usuario con id %',
    p_format,
    v_codiExemplar,
    p_idUsuari;

END IF;  


RAISE NOTICE 'cd = %      dvd = %    docs = %   format = % codigo_ejemplar = %',
v_cd,
v_dvd,
v_docs, p_format, v_codiExemplar;

-- informar dels documents actual
/* CASE LOWER(p.format)
 WHEN 'cd' THEN
 IF v_cd */
END $$ LANGUAGE plpgsql;

