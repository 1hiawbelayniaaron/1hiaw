# 1. Zoos

Es vol dissenyar una base de dades relacional per emmagatzemar informació relativa als zoos existents en el món, així com les espècies i animals que aquests alberguen.

De cada zoo es vol emmagatzemar el seu codi, nom, la ciutat i país on es troba, mida (m2) i pressupost anual.
Cada zoo codifica els seus animals amb un codi propi, de manera que entre zoos es pot donar el cas que es repeteixi.
De cada espècie animal s'emmagatzema un codi d'espècie, el nom vulgar, el nom científic, família a la qual pertany i si es troba en perill d'extinció.
A més, s'ha de guardar informació sobre cada animal que els zoos tenen, com el seu número d'identificació, espècie, sexe, any de naixement, país d'origen i continent.

Establiu els atributs que considereu oportuns i d'ells, trieu l'indentificador adient per a cada entitat.

# 2. Model conceptual

## 2.1. Enllaç públic a l'esquema

[cas zoos](https://drive.google.com/file/d/1g6IlZOVaYk8BxXYsh98wHR9_f7qkOAa3/view?usp=sharing)

## 2.2. Esquema conceptual (EC ó ER)

  ![cas zoos](./3_zoos.drawio.png)

# 3. Model lògic relacional

## 3.1. Esquema lògic

  Zoo(<ins>IdZoo</ins>, NomZoo, CiutatZoo, PaisZoo, MidaEnM2, PressupostAnual)  
  Animal(<ins>IdZoo, IdAnimal</ins>, SexeAnimal, AnyNaixement, PaisOrigen, ContinentOrigen, IdEspecie)  
  Especie(<ins>IdEspecie</ins>, NomVulgar, NomCientific, FamiliaEspecie, PerillExtincio)  
  \...

## 3.2. Diagrama referencial

| Relació referencial | Clau aliena | Relació referida |
| ------------------- |:-----------:| ---------------- |
| Animal              | IdZoo       | Zoo              |
| Animal              | IdEspecie   | Especie          |

# 4. Model físic

## 4.1 Enllaç a l'esquema físic

[script zoos.sql](./zoos.sql)
