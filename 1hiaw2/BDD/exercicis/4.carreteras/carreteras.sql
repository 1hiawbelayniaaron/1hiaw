\c template1
drop database if exists carreteras;
create database carreteras;
\c carreteras
CREATE TABLE Carretera (
	IdCarretera varchar(13) CONSTRAINT Carretara_pk PRIMARY KEY
);

CREATE TABLE Tram (
	IdTram varchar(13) CONSTRAINT tram_pk PRIMARY KEY,
	IdCarretera varchar(13),
	CONSTRAINT carretara_tram_fk FOREIGN KEY (IdCarretera) REFERENCES Carretera(IdCarretera)
);

CREATE TABLE Municipi (
	IdMunicipi varchar(13) CONSTRAINT municipi_pk PRIMARY KEY
);

CREATE TABLE Area (
	IdArea varchar(13) CONSTRAINT  area_pk PRIMARY KEY
);

CREATE TABLE TramXMunicipi (
	IdMunicipi varchar(13),
	IdTram varchar(13),
	km_entrada smallint,
	km_sortida smallint,
	CONSTRAINT tramxmunicipi_pk PRIMARY KEY(IdMunicipi,IdTram),
	CONSTRAINT tramxmunicipi_idMunicipi_fk FOREIGN KEY (IdMunicipi) REFERENCES Municipi(IdMunicipi),
	CONSTRAINT tramxmunicipi_idTram_fk FOREIGN KEY (IdTram) REFERENCES Tram(IdTram)
);


--Inserts

INSERT INTO Carretera VALUES ('123');
INSERT INTO Carretera VALUES ('213');

INSERT INTO Tram VALUES ('612', '123');
INSERT INTO Tram VALUES ('458', '213');

INSERT INTO Municipi VALUES ('788');
INSERT INTO Municipi VALUES ('878');

INSERT INTO Area VALUES ('58'); 
INSERT INTO Area VALUES ('85'); 

INSERT INTO TramXMunicipi VALUES ('788', '612', 12, 12);
INSERT INTO TramXMunicipi VALUES ('878', '458', 7, 10);
