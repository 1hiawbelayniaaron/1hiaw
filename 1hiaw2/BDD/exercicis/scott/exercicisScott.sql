/* 
	Nom i cognom: Aaron Belayni  
	codi: a211762hb
*/

-- 15. Mostreu el codi d’empleat, salari, comissió, n o de departament i data de la taula EMP.
SELECT empno, sal, comm, deptno, hiredate
from emp;

-- 16. Mostra tota la informació de la taula departaments
SELECT *
FROM dept;

-- 17. Mostreu el nom i l’ocupació d’aquells que siguin salesman.
SELECT ename, job
from emp
where UPPER(job)='SALESMAN';

-- 18. Mostreu el nom i el codi de departament d’aquells empleats que no treballen en el departament AL DEPT 30
SELECT ename, deptno
from emp
where NOT deptno=30;

-- 19. Mostreu els empleats que treballen en els departaments 10 y 20 (es vol les dues solucions).

SELECT ename, deptno
from emp
where deptno IN (10,20);
-- segona solucio
SELECT ename ,deptno
from emp
where deptno=20 OR deptno=10;

-- 20. Mostreu el nom i salari d’aquells empleats que guanyin més de 2000.
SELECT ename        
from emp
where sal > 2000;

-- 21. Mostreu el nom i la data de contractació d’aquells empleats que hagin entrat abans de l’1/1/82.
SELECT ename, hiredate
from emp
where hiredate < TO_DATE('1/1/82','mm,dd,yy');

-- 22. Mostreu el nom dels salesmans que guanyin més de 1500.
SELECT ename          
from emp
where sal > 1500 AND UPPER(job)='SALESMAN';

-- 23. Mostreu el nom d’aquells que siguin ‘Clerk’ o treballin en el departament 30.
SELECT ename               
from emp
where deptno=30 OR INITCAP(job)='Clerk';

-- 24. Mostreu aquells que es diguin ‘SMITH’, ‘ALLEN’ o ‘SCOTT’.
SELECT ename
from emp
where UPPER(ename)='SMITH' OR UPPER(ename)='ALLEN' OR UPPER(ename)= 'SCOTT';

-- 25. Mostreu aquells que no es diguin ‘SMITH’, ‘ALLEN’ o ‘SCOTT’.
SELECT ename
from emp
where NOT ename='SMITH' 
  AND NOT ename='ALLEN' 
  AND NOT ename = 'SCOTT';
  
SELECT ename
from emp
where LOWER(ename) NOT IN('smith','allen','scott');

-- 26. Mostreu aquells el salari estigui entre 2000 i 3000.
SELECT ename
from emp
where sal between 2000 AND 3000;

SELECT ename
from emp
where sal IN (2000,3000);

-- 27. Mostreu aquells empleats que treballin en el departament 10 o 20.
SELECT ename     
from emp
where deptno=10 OR deptno=20;

-- 28. Mostreu aquells empleats el nom comenci per ‘A’.
SELECT *
FROM emp
WHERE UPPER(ename) LIKE 'A%';



-- 29. Mostreu aquells empleats el nom tingui com a segona lletra una “D”.
SELECT *
FROM emp
WHERE UPPER(ename) LIKE '_D%';

-- exercici Extra (enmame acabat amb s)
scott=> SELECT *
FROM emp
WHERE UPPER(ename) LIKE '%_S';


-- 30. Mostreu els diferents departaments que hi ha a la taula EMP.
SELECT DISTINCT deptno
from emp;

-- 31. Mostreu el departament i el treball dels empleats (evitant repeticions).
SELECT DISTINCT deptno, job
from emp;

-- 32. Mostreu aquells empleats que hagin entrat el 1981.
SELECT ename
FROM emp
WHERE to_char(hiredate,'yyyy') = '1981';

-- 33. Mostreu aquells empleats que tenen comissió, mostrant nom i comissió.
SELECT ename,comm
FROM emp
WHERE comm IS NOT null;

-- 34. Mostreu aquells empleats que guanyin més de 1500, ordenats per ocupació.
SELECT ename,job
FROM emp
WHERE sal<1500
ORDER BY job;

-- 35. Calcular el salari anual a percebre per cada empleat (tingueu en compte 14 pagues).
SELECT ename,sal*14 sal_anual
FROM emp;

-- 36. Mostreu el nom de l’empleat, el salari i l’increment de l’15% de l’salari.
SELECT ename,sal*1.15-sal increment_sal_15_percent
FROM emp;

-- 37. Mostreu el nom de l’empleat, el salari, l’increment de l’15% de l’salari i el salari augmentat un 15%.
SELECT ename,sal*1.15-sal increment_sal_15_percent,   sal*1.15 sal_més_15_percent
FROM emp;

