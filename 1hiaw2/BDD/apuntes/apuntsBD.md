# **Introducció a SQL**

#### \h + ordre ==> es un man de sql

#### conectar-se a la base desde terminal: psql -f  'arxiu' nom__basedd

### mayuscula i minusculas:

SELECT LOWER('Hola que tal') => hola que tal
SELECT UPPER('Hola que tal') => HOLA QUE TAL
SELECT INITCAP('Hola que tal') => Hola Que Tal

### (LOWER,UPPER,INITCAP): sempre posar un d'aquests

> WHERE INITCAP(job) = 'Salesman'

amb \d + taula podem veure el tipu de dada que necesitem.

### default:

nomes servirà per a l'insert.

> hiredate date default CURRENT_DATE

### date:

per passar entre int, date i string.

##### int <= => String <= => date  (podem passar de string a int i de string a date)

> to_char(current_timestamp,'yyyy-mm/dd hh24:mi:ss')

Serveix per donar una data en format date en format de cadena.  to_char(date,'yy/mm/dd')

> to_date('cadena','dd/mm/yyyy')
> <b>per fer insert sempre posar això</b>

Serveix per passar de cadena de text a dada de tipus date.

### permisos:

Si tenim un problema amb els problemes d'execució:

> DROP DATABASE   nom_base
> \i ruta_script_base.sql

### select:

Per buscar mayuscules i minuscules

> SELECT

___

## <u>SQL</u>

**DDL:** (modificar les taules, el contenidor)
**commands: create, drop, alter, rename, truncate**

**DQL:**  (consultar dades)
**commands: select**

**DML:**(modificar les dades)
**commands: insert, delete, update**

**TCL:** (s’encarrreguen de que les transaccions siguin correctes)
**commands: commit, rollback, savepoint, begin**

**DCL:** (permisos)
**commands: grant, revoke**

___

# <b>SELECT:</b>

El select no pot modificar ja que és DML

- 1r cas de SELECT:

>  SELECT *
>     FROM emp;

- 2n cas de SELECT:

> SELECT ename,sal
> FROM emp;

</br></br>

#### Etiquetes:

Són el nom de la columna,
**Si posem " " (cometes dobles) a les etiquetes respectarà les mayuscules i minuscules**
</br></br>

#### Aritmetica al select:

```sql
SELECT ename, sal,sal+3000
FROM emp;
```

> Ens dona el nom del treballador, el seu salari i el salari augmentat

</br></br>

#### Null (absencia de valor):

  Un null no es pot ni operar ni comparar com a tal.

> Per tant primer el filtrarem.

```sql
-- EXEMPLE:
  SELECT ename, job, sal, comm, sal + comm
  FROM emp;

 -- o també

  SELECT ename, job, sal, comm, sal + comm acumulat
  FROM emp;
```

##### **Solució nulls**

> COALESCE(quin_camp_volem_mirar_si_te_nulls,valor_en_que_el_vull_transformar)

*important: el tipus de data ha de ser el mateix  ==> COALESCE(date,date)*

> SELECT ename, COALESCE(CAST(mgr AS varchar),'no té jefe')
>   FROM EMP;

<u>exemple:</u>
  SELECT ename, job, sal, comm, sal + COALESCE(comm,0) acumulat
  FROM emp;

 ![](./null.png)

         D'aquesta manera podrem fer servir COALESCE per convertir els nulls amb valors que si que es puguin sumar.
    
           null = 0

</br></br>

#### Concatenar

**Operador ||:   serveix per concatenar (com el + al java)**

```sql
SELECT ename || ' treballa de ' ||
FROM emp;

/*
     SMITH treballa de CLERK
     ALLEN treballa de SALESMAN
     WARD treballa de SALESMAN
     JONES treballa de MANAGER
     MARTIN treballa de SALESMAN
     BLAKE treballa de MANAGER
     CLARK treballa de MANAGER
     SCOTT treballa de ANALYST
     KING treballa de PRESIDENT
     TURNER treballa de SALESMAN
     ADAMS treballa de CLERK
     JAMES treballa de CLERK
     FORD treballa de ANALYST
     MILLER treballa de CLERK
*/
```

</br></br>

#### DISTINCT:

SELECT DISTINCT job
from emp;

**job**
 CLERK
 PRESIDENT
 MANAGER
 SALESMAN
 ANALYST
(5 rows)
![](./jobDeptno.png)

</br></br>

#### WHERE al SELECT:

1. Mostrar els nom dels treballadors i el sou, només dels que tinguin un sou entre [1000,1500]
   
   > SELECT ename, sal
   > FROM emp
   > WHERE sal >=1000 AND sal <=1500;

2. Mostrar treballadors que són d'un departament superior al 10.
   
   > select ename, deptno
   > from emp
   > where deptno>10;

3. Mostrar els treballadors que siguin Salesman, clerk o analyst.
   
   > SELECT empno,job
   > FROM emp
   > WHERE UPPER(job)='CLERK' OR UPPER(job)='SALESMAN' OR UPPER(job)='ANALYST' ;

##### IN és per comparar amb una llista de valors

3. Mostrar els treballadors que siguin Salesman, clerk o analyst.
   
   > SELECT empno,job
   > FROM emp
   > WHERE LOWER(job) IN('analyst','clerk','salesman');

</br></br>

#### BETWEEN:

> SELECT ename, sal
> FROM emp
> WHERE sal BETWEEN 1000 AND 1500;

**Inclou els LIMITS sempre: és a dir que podem fer [1,4] però no (1,4)**
</br></br>

#### DELETE:

borrar empleats de l'any 2007

> delete from emp
> where to_char(hiredate,'yyyy') = '2007';

</br></br>

###### RECORDAR:

#### joc de proves:

serveix per comprovar que tota la base funciona, a partir de inserts i delete

</br></br>

#### tipus de dades:

Per no equivocarnos posar als dos costats de l'operador el mateix tipus de dades:

> WHERE hiredate(date) = '2007'(varchar) <b>MALAMENT</b>
>   WHERE TO_CHAR(hiredate,'YYYY')(varchar) = '2007' (varchar) <b>BÉ</b>

     int <= => String <= => date  (podem passar de string a int i de string a date).
    
     Per passar de int a date o al reves s'ha de passar per string.

Casting = canvi de tipus de dades

Sempre que comparem <b>dates</b> fer servir <b>DADES de tipus DATE.</b> fer servir TO_DATE().
Si ho fem amb TO_CHAR necessitem que estigui ordenat per any, mes, dia. ja que compara amb la taula <b>ASCII.</b>
</br></br>

#### IN

L' IN equivaldrà a més d'un valor, sempre que sigui més d'un valor fer servir IN sinó està malament.

#### Caracter

**metacaracteres:**
_ => un caracter qualsevol
% => x quantitat de caracters.

# <b>INNER joins (amb claus alienes nul.les no funciona)</b>

Servirà principalment per fer consultes més  complexes que necessiten més d'una fila o més d'una taula

```sql
  -- SENTENCIA:

  SELECT table1.column, table2.columna2
  FROM table1 [INNER] JOIN table2
        ON table1.Clave_Ajena = table2.Clave_primaria;

 -- o també

  FROM table1,
  DEPT
  WHERE table1.clau_aliena = table1.clau_aliena;
```

---

```sql
 EXEMPLE:

 SELECT emp.empno,emp.ename,emp.deptno,
    dept.deptno, dept.loc
 FROM emp JOIN dept ON emp.deptno = dept.deptno;

                       (primer on manca la info i despres d'on la treiem)
```

---

```sql
  --EXERCICI: de cada comanda volem el codi de comanda, data , import , nom client

  SELECT pedido.pednum, pedido.fecha, pedido.importe, cliente.nombre
  FROM pedido JOIN cliente ON pedido.cliecod = cliente.cliecod;



 -- podriem fer el mateix pro possant nomes les taules necessaries:

  SELECT pednum, fecha, importe, nombre
  FROM pedido JOIN cliente ON pedido.cliecod = cliente.cliecod;
```

## <b> Alias</b>

Serveixien per simplificar les consultes.

```sql
  SELECT p.pednum, p.fecha, p.importe, c.nombre
  FROM pedido p JOIN cliente c ON c.cliecod = p.cliecod;

  -- li posem alias 'p' a 'pedido' i alias 'c' a 'cliente'
```

# <b>OUTER Joins (quan hi ha clau aliena nula)</b>

#### Per saber si una clau aliena té nulls fer servir WHERE clau_aliena IS NULL

> hi ha tres tipus de Joins
>     -  LEFT JOIN: nomes una taula
>     -  RIGHT JOIN: nomes una taula
>     -  FULL JOIN: tota info de les dues taules

<b>per saber els joins que podem fer \d nom_taula i mirar al final a "referenced by:"</b>

el Outer join servira per mostrar una fila si o si, encara que no cumpleixi la condició de join.

```sql
  -- SENTENCIA:
  SELECT table1.column, table2.columna2
  FROM table1 LEFT/RIGHT/FULL JOIN table2
        ON table1.Clave_Ajena = table2.Clave_primaria;
```

És molt important dir a quina taula tenim tota la informació, en funció d'això triem si es LEFT o RIGHT.

```sql
-- EXEMPLE:
SELECT nombre , ciudad
FROM repventa r LEFT JOIN oficina o ON o.ofinum = r.ofinum;
```

<b>Si la taula important és la primera posem LEFT i si és la segona posem RIGHT</b>

## <b>left / right JOIN</b>

### cas 2

A <=== B (CA nuls) <=== C (CA nuls)
(c LEFT JOIN b LEFT JOIN a)<b> si l'ordre és c,b,a </b>
(a RIGHT JOIN b RIGHT JOIN c)<b>  si l'ordre és a,b,c</b>

### cas 2

A <=== B  <=== C (CA nuls)
(c LEFT JOIN b JOIN a)<b> no cal el segon RIGHT </b>
(a RIGHT JOIN b RIGHT JOIN c)<b>  si l'ordre és a,b,c</b>

## <b> Self join</b>

```sql
-- veure el cap d'un empleat quan el cap és empleat també
SELECT emp.ename EMPLEAT, jefe.ename CAP
FROM emp JOIN emp jefe ON emp.mgr = jefe.empno;
```

   RESULTAT:
   ![](./empleatXcap.png)

## <b> Volumetria</b>

```sql
-- Hem de mirar quina taula és la important, i hem de fer que sorti el numero de files de la taula important

-- EXEMPLE:

    SELECT nombre, COALESCE(o.ciudad,'** no té valor **') , COALESCE(o.region,'** no té valor **')
    FROM Repventa r LEFT JOIN oficina o ON o.ofinum = r.ofinum;
```

<b>
     afegim COALESCE() per tractar els valors nulls de la clau aliena de la segona taula (la taula no important) </b>
<br><br><br>

   RESULTAT:
   ![](./coalesce_volumetria.png)

## <b> ORDER BY</b>

<u>**per ordre d'arguments al SELECT:**</u>
SELECT apellido1 || ', ' || apellido2 || ', ' || nombre
FROM alumnos
ORDER BY 1,2,3;

<u>**per ordre que li donem nosaltres:**</u>
SELECT apellido1 || ', ' || apellido2 || ', ' || nombre
FROM alumnos
ORDER BY apellido1,apellido2,nombre;

## <b> regla: 1 campo 1 valor:</b>

      en la clausula where :
          - No puede tener dos valores un campo, campo=null AND campo=0

## <b> data +/- int = data </b>

      sempre que sumem un enter a un date ens retorna un date

## <b> regla: data-data = dies </b>

## <b> regla: timestamp2-timestamp2 = interval </b>

## <b> JOINS COMPOSTOS</b>

quan fem un JOIN de un JOIN tenim tots els camps del primer JOIN disponibles.

podem comparar amb JOIN camps  que no siguin clau aliena i clau composta

## <b> CROSS JOIN (Producto cartesiano) </b>

El producte cartesià és el conjunt de convinacions que podem fer

a (1,2,3)
b (a,b,c)

1a, 1b, 1c, 2a, 2b, 2c, 3a, 3b, 3c

```sql
    -- sempre que no possem un igual en el join farem un producte cartesià

    SELECT *
    FROM EMP,
    DEPT;
```

![](./menu.png)

 The syntax of the CROSS JOIN in SQL will look like the below syntax:

```sql
  SELECT ColumnName_1,
         ColumnName_2,
          ColumnName_N
 FROM [Table_1]
 CROSS JOIN [Table_2]
```

Or we can use the following syntax instead of the previous one.

```sql
  SELECT ColumnName_1,
         ColumnName_2,
         ColumnName_N
  FROM [Table_1],[Table_2]
```

![](./cross_join_restrictius.png)

## <b> FUNCIONS DE GRUP </b>

les funcions normals de SQL miren només una fila, en canvi les funcions de grup miren en N files.

NO ACCEPTEN NULLS

```sql
Les principals funcions de grup són:
```

<b>
SUM() <br>
MAX()<br>
MIN()<br>
COUNT(*)<br> count incondicional ==> si tenim un outer join 
COUNT(camp) => if camp is not null <br> count condicional
AVG()
</b>

#### <b> Exemple1 </b>

mostra el salari mitja, el salari major, el salari més petit i el total de salaris

```sql
  SELECT AVG(sal), MAX(sal),
         MIN(sal), SUM(sal)
  FROM emp
  WHERE lower(sal)='salesman';
```

#### <b> Exemple2 </b>

mostrar la data de contractació més antiga i la més nova:

```sql
  SELECT MIN(hiredate),MAX(hiredate)
  FROM emp;
```

#### <b> Exemple3 </b> (conta el total de files)

conta el total de files que hi ha a empleat

```sql
  SELECT COUNT(*)
  FROM emp;
```

#### <b> Exemple3 </b> (les files que tenen un valor no null)

conta les files de la taula emp els valors no nulls de comm

```sql
  SELECT COUNT(comm)
  FROM emp;
```

#### <b> Canviar tipus de dades</b> (segur que entra a examen) (CAST)

```sql
SELECT ename, COALESCE(mgr::varchar,'no té jefe')
FROM EMP;
```

un altre manera més rapida és:

> expression::text

```sql
SELECT ename, COALESCE(mgr::varchar,'no té jefe')
FROM EMP;
```

#### <b> COUNT </b> (segur que entra a examen)

Normalment és recomanable fer COUNT(*) per que sutin els valors nulls també. Només es fer COUNT(camp) si només volem els camps no nulls.

```sql
  SELECT COUNT(*)
  FROM emp
  WHERE mgr IS NOT NULL;

  SELECT COUNT(mgr)
  FROM emp;
```

##### OUTER JOIN + COUNT (*):

- Hem de vigilar molt perquè no si fem un outer JOIN agafem també els valors null de la segona taula per aixó com a exemple si tenim un representant sense cap pedido i contem els pedidos amb COUNT(*) ens dirà que té un pedido i realment es perq conta les files del JOIN com una sola. **Per posar solució necessitarem possar (nom_taula\*)**

#### <b> Pis de l'SQL </b>

1. Subconsultes
2. funcions de grup
3. Joins
4. SQL bàsic

si ens demanen un join hem de fer servir el 3. i el 4. , si ens demanen una funció de grup necessitem el 2. el 3. i el 4.

#### ****GROUP BY****

Necessitem agrupar les files segons els camps d'agrupació.![](/home/users/inf/hiaw1/iaw49656032/Desktop/1hiaw/1hiaw/bd/group_by.png)

Totes les parts de la clausula SELECT que no siguin funcions de grup les posem a la clausula GROUP BY. Per tant tindrem el numero de files que tingui el camp d'agrupació

**HAVING**

No es pot utilitzar la clausula WHERE per restringir grups i per tant utilitzem HAVING (Filtrar funcions de grup)

```sql
-- Si al Having hi ha un JOIN previ i volem contar, 
-- sempre contarem els de la taula que mani:


SELECT d.deptno, dname
FROM emp e LEFT JOIN dept d ON e.deptno = d.deptno
GROUP BY d.deptno, dname
HAVING COUNT(*) > 3;
```

#### ****ORDRE DE COMPIALCIÓ:****

- S'agrupen les files segons el camp d'agrupació

- S'aplica la funció de grup

- Es filtren les files resultant amb HAVING
  
  ****
  
  **8** [SELECT               column, group_function]
  
  **1** [FROM                 table]
  
  **2** [WHERE               condition]
  
  **3** [GROUP BY         group_by_expression]
  
  **5** [HAVING             group_condition]
  
  **6** [ORDER BY         column];
  
  **7** [LIMIT]
  
  **4** Executar la funcio de grup que pot estar al SELECT o al HAVING

    

#### ****Crear BDD:****

Posar sempre el nom de la base de dades que creem en minuscula perq sino postgres ho passa a minuscula i quan fem el \c ens diu que no s'ha creat la base de dades:

```sql
-- En aquest exemple no funcionaria:
\c template1
DROP DATABASE IF EXISTS Clubnautic;
CREATE DATABASE clubnautic;
\c clubnautic
```

```sql
-- En aquest exemple funciona:
\c template1
DROP DATABASE IF EXISTS clubnautic;
CREATE DATABASE clubnautic;
\c clubnautic
```

#### ****Anidament de funcions de grup:****

SQL no permet anidar funcions de la següent manera:

```sql
-- no funciona a postgres
SELECT MAX(AVG(sal))
FROM emp
GROUP BY deptno;
```

```sql
-- d'aquesta manera 
SELECT *
FROM (SELECT MAX(mitjana) maxim
      FROM (SELECT AVG(SAL) mitjana
      FROM emp
GROUP BY deptno) t
    ) t2;
```

#### ****Subconsultas:****

- Les subconsultes són una consulta però entre parentesis, si porta un nom, serà una taula temporal.

- Serveix per obtenir dades que no disposem.

- Les subconsultes no es veuen, per això no té sentit posar DISTINCT i ORDER BY prohibit.

```sql
-- empleats que cobren més que jones:
SELECT ename
FROM emp
WHERE sal > (SELECT sal
             FROM emp
             WHERE LOWER(ename) = 'jones');
```

#### ****Operador IN:****

```sql
-- Mostrar els caps:

SELECT ename
FROM emp
WHERE empno IN (SELECT mgr
                 FROM emp);

-- també podem ferho així
SELECT ename
FROM emp
WHERE empno =ANY (SELECT mgr
                 FROM emp);
```

- operador **IN** equival a **=ANY**

#### ****Operador NOT IN fer restes:****

```sql
-- mostrar el nom dels empleats que  NO son cap.
SELECT ename
FROM emp
WHERE empno NOT IN (SELECT mgr
                 FROM emp
                 WHERE mgr IS NOT NULL);


-- també podem ferho així
SELECT ename
FROM emp
WHERE empno !=ALL (SELECT mgr
                 FROM emp
                 WHERE mgr IS NOT NULL);
```

- operador **NOT IN** equival a **!=ALL***
  
  ```sql
  -- SEMPRE QUE UTILITZEM NOT IN NECESSITAREM POSSAR UN WHERE ... IS NOT NULL
  SELECT ename
  FROM emp
  WHERE empno !=ALL (SELECT mgr
                   FROM emp
                   WHERE mgr IS NOT NULL);
  ```

#### ****Tipus de subconsultas:****

        Subconsulta monoregistre ==> un camp ===> operadors relacionals:
        <
        >
        =
        !=
        >=
        <=
        <> diferent

```sql
-- mostrar el nom de l'empleat i salari i departament d'aquells 
-- que tenen su salari inferor per departament
```

```sql
-- quin és el nom del representant que té 
-- el millor rendiment de ventas de tots els representants 
SELECT nombre
FROM repventa 
WHERE (ventas/cuota)*100 = (SELECT MAX((ventas/cuota)*100)
           FROM repventa);
```

```sql
-- Mostrar el pedido más antiguo

SELECT *
FROM pedido
WHERE fecha = (SELECT MIN(fecha)
               FROM pedido );


-- Mostrar el representante (Nombre) con más con más pedidos realizados.
SELECT nombre
FROM repventa r JOIN pedido p
GROUP BY r.repcod, nombre
(SELECT MAX(num_comandes) 
FROM (SELECT COUNT(*) num_comandes
      FROM pedido 
      GROUP BY repcod) t);



-- Mostrar producto/s que nunca han sido solicitados. (clau aliena composta)

SELECT *
FROM producto
WHERE (fabcod,prodcod) NOT IN (SELECT fabcod, prodcod
     FROM pedido);

/* RESULTAT
fabcod | prodcod |      descrip      | precio | exist 
--------+---------+-------------------+--------+-------
 bic    | 41672   | Plate             | 180.00 |     0
 imm    | 887p    | Perno Riostra     | 250.00 |    24
 qsa    | xk48    | Reductor          | 134.00 |   203
 imm    | 887h    | Soporte Riostra   |  54.00 |   223
 bic    | 41089   | Retn              | 225.00 |    78
 aci    | 41001   | Articulo Tipo 1   |  55.00 |   277
 qsa    | xk48a   | Reductor          | 117.00 |    37
 imm    | 887x    | Retenedor Riostra | 475.00 |    32 
(8 rows)
*/


--Mostrar la oficina que más vende (suponed que no existen 
-- los campos ventas de oficina y de representante)

SELECT ciudad
FROM repventa r JOIN pedido p ON r.repcod = p.repcod
           JOIN oficina o ON r.ofinum = o.ofinum
GROUP BY o.ofinum, ciudad
HAVING SUM(importe) = (SELECT MAX(total)
                                           FROM (SELECT SUM(importe) Total
                                                        FROM pedido p JOIN repventa r ON p.repcod = r.repcod
                                                        GROUP BY ofinum) t);
```

#### ****ROUND:****

Només serveix només quan s'ha de veure un valor per pantalla sinó no l'arrodonirem mai.

#### ****Que estem contant:****

A =====> B

n                m       

- hi haurà el màxim de n files, ja que b depen de a per això si fem un count despres d'un join contem el que té la taula principal
  
    Per mirar en joins més complexos necessitem mirar quina taula guanya a cada join:
  
    PEDIDO =====> REPVENTA <======= OFICINA
  
    primer tindriem pedidos entre(pedido JOIN repventa) 
    i després guanya oficina entre(pedido JOIN repventa JOIN OFICINA)

# Transaccions, DCL, PLpsSQL:

- TCL(TRANSACCIONS & CONNEXIÓ REMOTA)

- DCL (data Control Language)

- Programació(PLpsSQL)

---

- vista = consulta emagatzemada![](/home/aaron/.var/app/com.github.marktext.marktext/config/marktext/images/2022-05-30-19-09-37-image.png)

¡¡*

 en BBDD no és commutativa*!!
si vull saber si dos conjunts són iguals(mateix nombre d'elements), si A-B = 0, i B-A = 0, serán iguals (les dues taules tenen la mateixa volumetría), s'ha de fer la resta en tots dos sentits. **En les restes no podem utilitzar joins a la primera part perquè els volem tots, al segon conjunt si podem perquè volem filtrar-ho ==> TOTS - Informatics = no informatics**#### ****ROUND:****OPERADORS ALGEBRAICS
 --> Union
 --> Intersect
 --> Except *les dues columnes han de tenir el mateix grau i tipus*

EXEMPLE: volem els que no tenen depts assignats.
{ TOTS - DEPTS ASSIGNATS = DEPTS NO ASSIGNATS }

SELECT deptno 
FROM dept

EXCEPT

SELECT deptno
FROM emp;

deptno

```
 40
```

(1 row)

OTRO EXEMPLE: restar 2 taules (emp i emp2)
es crea una taula apartir d'una consulta.

CREATE TABLE emp2 AS
SELECT *
FROM emp;

si faig \d :

scott=> \d
 List of relations
 Schema | Name | Type | Owner  
--------+------------+----------+------------
 public | dept | table | iaw8827567
 public | deptno_seq | sequence | iaw8827567
 public | emp | table | iaw8827567 *public | emp2 | table | iaw8827567* public | empleat | view | iaw8827567
 public | empno_seq | sequence | iaw8827567
 public | salgrade | table | iaw8827567
(7 rows)

#### **Union:**

- serveix per a sumar files

- tant amb UNION, EXPECT **Necessitem que els camps(nom de les columnes) siguin iguals**

```sql
-- exemple 1
SELECT nom, 'professor' AS tipus
FROM professor
UNION
SELECT nom, 'Alumne' tipus
FROM alumme
```

```sql
-- exemple 2
(SELECT *
FROM emp 
EXCEPT
SELECT *
FROM emp2)

UNION

(SELECT *
FROM emp2
EXCEPT
SELECT *
FROM emp);
```

> **VIEW(vista) =** es una finestra ala BBDD ala que restringeixo què es vol veure.
> És una consulta emmagatzemada.

    --Per a accedir a VIEWS
    scott=> \dv
    
     List of relations
     Schema | Name | Type | Owner
    --------+---------+------+------------
     public | empleat | view | iaw8827567
    
    scott=> \sv empleat --show view--

```sql
CREATE OR REPLACE VIEW public.empleat AS
SELECT emp.ename AS nomemp,
 emp.sal AS salari,
 emp.job AS ofici,
 dept.dname AS nomdept,
 dept.loc AS localitat
 FROM emp,
 dept
 WHERE emp.deptno = dept.deptno;

SELECT nomemp, salari
FROM empleat;
```

**CREAR UNA VISTA:(si vull fer canvis, l'hauré d'esborrar)**

```sql
-- crear vista amb els empleats del departament 30
CREATE OR REPLACE VIEW emp30 AS
SELECT empno, ename, sal, deptno
 FROM emp
 WHERE deptno = 30
 WITH CHECK OPTION; --valida la condició (where...)
```

> Quan volem inserir camps nous tenim un problema ja que hi ha ambiguitat amb les files de la taula empleat i per tant haurem de borrar la vista i afegir alias als camps que presentin l'ambiguitat.

```sql
DROP VIEW emp30;
--scott=> DROP VIEW
CREATE OR REPLACE VIEW emp30 AS
 SELECT empno, ename nom, sal salari, deptno dept
 FROM emp
 WHERE deptno = 30
WITH CHECK OPTION;
CREATE VIEW


UPDATE emp30
SET salari = 1500
WHERE LOWER(nom) = 'james';
--scott=> UPDATE 1
```

#### ****Tipus de vistes:****

##### comandes utils:

\dv ==> display views

\sv ==> codi de la vista

[tutorials SQL](https://www.postgresqltutorial.com/)

- vistes simples ==> PODEM FER DML i només inclueix una taula

- vistes complexes ==> 
  
  - NO PODEM FER DML (s'haura de programar) i més d'una taula
  
  - serà una vista complexe si té **n taules** o té **camps calculats (si el cap no és exactament el de la taula: concatenació, op.aritmetiques i funcions de grup)**

```sql
-- crear la vista comanda (idcomanda, idproducte, descripció, preuUnitari
-- quantitat, client, venedor, total)
```

#### ****CASE:****

[PostgreSQL CASE](https://www.postgresqltutorial.com/postgresql-tutorial/postgresql-case/)

```sql
CASE 
      WHEN condition_1  THEN result_1
      WHEN condition_2  THEN result_2
      [WHEN ...]
      [ELSE else_result] -- només el posem si no entra a cap codició
END
```

```sql
-- exemple 1:

SELECT title,
       length,
       CASE
           WHEN length> 0
                AND length <= 50 THEN 'Short'
           WHEN length > 50
                AND length <= 120 THEN 'Medium'
           WHEN length> 120 THEN 'Long'
       END duration
FROM film
ORDER BY title;
```

```sql
-- exemple 2
-- mostrar per a cada emp el seu nom, nom_dept, rang salarial
--
--   Rang SALARIAL:  0 - 1000 baix
--                   1000.01 - 2000 mitjà
--                   2000.01 - 3000 alt

SELECT ename , d.dname, 
CASE
    WHEN emp.sal BETWEEN 0 AND 1000 THEN 'baix'
    WHEN emp.sal BETWEEN 1000.01 AND 2000 THEN 'mitjà'
    WHEN emp.sal BETWEEN 2000.01 AND 3000 THEN 'alt'
        WHEN emp.sal > 300 THEN 'executiu'
END rang_salarial
FROM emp emp LEFT JOIN dept d ON emp.deptno = d.deptno;
```

#### **TCL (DML):**

- **transacció:** conjunt d'operacions DML(INSERT, UPDATE, DELETE) que s'executa de manera atomica (o tot o res, si algo falla tot falla).
  
  - **BEGIN [TRANSACTION]**: Començar una transacció.
  
  - **ROLLBACK:** desfer els canvis.
    
    - **ROLLBACK TO SAVEPOINT:** Desfer només una part, el sistema gestor                                                     també ho fa (abrazo de la muerte).
      
      - **SAVEPOINT:** Fita per a fer **ROLEBACK** només fins al punt que volguem.
  
  - **COMMIT:** fer la operació definitiva.
  1. **Si una transsació no s'acaba els altres usuaris no la veuran.**
  
     2. **La transsació s'acaba si és rollback; o commit; , si fos rollback to save_point  no conta**

#### **TCL (PostgresSQL vs ORACLE):**

| PostgresSQL       | ORACLE                          |
| ----------------- | ------------------------------- |
| Autocommit = true | Autocommit = false (exepte DDL) |

a Postgres tot el que fas al DML queda reflectit de forma definitiva en canvi a Oracle és al revés, de normal tot el que fas no és fa de manera definitiva per això serà necessari fer un commit.

```sql
-- exemple1 TCL
BEGIN;

INSERT INTO emp VALUES(nextval('empno_seq'), 'AARON', 
                      'FURBOL', null,'1980-10-17', 2000, null, 30);

SAVEPOINT A;

UPDATE emp
SET sal = sal*1.25
WHERE deptno = 20;

SAVEPOINT B;

DELETE FROM emp;

SAVEPOINT C;


-- hem de posar ROLLBACK o COMMIT sempre
ROLLBACK TO A;
```

#### **Conexió remota entre ordinadors:**

1.

```bash
# amb aquesta configuració el nostres pc escolta peticions externes
cd /etc/postgresql/13/main/
vim postgresql.conf

# dintre de vim podem buscar amb ==> /cosa_que_volem_buscar + INTRO
/localhost + INTRO
```

2.

```bash
# triar qui es pot conectar i qui no
cd /etc/postresql/13/main/
vim pg_hba.conf 
# type (de manera local o remota) local, host
# database (una base en concret o all si va bé)
# address (una ip o una mascara de xarxa) 
# method (per defecte és peer ==> SO == BDD)
#        (trust == incondicional)
#        (passwd == contraseya sense encriptar)
#        (md5 == contrasenya encriptada) (necessitem que tingui passwd)
psql -d template1
```

3. copia de seguretat sempre:

```bash
cp pg_hba.conf pg_hba.conf.bak 
```

4. creem l'usuari
   
   ```sql
   CREATE USER aaronsql [WITH] PASSWORD  'jupiter' CREATEDB;
   ```

Sempre que canviem algo en els fitxers de configuració necessitem fer un systemctl restart postgresql

5. conectar-se al company
   
   1. hem de tenir els dos l'usuari de l'altre afegit a pg_hba.conf

```bash
psql -h ip_que_vull_conectarme -p port(normalment 5432) -U meu_usuari nom_basedd
```

#### DLC (permisos/privilegis):

- DCL
  
  - REVOKE OPS DML,SELECT ON taula FROM usuari

- ```sql
  GRANT OPS DML, SELECT  ON objectes TO usuari;
  ```

```sql
-- permisos a totes les taules de la base de dades on estic
  GRANT OPS DML, SELECT  ON ALL TABLES IN SCHEMA PUBLIC TO usuari, usuari2;
-- \dp o \z (display permisio)


-- veure qui està conectat al servidor
SELECT usename FROM pg_stat_activity;
```

```sql
-- si creem una base desde el terminal 
-- hem de crearla i despres fer \c abans de poder crear les files
```

**blocat:** perquè aixó passi necessitem que s'sestigui FENT UN DELETE O UN UPDATE

**desblocat:** Nomès passarà si previament s'ha blocat

**deadlock:** perquè aixó es doni necessitem que hi hagi multiples bloquejos.

**rollback del s.gestor:** es farà quan creei un deadlock una conexió, el que farà és fer un ROLLBACK fins a l'ultim SAVEPOINT o sinó fins a on s'ha iniciat la seva transsacció.

#### Functions PLpgSQL:

```sql
-- Sintaxis
CREATE OR REPLACE FUNCTION functionName([parametres])
RETURNS  tipus_de_dades
RETURNS $$
[DECLARE
   variables_locals ;]
BEGIN


END;
$$ LANGUAGE PLPGSQL;
```

```sql
-- definir la funció
CREATE OR REPLACE FUNCTION helloWorld()
RETURNS  VARCHAR
RETURNS $$
BEGIN
        RETURN 'Hola Mundo';
END;
$$ LANGUAGE PLPGSQL;
```

```sql
-- executar la funció
SELECT helloWorld() ;
```

```sql
-- volem distinguir entre el CAMP, VARIABLE LOCAL, PARAMETRE
-- sense_res :  camp   
-- v_ :  serveix per dir que es una variable local
-- p_ :  parametres

SELECT ename
INTO v_ename
FROM emp
WHERE empno = p_empno;
```

```sql
-- crea una funció  infoUsuari donat un codi d'empleat
-- com a parametre em mostrara el nom de l'empleat i el seu treball
CREATE OR REPLACE FUNCTION  infoUsuari(p_empno INT)
RETURNS VARCHAR                        -- hem de fer casting
AS $$
    DECLARE
        v_ename VARCHAR;
        v_job VARCHAR;
BEGIN
    SELECT ename, job
    INTO STRICT v_ename, v_job
         -- OBLIGATORI posar el STRICT tot i que es pot no possar
    FROM emp
    WHERE empno = p_empno;
RETURN 'El nom és ' || v_ename || ' i treballa de ' || v_job;
END;
$$ LANGUAGE PLPGSQL;
```

- **Quan canviem coses a la capcelera del metode (create, returns, as) haurem de borrar la funció [   DROP FUNCTION nom_funcio(); ]**

- **No pot haver més d'un return a la part d'execució**

#### Excepcions:

- NO_DATA_FOUND (siempre hay que )

- TOO_MANY_ROWS (error del programador ==> per desenvolupament)

Fem servir **STRICT** per tractar excepcions

```sql
-- crea una funció  infoUsuari donat un codi d'empleat
-- com a parametre em mostrara el nom de l'empleat i el seu treball
CREATE OR REPLACE FUNCTION  infoUsuari(p_empno INT)
RETURNS VARCHAR                        -- hem de fer casting
AS $$
    DECLARE
        v_ename VARCHAR; -- podem possar v_cant pedido.cant%type
        v_job VARCHAR;
BEGIN
    SELECT ename, job
    INTO STRICT v_ename, v_job
         -- OBLIGATORI posar el STRICT tot i que es pot no possar
    FROM emp
    WHERE empno = p_empno;
RETURN 'El nom és ' || v_ename || ' i treballa de ' || v_job;
-- quan un no hi ha un usuari s'obre l'excepció, 
-- això es deu a que quan fem INTO  el s.gestor es pregunta si existeixen
-- dades sino existeixen tria el cami de EXCEPTION
EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN 'El numero ' || p_empno || ' no existeix';
       WHEN TOO_MANY_ROWS THEN
        RETURN 'El programador no es experto';
END;
$$ LANGUAGE PLPGSQL;
```

#### Estructura condicional:

```sql
IF  CONDITION/S THEN
    [ELSE
    [IF    ]
END IF;
```

```sql
-- si es SALEMAN => vendedor CLERK => oficinistqa otros other
CREATE OR REPLACE FUNCTION  infoUsuari(p_empno INT)
RETURNS VARCHAR
AS $$
    DECLARE
        v_ename VARCHAR;
        v_job VARCHAR;
BEGIN
    SELECT ename, job
    INTO STRICT v_ename, v_job
    FROM emp
    WHERE empno = p_empno;
    IF LOWER(v_job) = 'salesman' THEN
        v_job := 'VENDEDOR';
    ELSIF LOWER(v_job) = 'clerk' THEN
           v_job := 'OFICINISTA';
    ELSE
           v_job := 'OTHER';
    END IF;


RETURN 'El nom és ' || v_ename || ' i treballa de ' || v_job;

EXCEPTION
       WHEN NO_DATA_FOUND THEN
       RETURN 'El numero ' || p_empno || ' no existeix';
       WHEN TOO_MANY_ROWS THEN
        RETURN 'El programador no es experto';
END;
$$ LANGUAGE PLPGSQL;
```

#### TYPE CAST ERROR:

Afegir als parametres de la funció un cast amb els parametres de la funció

#### SOBRE CARREGA:

- numero d'arguments diferents o tipus de dada diferent 

- Podem tenir dos funcions amb mateix nom però diferent nombre d'argument  o tipus de dada diferent

- **Si declaramos como INT la consola ya hace el casting**

- **SI TINC AQUEST PROBLEMA BORRAR TOTES LES FUNCIONS**
  
  - ```sql
    -- si hi ha més d'una funció
    DROP FUNCTION functionName(tipus_de_dada,...);
    
    -- si hi ha un nomes una
    DROP FUNCTION functionName;
    ```
