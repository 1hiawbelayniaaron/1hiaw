/* Muestra de los representantes su nombre, la ciudad de su oficina así como su región.*/

/* si usamos inner no saldrá solo los representantes que tienen oficina asignada en la tabla repventa por lo tanto necesitamos OUTER*/

SELECT r.nombre, o.ciudad, o.region
FROM repventa r LEFT JOIN oficina o ON r.ofinum = o.ofinum;


/*Obtener una lista de todos los pedidos, mostrando el número de
pedido, su importe, el nombre del cliente que lo realizó y el límite
de crédito de dicho cliente.*/

SELECT p.pednum, p.importe, c.nombre, c.limcred
FROM pedido p LEFT JOIN cliente c ON p.cliecod = c.cliecod;



/*Obtener una lista de representantes ordenada alfabéticamente,
en la que se muestre el nombre del representante, codigo de la
oficina donde trabaja, ciudad y la región a la que vende.*/

-- tablas : representantes , oficina


SELECT r.nombre , r.ofinum , o.ciudad, o.region
FROM repventa r LEFT JOIN oficina o ON r.ofinum = o.ofinum;


/*Obtener una lista de las oficinas (ciudades, no códigos) que tienen
un objetivo superior a 360000 euros. Para cada oficina mostrar la
ciudad, su objetivo, el nombre de su director y puesto del mismo.*/

-- tablas : oficina, repventa
SELECT o.ciudad , o.objetivo , jefe.nombre , jefe.puesto
FROM oficina o LEFT JOIN repventa jefe ON o.director = jefe.jefe
WHERE o.objetivo > 360000;



/*Obtener una lista de todos los pedidos mostrando su número, el
importe y la descripción de los productos solicitados.*/

-- pedido , producto
SELECT  p.pednum , p.importe , pr.descrip
FROM pedido p LEFT JOIN producto pr ON p.fabcod || p.prodcod = pr.fabcod || pr.prodcod;


/*Obtener una lista de los pedidos con importes superiores
a 4000. Mostrar el nombre del cliente que solicitó el pedido,
numero del pedido, importe del mismo, la descripción del producto
solicitado y el nombre del representante que lo tomó. Ordenad la
lista por cliente alfabéticamente y luego por importe de mayor a menor.*/

-- tablas : pedido , cliente , producto , repventa
-- 		cliecod	   	      repcod
SELECT cli.nombre , ped.pednum, ped.importe , pr.descrip, r.nombre
FROM pedido ped LEFT JOIN cliente cli 
	ON ped.cliecod = cli.cliecod
LEFT JOIN producto pr 
	ON ped.fabcod || ped.prodcod = pr.fabcod || pr.prodcod
LEFT JOIN repventa r 
	ON ped.repcod = r.repcod
WHERE ped.importe > 4000
ORDER BY cli.nombre, ped.nombre DESC;


/*Obtener una lista de los pedidos con importes superiores
a 2000 euros, mostrando el número de pedido, importe, nombre del
cliente que lo solicitó y el nombre del representante que contactó
con el cliente por primera vez.*/

-- pedido , cliente , repventa
SELECT p.pednum , p.importe , cl.nombre , r.nombre
FROM pedido p JOIN cliente cl 
	ON p.cliecod = cl.cliecod
LEFT JOIN repventa r 
	ON p.repcod = r.repcod
WHERE p.importe > 2000;



/*Obtener una lista de los pedidos con importes superiores a 150
euros, mostrando el código del pedido, el importe, el nombre del
cliente que lo solicitó, el nombre del representante que contactó
con él por primera vez y la ciudad de la oficina donde el
representante trabaja.*/

-- pedido , cliente (cliecod)
--	  , repventa (repcod)
--	  , oficina  (ofinum)
SELECT  ped.pednum , ped.importe , cli.nombre , rep.nombre , ofi.ciudad
FROM pedido ped JOIN cliente cli ON ped.cliecod = cli.cliecod
	LEFT JOIN repventa rep ON ped.repcod = rep.repcod
	LEFT JOIN oficina ofi ON rep.ofinum = ofi.ofinum
WHERE ped.importe > 150;


/*Lista los pedidos tomados durante el mes de octubre del año 2003 ,
mostrando solamente el número del pedido, su importe, el nombre del
cliente que lo realizó, la fecha y la descripción del producto
solicitado*/

--pedidos  ,  cliente (cliecod)
--	   ,  producto (fabcod || prodcod)

/*Obtener una lista de todos los pedidos tomados por representantes de
oficinas de la región Este, mostrando solamente el número del
pedido, la descripción del producto y el nombre del representante
que lo tomó*/
SELECT pednum, importe, cli.nombre, fecha, pr.descrip
FROM pedido ped JOIN cliente cli ON ped.cliecod = cli.cliecod
JOIN producto pr ON ped.fabcod || ped.prodcod = pr.fabcod || pr.prodcod
WHERE TO_CHAR(fecha+5000,'YYYY,MM') = '2003,10';


/*Obtener los pedidos tomados en los mismos días en que un nuevo
representante fue contratado. Mostrar número de pedido, importe,
fecha pedido.*/


/*Obtener una lista con parejas de representantes y oficinas en donde
la cuota del representante es mayor o igual que el objetivo de la
oficina, sea o no la oficina en la que trabaja. Mostrar Nombre del
representante, cuota del mismo, Ciudad de la oficina, objetivo de la
misma.*/
SELECT nombre , cuota , ciudad, objetivo
FROM repventa, oficina
WHERE cuota >= objetivo;


/*Muestra el nombre, las ventas y la ciudad de la oficina de cada
representante de la empresa.*/


/*Obtener una lista de la descripción de los productos para los que
existe algún pedido en el que se solicita una cantidad mayor a las
existencias de dicho producto.*/


/*Lista los nombres de los representantes que tienen una cuota
superior a la de su director.*/


/*Obtener una lista de los representantes que trabajan en una oficina
distinta de la oficina en la que trabaja su director, mostrando
también el nombre del director y el código de la oficina donde
trabaja cada uno de ellos.*/


/*El mismo ejercicio anterior, pero en lugar de ofinum, la ciudad.*/


/*Mostrar el nombre y el puesto de los que son jefe.*/
