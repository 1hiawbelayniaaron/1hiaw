--Aaron Belayni
--a211762hb


\c template1
drop database if exists clubnautic;
create database clubnautic;
\c clubnautic



-- sequences

create sequence numSoci_seq
 START WITH 1
 increment by 1;


-- tables

CREATE TABLE Usuari(
    DNI varchar(9) constraint DNIpatro_pk primary key,
    nom varchar(100),
    telefon varchar(9),
    email varchar(50),
    tipus varchar(1),
    dataAlta date DEFAULT current_date,
    numSoci varchar(9),
    ciutat varchar(70),
CONSTRAINT numSoci_uk UNIQUE (numSoci),
CONSTRAINT tipus_ck CHECK(tipus IN('A','B','C')) 
          );
          
          
         
CREATE TABLE Vaixell(
    matricula varchar(8) constraint matricula_pk primary key,
    soci varchar(9), -- agafant el numero de soci
    nomVaixell varchar(50),
    numAmarre int,
    quota decimal(6,2),
CONSTRAINT vaixell_soci_fk FOREIGN KEY (soci) REFERENCES Usuari(numSoci) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT nomVaixell_UC UNIQUE (nomVaixell),
CONSTRAINT numAmarre_UC UNIQUE (numAmarre)
      );
      
      
CREATE TABLE Sortida(
        dataSortida timestamp DEFAULT CURRENT_TIMESTAMP,
        vaixell varchar(8),
        patro varchar(9), -- agafant el dni
        dataRetorn timestamp,
        destinacio varchar(100),
CONSTRAINT dataSortidaVaixellcod_pk PRIMARY KEY (dataSortida,vaixell),
CONSTRAINT dataSortida_dataRetorn_ck CHECK(dataSortida<dataRetorn),
CONSTRAINT vaixell_Sortida_fk FOREIGN KEY(vaixell) REFERENCES Vaixell(matricula) ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT sortida_patro_fk FOREIGN KEY(patro) REFERENCES Usuari(DNI) ON DELETE SET NULL ON UPDATE CASCADE
      );
      
      
-- inserts

INSERT INTO Usuari
VALUES ('12345678a','Jaume','628105687','a@gmail.com','A',NULL,NULL,'VIC'),
	('87654321b','Aaron Belayni','628105687','b@gmail.com','B',to_date('09-12-2020','dd/mm/yyyy'),nextval('numSoci_seq'),'BARCELONA'),
	('12312312k','Raquel bofill','123456789','c@gmail.com','C',to_date('05-12-2020','dd/mm/yyyy'),nextval('numSoci_seq'),'PARIS' );


insert into Vaixell 
values ('6907bcd',1,'perla negra IV',2,425.00),
	('1234acb',1,'Vergantin VI',37,250.00),
	('6784hku',2,'Vergantin XI',201,350.00);


insert into Sortida
values (to_timestamp('05:00','HH:MM'),'6907bcd','12345678a',to_timestamp('10:00','HH:MM'),'Islas Azores');


-- delete
DELETE FROM usuari 
WHERE LOWER(ciutat) = 'barcelona';



-- update
UPDATE vaixell 
SET quota = quota * 1.2
WHERE quota < 400.00;



