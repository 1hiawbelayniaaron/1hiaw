-- 1. Mostrar la suma de las cuotas y la suma de las ventas totales de
-- todos los representantes.
SELECT SUM(cuota), SUM(ventas)
FROM repventa ;
-- 2. ¿Cuál es el importe total de los pedidos tomados por Bill Adams?
SELECT SUM(importe)
FROM pedido 
WHERE repcod = (SELECT repcod
                FROM repventa 
                WHERE lower(nombre) = 'bill adams');


-- 3. Calcula el precio medio de los productos del fabricante ACI.
 SELECT ROUND(AVG(precio),2) 
 FROM producto 
 WHERE LOWER(fabcod) = 'aci';

-- 4. ¿Cuál es el importe medio de los pedido solicitados por el cliente
-- 2103
SELECT ROUND(AVG(importe),2) 
FROM pedido 
WHERE cliecod = 2103;

-- 5. Mostrar la cuota máxima y la cuota mínima de las cuotas de los
-- representantes.
SELECT MIN(COALESCE(cuota,0)), MAX(COALESCE(cuota,0)) 
FROM repventa;
-- 6. ¿Cuál es la fecha del pedido más antiguo que se tiene registrado?
SELECT MIN(fecha) 
FROM pedido ;

-- 7. ¿Cuál es el mejor rendimiento de ventas de todos los representantes?
-- (considerarlo como el porcentaje de ventas sobre la cuota).
SELECT ROUND(MAX(COALESCE(ventas/cuota,0)),2) 
FROM repventa ;

-- 8. ¿Cuántos clientes tiene la empresa?
SELECT COUNT(*) FROM cliente ;


-- 9. ¿Cuántos representantes han obtenido un importe de ventas superior a
-- su propia cuota?

-- 10. ¿Cuántos pedidos se han tomado de más de 150 euros?

-- 11. Halla el número total de pedidos, el importe medio, el importe total
-- de los mismos.

-- 12. ¿Cuántos puestos de trabajo diferentes hay en la empresa?

-- 13. ¿Cuántas oficinas de ventas tienen representantes que superan sus
-- propias cuotas?

-- 14. ¿Cuál es el importe medio de los pedidos tomados por cada
-- representante?

-- 15. ¿Cuál es el rango de las cuotas de los representantes asignados a
-- cada oficina (mínimo y máximo)?

-- 16. ¿Cuántos representantes hay asignados a cada oficina? Mostrar Ciudad
-- y número de representantes.

-- 17. ¿Cuántos clientes ha contactado por primera vez cada representante?
-- Mostrar el código de representante, nombre y número de clientes.

-- 18. Calcula el total del importe de los pedidos solicitados por cada
-- cliente a cada representante.

-- 19. Lista el importe total de los pedidos tomados por cada
-- representante.

-- 20. Para cada oficina con dos o más representantes, calcular el total de
-- las cuotas y el total de las ventas de todos sus representantes.

-- 21. Muestra el número de pedidos que superan el 75% de las existencias.
-- 
-- D. Subconsultas
-- 



-- Mostrar el nombre y el puesto de los que son jefe (ya está hecho
-- con self join, ahora con subconsultas)
-- 
SELECT nombre , puesto
FROM repventa
WHERE repcod IN (SELECT jefe FROM repventa) ;

-- 1. Obtener una lista de los representantes cuyas cuotas son iguales ó
-- superiores al objetivo de la oficina de Atlanta.
SELECT *
FROM repventa 
WHERE cuota >= (SELECT objetivo
                FROM oficina 
                WHERE lower(ciudad) = 'atlanta');
-- 2. Obtener una lista de todos los clientes (nombre) que fueron
-- contactados por primera vez por Bill Adams.
SELECT * FROM cliente
WHERE repcod = (SELECT repcod 
                FROM repventa 
                WHERE LOWER(nombre) = 'bill adams');
-- 3. Obtener una lista de todos los productos del fabricante ACI cuyas
-- existencias superan a las existencias del producto 41004 del mismo
-- fabricante.
SELECT * 
FROM producto 
WHERE LOWER(fabcod) = 'aci' AND exist > (SELECT exist 
                                        FROM producto 
                                        WHERE LOWER(prodcod) = '41004' 
                                            AND LOWER(fabcod) = 'aci');

-- 4. Obtener una lista de los representantes que trabajan en las oficinas
-- que han logrado superar su objetivo de ventas.
SELECT *
FROM repventa
WHERE ofinum IN (SELECT ofinum
                FROM oficina 
                WHERE ventas > objetivo);

-- 5. Obtener una lista de los representantes que no trabajan en las
-- oficinas dirigidas por Larry Fitch.
SELECT * 
FROM oficina 
WHERE director != (SELECT repcod
                FROM repventa
                WHERE LOWER(nombre) = 'larry fitch');
-- 6. Obtener una lista de todos los clientes que han solicitado pedidos
-- del fabricante ACI entre enero y junio de 2003.
SELECT * FROM cliente
WHERE cliecod IN (SELECT cliecod
FROM pedido
WHERE EXTRACT(YEAR FROM fecha+5000) = 2003 AND 
      (EXTRACT(MONTH FROM fecha) BETWEEN 1 AND 6)
      AND LOWER(fabcod) = 'aci');de los productos de los que se ha tomado un pedido
-- de 150 euros ó mas.
SELECT * 
-- 7. Obtener una lista 
FROM producto 
WHERE fabcod||prodcod IN (SELECT fabcod||prodcod 
                        FROM pedido 
                        WHERE importe > 150
);

-- 8. Obtener una lista de los clientes contactados por Sue Smith que no
-- han solicitado pedidos con importes superiores a 18 euros.



SELECT * FROM cliente                   
WHERE repcod IN (SELECT * FROM repventa 
                  WHERE LOWER(nombre) = 'sue smith' 
                  AND cliecod 
EXCEPT 
SELECT cliecod FROM pedido WHERE importe < 18
);

SELECT * FROM cliente
WHERE repcod IN (SELECT * FROM repventa WHERE LOWER(nombre) = 'sue smith' AND cliecod NOT IN (SELECT cliecod FROM pedido WHERE importe > 18));





SELECT * FROM cliente
WHERE repcod IN (SELECT repcod 
                  FROM repventa 
                  WHERE LOWER(nombre) = 'sue smith' 
                  AND cliecod NOT IN (SELECT cliecod 
                                    FROM pedido 
                                    WHERE importe > 18));

-- 9. Obtener una lista de las oficinas en donde haya algún representante
-- cuya cuota sea más del 55% del objetivo de la oficina. Para comprobar vuestro
-- ejercicio, haced una Consulta previa cuyo resultado valide el ejercicio.
SELECT * FROM oficina
WHERE ofinum IN (SELECT ofinum 
                  FROM repventa 
                  WHERE cuota > 0.55 * objetivo);

-- misma consulta pero con JOINS
SELECT o.*
FROM oficina o  LEFT JOIN repventa r ON o.ofinum = r.ofinum
WHERE cuota > objetivo*0.55;


-- 10. Obtener una lista de los representantes que han tomado algún pedido
-- cuyo importe sea más del 10% de de su cuota.


-- amb JOINS
SELECT r.* FROM repventa r LEFT JOIN pedido p ON r.repcod = p.repcod 
WHERE importe > 0.1*cuota;


-- 11. Obtener una lista de las oficinas en las cuales el total de ventas
-- de sus representantes han alcanzado un importe de ventas que supera el
-- 50% del objetivo de la oficina. Mostrar también el objetivo de cada
-- oficina (suponed que el campo ventas de oficina no existe).
-- 
-- 



-- other examples:

SELECT * FROM emp
WHERE deptno IN (SELECT deptno FROM emp WHERE LOWER(ename) = 'clark'
);


-- emp que treballa a dallas
SELECT * FROM emp
WHERE deptno IN (SELECT deptno FROM dept WHERE LOWER(loc) = 'dallas');


--Llistar el nom, treball, departament, localitat i salari d’aquells empleats que tinguin un salari major de
--2000 i treballin a ‘DALLAS’ o ‘NEW YORK’.
