--Aaron Belayni
--a211762hb
--07/03/23

--1
SELECT s.codsoci "Id Soci", 
	cognoms || ',' || nom "Soci", 
	COUNT(l.*) "Prestects", 
	SUM(COALESCE(import,0))"Despesa"
FROM soci s LEFT JOIN lloguer l ON s.codsoci = l.codsoci
GROUP BY  s.codsoci ,  cognoms || ',' || nom
ORDER BY 4 DESC;

--2
SELECT nom , telefon
FROM soci
WHERE codsoci IN (SELECT codsoci 
		FROM llistaespera
		WHERE codpeli = (SELECT codpeli 
				FROM pelicula 
				WHERE LOWER(titol) = 'juana la loca') AND data_res = 
							(SELECT MIN(data_res) FROM llistaespera
							WHERE codpeli = 
							     (SELECT codpeli 
							     FROM pelicula 
							     WHERE LOWER(titol) = 'juana la loca')));

--4
SELECT l.codsoci FROM genere g JOIN pelicula p ON g.codgen = p.codgen 
                 JOIN dvd d ON p.codpeli = d.codpeli 
                 JOIN lloguer l ON l.coddvd = d.coddvd
GROUP BY l.codsoci , g.genere
HAVING COUNT(*) = 1;


--5
SELECT genere FROM genere
WHERE codgen NOT IN (SELECT g.codgen FROM genere g JOIN pelicula p ON g.codgen = p.codgen 
                 JOIN dvd d ON p.codpeli = d.codpeli 
                 JOIN lloguer l ON l.coddvd = d.coddvd);

--6
SELECT nom , cognoms FROM soci s JOIN  lloguer l ON s.codsoci = l.codsoci
GROUP BY nom, cognoms
HAVING COUNT(*) > 2
ORDER BY 2;

--7
SELECT titol FROM pelicula
WHERE coddir IN (SELECT coddir 
			FROM director
			WHERE LOWER(nom)||LOWER(cognoms) IN (SELECT LOWER(nom)||LOWER(cognoms) 
								FROM actor));



