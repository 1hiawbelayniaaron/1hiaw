#!/bin/bash
# Filename:		git_push.sh
# Author:		iaw49656032
# Date:			17/12/2021
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		git_push.sh [arg1...]
# Description:		



# obrir el menu
function menu {
	echo "=== Quina operació vols fer? ==="
	echo "=== 1. pujar arxius          ==="
	echo "=== 2. baixar arxius         ==="
read opcio
}




menu
# mirem que l'usuari posi 1 o 2
if [ $opcio = '1' ]
then	
	# fem git push
	echo '=== Quin commit vols posar? ==='
	read commit
	git add . 
	git commit -m "$commit" 
	git push origin main 
	echo "=== arxius pujats amb exit ==="
	
	
	
elif [ $opcio = '2' ]
then

	# fem git pull
	git pull origin main 
	echo "=== arxius baixats amb exit ==="
 	
else 
	# si no posa ni un ni dos
	echo 'ERROR: Només pots introduïr "1" ó "2"'
	menu
fi



stat 1hiaw/ | grep -E '^Modify'

