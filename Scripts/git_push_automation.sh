#!/bin/bash
# Filename:		git_push.sh
# Author:		iaw49656032
# Date:			17/12/2021
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		git_push.sh [arg1...]
# Description:		



# mirar si s'ha editat algo al document 
NEW_STAT=$(stat 1hiaw/ | grep -E '^Modify')
OLD_STAT_FILE="antic_arxiu.txt"


# mirem que el fitxer existeixi
if [ -e $OLD_STAT_FILE ]
then
	# guardem l'ultima modificació
        OLD_STAT=$(cat $OLD_STAT_FILE)
else
	echo "ERROR: no hi ha cap fitxer anomenat $OLD_STAT_FILE"
        exit 1
fi

NEW_STAT=$(stat 1hiaw/ | grep -E '^Modify')

if [ "$OLD_STAT" != "$NEW_STAT" ]
then
	# fem un git push
        git add . &>2 /dev/null
	git commit -m "$(echo $(stat 1hiaw/ | grep -E '^Modify') $RANDOM)" # <&>2 /dev/null
	git push origin main  # &>2 /dev/null
        # guardem la nova data de modificacio al fitxer
        echo $NEW_STAT > $OLD_STAT_FILE
        echo "ARXIUS PUJATS AMB EXIT"
else
 	echo "No hi ha canvis"        
fi
