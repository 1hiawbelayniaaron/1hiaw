#!/bin/bash
# Filename:		git_push.sh
# Author:		iaw49656032
# Date:			17/12/2021
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		git_push.sh [arg1...]
# Description:		L'script comprova si hi ha canvis a dintre del repositori del git en qüestió 		
#			de manera que si hi ha s'encarrega d'actualitzar aquests canvis al nuvol

# qualsevol cosa mirar : https://www.bonaval.com/kb/sistemas/debian/ejecutar-un-script-al-arrancar-linux-debian

# comprovem si hi ha canvis en el repositori
if [ $(git status --porcelain | wc -l) -eq "0" ]; then

  # no hi ha canvis
    echo -e "  🔴 s'ha de pujar els arxius"
else
  # hem de fer un git pull
  git pull origin main
  echo "  🟢 arxius baixats amb exit."
fi

