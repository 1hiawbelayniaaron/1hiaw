#!/bin/bash
# Filename:		epopteskiller.sh
# Author:		iaw49656032
# Date:			10/07/2021
# Version:		0.1
# License:		This is free software, licensed under the GNU General Public License v3.
#				See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:		git_push.sh [arg1...]
# Description:		this script  kill the fucking epoptes processes




is_active=$(systemctl is-active epoptes-client.service)


# see if epoptes is active
if [ $is_active = 'active' ]
then
	# epoptes is active
        systemctl stop epoptes-client.services
        echo "💀 epoptes-client.service has been killed"
        exit 0
else
	#epoptes is inactive or failed
	echo "epoptes-client.service was inactive"
        exit 1
fi

