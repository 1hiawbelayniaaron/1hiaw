#!/bin/bash

# Script Name: push.sh
# Author: a211762hb
# Date: 19/04/2023
# Version: 2.0
# License: This is free software, licensed under the GNU General Public License v3. See http://www.gnu.org/licenses/gpl.html for more information.
# Usage: 
# Description: script that helps you when pushing in git


# comprovem si hi ha canvis en el repositori
if [ $(git status --porcelain | wc -l) -eq "0" ]; then
  # no hi ha canvis
  
  echo "🟢| no changes"
  exit 1
else
  # hi ha canvis
    echo -e "🔴| some files have chages"
  # commit
    echo -e "📝| message of commit: "
    read commit
  # branch
    git branch
    echo -e "🌿| branch: "
    read branch
  # username
    echo -e "👤| username (empty for using the default username): "
    read username
  # email
    echo -e "📧| email (empty for using the default email): "
    read email
  # configurate the user
    # if username is not null
    if [ ! -z "$username" ]; then
    	git config --global user.name $username
    fi
    # if email is not null
    if [ ! -z "$email" ]; then
    	git config --global user.email $email
    fi
  # push
    git add .
    git commit -m "msg: $commit || date: ($(date +%F' // '%R))"
    git push origin $branch
    echo -e "✔️| files pushed."
    exit 0
fi

